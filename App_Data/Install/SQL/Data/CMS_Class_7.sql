set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Department Tax Class', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="DepartmentID" fieldcaption="DepartmentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ceb1cdfa-285e-418d-b2d4-235f81506645" /><field column="TaxClassID" fieldcaption="TaxClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1713a53c-6b68-4cca-8441-427e456ea5fe" /></form>', '20081229 09:07:00', 1720, N'ecommerce.departmenttaxclass', 0, 0, 0, 'de031fe1-8424-43da-9a04-0635feb47484', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_DepartmentTaxClass', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_DepartmentTaxClass">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DepartmentID" type="xs:int" />
              <xs:element name="TaxClassID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_DepartmentTaxClass" />
      <xs:field xpath="DepartmentID" />
      <xs:field xpath="TaxClassID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Category', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="CategoryID" fieldcaption="CategoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a7e6de80-6774-4fc9-8764-fde25832fce0" /><field column="CategoryDisplayName" fieldcaption="CategoryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="62f59aab-9b3e-4ddc-8f7a-fb97e039de40" columnsize="250" /><field column="CategoryName" fieldcaption="CategoryName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6ec936dc-3b0f-476c-8d07-9dbb7e5a19b9" columnsize="250" /><field column="CategoryDescription" fieldcaption="CategoryDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="21b4dc27-69d2-4ee5-ad21-31ce7362a169" /><field column="CategoryCount" fieldcaption="CategoryCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d333614c-e26b-45fa-803f-2815644d9b6e" /><field column="CategoryEnabled" fieldcaption="CategoryEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="60a19437-daca-476d-a9c7-f771810012d2" /><field column="CategoryUserID" fieldcaption="CategoryUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aebc6d41-6911-4955-8566-3e46053d2243" /><field column="CategoryGUID" fieldcaption="CategoryGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="09d6706d-6145-400c-9e12-c47f14fdfa44" /><field column="CategoryLastModified" fieldcaption="CategoryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="67e9377c-c25a-4dd7-9ef8-9b9d97a408ce" /></form>', '20090515 11:51:16', 1721, N'cms.category', 0, 0, 0, 'f9bd0914-ee13-41f8-85a3-4f2b50875c99', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Category', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Category">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CategoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CategoryDisplayName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryCount" type="xs:int" />
              <xs:element name="CategoryEnabled" type="xs:boolean" />
              <xs:element name="CategoryUserID" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="CategoryLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Category" />
      <xs:field xpath="CategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Document category', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="DocumentID" fieldcaption="DocumentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ace05777-73b8-4c94-862e-058c49920985" /><field column="CategoryID" fieldcaption="CategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="18ad18e0-4e0f-4cd9-b4cc-2caa3ccb283f" /></form>', '20081231 14:54:50', 1726, N'cms.documentcategory', 0, 0, 0, '88416cd6-9aa6-4ccc-9f9c-434380b8cdc6', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_DocumentCategory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_DocumentCategory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DocumentID" type="xs:int" />
              <xs:element name="CategoryID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_DocumentCategory" />
      <xs:field xpath="DocumentID" />
      <xs:field xpath="CategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Tag Group', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TagGroupID" fieldcaption="TagGroupID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="5e8edd4a-c7b0-48aa-8a15-0a9790b66d0e" /><field column="TagGroupDisplayName" fieldcaption="TagGroupDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7252c09e-85f7-47b9-911d-65b38b00b6bc" /><field column="TagGroupName" fieldcaption="TagGroupName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bff5999d-c415-4dd5-9c54-2c669d04cf59" /><field column="TagGroupDescription" fieldcaption="TagGroupDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="30c422da-9c71-4e22-925f-43242f7a7b26" /><field column="TagGroupSiteID" fieldcaption="TagGroupSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="881da915-c269-4baa-a044-f0ffec268312" /><field column="TagGroupIsAdHoc" fieldcaption="TagGroupIsAdHoc" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2c34c408-d66e-4001-a812-631c75ca01e1" /><field column="TagGroupLastModified" fieldcaption="TagGroupLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5042f698-31d8-4e86-b003-6e50839158a1" /><field column="TagGroupGUID" fieldcaption="TagGroupGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="23bea836-c7e5-46a6-af25-2add63da2e08" /></form>', '20081229 09:07:24', 1736, N'CMS.TagGroup', 0, 0, 0, '2bf6dd1b-4ab4-4146-b7de-fd9cd86db7c2', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_TagGroup', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_TagGroup">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TagGroupID" type="xs:int" />
              <xs:element name="TagGroupDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TagGroupName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TagGroupDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TagGroupSiteID" type="xs:int" />
              <xs:element name="TagGroupIsAdHoc" type="xs:boolean" />
              <xs:element name="TagGroupLastModified" type="xs:dateTime" />
              <xs:element name="TagGroupGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_TagGroup" />
      <xs:field xpath="TagGroupID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Document tag', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'', '20081231 14:55:00', 1737, N'cms.documenttag', 0, 0, 0, 'ec4ce0c0-f7e9-43af-935e-c67f38bbfad5', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_DocumentTag', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_DocumentTag">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DocumentID" type="xs:int" />
              <xs:element name="TagID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_DocumentTag" />
      <xs:field xpath="DocumentID" />
      <xs:field xpath="TagID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Tag', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TagID" fieldcaption="TagID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="10afa018-8f08-43b9-b61d-2991f7a6ed7e" /><field column="TagName" fieldcaption="TagName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="41a12c14-98dc-453b-8559-17ffb6482399" /><field column="TagCount" fieldcaption="TagCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="abd76b88-a87c-42b1-b27e-6fdf27daf597" /><field column="TagGroupID" fieldcaption="TagGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b1fccb81-7d06-4804-be58-db3784ee57a6" /></form>', '20090324 11:43:56', 1738, N'cms.tag', 0, 0, 0, '335d8316-ecf9-46d6-b8a1-5f5c162becd1', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_Tag', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Tag">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TagID" type="xs:int" />
              <xs:element name="TagName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TagCount" type="xs:int" />
              <xs:element name="TagGroupID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Tag" />
      <xs:field xpath="TagID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Membership - Banned IP', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="IPAddressID" fieldcaption="IPAddressID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f17b1adf-5910-410e-a5ce-d368224d39c5" /><field column="IPAddress" fieldcaption="IPAddress" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="51d8bbe7-d868-4461-bf0f-63d985be58b6" /><field column="IPAddressRegular" fieldcaption="IPAddressRegular" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="607e55d8-26b2-47bb-95d7-53ea227f8ce4" /><field column="IPAddressAllowed" fieldcaption="IPAddressAllowed" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="747a7ded-c824-4085-81e5-e69bb9055146" /><field column="IPAddressAllowOverride" fieldcaption="IPAddressAllowOverride" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a1e5675e-7faa-4fcb-bb2d-d0fa54d809f1" /><field column="IPAddressBanReason" fieldcaption="IPAddressBanReason" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="a4fd328a-c60c-4d8e-80f6-a75efe0c01e1" /><field column="IPAddressBanType" fieldcaption="IPAddressBanType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="059fbe9f-1eb4-41c9-b696-70fb34a5a593" /><field column="IPAddressBanEnabled" fieldcaption="IPAddressBanEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="38e3e820-f239-479d-aeea-b7bdc709743e" /><field column="IPAddressSiteID" fieldcaption="IPAddressSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f1d44929-cacf-4971-ae2b-c9ad81713a51" /><field column="IPAddressGUID" fieldcaption="IPAddressGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2f97a7b0-2fe3-40e0-9b1e-7eb435aed8ed" /><field column="IPAddressLastModified" fieldcaption="IPAddressLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3c0734cf-b556-4747-bd97-a9780b7fbc8b" /></form>', '20081229 09:07:11', 1740, N'cms.BannedIP', 0, 0, 0, '0baf8283-cdd6-4b36-9630-42009ede46af', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_BannedIP', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_BannedIP">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="IPAddressID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="IPAddress">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IPAddressRegular">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IPAddressAllowed" type="xs:boolean" />
              <xs:element name="IPAddressAllowOverride" type="xs:boolean" />
              <xs:element name="IPAddressBanReason" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IPAddressBanType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IPAddressBanEnabled" type="xs:boolean" minOccurs="0" />
              <xs:element name="IPAddressSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="IPAddressGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="IPAddressLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_BannedIP" />
      <xs:field xpath="IPAddressID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Alternative forms', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="FormID" fieldcaption="FormID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d65b8a4f-95fb-4a35-9f98-04c2a746f6de" /><field column="FormDisplayName" fieldcaption="FormDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0be5e0a7-1704-47dc-b063-5e6fb3e91c46" /><field column="FormName" fieldcaption="FormName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4c90ce6a-e202-4035-a51d-f27469743148" /><field column="FormClassID" fieldcaption="FormClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a4749e1-d4b8-4753-9b2e-298433d675c6" /><field column="FormDefinition" fieldcaption="FormDefinition" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a1307cae-635b-48b5-a291-e992f746fc9e" /><field column="FormLayout" fieldcaption="FormLayout" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5c028970-d2db-40c6-9284-f01edf3395cf" /><field column="FormGUID" fieldcaption="FormGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b1ff5048-0f5d-462f-818a-b6c582e45db1" /><field column="FormLastModified" fieldcaption="FormLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="12a04abb-af1c-493c-9350-c8c08f5736ff" /><field column="FormCoupledClassID" visible="true" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b9cb787e-87b6-4ebe-86ba-71470ae3e698" fieldcaption="Form Coupled Class ID" /></form>', '20081229 09:06:53', 1745, N'cms.AlternativeForm', 0, 0, 0, '7d7cbe11-e101-469f-a4a3-ee452f3982df', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_AlternativeForm', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_AlternativeForm">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="FormID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="FormDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormClassID" type="xs:int" />
              <xs:element name="FormDefinition" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormLayout" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="FormLastModified" type="xs:dateTime" />
              <xs:element name="FormCoupledClassID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_AlternativeForm" />
      <xs:field xpath="FormID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'TimeZone', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TimeZoneID" fieldcaption="TimeZoneID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="16021269-14ff-482a-be81-8870b15426d9" /><field column="TimeZoneName" fieldcaption="TimeZoneName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a8cde1d0-13dd-424d-a309-d9a0dddd6e19" /><field column="TimeZoneDisplayName" fieldcaption="TimeZoneDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a75bc013-14be-45f5-9ee9-3e5d506f3b8b" /><field column="TimeZoneGMT" fieldcaption="TimeZoneGMT" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a77cd69-6cde-47eb-be36-8e8b50a435a9" /><field column="TimeZoneDaylight" fieldcaption="TimeZoneDaylight" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9e95e18b-299e-4cdb-a6f7-0775003a7181" /><field column="TimeZoneRuleStartIn" fieldcaption="TimeZoneRuleStartIn" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8ea9e596-b370-4e8d-a96f-0ff73f839707" /><field column="TimeZoneRuleStartRule" fieldcaption="TimeZoneRuleStartRule" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e4b0ed67-2e61-44bf-9218-97b965cf7281" /><field column="TimeZoneRuleEndIn" fieldcaption="TimeZoneRuleEndIn" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1271bb6b-16b2-4f16-b2e2-89978d9a60e9" /><field column="TimeZoneRuleEndRule" fieldcaption="TimeZoneRuleEndRule" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9c233cde-ce0f-475e-bd45-4e1aa6976167" /></form>', '20081229 09:07:39', 1747, N'cms.timezone', 0, 0, 0, '01d6dfc3-0adc-444b-a86f-de19e72f76ff', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_TimeZone', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_TimeZone">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TimeZoneID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TimeZoneName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TimeZoneDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TimeZoneGMT" type="xs:double" />
              <xs:element name="TimeZoneDaylight" type="xs:boolean" minOccurs="0" />
              <xs:element name="TimeZoneRuleStartIn" type="xs:dateTime" />
              <xs:element name="TimeZoneRuleStartRule">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TimeZoneRuleEndIn" type="xs:dateTime" />
              <xs:element name="TimeZoneRuleEndRule">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TimeZoneGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="TimeZoneLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_TimeZone" />
      <xs:field xpath="TimeZoneID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Group', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="GroupID" fieldcaption="GroupID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d92a0eec-1867-4753-8afa-08f0af1fc023" /><field column="GroupGUID" fieldcaption="GroupGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8cf05f22-da47-4502-b676-a0411398bc5b" /><field column="GroupLastModified" fieldcaption="GroupLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c6035dfc-3b72-4a0f-b133-a92e3587cae8" /><field column="GroupSiteID" fieldcaption="GroupSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0ead60a9-72c7-4534-8acc-24037b605e4e" /><field column="GroupDisplayName" fieldcaption="GroupDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="7fd08d9d-dd31-491e-9c89-cf5b288e3337" /><field column="GroupName" fieldcaption="GroupName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="35fc6c9e-2c1c-4883-82ac-f095ed5b93b1" /><field column="GroupDescription" fieldcaption="GroupDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3838358e-5e5b-4a0a-adb9-ac0769302f3d" /><field column="GroupNodeGUID" fieldcaption="GroupNodeGUID" visible="true" columntype="guid" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7b29ef51-8705-4845-a92f-eedf6ddfc49c" /><field column="GroupApproveMembers" fieldcaption="GroupApproveMembers" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a02c3e8f-c88c-4ec1-af62-1642c3a4cbfb" /><field column="GroupAccess" fieldcaption="GroupAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a3a470bb-3bee-4b18-8916-529651e021ac" /><field column="GroupCreatedByUserID" fieldcaption="GroupCreatedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="17195728-441a-49da-8c51-24705c2c935a" /><field column="GroupApprovedByUserID" fieldcaption="GroupApprovedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f467aa85-3f49-491e-9b0b-99159b8f0457" /><field column="GroupAvatarID" fieldcaption="GroupAvatarID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1f719675-4d6b-4e04-94c8-90c9fbafebfc" /><field column="GroupApproved" fieldcaption="GroupApproved" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d73d91c7-edcb-4c8a-bd1d-90f821f5a345" /><field column="GroupCreatedWhen" fieldcaption="GroupCreatedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="216f55d8-156a-4c00-a3a0-a6d6738c907f" /><field column="GroupSendJoinLeaveNotification" fieldcaption="GroupSendJoinLeaveNotification" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="24d6c3d4-8783-45ab-a805-d279c35ac029" /><field column="GroupSendWaitingForApprovalNotification" fieldcaption="GroupSendWaitingForApprovalNotification" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9a3d8f1a-d759-4dbc-9bd2-311649b795c0" /><field column="GroupSecurity" fieldcaption="GroupSecurity" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="071780b2-4735-46b4-9185-1a90738989f1" /></form>', '20090422 16:13:59', 1748, N'Community.Group', 0, 0, 0, '78a6ade6-ca83-400c-b537-d5213b7162e4', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Community_Group', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Community_Group">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="GroupID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="GroupGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="GroupLastModified" type="xs:dateTime" />
              <xs:element name="GroupSiteID" type="xs:int" />
              <xs:element name="GroupDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupNodeGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="GroupApproveMembers" type="xs:int" />
              <xs:element name="GroupAccess" type="xs:int" />
              <xs:element name="GroupCreatedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="GroupApprovedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="GroupAvatarID" type="xs:int" minOccurs="0" />
              <xs:element name="GroupApproved" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupCreatedWhen" type="xs:dateTime" />
              <xs:element name="GroupSendJoinLeaveNotification" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupSendWaitingForApprovalNotification" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupSecurity" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Community_Group" />
      <xs:field xpath="GroupID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Group member', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="MemberID" fieldcaption="MemberID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="6fc6b001-4f9b-4c0e-af51-32a192f22acf" /><field column="MemberGUID" fieldcaption="MemberGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6ef2c92f-5917-4c95-b14d-41b544f2b244" /><field column="MemberUserID" fieldcaption="MemberUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="061e30be-08bf-48a0-844f-6325030adbca" /><field column="MemberGroupID" fieldcaption="MemberGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4f5e1481-fdbb-4083-9c95-8fe2574612ff" /><field column="MemberJoined" fieldcaption="MemberJoined" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="72f36245-c011-4be6-8069-801f4b382700" /><field column="MemberStatus" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d5bd6bf1-1751-4e8d-b0eb-86bb5d216f5e" /><field column="MemberApprovedWhen" fieldcaption="MemberApprovedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1f51c903-8960-40d7-ae12-5e44eeaaf966" /><field column="MemberRejectedWhen" fieldcaption="MemberRejectedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fd7aabec-14e4-4614-84a4-ea1a8b802860" /><field column="MemberApprovedByUserID" fieldcaption="MemberApprovedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c7ecf313-ac27-44f1-8375-cb2453df25f0" /><field column="MemberComment" fieldcaption="MemberComment" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9008448d-cca2-4297-93f9-aa65a78110bc" /><field column="MemberInvitedByUserID" fieldcaption="MemberInvitedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="32410c3a-92ca-48aa-987c-b8a73a884961" /></form>', '20090824 09:42:49', 1749, N'Community.GroupMember', 0, 0, 0, 'e15cfc90-107f-4196-a39d-7f5ea7824b08', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Community_GroupMember', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Community_GroupMember">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="MemberID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="MemberGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="MemberUserID" type="xs:int" />
              <xs:element name="MemberGroupID" type="xs:int" />
              <xs:element name="MemberJoined" type="xs:dateTime" />
              <xs:element name="MemberApprovedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="MemberRejectedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="MemberApprovedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="MemberComment" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MemberInvitedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="MemberStatus" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Community_GroupMember" />
      <xs:field xpath="MemberID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Avatar', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="AvatarID" fieldcaption="AvatarID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="070e07c2-613b-4ec3-b253-85490b5c2123" /><field column="AvatarName" fieldcaption="AvatarName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="fe64d27a-a61c-4f3f-af58-d61e35faa192" /><field column="AvatarFileName" fieldcaption="AvatarFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="c57fbf46-2904-4329-b081-7be3ff62e105" /><field column="AvatarFileExtension" fieldcaption="AvatarFileExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="10" publicfield="false" spellcheck="true" guid="44852334-d234-4cb6-84db-d8f96b5a0411" /><field column="AvatarBinary" fieldcaption="AvatarBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d1455dfc-5c3c-4e66-a012-b567b76d20b1" /><field column="AvatarType" fieldcaption="AvatarType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="eb09494b-aa8f-455f-8282-0f305be2c4a0" /><field column="AvatarIsCustom" fieldcaption="AvatarIsCustom" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="824bb523-a40b-4c36-9b61-55cbd38e1572" /><field column="AvatarGUID" fieldcaption="AvatarGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="76a41f68-0412-46ef-9a71-f3b0b23f3c14" /><field column="AvatarLastModified" fieldcaption="AvatarLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a0df33d7-2ea5-4ab1-8204-0b97d6b4bd40" /><field column="AvatarMimeType" fieldcaption="AvatarMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="f54b64b6-e474-48f8-82bf-d774a84a0134" /><field column="AvatarFileSize" fieldcaption="AvatarFileSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cc46dcfc-ffb2-4901-9d99-1a066d74113c" /><field column="AvatarImageHeight" fieldcaption="AvatarImageHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1c43a226-2790-4c55-9bd1-69a5d738f3fb" /><field column="AvatarImageWidth" fieldcaption="AvatarImageWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6df10f1e-1218-4cda-a594-1f991be5806c" /><field column="DefaultMaleUserAvatar" fieldcaption="DefaultMaleUserAvatar" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4904d149-1611-444c-8306-2f2d3dcecd7d" defaultvalue="false" /><field column="DefaultFemaleUserAvatar" fieldcaption="DefaultFemaleUserAvatar" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="70d2f645-011d-49d3-8740-bf9da38746bc" defaultvalue="false" /><field column="DefaultGroupAvatar" fieldcaption="DefaultGroupAvatar" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="673ae3c3-2bfd-41c8-9747-58da08d6ca7b" defaultvalue="false" /><field column="DefaultUserAvatar" fieldcaption="DefaultUserAvatar" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5462d085-166f-4a76-a5e7-c5f912bf7538" defaultvalue="false" /></form>', '20090624 15:16:47', 1750, N'cms.avatar', 0, 0, 0, 'ee73a58a-898b-4ca4-9abf-442380daae84', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Avatar', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Avatar">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AvatarID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AvatarName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AvatarFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AvatarFileExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AvatarBinary" type="xs:base64Binary" minOccurs="0" />
              <xs:element name="AvatarType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AvatarIsCustom" type="xs:boolean" />
              <xs:element name="AvatarGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="AvatarLastModified" type="xs:dateTime" />
              <xs:element name="AvatarMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AvatarFileSize" type="xs:int" />
              <xs:element name="AvatarImageHeight" type="xs:int" minOccurs="0" />
              <xs:element name="AvatarImageWidth" type="xs:int" minOccurs="0" />
              <xs:element name="DefaultMaleUserAvatar" type="xs:boolean" minOccurs="0" />
              <xs:element name="DefaultFemaleUserAvatar" type="xs:boolean" minOccurs="0" />
              <xs:element name="DefaultGroupAvatar" type="xs:boolean" minOccurs="0" />
              <xs:element name="DefaultUserAvatar" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Avatar" />
      <xs:field xpath="AvatarID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'User - Settings', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="UserSettingsID" fieldcaption="UserSettingsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="b12de09b-4aef-43f4-affd-0237ffa9349e" /><field column="UserNickName" fieldcaption="UserNickName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="a89f8fdb-8cc3-4f7a-9357-b24e08661d9e" /><field column="UserPicture" fieldcaption="UserPicture" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="40cb3e2d-b833-4a53-a9e5-1517d9dc591a" /><field column="UserSignature" fieldcaption="UserSignature" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d69a3871-0a68-4b73-aafd-4d170eab8631" /><field column="UserURLReferrer" fieldcaption="UserURLReferrer" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="a1720664-e42e-4650-8a0a-2d3eaa3f0b4f" /><field column="UserCampaign" fieldcaption="UserCampaign" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="b060fa80-913f-4641-af64-174195e2abee" /><field column="UserMessagingNotificationEmail" fieldcaption="UserMessagingNotificationEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="151a1b34-0af8-412e-8b7f-136577d49441" /><field column="UserCustomData" fieldcaption="UserCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="04716634-23a6-411c-94e2-7fbb33ca3264" /><field column="UserRegistrationInfo" fieldcaption="UserRegistrationInfo" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3d01cc42-9773-40b0-962b-757f4747ca3c" /><field column="UserPreferences" fieldcaption="UserPreferences" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="095ce444-a1e4-4150-bbaf-64039565bf23" /><field column="UserActivationDate" fieldcaption="UserActivationDate" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f66b91e5-ee7f-4a40-bda1-314096e32e55" /><field column="UserActivatedByUserID" fieldcaption="UserActivatedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="10852c60-0ce8-49d2-8b4b-1edacf9719d8" /><field column="UserTimeZoneID" fieldcaption="UserTimeZoneID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="64c1e09c-97fa-4c69-9c29-658ef1490a01" /><field column="UserAvatarID" fieldcaption="UserAvatarID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="17d4765d-882f-4d2d-b480-ea3220a2f800" /><field column="UserBadgeID" fieldcaption="UserBadgeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3a205392-4f52-48aa-ae19-66bd4cdf3b3e" /><field column="UserShowSplashScreen" fieldcaption="UserShowSplashScreen" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ee687124-8dae-4495-9aa3-6615c5d88ceb" /><field column="UserActivityPoints" fieldcaption="UserActivityPoints" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f25f07f3-ae23-4f4c-8086-5f8d289005c8" /><field column="UserForumPosts" fieldcaption="UserForumPosts" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a9f4355e-5f7d-4a7e-83cd-6c990b8b1918" /><field column="UserBlogComments" fieldcaption="UserBlogComments" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5b44c1e7-44d5-4a81-b226-0f07da22ca75" /><field column="UserGender" fieldcaption="UserGender" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="483a481c-1476-4877-b451-b6914ef2bfd3" /><field column="UserDateOfBirth" fieldcaption="UserDateOfBirth" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5457e36-200c-4628-95c5-8b46bc4742ab" /><field column="UserMessageBoardPosts" fieldcaption="UserMessageBoardPosts" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2d1bb6a0-66aa-45ab-a45e-9df59df4cad9" /><field column="UserSettingsUserGUID" fieldcaption="UserSettingsUserGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="16d3b39a-e0fe-44a7-9616-b9b0b172c42f" /><field column="UserSettingsUserID" fieldcaption="UserSettingsUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4187555e-f380-4279-bb32-2c182f88f4fa" /><field column="WindowsLiveID" fieldcaption="WindowsLiveID" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="1dbf8086-73c0-4e04-ac40-09afffd32284" visibility="none" ismacro="false" /><field column="UserBlogPosts" fieldcaption="UserBlogPosts" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b0d5005c-5279-48ba-81ea-0c48d6f96918" /><field column="UserWaitingForApproval" fieldcaption="UserWaitingForApproval" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8e4e8949-5854-45a8-a199-4e98d14b442e" /><field column="UserDialogsConfiguration" fieldcaption="UserDialogsConfiguration" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3c1b8b9d-d217-4696-9ff4-6ede129b64be" /><field column="UserDescription" fieldcaption="UserDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cffbe6b6-33f9-4c20-8e0c-ba37df7ba604" /><field column="UserUsedWebParts" fieldcaption="UserUsedWebParts" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="1000" publicfield="false" spellcheck="true" guid="5a90a606-d205-4a43-8b12-5a450122d983" visibility="none" ismacro="false" /><field column="UserUsedWidgets" fieldcaption="UserUsedWidgets" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="1000" publicfield="false" spellcheck="true" guid="2b30fdd0-9ab8-4038-a2dd-7d7fa7e654ad" visibility="none" ismacro="false" /><field column="UserFacebookID" fieldcaption="UserFacebookID" visible="false" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="d72bc50c-0c7f-45fe-9b3d-41190e454da7" visibility="none" ismacro="false" /></form>', '20100310 10:39:23', 1768, N'cms.usersettings', 0, 0, 0, 'a8203aeb-7709-4079-ad7e-3f9fccadc929', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_UserSettings', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_UserSettings">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UserSettingsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="UserNickName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserPicture" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserSignature" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserURLReferrer" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserCampaign" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserMessagingNotificationEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserRegistrationInfo" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserPreferences" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserActivationDate" type="xs:dateTime" minOccurs="0" />
              <xs:element name="UserActivatedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="UserTimeZoneID" type="xs:int" minOccurs="0" />
              <xs:element name="UserAvatarID" type="xs:int" minOccurs="0" />
              <xs:element name="UserBadgeID" type="xs:int" minOccurs="0" />
              <xs:element name="UserShowSplashScreen" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserActivityPoints" type="xs:int" minOccurs="0" />
              <xs:element name="UserForumPosts" type="xs:int" minOccurs="0" />
              <xs:element name="UserBlogComments" type="xs:int" minOccurs="0" />
              <xs:element name="UserGender" type="xs:int" minOccurs="0" />
              <xs:element name="UserDateOfBirth" type="xs:dateTime" minOccurs="0" />
              <xs:element name="UserMessageBoardPosts" type="xs:int" minOccurs="0" />
              <xs:element name="UserSettingsUserGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="UserSettingsUserID" type="xs:int" />
              <xs:element name="WindowsLiveID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserBlogPosts" type="xs:int" minOccurs="0" />
              <xs:element name="UserWaitingForApproval" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserDialogsConfiguration" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserUsedWebParts" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserUsedWidgets" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserFacebookID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_UserSettings" />
      <xs:field xpath="UserSettingsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Bad word', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="WordID" fieldcaption="WordID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e55fab30-93b1-4726-ac24-70db1744bd52" /><field column="WordGUID" fieldcaption="WordGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bfab8943-80c4-4a8f-9edd-4058567f964f" /><field column="WordLastModified" fieldcaption="WordLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="66a1e9e5-27e1-4807-9138-edf7f19543f8" /><field column="WordExpression" fieldcaption="WordExpression" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="06c844fc-a206-4ce9-8475-46235c72007b" /><field column="WordReplacement" fieldcaption="WordReplacement" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43afe0be-68dd-4e9d-8b52-196416a9743f" /><field column="WordAction" fieldcaption="WordAction" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="362cfe9c-423a-40f0-8918-2529c1bc7d02" /><field column="WordIsGlobal" fieldcaption="WordIsGlobal" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4043c1a4-0979-4cb4-9956-3210e752b9f3" /><field column="WordIsRegularExpression" fieldcaption="Word is regular expression" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9c891be9-e869-4041-8e50-fb10c51d25ce" /></form>', '20091012 08:50:02', 1771, N'badwords.word', 0, 0, 0, '9ff65ce7-ed48-48ea-97d6-cf88e644a10e', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'BadWords_Word', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="BadWords_Word">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WordID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WordGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="WordLastModified" type="xs:dateTime" />
              <xs:element name="WordExpression">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WordReplacement" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WordAction" type="xs:int" minOccurs="0" />
              <xs:element name="WordIsGlobal" type="xs:boolean" />
              <xs:element name="WordIsRegularExpression" type="xs:boolean" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//BadWords_Word" />
      <xs:field xpath="WordID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Badge', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="BadgeID" fieldcaption="BadgeID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e6c959c1-3f56-4b98-9ba1-56df31742a3c" visibility="none" /><field column="BadgeName" fieldcaption="BadgeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="8f7f989e-3115-43d5-afce-2c382577893f" /><field column="BadgeDisplayName" fieldcaption="BadgeDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="8b346663-f678-419d-9c5e-3de2bb247f33" /><field column="BadgeImageURL" fieldcaption="BadgeImageURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="be04c9a0-4afe-4ad6-8a71-d17ef454968b" /><field column="BadgeIsAutomatic" fieldcaption="BadgeIsAutomatic" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3c65408f-702f-4556-badd-c2d0f813cf36" /><field column="BadgeTopLimit" fieldcaption="BadgeTopLimit" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="962cbf48-513b-4990-a515-a8466fcfecfc" /><field column="BadgeGUID" fieldcaption="BadgeGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ee6c4e89-b05f-409d-a81e-b49a40e6710e" /><field column="BadgeLastModified" fieldcaption="BadgeLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1e4427e7-5438-4ba4-a757-41e2cc386a23" /></form>', '20090421 12:20:26', 1787, N'CMS.Badge', 0, 0, 0, 'd2e16806-304a-45d1-8158-d444be21e3a8', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Badge', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Badge">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="BadgeID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="BadgeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BadgeDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BadgeImageURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BadgeIsAutomatic" type="xs:boolean" />
              <xs:element name="BadgeTopLimit" type="xs:int" minOccurs="0" />
              <xs:element name="BadgeGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="BadgeLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Badge" />
      <xs:field xpath="BadgeID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Message board', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="BoardID" fieldcaption="BoardID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="3f5d2bb9-22a8-45ce-ba68-cfada44d509d" /><field column="BoardName" fieldcaption="BoardName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="6860b999-d9dd-4a80-ad42-370742f6f1a7" /><field column="BoardDisplayName" fieldcaption="BoardDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="e54226a6-bda3-4877-b57e-8c377fbd3df4" /><field column="BoardDescription" fieldcaption="BoardDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3366b9f9-096e-4ef5-97be-628a1bd069a0" /><field column="BoardOpened" fieldcaption="BoardOpened" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="33127b8a-43bc-46a4-bf30-7e7629e5e9e4" /><field column="BoardOpenedFrom" fieldcaption="BoardOpenedFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ce56e685-96cf-41f0-9ea6-7c2befbf63d2" /><field column="BoardOpenedTo" fieldcaption="BoardOpenedTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="102e25e0-f65c-42e5-bce1-e039f2488ca8" /><field column="BoardEnabled" fieldcaption="BoardEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1b810c74-531d-4257-861d-4fd0841dd252" /><field column="BoardAccess" fieldcaption="BoardAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="45cb27c6-e488-43db-abcb-66cb73d94dd7" /><field column="BoardModerated" fieldcaption="BoardModerated" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9249e285-e812-4470-86f9-c7513c2c87a0" /><field column="BoardUseCaptcha" fieldcaption="BoardUseCaptcha" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7a75986f-b121-4d04-83d5-16aeb5ec9948" /><field column="BoardMessages" fieldcaption="BoardMessages" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6277f200-4dfe-4a38-92cb-70ed978fb6b1" /><field column="BoardLastModified" fieldcaption="BoardLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="891b7bee-2381-41e4-b295-34313a492acf" /><field column="BoardGUID" fieldcaption="BoardGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b872e82a-7bc3-4ccb-83e6-6956c039e3af" /><field column="BoardDocumentID" fieldcaption="BoardDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="28a925ed-948f-4b54-b340-1703631f8a0c" /><field column="BoardUserID" fieldcaption="BoardUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="50008c74-73f4-4392-82b5-fb56fab1adae" /><field column="BoardGroupID" fieldcaption="BoardGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5b3db77c-1bef-48da-9cd3-f0b003d24c37" /><field column="BoardLastMessageTime" fieldcaption="BoardLastMessageTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="02e56770-7ff1-47a2-b6d1-85336ac0d1f0" /><field column="BoardLastMessageUserName" fieldcaption="BoardLastMessageUserName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="14f779a7-3cf2-4c6f-a47a-bfa569d90771" /><field column="BoardUnsubscriptionURL" fieldcaption="BoardUnsubscriptionURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="e2d08e2a-e814-4172-a186-8dd588299594" /><field column="BoardRequireEmails" fieldcaption="BoardRequireEmails" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="58aa827f-bb57-41df-bd0f-8cf3d205c83a" /><field column="BoardSiteID" fieldcaption="BoardSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="14756c64-a27b-4722-b2f9-172fe6fe8628" /><field column="BoardEnableSubscriptions" fieldcaption="BoardEnableSubscriptions" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d3612c9c-913e-4e8e-a2e7-4d7785768062" /><field column="BoardBaseURL" fieldcaption="BoardBaseURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="f46ac2d6-2a75-4463-9298-88ed457b0983" /></form>', '20091217 11:15:33', 1788, N'board.board', 0, 0, 0, 'c30cef2f-0eb5-4568-a8ab-93bfe91066e8', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Board_Board', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Board_Board">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="BoardID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="BoardName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BoardDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BoardDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BoardOpened" type="xs:boolean" />
              <xs:element name="BoardOpenedFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="BoardOpenedTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="BoardEnabled" type="xs:boolean" />
              <xs:element name="BoardAccess" type="xs:int" />
              <xs:element name="BoardModerated" type="xs:boolean" />
              <xs:element name="BoardUseCaptcha" type="xs:boolean" />
              <xs:element name="BoardMessages" type="xs:int" />
              <xs:element name="BoardLastModified" type="xs:dateTime" />
              <xs:element name="BoardGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="BoardDocumentID" type="xs:int" />
              <xs:element name="BoardUserID" type="xs:int" minOccurs="0" />
              <xs:element name="BoardGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="BoardLastMessageTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="BoardLastMessageUserName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BoardUnsubscriptionURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="BoardRequireEmails" type="xs:boolean" minOccurs="0" />
              <xs:element name="BoardSiteID" type="xs:int" />
              <xs:element name="BoardEnableSubscriptions" type="xs:boolean" />
              <xs:element name="BoardBaseURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Board_Board" />
      <xs:field xpath="BoardID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'E-mail attachment', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="AttachmentID" fieldcaption="AttachmentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ebf34bd7-e1d2-4488-85a3-9e3127f6a69b" /><field column="AttachmentName" fieldcaption="AttachmentName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="255" publicfield="false" spellcheck="true" guid="5340fb1e-0d50-4f87-aa6a-8029f188e64b" /><field column="AttachmentExtension" fieldcaption="AttachmentExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="4511305d-5456-4bbc-b99c-7eba264d379c" /><field column="AttachmentSize" fieldcaption="AttachmentSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3a4cffe6-51a8-41d6-946c-b35ee0846c9a" /><field column="AttachmentMimeType" fieldcaption="AttachmentMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="8c49f5fb-e8a0-4c7e-bcde-447e370cbb7f" /><field column="AttachmentBinary" fieldcaption="AttachmentBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43c6ec9d-8c01-4b7c-9178-1cea0d2fda61" /><field column="AttachmentGUID" fieldcaption="AttachmentGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="096f5598-6e57-446f-918d-168beb17e67c" /><field column="AttachmentLastModified" fieldcaption="AttachmentLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9a39bd85-4847-41ee-a7e1-3fb867eb5964" /><field column="AttachmentContentID" fieldcaption="AttachmentContentID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="255" publicfield="false" spellcheck="true" guid="0a09be9c-cc0c-4342-9549-af1dec53a02d" /><field column="AttachmentSiteID" fieldcaption="Site ID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="701e96b9-634a-4c43-8e95-af75300b2262" visibility="none" /></form>', '20091015 17:28:29', 1790, N'cms.EmailAttachment', 0, 0, 0, '3a6a10ed-8426-4240-9f0f-ae054f612c61', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_EmailAttachment', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_EmailAttachment">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AttachmentID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AttachmentName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="255" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentSize" type="xs:int" />
              <xs:element name="AttachmentMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentBinary" type="xs:base64Binary" />
              <xs:element name="AttachmentGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="AttachmentLastModified" type="xs:dateTime" />
              <xs:element name="AttachmentContentID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="255" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentSiteID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_EmailAttachment" />
      <xs:field xpath="AttachmentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Message', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="MessageID" fieldcaption="MessageID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="false" publicfield="false" spellcheck="true" guid="213451af-1a3a-4540-847a-7c91c1c8185d" /><field column="MessageUserName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="c3a2f1d2-c50e-4626-9196-b92374ae982c" fieldcaption="User name:" /><field column="MessageText" fieldcaption="Text" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="da3e536f-16ff-4ca5-b70a-6d0ddc016b76" /><field column="MessageEmail" fieldcaption="Email" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="1ac6e606-13e7-439e-b51f-8c09fa79984c" /><field column="MessageURL" fieldcaption="URL" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="67503096-e57d-4278-ac92-e6561419c214" /><field column="MessageIsSpam" fieldcaption="Is SPAM" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2f20a317-502b-41df-90e9-023866b017fe" /><field column="MessageBoardID" fieldcaption="Message Board ID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="966041a1-ce21-4cb6-a41f-4141637e5a58" /><field column="MessageApproved" fieldcaption="Approved" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="50d16031-33c1-4ac9-b5e8-f1a7ce0847e0" /><field column="MessageApprovedByUserID" fieldcaption="Approved By User ID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="11bfc429-bab9-4b26-91d9-b4e064b3829b" /><field column="MessageUserID" fieldcaption="User ID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="93218b64-ec61-4b99-89b3-6a576a219fbe" /><field column="MessageUserInfo" fieldcaption="User Info" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="463acf35-fe22-4b96-9a0f-42b6de4c34b9" /><field column="MessageAvatarGUID" fieldcaption="Avatar GUID" visible="true" columntype="guid" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0bac580c-b976-4c63-8cf1-2fbefbaaa148" /><field column="MessageInserted" fieldcaption="Inserted" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7282dfef-a951-44f4-a460-f472c2936692"><settings><editTime>true</editTime></settings></field><field column="MessageLastModified" fieldcaption="Last Modified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c8354000-5a97-4932-899a-caba73315eff"><settings><editTime>true</editTime></settings></field><field column="MessageGUID" fieldcaption="GUID" visible="true" columntype="guid" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4f7a909f-3352-47d0-9b55-ed6c9822fcd1" /><field column="MessageRatingValue" fieldcaption="MessageRatingValue" visible="true" columntype="double" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="67f5c140-09e6-42e6-adf9-ca9a2fdf5b84" /></form>', '20100303 12:11:27', 1791, N'board.message', 0, 0, 0, 'a6fc171e-86df-4222-a5cf-c95259deaeb9', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Board_Message', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Board_Message">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="MessageID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="MessageUserName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageEmail">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageURL">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageIsSpam" type="xs:boolean" />
              <xs:element name="MessageBoardID" type="xs:int" />
              <xs:element name="MessageApproved" type="xs:boolean" />
              <xs:element name="MessageApprovedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="MessageUserID" type="xs:int" minOccurs="0" />
              <xs:element name="MessageUserInfo">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageAvatarGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="MessageInserted" type="xs:dateTime" />
              <xs:element name="MessageLastModified" type="xs:dateTime" />
              <xs:element name="MessageGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="MessageRatingValue" type="xs:double" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Board_Message" />
      <xs:field xpath="MessageID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="True" name="MessageUserInfo" content="True" searchable="True" id="94207591-fd30-4260-bda3-2ef62f743c6d"></item><item tokenized="True" name="MessageUserName" content="True" searchable="True" id="f7fbc73c-44d0-4bcf-ad7f-e7f80a056e3f"></item><item tokenized="False" name="MessageLastModified" content="False" searchable="True" id="58405079-5d4f-46ac-baea-b967f02309c1"></item><item tokenized="False" name="MessageID" content="False" searchable="True" id="1ca1c720-4aa6-4a0e-ac6b-ff4c75364044"></item><item tokenized="True" name="MessageEmail" content="True" searchable="True" id="ce3feadb-628b-4014-9854-dc7a306c26e6"></item><item tokenized="False" name="MessageApproved" content="False" searchable="True" id="68f414d2-5d2d-4419-a3ba-5915dad2b9d5"></item><item tokenized="False" name="MessageApprovedByUserID" content="False" searchable="True" id="d3afa628-28cb-4565-ba7f-3d00d3bbb677"></item><item tokenized="False" name="MessageGUID" content="False" searchable="False" id="0fafc405-de48-4170-946f-246a1045907b"></item><item tokenized="False" name="MessageAvatarGUID" content="False" searchable="False" id="86964a7d-983b-44de-8718-0f82607b7682"></item><item tokenized="True" name="MessageText" content="True" searchable="True" id="a6ab08ea-5c71-42ed-8710-12034db98163"></item><item tokenized="False" name="MessageBoardID" content="False" searchable="True" id="e61ca7f3-7965-490f-ae61-1f19b8779c83"></item><item tokenized="False" name="MessageInserted" content="False" searchable="True" id="adbb6d16-049e-455d-9a1a-64a9362c12ac"></item><item tokenized="False" name="MessageRatingValue" content="False" searchable="True" id="7c9b2cf2-7e7f-4d95-b7a2-364e5f9df123"></item><item tokenized="False" name="MessageIsSpam" content="False" searchable="True" id="750551f6-ad21-4cc2-b536-94a9beb00e41"></item><item tokenized="False" name="MessageUserID" content="False" searchable="True" id="90dbeb9e-0091-4d52-97c7-4902c3e3dfcf"></item><item tokenized="True" name="MessageURL" content="True" searchable="True" id="3285dbb0-c1e2-4b39-81bc-27bcb50dd850"></item></search>', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Message board role', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="BoardID" fieldcaption="BoardID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="4e0e8f36-218d-4b3b-b133-d45c55bbe586" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="33dfe7c5-a602-4f9b-bb20-4d3f922eea79" /></form>', '20090903 12:38:21', 1797, N'board.boardrole', 0, 0, 0, '7ae09413-b447-4b1f-93e0-58421ca1f6ba', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Board_Role', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Board_Role">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="BoardID" type="xs:int" />
              <xs:element name="RoleID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Board_Role" />
      <xs:field xpath="BoardID" />
      <xs:field xpath="RoleID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Bad word culture', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="WordID" fieldcaption="WordID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="53b7d596-5a05-4d30-88fa-fdd58c7733d8" /><field column="CultureID" fieldcaption="CultureID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="851d3cb2-dd4d-44c2-8834-36b5091128b4" /></form>', '20081231 14:53:30', 1803, N'badwords.wordculture', 0, 0, 0, 'ec176759-6109-4794-9de9-372f91efb878', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'BadWords_WordCulture', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="BadWords_WordCulture">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WordID" type="xs:int" />
              <xs:element name="CultureID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//BadWords_WordCulture" />
      <xs:field xpath="WordID" />
      <xs:field xpath="CultureID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
set identity_insert [cms_class] off
