<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Issue_List.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_List" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Tools - Newsletter issues</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        function RefreshPage() {
            document.location.replace(document.location);
        }
        //]]>
    </script>

</head>
<body class="TabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="Panel1" CssClass="TabsPageBody">
        <asp:Panel runat="server" ID="pnlContainer" CssClass="TabsPageContainer">
            <asp:Panel runat="server" ID="pnlScroll" CssClass="TabsPageScrollArea2">
                <asp:Panel runat="server" ID="pnlTab" CssClass="TabsPageContent">
                    <asp:Panel ID="pnlNewIssue" runat="server" CssClass="PageHeaderLine">
                        <asp:Panel runat="server" ID="pnlNew" CssClass="PageHeaderItem">
                            <asp:Image ID="imgNewIssue" runat="server" CssClass="NewItemImage" EnableViewState="false" />
                            <asp:LinkButton ID="btnNewIssue" runat="server" CssClass="NewItemLink" EnableViewState="false" />
                        </asp:Panel>
                        <br style="clear: both" />
                    </asp:Panel>
                    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
                        <cms:UniGrid runat="server" ID="UniGrid" GridName="Newsletter_Issue_List.xml" Columns="IssueID, IssueSubject, IssueMailoutTime, IssueSentEmails, IssueUnsubscribed"
                            IsLiveSite="false" OrderBy="IssueID" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
