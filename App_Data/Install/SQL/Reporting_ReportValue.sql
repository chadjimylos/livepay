CREATE TABLE [Reporting_ReportValue] (
		[ValueID]                         int NOT NULL IDENTITY(1, 1),
		[ValueName]                       nvarchar(100) NOT NULL,
		[ValueDisplayName]                nvarchar(450) NOT NULL,
		[ValueQuery]                      nvarchar(max) NOT NULL,
		[ValueQueryIsStoredProcedure]     bit NOT NULL,
		[ValueFormatString]               nvarchar(200) NULL,
		[ValueReportID]                   int NOT NULL,
		[ValueGUID]                       uniqueidentifier NOT NULL,
		[ValueLastModified]               datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_ReportValue]
	ADD
	CONSTRAINT [PK_Reporting_ReportValue]
	PRIMARY KEY
	([ValueID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Reporting_ReportValue]') IS NOT NULL
	AND OBJECT_ID(N'[Reporting_Report]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_ReportValue_ValueReportID_Reporting_Report]') IS NULL
BEGIN
		ALTER TABLE [Reporting_ReportValue]
			ADD CONSTRAINT [FK_Reporting_ReportValue_ValueReportID_Reporting_Report]
			FOREIGN KEY ([ValueReportID]) REFERENCES [Reporting_Report] ([ReportID])
END
CREATE INDEX [IX_Reporting_ReportValue_ValueName_ValueReportID]
	ON [Reporting_ReportValue] ([ValueName], [ValueReportID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
