function checkAnauthorizedPage() {
    var url = document.location.href.toLowerCase()
    if (url.indexOf('anauthorized=1') != -1) {
        var urlFinal = url.replace("?anauthorized=1", "");
        document.location.href = urlFinal;
    }
}

// page init
jQuery(function () {
    initCustomForms();

    //popups
    //initPopups();

    InitializeLightbox();
    initPopups();
});

function InitializeLightbox() {
    //var rel_width = "";
    var rel_height = "";
    //var final_rel_width = "";
    var final_rel_height = "";

    if ($('#Right-sidebar')) {
        $('#Right-sidebar a').each(function (index) {
            if ($(this).attr('href').indexOf("?lightbox") > -1) {
//                if ($(this).attr('href').indexOf(",width=") > -1) {
//                    var width = $(this).attr('href').split(',width=')[1].split(',')[0]
//                    rel_width = width;
//                }

                if ($(this).attr('href').indexOf(",height=") > -1) {
                    var height = $(this).attr('href').split(',height=')[1].split(',')[0]
                    rel_height = height;
                }
//                if (rel_width != "") {
//                    final_rel_width = rel_width
//                }
//                else {
//                    final_rel_width = '460'
//                }

                if (rel_height != "") {
                    final_rel_height = rel_height
                }
                else {
                    final_rel_height = '450'
                }

                $(this).attr('rel', '460|' + final_rel_height);
                //$(this).attr('rel', +final_rel_width + '|' + final_rel_height);
                $(this).attr('class', 'iframe-popup');
            }
        });
    } 
}



//function getParameterByName(name) {
//    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
//    var regexS = "[\\?&]" + name + "=([^&#]*)";
//    var regex = new RegExp(regexS);
//    var results = regex.exec(window.location.search);
//    alert(results);
//    if (results == null)
//        return "";
//    else
//        return decodeURIComponent(results[1].replace(/\+/g, " "));
//}



//function for click to call
function OpenClickToCall(pageURL, title, w, h) {
    w =580;
    h = 410;
    var left = (screen.width / 2.5) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

//function for click to chat
function OpenClickToChat(pageURL, title, w, h) {
    w = 484;
    h = 361;
    var left = (screen.width / 2.5) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
} 

// function for the FAQs

function _hideBeforeExpand(id) {
    var c = $(id).attr('className');
    var visible = $('div.' + c).filter(function () { return $(this).is(':visible') });
    var onCompleted = function () {

        if ($(id).is(':visible')) {            
            $(id).slideUp('normal');
            $('#boxOfInsideTitle').slideDown('normal');
            $(id + 'Link').removeClass('sel');
        }
        else {
            $(id).slideDown('normal');
            $('#boxOfInsideTitle').slideUp('normal');
            $(id + 'Link').addClass('sel');
        }
    };

    if (visible.size() > 0)
        visible.slideUp('normal');

    onCompleted();
}

// function for the lightbox

function show_lightbox(DivID,Width,Height,Left)
{
	// get popup
    var popup = $('div#' + DivID);
	Left = (document.body.offsetWidth / 2) - (Width / 2) + 'px'; 
	// show popup
	$.blockUI(
	{
		message: popup
		,css:
		{
			border: 0,
			top:'20%',
			left:Left,
			width:Width,
			height:Height,
			backgroundColor: 'transparent'
		}
		,overlayCSS:
		{
			backgroundColor: '#000'
		}
	});
}




// popups function
function initPopups() {

  if (document.location.href.toLowerCase().indexOf('cmsp') != -1) {
        return false
    }
    var _zIndex = 1000;
    var _fadeSpeed = 350;
    var _faderOpacity = 0.75;
    var _faderBackground = '#000';
    var _faderId = 'lightbox-overlay';
    var _closeLink = 'a.close';
    var _fader;
    var _lightbox = $('<div class="lightbox"><div class="t"></div><div class="m"><div class="inside"><a href="#" class="close" id="aClosePopUp"></a><div class="holder"></div></div></div><div class="b"></div></div>');
   
    var _iframeHolder = _lightbox.find('.holder');
    var _btnClose = _lightbox.find('a.close');
    
    var _openers = jQuery('a.iframe-popup');
    var _page = jQuery(document);
    var _minWidth = parseInt(jQuery('body').css('minWidth'));
    var _scroll = false;

    

    // init popup fader
    _fader = jQuery('#' + _faderId);
    if (!_fader.length) {
        _fader = jQuery('<div />');
        _fader.attr('id', _faderId);
        jQuery('body').append(_fader).append(_lightbox.hide());
    }
    _fader.css({
        opacity: _faderOpacity,
        backgroundColor: _faderBackground,
        position: 'fixed',
        overflow: 'hidden',
        display: 'none',
        top: 0,
        left: 0,
        zIndex: _zIndex
    });

    // IE6 iframe fix
    if (jQuery.browser.msie && jQuery.browser.version < 7) {
        if (!_fader.children().length) {
            var _frame = jQuery('<iframe  src="javascript:false" frameborder="0" scrolling="no" />');
            _frame.css({
                opacity: 0,
                width: '100%',
                height: '100%'
            });
            var _frameOverlay = jQuery('<div>');
            _frameOverlay.css({
                top: 0,
                left: 0,
                zIndex: 1,
                opacity: 0,
                background: '#000',
                position: 'absolute',
                width: '100%',
                height: '100%'
            });
            _fader.empty().append(_frame).append(_frameOverlay);
        }
    }

    // lightbox positioning function
    function positionLightbox() {
        if (_lightbox) {
            var _windowHeight = jQuery(window).height();
            var _windowWidth = jQuery(window).width();
            var _lightboxWidth = _lightbox.outerWidth();
            var _lightboxHeight = _lightbox.outerHeight();
            var _pageHeight = _page.height();

            if (_windowWidth < _minWidth) _fader.css('width', _minWidth);
            else _fader.css('width', '100%');
            if (_windowHeight < _pageHeight) _fader.css('height', _pageHeight);
            else _fader.css('height', _windowHeight);

            _lightbox.css({
                position: 'absolute',
                zIndex: (_zIndex + 1)
            });

            // vertical position
            if (_windowHeight > _lightboxHeight) {
                if (jQuery.browser.msie && jQuery.browser.version < 7) {
                    _lightbox.css({
                        position: 'absolute',
                        top: parseInt(jQuery(window).scrollTop()) + (_windowHeight - _lightboxHeight) / 2
                    });
                } else {
                    _lightbox.css({
                        //position: 'fixed',
                        position: 'absolute',
                        top: (_windowHeight - _lightboxHeight) / 2
                    });
                }
            } else {
                var _faderHeight = _fader.height();
                if (_faderHeight < _lightboxHeight) _fader.css('height', _lightboxHeight);
                if (!_scroll) {
                    if (_faderHeight - _lightboxHeight > parseInt(jQuery(window).scrollTop())) {
                        _faderHeight = parseInt(jQuery(window).scrollTop())
                        _scroll = _faderHeight;
                    } else {
                        _scroll = _faderHeight - _lightboxHeight;
                        if (_scroll < 0) {
                            _scroll = 0
                        }
                    }
                }
                _lightbox.css({
                    position: 'absolute',
                    top: _scroll
                });
            }

            // horizontal position
            if (_lightbox.outerWidth() < $(window).width()) {
                _lightbox.css({ left: ($(window).width() - _lightbox.outerWidth()) / 2 });
            } else {
                if (_fader.width() > _lightbox.outerWidth()) _lightbox.css({ left: (_fader.width() - _lightbox.outerWidth()) / 2 });
                else _lightbox.css({ left: 0 });
            }
        }
    }

    // show/hide lightbox
    function toggleState(_state) {
        if (!_lightbox) return;

        if (_state) {
            _fader.fadeIn(_fadeSpeed, function () {
                _lightbox.fadeIn(_fadeSpeed);
            });
            _scroll = false;
            positionLightbox();
        } else {
            _lightbox.fadeOut(_fadeSpeed, function () {
			
                _fader.fadeOut(_fadeSpeed);
                _scroll = false;
                //$('.holder iframe').attr('src', '/');
                $('.holder iframe').attr('src', '/');

				
            });
            checkAnauthorizedPage()
        }        
    }
   
    // close lightbox button
    _btnClose.click(function () {
        toggleState(false);
        return false;
    })

  
    // lightbox openers
    _openers.each(function () {
        var _opener = jQuery(this);
        var _meta = _opener.attr('rel').split('|');
        var _target = _opener.attr('href');

        _opener.click(function () {
            //var _iframe = $('<iframe name="test" allowtransparency="true" frameborder="0" style="overflow-x:hidden;overflow-y:scroll;overflow-x:hidden;"></iframe>');
            var _iframe = $('<iframe name="test" id="myDesclimer" allowtransparency="true" frameborder="0" scrolling="no"></iframe>');
            _iframe.attr('width', _meta[0]);
            _iframe.attr('height', _meta[1]);

            _iframe.attr('src', _target);
            _iframeHolder.empty().append(_iframe);
            //setTimeout(function () {
                
            //}, 500);

            $('#myDesclimer').load(function () {
                $('head', window.frames['test'].document).append("<link type='text/css' rel='stylesheet' href='/CMSScripts/EurobankEquities/css/EurobankEquities.css'></link>");
            });
            
            toggleState(true);
            return false;
        });
    });

    // event handlers
    jQuery(window).resize(positionLightbox);
    jQuery(window).scroll(positionLightbox);
    jQuery(document).keydown(function (e) {
        if (e.keyCode == 27) toggleState(false);
    });
    _fader.click(function () {
        if (!_fader.is(':animated')) toggleState(false);
        return false;
    });

}

// page init
function initCustomForms() {
    $('input:radio').customRadio();
}


// custom radios module
jQuery.fn.customRadio = function (_options) {
    var _options = jQuery.extend({
        radioStructure: '<div></div>',
        radioDisabled: 'disabled',
        radioDefault: 'radioArea',
        radioChecked: 'radioAreaChecked'
    }, _options);
    return this.each(function () {
        var radio = jQuery(this);
        if (!radio.hasClass('outtaHere') && radio.is(':radio')) {
            var replaced = jQuery(_options.radioStructure);
            this._replaced = replaced;
            if (radio.is(':disabled')) replaced.addClass(_options.radioDisabled);
            else if (radio.is(':checked')) replaced.addClass(_options.radioChecked);
            else replaced.addClass(_options.radioDefault);
            replaced.click(function () {
                if ($(this).hasClass(_options.radioDefault)) {
                    radio.attr('checked', 'checked');
                    changeRadio(radio.get(0));
                }
            });
            radio.click(function () {
                changeRadio(this);
            });
            replaced.insertBefore(radio);
            radio.addClass('outtaHere');
        }
    });
    function changeRadio(_this) {
        $(_this).change();
        $('input:radio[name=' + $(_this).attr("name") + ']').not(_this).each(function () {
            if (this._replaced && !$(this).is(':disabled')) this._replaced.removeClass().addClass(_options.radioDefault);
        });
        _this._replaced.removeClass().addClass(_options.radioChecked);
    }

}


