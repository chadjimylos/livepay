CREATE TABLE [Community_GroupMember] (
		[MemberID]                   int NOT NULL IDENTITY(1, 1),
		[MemberGUID]                 uniqueidentifier NOT NULL,
		[MemberUserID]               int NOT NULL,
		[MemberGroupID]              int NOT NULL,
		[MemberJoined]               datetime NOT NULL,
		[MemberApprovedWhen]         datetime NULL,
		[MemberRejectedWhen]         datetime NULL,
		[MemberApprovedByUserID]     int NULL,
		[MemberComment]              nvarchar(max) NULL,
		[MemberInvitedByUserID]      int NULL,
		[MemberStatus]               int NULL
)
ON [PRIMARY]
ALTER TABLE [Community_GroupMember]
	ADD
	CONSTRAINT [PK_Community_GroupMember]
	PRIMARY KEY
	NONCLUSTERED
	([MemberID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Community_GroupMember]
	ADD
	CONSTRAINT [DEFAULT_Community_GroupMember_MemberStatus]
	DEFAULT ((0)) FOR [MemberStatus]
IF OBJECT_ID(N'[Community_GroupMember]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupMember_MemberApprovedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupMember]
			ADD CONSTRAINT [FK_Community_GroupMember_MemberApprovedByUserID_CMS_User]
			FOREIGN KEY ([MemberApprovedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Community_GroupMember]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupMember_MemberGroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupMember]
			ADD CONSTRAINT [FK_Community_GroupMember_MemberGroupID_Community_Group]
			FOREIGN KEY ([MemberGroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[Community_GroupMember]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupMember_MemberInvitedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupMember]
			ADD CONSTRAINT [FK_Community_GroupMember_MemberInvitedByUserID_CMS_User]
			FOREIGN KEY ([MemberInvitedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Community_GroupMember]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupMember_MemberUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupMember]
			ADD CONSTRAINT [FK_Community_GroupMember_MemberUserID_CMS_User]
			FOREIGN KEY ([MemberUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Community_GroupMember_MemberApprovedByUserID]
	ON [Community_GroupMember] ([MemberApprovedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_Community_GroupMember_MemberGroupID]
	ON [Community_GroupMember] ([MemberGroupID])
	ON [PRIMARY]
CREATE INDEX [IX_Community_GroupMember_MemberInvitedByUserID]
	ON [Community_GroupMember] ([MemberInvitedByUserID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Community_GroupMember_MemberJoined]
	ON [Community_GroupMember] ([MemberJoined] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Community_GroupMember_MemberStatus]
	ON [Community_GroupMember] ([MemberStatus])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Community_GroupMember_MemberUserID]
	ON [Community_GroupMember] ([MemberUserID])
	ON [PRIMARY]
