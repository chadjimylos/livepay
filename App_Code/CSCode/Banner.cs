﻿using System;
using System.Data;
using System.Configuration;
//using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using CMS.GlobalHelper;
//using System.Xml.Linq;

/// <summary>
/// Summary description for Banner
/// </summary>
public static class Banner
{
    public static string BannerTransformation(object Banner, object FileAttachment, object FileImageUrl, object ScriptAdServer, object width, object height, object CSS, object HrefLink, object Target, object QueryString)
	{
        if ((Banner == null | Banner == DBNull.Value))
        {
            return "";
        }
        else
        {
            string ObjBanner = (string)Banner;
            string ObjTarget = (string)Target;
            string ObjQueryString = "";
            if ((QueryString != null | QueryString != DBNull.Value))
            {
                ObjQueryString = (string)QueryString;
            }


            ObjTarget = (string)Target;

            string ObjFileAttachment = (string)FileAttachment;
            string ObjScriptAdServer = (string)ScriptAdServer;

            int Objwidth = (int)width;
            int Objheight = (int)height;

            string CssClass = (string)CSS;


            string LinkUrl = (string)HrefLink;

            string scheme = HttpContext.Current.Request.Url.Scheme;

            switch (ObjBanner.ToString().ToLower())
            {   
                case "adscript":
                    return "<div class='" + CssClass + "'>" + ObjScriptAdServer + "</div>";
                    break;
                case "swf":

                    string FlashScript = "<script language='javascript'> AC_FL_RunContent = 0; </script>\n" +
                                         "<script language='javascript'> DetectFlashVer = 0; </script>\n" +
                                         "<script src='/CMSScripts/AC_RunActiveContent.js' language='javascript'></script>\n";

                    if (CssClass == "no11")
                    {
                        
                        string dom = String.Format(scheme + "://{0}{1}", HttpContext.Current.Request.Url.Host, ObjFileAttachment);

                        return @"<div class='" + CssClass + "'><object type='application/x-shockwave-flash' width='" + Objwidth + "' height='" + Objheight + "' data='" + HttpContext.Current.Server.UrlEncode(dom) + "?ClickTag=" + HttpContext.Current.Server.UrlEncode(LinkUrl) + "&ext=.swf'>" +
                        "<param name='classid' value='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' />" +
                        "<param name='codebase' value='" + scheme + "://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0' />" +
                        "<param name='movie' value='" + HttpContext.Current.Server.UrlEncode(dom) + "?ClickTag=" + HttpContext.Current.Server.UrlEncode(LinkUrl) + "&ext=.swf' />" +
                        "<param name='quality' value='best' />" +
                        "<param name='scale' value='noscale' />" +
                        "<param name='play' value='true' />" +
                        "<param name='loop' value='true' />" +
                        "<param name='pluginurl' value='" + scheme + "://www.adobe.com/go/getflashplayer' />" +
                        "<param name='wmode' value='transparent' />" +
                        "</object></div>";
                    }
                    else
                    {
                        string dom = String.Format(scheme + "://{0}/{1}", HttpContext.Current.Request.Url.Host, HttpContext.Current.Request.ApplicationPath);
                        string url = dom + "/App_Themes/loader?ClickTag=" + HttpContext.Current.Server.UrlEncode(LinkUrl) + "&swfToLoad=" + HttpContext.Current.Server.UrlEncode(ObjFileAttachment.Replace(".", "~!@")) + "&WIDTH=" + width + "&HEIGHT=" + height + ObjQueryString;

                        FlashScript += "<script language='JavaScript' type='text/javascript'> \n" +
                                       "startFlash('" + width + "','" + height + "','" + url + "','" + scheme + "','" + FileImageUrl + "') \n" +
                                       "</script> \n" +
                                       "<noscript> \n" +
                                       "Alternate HTML content should be placed here. This content requires the Adobe Flash Player. \n" +
                                       "<a href='http://www.macromedia.com/go/getflash/'>Get Flash</a> \n" +
                                       "</noscript> \n";

                        return FlashScript;
                        //return @"<div class='" + CssClass + "' style='overflow:hidden;width:" + Objwidth + "px;height:" + Objheight + "px'><object type='application/x-shockwave-flash' width='999' height='999' data='" + dom + "/App_Themes/loader.swf?ClickTag=" + HttpContext.Current.Server.UrlEncode(LinkUrl) + "&swfToLoad=" + HttpContext.Current.Server.UrlEncode(ObjFileAttachment.Replace(".", "~!@")) + ObjQueryString + "'>" +
                        //"<param name='classid' value='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' />" +
                        //"<param name='codebase' value='" + scheme + "://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0' />" +
                        //"<param name='movie' value='" + url +"' />" +
                        //"<param name='quality' value='best' />" +
                        //"<param name='scale' value='noscale' />" +
                        //"<param name='play' value='true' />" +
                        //"<param name='loop' value='true' />" +
                        //"<param name='pluginurl' value='" + scheme + "://www.adobe.com/go/getflashplayer' />" +
                        //"<param name='wmode' value='transparent' />" +
                        //"</object></div>";
                    }

                    break;
                case "img":

                    if (string.IsNullOrEmpty(LinkUrl))
                    {
                        return @"<div class='" + CssClass + "'><img border='0' src='" + FileAttachment + "' width='" + Objwidth + "' height='" + Objheight + "' alt=''/></div>";
                    }else{
                        return @"<div class='" + CssClass + "'><a href='" + LinkUrl + "' target='" + ObjTarget + "' ><img border='0' src='" + FileAttachment + "' width='" + Objwidth + "' height='" + Objheight + "' alt=''/></a></div>";
                    }
                    break;
                case "imgurl":
                    

                    if (string.IsNullOrEmpty(LinkUrl))
                    {
                        return @"<div class='" + CssClass + "'><img border='0' src='" + FileImageUrl + "' width='" + Objwidth + "' height='" + Objheight + "' alt=''/></div>";
                    }
                    else
                    {
                        return @"<div class='" + CssClass + "'><a href='" + LinkUrl + "' target='" + ObjTarget + "'><img border='0' src='" + FileImageUrl + "' width='" + Objwidth + "' height='" + Objheight + "' alt=''/></a></div>";
                    }
                    break;
                default:
                    return "";
                    break;
            }
        }
        
	    //
	    // TODO: Add constructor logic here
	    //
        //return "";
	}
}
