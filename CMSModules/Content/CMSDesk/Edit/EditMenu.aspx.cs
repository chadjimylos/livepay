using System;
using System.Text;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Edit_EditMenu : CMSContentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Register scripts
        ScriptHelper.RegisterProgress(Page);

        // Get the document ID
        int nodeId = QueryHelper.GetInteger("nodeid", 0);
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);

        if (node != null)
        {
            if (node.NodeClassName.ToLower() == "cms.root")
            {
                menuElem.AllowSave = false;
            }
        }
        else
        {
            menuElem.Visible = false;
        }

        StringBuilder script = new StringBuilder();
        script.AppendLine("function PassiveRefresh(nodeId) {");
        script.AppendLine("    if (parent.frames['editview'] != null) {");
        script.AppendLine("        if (parent.frames['editview'].NotChanged) {");
        script.AppendLine("            parent.frames['editview'].NotChanged();");
        script.AppendLine("        }");
        script.AppendLine("        parent.frames['editview'].location.replace(parent.frames['editview'].location);");
        script.AppendLine("    }");
        script.AppendLine("}");

        script.AppendLine("function RefreshTree(expandNodeId, selectNodeId) {");
        script.AppendLine("    parent.RefreshTree(expandNodeId, selectNodeId);");
        script.AppendLine("}");

        script.AppendLine("var isSideWindow = true;");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "passiveRefresh", ScriptHelper.GetScript(script.ToString()));
    }
}
