using System;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.FormControls;


public partial class CMSModules_Content_CMSDesk_Properties_Template : CMSPropertiesPage
{
    #region "Variables & constants"

    private const string PORTALENGINE_UI_LAYOUTPATH = "~/CMSModules/PortalEngine/UI/Layout/";

    protected int nodeId = 0;

    protected string mSave = null;
    protected string mInherit = null;
    protected string mClone = null;
    protected string mEditTemplateProperties = null;
    protected string mSaveDoc = null;

    private string btnSaveOnClickScript = "";
    private int siteid = 0;
    private int cloneId = 0;

    protected bool hasModifyPermission = true;

    protected TreeNode node = null;
    protected TreeProvider tree = null;
    protected CurrentUserInfo currentUser = null;

    protected bool hasDesign = false;

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Template"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Template");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);
        inheritElem.DocumentSettings = true;
        int documentId = 0;

        UIContext.PropertyTab = PropertyTabEnum.Template;

        currentUser = CMSContext.CurrentUser;

        nodeId = QueryHelper.GetInteger("nodeid", 0);

        tree = new TreeProvider(currentUser);
        node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode, tree.CombineWithDefaultCulture);

        if (node != null)
        {
            siteid = node.NodeSiteID;
            documentId = node.DocumentID;
        }

        imgSaveDoc.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        imgSaveDoc.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/savedisabled.png");
        mSaveDoc = ResHelper.GetString("general.save");

        pnlInherits.GroupingText = ResHelper.GetString("PageProperties.InheritLevels");

        ltlScript.Text = "";
        string initScript = null;

        hasDesign = currentUser.IsAuthorizedPerResource("CMS.Content", "Design");
        if (hasDesign)
        {
            btnEditTemplateProperties.Attributes.Add("onclick", "modalDialog('" + ResolveUrl("~/CMSModules/PortalEngine/UI/PageTemplates/PageTemplate_Edit.aspx") + "?templateid=' + document.getElementById('SelectedTemplateId').value + '&nobreadcrumbs=1', 'Template selection', 750, 550);return false;");

            bool allowEditShared = currentUser.IsAuthorizedPerUIElement("CMS.Content", "Template.ModifySharedTemplates");

            // Define GetCurrentTemplateId() used for specifing teplateId in the SaveAsNewTemplate onClick handler
            initScript =
                "var allowEditShared = " + allowEditShared.ToString().ToLower() + @";

                function GetCurrentTemplateId() {
                    if (document.getElementById('SelectedTemplateId').value > 0) { 
                        return document.getElementById('SelectedTemplateId').value;
                    } else { 
                        return document.getElementById('InheritedTemplateId').value;
                    }
                };";

            ltlPreInitScript.Text = ScriptHelper.GetScript(initScript);

            btnSelect.Text = ResHelper.GetString("PageProperties.Select");
            btnSelect.Attributes.Add("onclick", "modalDialog('" + ResolveUrl(PORTALENGINE_UI_LAYOUTPATH + "PageTemplateSelector.aspx") + "?documentid=" + documentId + "', 'Page template selection', '90%', '85%'); return false;");

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

            ltlElemScript.Text += ScriptHelper.GetScript(
                "var cloneElem = document.getElementById('" + btnClone.ClientID + @"');
                if (cloneElem != null) var cloneElemStyle = (cloneElem.style != null) ? cloneElem.style : cloneElem;
                var inheritElem = document.getElementById('" + btnInherit.ClientID + @"');
                if (inheritElem != null) var inheritElemStyle = (inheritElem.style != null) ? inheritElem.style : inheritElem;
                var saveElem = document.getElementById('" + btnSave.ClientID + @"');
                if (saveElem != null) var saveElemStyle = (saveElem.style != null) ? saveElem.style : saveElem;
                var editTemplatePropertiesElem = document.getElementById('" + btnEditTemplateProperties.ClientID + @"');
                if (editTemplatePropertiesElem != null) var editTemplatePropertiesElemStyle = (editTemplatePropertiesElem.style != null) ? editTemplatePropertiesElem.style : editTemplatePropertiesElem;"
            );

            txtTemplate.Text = ValidationHelper.GetString(Request.Params["txtTemplate"], "");

            pnlActions.GroupingText = ResHelper.GetString("PageProperties.Template");

            mClone = ResHelper.GetString("PageProperties.Clone");
            mSave = ResHelper.GetString("PageProperties.Save");
            mInherit = ResHelper.GetString("PageProperties.Inherit");
            mEditTemplateProperties = ResHelper.GetString("PageProperties.EditTemplateProperties");

            imgClone.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/clone.png");
            imgInherit.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/inherit.png");
            imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/save.png");
            imgEditTemplateProperties.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/edit.png");
            imgClone.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/clonedisabled.png");
            imgInherit.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/inheritdisabled.png");
            imgSave.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/savedisabled.png");
            imgEditTemplateProperties.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/editdisabled.png");
        }
        else
        {
            pnlActions.Visible = false;
        }

        if (!RequestHelper.IsPostBack())
        {
            ReloadData();

            // Modal dialog for save
            btnSaveOnClickScript = "modalDialog('" + ResolveUrl(PORTALENGINE_UI_LAYOUTPATH + "SaveNewPageTemplate.aspx") + "?templateId=' + GetCurrentTemplateId() + '&siteid=" + siteid + "', 'Save new template', 470, 360);return false;";

            if (node != null)
            {
                if (node.NodeAliasPath != "/")
                {
                    inheritElem.Value = node.NodeInheritPageLevels;
                    inheritElem.TreePath = TreePathUtils.GetParentPath("/" + node.DocumentNamePath);
                }
                else
                {
                    pnlInherits.Visible = false;
                }
            }
        }
        else if (hasDesign)
        {
            initScript =
                "document.getElementById('SelectedTemplateId').value = " + ValidationHelper.GetInteger(Request.Params["SelectedTemplateId"], 0) + "; \n " +
                "document.getElementById('InheritedTemplateId').value = " + ValidationHelper.GetInteger(Request.Params["InheritedTemplateId"], 0) + "; \n " +
                "document.getElementById('Saved').value = " + ValidationHelper.GetBoolean(Request.Params["Saved"], false).ToString().ToLower() + "; \n" +
                "document.getElementById('TemplateDisplayName').value = '" + ValidationHelper.GetString(Request.Params["TemplateDisplayName"], "") + "'; \n " +
                "document.getElementById('TemplateDescription').value = '" + ValidationHelper.GetString(Request.Params["TemplateDescription"], "") + "'; \n " +
                "document.getElementById('TemplateCategory').value = '" + ValidationHelper.GetString(Request.Params["TemplateCategory"], "") + "'; \n " +
                "document.getElementById('isPortal').value = " + ValidationHelper.GetBoolean(Request.Params["isPortal"], false).ToString().ToLower() + "; \n " +
                "document.getElementById('isReusable').value = " + ValidationHelper.GetBoolean(Request.Params["isReusable"], false).ToString().ToLower() + "; \n " +
                "document.getElementById('isAdHoc').value = " + ValidationHelper.GetBoolean(Request.Params["isAdHoc"], false).ToString().ToLower() + "; \n ";

            string textTemplate = ValidationHelper.GetString(Request.Params["txtTemplate"], "");
            if (textTemplate == "")
            {
                textTemplate = ValidationHelper.GetString(Request.Params["TextTemplate"], "");
            }
            initScript += "document.getElementById('TextTemplate').value = " + ScriptHelper.GetString(textTemplate) + "; \n ";

            ltlInitScript.Text = ScriptHelper.GetScript(initScript);
            ltlScript.Text += ScriptHelper.GetScript("ShowButtons(document.getElementById('isPortal').value, document.getElementById('isReusable').value, document.getElementById('isAdHoc').value); \n");

            ltlScript.Text += ScriptHelper.GetScript("if (document.getElementById('SelectedTemplateId').value == 0) { if (inheritElemStyle != null) inheritElemStyle.display = 'none'; if (editTemplatePropertiesElemStyle != null) editTemplatePropertiesElemStyle.display = 'none'; }");
            txtTemplate.Text = textTemplate;

            btnSaveOnClickScript = "modalDialog('" + ResolveUrl(PORTALENGINE_UI_LAYOUTPATH + "SaveNewPageTemplate.aspx") + "?templateId=' + GetCurrentTemplateId() + '&siteid=" + siteid + "', 'Save new template', 470, 360);return false;";
        }

        // Javascript function for updating template name 
        string updateTemplateName = ScriptHelper.GetScript(@"function SetTemplateName(templateName) {
        var txtTemplate = document.getElementById('" + txtTemplate.ClientID + "'); txtTemplate.value = templateName;}");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SetTemplateName", updateTemplateName);

    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        btnSave.Attributes.Add("onclick", btnSaveOnClickScript);

        if (hasModifyPermission)
        {
            ScriptHelper.RegisterShortcuts(this);
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SaveDocument", ScriptHelper.GetScript("function SaveDocument() { " + ClientScript.GetPostBackEventReference(lnkSaveDoc, null) + " } \n"));
        }

        if (node != null)
        {
            siteid = node.NodeSiteID;

            // If node is root
            if (node.NodeClassName.Equals("CMS.Root", StringComparison.InvariantCultureIgnoreCase))
            {
                mInherit = ResHelper.GetString("PageProperties.Clear");
                imgInherit.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/clear.png");
                imgInherit.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Template/cleardisabled.png");
            }
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Disables form editing.
    /// </summary>
    private void DisableFormEditing()
    {
        hasModifyPermission = false;

        //pnlSoftTemplate.Enabled = false;
        txtTemplate.Enabled = false;
        btnSelect.Enabled = false;
        btnInherit.Enabled = false;
        btnClone.Enabled = false;
        pnlInherits.Visible = false;
        btnSave.Enabled = true;
        btnEditTemplateProperties.Enabled = true;

        // Disable 'save button'
        lnkSaveDoc.Enabled = false;
        lnkSaveDoc.CssClass = "MenuItemEditDisabled";
        
    }


    private void ReloadData()
    {
        if (node != null)
        {
            // Check read permissions
            if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            // Check modify permissions
            else if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
            {
                DisableFormEditing();

                lblInfo.Visible = true;
                lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
            }

            if (hasDesign)
            {
                // Show template name by template id
                if (node.GetValue("DocumentPageTemplateID") != null)
                {
                    PageTemplateInfo pt = PageTemplateInfoProvider.GetPageTemplateInfo(Convert.ToInt32(node.GetValue("DocumentPageTemplateID")));
                    if (pt != null)
                    {
                        ltlScript.Text += ScriptHelper.GetScript("SelectTemplate(" + pt.PageTemplateId + ", " + ScriptHelper.GetString(pt.DisplayName) + ", " + pt.IsPortal.ToString().ToLower() + ", " + pt.IsReusable.ToString().ToLower() + "); RememberTemplate(" + ScriptHelper.GetString(pt.DisplayName) + ");");
                    }
                    else
                    {
                        ltlScript.Text += ScriptHelper.GetScript("NoTemplateSelected();");
                    }
                    btnInherit.Visible = true;
                }
                else
                {
                    btnInherit_Click(null, null);
                }
            }
        }
    }

    #endregion


    #region "Button handling"

    protected void btnInherit_Click(object sender, EventArgs e)
    {
        int pageTemplateId = 0;
        if ((node != null) && (node.NodeParentID > 0))
        {
            // Get inherited page template
            object currentPageTemplateId = node.GetValue("DocumentPageTemplateID");
            node.SetValue("DocumentPageTemplateID", DBNull.Value);
            node.LoadInheritedValues(new string[] { "DocumentPageTemplateID" }, false);
            pageTemplateId = ValidationHelper.GetInteger(node.GetValue("DocumentPageTemplateID"), 0);
            node.SetValue("DocumentPageTemplateID", currentPageTemplateId);
        }

        if (pageTemplateId > 0)
        {
            PageTemplateInfo pt = PageTemplateInfoProvider.GetPageTemplateInfo(pageTemplateId);
            if (pt != null)
            {
                txtTemplate.Text = pt.DisplayName + " (inherited)";
                ltlScript.Text += ScriptHelper.GetScript("pressedInherit(" + pageTemplateId + "); ShowButtons(" + pt.IsPortal.ToString().ToLower() + ", " + pt.IsReusable.ToString().ToLower() + ");");
                btnSaveOnClickScript = "modalDialog('" + ResolveUrl(PORTALENGINE_UI_LAYOUTPATH + "SaveNewPageTemplate.aspx") + "?templateid=' + document.getElementById('InheritedTemplateId').value + '&siteid=" + siteid + "', 'Save new template', 470, 360);return false;";
            }
        }
        else
        {
            txtTemplate.Text = "";
            ltlScript.Text += ScriptHelper.GetScript("pressedInherit(" + pageTemplateId + "); ShowButtons(false, false); ");
        }

        if (sender != null)
        {
            // Log the synchronization
            DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, tree);
        }
    }


    protected void btnClone_Click(object sender, EventArgs e)
    {
        int pageTemplateId = ValidationHelper.GetInteger(Request.Params["SelectedTemplateId"], 0);
        if (pageTemplateId == 0)
        {
            pageTemplateId = ValidationHelper.GetInteger(Request.Params["InheritedTemplateId"], 0);
        }

        if (pageTemplateId > 0)
        {
            PageTemplateInfo pt = PageTemplateInfoProvider.GetPageTemplateInfo(pageTemplateId);
            if (pt != null)
            {
                // Clone the info
                string docName = node.DocumentName;
                if (docName == "")
                {
                    docName = "/";
                }

                string displayName = "Ad-hoc: " + docName;

                PageTemplateInfo newInfo = PageTemplateInfoProvider.CloneTemplateAsAdHoc(pt, displayName, CMSContext.CurrentSite.SiteID);

                newInfo.Description = String.Format(ResHelper.GetString("PageTemplate.AdHocDescription"), node.DocumentNamePath);
                PageTemplateInfoProvider.SetPageTemplateInfo(newInfo);

                ltlScript.Text += ScriptHelper.GetScript("pressedClone(" + newInfo.PageTemplateId + "); ShowButtons(" + pt.IsPortal.ToString().ToLower() + ", " + pt.IsReusable.ToString().ToLower() + ", true);");
                btnSaveOnClickScript = "modalDialog('" + ResolveUrl(PORTALENGINE_UI_LAYOUTPATH + "SaveNewPageTemplate.aspx") + "?templateid=' + document.getElementById('SelectedTemplateId').value + '&siteid=" + siteid + "', 'Save new template', 470, 360);return false;";
                txtTemplate.Text = newInfo.DisplayName;

                cloneId = newInfo.PageTemplateId;
            }
            btnSave.Visible = true;

            lnkSave_Click(sender, e);
        }
    }


    protected void lnkSave_Click(object sender, EventArgs e)
    {
        if (node != null)
        {
            if (hasDesign)
            {
                // Update the data
                int selectedPageTemplateId = ValidationHelper.GetInteger(Request.Params["SelectedTemplateId"], 0);

                // Save just created ad-hoc template
                if (cloneId > 0)
                {
                    selectedPageTemplateId = cloneId;
                }

                if (selectedPageTemplateId != 0)
                {
                    ltlScript.Text += ScriptHelper.GetScript("document.getElementById('SelectedTemplateId').value = " + selectedPageTemplateId);
                    if ((txtTemplate.Text.Length > 5) && (txtTemplate.Text.ToLower().Substring(0, 6) == "ad-hoc"))
                    {
                        // Ad-hoc page is used, set ad-hoc page as DocumentPageTemplateID
                        node.SetValue("DocumentPageTemplateID", selectedPageTemplateId);

                        PageTemplateInfo pt = PageTemplateInfoProvider.GetPageTemplateInfo(selectedPageTemplateId);
                        if (pt != null)
                        {
                            ltlScript.Text += ScriptHelper.GetScript("pressedClone(" + selectedPageTemplateId + ",null); ShowButtons(" + pt.IsPortal.ToString().ToLower() + ", " + pt.IsReusable.ToString().ToLower() + ", true)");
                        }
                    }
                    else
                    {
                        // Template was selected, set SelectedTemplateId as DocumentPageTemplateID
                        PageTemplateInfo pt = PageTemplateInfoProvider.GetPageTemplateInfo(selectedPageTemplateId);
                        if (pt != null)
                        {
                            node.SetValue("DocumentPageTemplateID", ValidationHelper.GetInteger(Request.Params["SelectedTemplateId"], 0));
                            ltlScript.Text += ScriptHelper.GetScript("SelectTemplate(" + selectedPageTemplateId + ", null, " + pt.IsPortal.ToString().ToLower() + ", " + pt.IsReusable.ToString().ToLower() + ")");
                        }
                    }
                }
                else
                {
                    // Template is inherited, set null as DocumentPageTemplateID
                    node.SetValue("DocumentPageTemplateID", null);
                    int inheritedTemplateID = ValidationHelper.GetInteger(Request.Params["InheritedTemplateId"], 0);
                    if (inheritedTemplateID > 0)
                    {
                        PageTemplateInfo pt = PageTemplateInfoProvider.GetPageTemplateInfo(inheritedTemplateID);
                        if (pt != null)
                        {
                            ltlScript.Text += ScriptHelper.GetScript("pressedInherit(" + inheritedTemplateID + "); ShowButtons(" + pt.IsPortal.ToString().ToLower() + ", " + pt.IsReusable.ToString().ToLower() + ", false)");
                        }
                    }
                    else
                    {
                        node.SetValue("DocumentPageTemplateID", null);
                        ltlScript.Text += ScriptHelper.GetScript("pressedInherit(0); ShowButtons(false, false, false); ");
                    }
                }
            }

            node.SetValue("NodeInheritPageLevels", inheritElem.Value);
            node.Update();

            // Update search index for node
            if (node.PublishedVersionExists && SearchIndexInfoProvider.SearchEnabled)
            {
                SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
            }

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        // Log the synchronization
        DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, tree);
    }

    #endregion
}

