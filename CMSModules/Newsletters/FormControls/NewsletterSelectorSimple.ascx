<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsletterSelectorSimple.ascx.cs"
    Inherits="CMSModules_Newsletters_FormControls_NewsletterSelectorSimple" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usNewsletters" runat="server" IsLiveSite="false" ObjectType="Newsletter.Newsletter"
            SelectionMode="SingleDropDownList" AllowEmpty="false" ResourcePrefix="newsletterselect" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
