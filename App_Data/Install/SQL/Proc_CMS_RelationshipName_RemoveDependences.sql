-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_RelationshipName_RemoveDependences]
	@RelationshipNameID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
    
	DELETE FROM [CMS_RelationshipNameSite] WHERE RelationshipNameID = @RelationshipNameID;
	DELETE FROM [CMS_Relationship] WHERE RelationshipNameID = @RelationshipNameID;
	DELETE FROM [CMS_ObjectRelationship] WHERE RelationshipNameID = @RelationshipNameID;
    DELETE FROM [CMS_Relationship] WHERE RelationshipNameID = @RelationshipNameID;
	COMMIT TRANSACTION;
END
