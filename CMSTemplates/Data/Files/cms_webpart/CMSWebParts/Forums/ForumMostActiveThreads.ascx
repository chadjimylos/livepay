<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Forums/ForumMostActiveThreads.ascx.cs"
    Inherits="CMSWebParts_Forums_ForumMostActiveThreads" %>
<%@ Register TagPrefix="cms" Namespace="CMS.Forums" Assembly="CMS.Forums" %>
<cms:BasicRepeater runat="server" ID="repMostActiveThread" />
<cms:ForumPostsDataSource runat="server" ID="forumDataSource" />
