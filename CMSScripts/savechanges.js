﻿var frameset = false;
var changed = false;
var allowSubmit = false;
var oldSubmit = null;
var iframes = null;
var confirmChanges = true;

function AllowChanged(elementId)
{
    if (window.checkChangedFields == null)
    {
        return true;            
    }
    
    for (var i = 0; i < checkChangedFields.length; i++)
    {
        if (checkChangedFields[i] == elementId)
        {
            return true;
        }
    }
    return false;
}

function Changed()
{
    changed = true;
}

function NotChanged()
{
    changed = false;
}

function AllowSubmit()
{
    allowSubmit = true;
}

function UnloadPage(evt)
{
    if (DataChanged() && !allowSubmit && confirmChanges)
    {
        return confirmLeave;
    }
}

function CheckChanges()
{
    if (DataChanged())
    {
        if (confirm(confirmLeave))
        {
            changed = false;
            ResetEditorsIsDirty();
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }
}

function SubmitAction()
{
    if (DataChanged() && !allowSubmit && (!confirmChanges || confirm(confirmSave)))
    {
        changed = false;
        allowSubmit = true;
        document.getElementById('saveChanges').value = '1';
        return true;
    }
    else
    {
        changed = false;
        allowSubmit = true;
        document.getElementById('saveChanges').value = '0';
        return true;
    }
}

function SubmitPage()
{
    if (!DataChanged() || allowSubmit || SubmitAction())
    {
        if (oldSubmit != null)
        {
            oldSubmit();
        }
    }
}

function InitChanges()
{
    if (window.theForm != null)
    {
        oldSubmit = theForm.onsubmit;
        theForm.onsubmit = SubmitPage;
        
        for (i = 0; i < theForm.elements.length; i++)
        {
            var elem = theForm.elements[i];
            if (AllowChanged(elem.id))
            {
                var check = false;
                switch (elem.tagName)
                {
                    case 'INPUT':
                        check = (elem.type == 'text') || (elem.type == 'checkbox') || (elem.type == 'radio') || (elem.type == 'file');
                        break;
                        
                    case 'TEXTAREA':
                    case 'SELECT':
                        check = true;                    
                        break;
                }
                
                if (check)
                {
                    if (elem.onchange == undefined)
                    {
                        elem.onchange = Changed;
                    }
                    
                    //var fnOnChangeOld = (elem.onchange) ? elem.onchange : function () {};
                    //elem.onchange = function (){ Changed(); fnOnChangeOld(); };
                }
            }
        }
    }
}

function DataChanged()
{
    return changed || EditorsChanged();
}

function FullTrim(text)
{
    return text.replace(/\s+/g, "");
}

function EditorsChanged()
{
    try
    {
        if ((typeof(FCKeditorAPI) != 'undefined') && (FCKeditorAPI.__Instances != null))
        {
            for (var name in FCKeditorAPI.__Instances)
            {
                if (AllowChanged(name))
                {
                    var oEditor = FCKeditorAPI.__Instances[name];
                    if (oEditor.IsDirty())
                    {
                        var oldText = FullTrim(oEditor.StartupValue);
                        
                        var newText = FullTrim(oEditor.GetData());
                        if (oldText != newText)
                        {
                            //alert(oldText + "\n\n\n" + newText);
                            return true;
                        }
                    }
                }
            }
         }
    }
    catch (ex)
    {
    }
    return false;
}

function ResetEditorsIsDirty()
{
    try
    {
        if ((typeof(FCKeditorAPI) != 'undefined') && (FCKeditorAPI.__Instances != null))
        {
            for (var name in FCKeditorAPI.__Instances)
            {
                if (AllowChanged(name))
                {
                    var oEditor = FCKeditorAPI.__Instances[name];
                    if (oEditor.IsDirty())
                    {
                        oEditor.ResetIsDirty();
                    }
                }
            }
         }
    }
    catch (ex)
    {
    }
    return false;
}

window.onload = InitChanges;

if (typeof (Sys) != "undefined" && typeof (Sys.Application) != "undefined") {
    Sys.Application.notifyScriptLoaded();
}