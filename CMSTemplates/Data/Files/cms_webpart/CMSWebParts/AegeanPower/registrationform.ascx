<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/AegeanPower/registrationform.ascx.cs"
    Inherits="CMSWebParts_AegeanPower_registrationform" %>
<%@ Register Src="~/CMSFormControls/Inputs/SecurityCode.ascx" TagName="SecurityCode" TagPrefix="cms" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cms" %>

<style>
.registration-textbox{color:#43474a;font-size:12px;font-family:Tahoma}
.registration-email{color:#43474a;font-size:12px;font-family:Tahoma}
.ContentButton
{
    border: none;
    background: url('/App_Themes/AegeanPower/Images/submitbutton.png') no-repeat top left;
    width:102px;
    height:19px;
}
</style>
<asp:Panel ID="pnlForm" runat="server" DefaultButton="btnOK">

        <div class="CUDContentBG">
             <%-- Email --%>
             <div style="height:35px;vertical-align:middle">
                <div style="float:left;font-size:12px;width:115px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" />
                </div>
                <div style="float:left;">
                    <cms:ExtendedTextBox ID="txtEmail" runat="server" CssClass="registration-textbox" MaxLength="100" Width="262" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtEmail" Display="Dynamic" EnableViewState="false" />
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" 
                    ValidationExpression="^([a-zA-Z0-9_\-])+(\.([a-zA-Z0-9_\-])+)*@((\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\]))|((([a-zA-Z0-9])+(([\-])+([a-zA-Z0-9])+)*\.)+([a-zA-Z])+(([\-])+([a-zA-Z0-9])+)*))$" 
                    Display="Dynamic" ValidationGroup="pnlForm"/>
                </div>
            </div>
             <%-- FristName --%>
             <div style="height:35px;vertical-align:middle">
                <div style="float:left;font-size:12px;width:115px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" EnableViewState="false" />
                </div>
                <div style="float:left;">
                    <cms:ExtendedTextBox ID="txtFirstName" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="262" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtFirstName" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- LastName --%>
             <div style="height:35px;vertical-align:middle">
                <div style="float:left;font-size:12px;width:115px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" />
                </div>
                <div style="float:left;">
                    <cms:ExtendedTextBox ID="txtLastName" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="262" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtLastName" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- Password --%>
             <div style="height:35px;vertical-align:middle">
                <div style="float:left;font-size:12px;width:115px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword" EnableViewState="false" />
                </div>
                <div style="float:left;">
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="262" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtPassword" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- ConfirmPassword --%>
             <div style="height:35px;vertical-align:middle">
                <div style="float:left;font-size:12px;width:115px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword"
                    EnableViewState="false" />
                </div>
                <div style="float:left;">
                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="262" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtConfirmPassword" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- Phone --%>
<%--             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblPhone" runat="server" AssociatedControlID="txtPhone" />
                </div>
                <div style="float:left;">
                    <cms:ExtendedTextBox ID="txtPhone" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="234" />
                </div>
                <div class="Clear"></div>
             </div>--%>
             <%-- Question --%>
<%--             <div style="">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right;padding-top:12px">
                    <asp:Label ID="lblQuestion" runat="server" AssociatedControlID="ddlQuestion" /> 
                </div>
                <div style="float:left;padding-top:14px">
                    <asp:DropDownList ID="ddlQuestion" runat="server" width="234" CssClass="registration-textbox"></asp:DropDownList>
                </div>
                <div class="Clear"></div>
             </div>--%>
             <%-- Answer --%>
<%--             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblAnswer" runat="server" AssociatedControlID="txtAnswer" />
                </div>
                <div style="float:left;">
                    <asp:TextBox ID="txtAnswer" runat="server" CssClass="registration-textbox" width="234"/>
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                 <div style="display:none">
                    <asp:RequiredFieldValidator ID="rfvAnswer" ErrorMessage="��������" runat="server" ControlToValidate="txtAnswer" text="*" ValidationGroup="pnlForm"/>
                </div> 
             </div>--%>
             <%-- Captchas --%>
             <asp:PlaceHolder runat="server" ID="plcCaptcha">
           <div style="height:60px;vertical-align:middle">
                <div style="float:left;font-size:12px;font-weight:bold;width:115px;padding-right:10px;text-align:right;height:60px;"></div>
                <div style="float:left" >
                    <cms:CaptchaControl ID="scCaptcha" runat="server" BorderWidth="1px" BorderColor="#808185" BorderStyle="Solid"
	                    CaptchaBackgroundNoise="None"
	                    CaptchaLength="5" 
	                    CaptchaHeight="58"
	                    CaptchaWidth="266"
	                    Width="266"
	                    CaptchaLineNoise="Extreme" 
	                    CacheStrategy="HttpRuntime"
	                    CaptchaMaxTimeout="240" 
                    />
                </div>
                <div style="margin-left:20px; float:left; width:230px;">
                    <asp:Label ID="lblError" runat="server" ForeColor="red" EnableViewState="false" />
                    <asp:Label ID="lblText" runat="server" Visible="false" EnableViewState="false" />
                </div>
                <div class="Clear"></div>
            </div>

           <div style="height:24px;vertical-align:middle; margin-top:10px;">
                <div style="float:left;font-size:12px;width:115px;padding-right:10px;text-align:right;height:14px;"></div>
                <div style="float:left;font-size:12px;">
                    <asp:Label ID="lblCaptcha" runat="server" AssociatedControlID="scCaptcha" EnableViewState="false" />
                </div>
                <div class="Clear"></div>
            </div>

           <div style="height:24px;vertical-align:middle;">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:115px;padding-right:10px;text-align:right;height:14px;"></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;">
                <asp:TextBox ID="txtCaptcha" runat="server" style="font-size:12px;font-family:Tahoma" width="262px"/>
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="�������������� �� ������� ��� ����������� ���� �������� ������" runat="server" ControlToValidate="txtCaptcha" Visible="true" text="egfsdeggsdgsd" ValidationGroup="RegisterForm"/>--%>
                

             </div>
            </asp:PlaceHolder>
            <%--</div>--%>
             <%-- Terms --%>
            <%-- <div style="">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right;padding-top:12px">
                    <asp:Label ID="lblTerms" runat="server" AssociatedControlID="txtTerms" /> 
                </div>
                <div style="float:left;padding-top:14px">
                    <asp:TextBox ID="txtTerms" TextMode="MultiLine" Rows="3" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="420px"/>
                </div>
                <div class="Clear"></div>
            </div>--%>
             <%-- Check Terms --%>
             <%--<div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:6px">
                    <asp:CheckBox ID="chbAcceptTerms" runat="server" style="color:#43474a;font-size:11px" />
                </div>
                <div class="Clear"></div>
             </div>--%>
             <%-- Check News --%>
             <%--<div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px">
                    <asp:CheckBox ID="chbAcceptSendNews" runat="server" style="color:#43474a;font-size:11px" />
                </div>
                <div class="Clear"></div>
             </div>--%>
             <%-- Button --%>
             <%--<div style="padding-top:5px;padding-bottom:10px">--%>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:282px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;width:102px;height:19px;">
                    <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" ValidationGroup="pnlForm"
                    CssClass="ContentButton" EnableViewState="false" /> 
                   <%-- <asp:ImageButton id="btnOk" runat="server" ImageUrl="/App_Themes/LivePay/UserRegistration/BtnRegister.png"
                   EnableViewState="false" ValidationGroup="pnlForm"  />--%>
                </div>
                <div class="Clear"></div>
            </div>
             <%--<div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:115px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;font-size:10px;color:#094595">�� ����� �� ��� ��������� * ����� �����������
                </div>
                <div class="Clear"></div>
            </div>--%>
</asp:Panel>
