using System;
using System.Data;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using System.IO;

public partial class CMSWebParts_AegeanPower_languageselectionwithflags: CMSAbstractWebPart
{
    private string mLayoutSeparator = " ";
    private string imgFlagIcon = "";
    public string selectionClass = "";

    #region "Public properties"

    /// <summary>
    /// Gets or sets the display layout
    /// </summary>
    public string DisplayLayout
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("DisplayLayout"), "");
        }
        set
        {
            this.SetValue("DisplayLayout", value);
            if (value.ToLower() == "vertical")
            {
                // Vertical
                mLayoutSeparator = "<br />";
            }
            else
            {
                // Horizontal
                mLayoutSeparator = " ";
            }
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the link for current culture should be hidden
    /// </summary>
    public bool HideCurrentCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("HideCurrentCulture"), false);
        }
        set
        {
            this.SetValue("HideCurrentCulture", value);
        }
    }


    /// <summary>
    /// Gets or sets the value than indicates whether culture names are displayed
    /// </summary>
    public bool ShowCultureNames
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ShowCultureNames"), true);
        }
        set
        {
            this.SetValue("ShowCultureNames", value);
        }
    }


    /// <summary>
    /// Gets or sets the separator between items
    /// </summary>
    public string Separator
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Separator"), "");
        }
        set
        {
            this.SetValue("Separator", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            GeneralConnection genconn = ConnectionHelper.GetConnection();

            string culture = CMSContext.PreferredCultureCode;
            string strcultureCode = "";
            if (this.DisplayLayout.ToLower() == "vertical")
            {
                // Vertical 
                mLayoutSeparator = "<br />";
            }
            else
            {
                // Horizontal
                mLayoutSeparator = " ";
            }

            DataSet ds = null;

            DataTable DocumentCultures = new DataTable();
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(genconn.ConnectionString);
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("[proc_AegeanPower_GetDocumentCultures]", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@aliaspath", this.Request.QueryString["aliaspath"].ToString());
            System.Data.SqlClient.SqlDataAdapter ad = new System.Data.SqlClient.SqlDataAdapter(cmd);
            ad.Fill(DocumentCultures);
            cmd.Dispose();
            conn.Dispose();
            // Get the cultures
            if (this.CacheMinutes == 0)
            {
                ds = CultureInfoProvider.GetSiteCultures(CMSContext.CurrentSiteName);
            }
            else
            {
                string useCacheItemName = DataHelper.GetNotEmpty(this.CacheItemName, CMSContext.BaseCacheKey + "|" + Context.Request.Url.ToString() + "|" + this.ClientID);

                // Try to retrieve data from the cache
                if (!CacheHelper.Contains(useCacheItemName))
                {
                    ds = CultureInfoProvider.GetSiteCultures(CMSContext.CurrentSiteName);
                    if (!DataHelper.DataSourceIsEmpty(ds))
                    {
                        CacheHelper.Add(useCacheItemName, ds, null, DateTime.Now.AddMinutes(this.CacheMinutes), System.Web.Caching.Cache.NoSlidingExpiration);
                    }
                }
                else
                {
                    ds = (DataSet)CacheHelper.GetItem(useCacheItemName);
                }
            }

            if (!DataHelper.DataSourceIsEmpty(ds) && (ds.Tables[0].Rows.Count > 1))
            {
                // Build current URL
                string url = UrlHelper.CurrentURL;
                url = UrlHelper.RemoveParameterFromUrl(url, "bizform");
                url = UrlHelper.RemoveParameterFromUrl(url, "lang");
                url = UrlHelper.RemoveParameterFromUrl(url, "aliaspath");

                // Render the cultures
                ltlHyperlinks.Text = "";

                int count = 0;
                int rows = ds.Tables[0].Rows.Count;
                
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string cultureCode = dr["CultureCode"].ToString();
                    string cultureShortName = dr["CultureShortName"].ToString();
                    strcultureCode = cultureCode;
                    Int32 cultureCount=DocumentCultures.Select(string.Format("DocumentCulture='{0}'", cultureCode)).Length;

                    if (!((this.HideCurrentCulture) && (String.Compare(CMSContext.CurrentDocument.DocumentCulture, cultureCode, true) == 0)))
                    {
                        if (culture.ToLower() != cultureCode.ToLower())
                        {
                            // If exists flag icon file in actual app theme image folder, use it
                            if (Directory.Exists(this.Server.MapPath("~/App_Themes/AegeanPower/Images/Flags/")))
                            {
                                string[] mArr = Directory.GetFiles(this.Server.MapPath("~/App_Themes/AegeanPower/Images/Flags/"), cultureCode + ".*");
                                if (mArr.GetLength(0) > 0)
                                {
                                    imgFlagIcon = ResolveUrl("~/App_Themes/AegeanPower/Images/Flags/" +
                                    System.IO.Path.GetFileName(mArr[0]));
                                }
                            }
                            // Else use default flag icon
                            else
                            {
                                imgFlagIcon = ResolveUrl("~/App_Themes/Default/Images/CMSWebParts/Flags/" + cultureCode + ".gif");
                            }

                            if (cultureCount == 0)
                            {
                                ltlHyperlinks.Text += "<a href=\"~/SpecialPages/Only-in-Greek.aspx\">" + "<img border='0' src=\"" + imgFlagIcon + "\" alt=\"" + HTMLHelper.HTMLEncode(cultureShortName) + "\" />";
                            }
                            else
                            {
                                if (cultureCode == "en-US")
                                {
                                    ltlHyperlinks.Text += "<a title=\"English\" href=\"" + UrlHelper.AddUrlParameter(url, "lang", cultureCode) + "\">" + "<img border='0' src=\"" + imgFlagIcon + "\" alt=\"" + HTMLHelper.HTMLEncode(cultureShortName) + "\" />";
                                }
                                else
                                {
                                    ltlHyperlinks.Text += "<a title=\"Ελληνικά\" href=\"" + UrlHelper.AddUrlParameter(url, "lang", cultureCode) + "\">" + "<img border='0' src=\"" + imgFlagIcon + "\" alt=\"" + HTMLHelper.HTMLEncode(cultureShortName) + "\" />";
                                }
                            }

                            

                            // Set surrounding div css class


                            selectionClass = "ImageEN";

                            count++;

                            // Check last item
                            if (count == rows)
                            {
                                ltlHyperlinks.Text += "</a>";
                            }
                            else
                            {
                                ltlHyperlinks.Text += "</a>" + this.Separator + mLayoutSeparator;
                            }
                        }
                    }
                }
            }

            if ((selectionClass == null) || (selectionClass == ""))
            {
                ltrDivOpen.Text = "<div>";
            }
            else
            {
                ltrDivOpen.Text = "<div class=\"" + selectionClass + "\">";
            }
            ltrDivClose.Text = "</div>";

            // Check if RTL hack must be applied
            if (CultureHelper.IsPreferredCultureRTL())
            {
                ltrDivOpen.Text += "<span style=\"visibility:hidden;\">a</span>";
            }
            
        }
            


    }


    /// <summary>
    /// Clears the cached items
    /// </summary>
    public override void ClearCache()
    {
        string useCacheItemName = DataHelper.GetNotEmpty(this.CacheItemName, CMSContext.BaseCacheKey + "|" + Context.Request.Url.ToString() + "|" + this.ClientID);

        CacheHelper.ClearCache(useCacheItemName);
    }
}
