using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_Ecommerce_Tools_Products_Product_Edit_VolumeDiscount_List : CMSEcommercePage
{
    private string productID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get parameters from URL
        productID = ValidationHelper.GetString(Request.QueryString["productID"], "");

        // Set unigrid properties
        SetUnigridProperties();

        // Initialize the master page elements
        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        // Set URL of new volume discount editing page
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Product_Edit_VolumeDiscount_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Products/Product_Edit_VolumeDiscount_Edit.aspx?ProductID=" + productID;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_VolumeDiscount/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName.ToLower() == "edit")
        {
            UrlHelper.Redirect("Product_Edit_VolumeDiscount_Edit.aspx?productID=" + productID + "&volumeDiscountID=" + Convert.ToString(actionArgument));
        }
        else if (actionName.ToLower() == "delete")
        {
            // delete VolumeDiscountInfo object from database
            VolumeDiscountInfoProvider.DeleteVolumeDiscountInfo(Convert.ToInt32(actionArgument));
        }
    }


    /// <summary>
    /// Handles the UniGrid's OnExternalDataBound event.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="sourceName">Source name.</param>
    /// <param name="parameter">Parameter.</param>
    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "discountvalue":
                DataRowView row = (DataRowView)parameter;
                double value = ValidationHelper.GetDouble(row["VolumeDiscountValue"], 0);
                bool isFlat = ValidationHelper.GetBoolean(row["VolumeDiscountIsFlatValue"], false);
                // If value is relative, add "%" next to the value.
                if (isFlat)
                {
                    CurrencyInfo ci = CurrencyInfoProvider.GetMainCurrency();
                    return CurrencyInfoProvider.GetFormatedPrice(value, ci);
                }
                else
                {
                    return value.ToString() + "%";
                }
        }
        return parameter;
    }


    protected void SetUnigridProperties()
    {
        UniGrid.OnAction += new OnActionEventHandler(UniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.WhereCondition = "VolumeDiscountSKUID = " + productID;
        UniGrid.OrderBy = "VolumeDiscountMinCount ASC";
    }
}
