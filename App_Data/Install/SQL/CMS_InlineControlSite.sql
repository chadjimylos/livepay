CREATE TABLE [CMS_InlineControlSite] (
		[ControlID]     int NOT NULL,
		[SiteID]        int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_InlineControlSite]
	ADD
	CONSTRAINT [PK_CMS_InlineControlSite]
	PRIMARY KEY
	([ControlID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_InlineControlSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_InlineControl]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_InlineControlSite_ControlD_CMS_InlineControl]') IS NULL
BEGIN
		ALTER TABLE [CMS_InlineControlSite]
			ADD CONSTRAINT [FK_CMS_InlineControlSite_ControlD_CMS_InlineControl]
			FOREIGN KEY ([ControlID]) REFERENCES [CMS_InlineControl] ([ControlID])
END
IF OBJECT_ID(N'[CMS_InlineControlSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_InlineControlSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_InlineControlSite]
			ADD CONSTRAINT [FK_CMS_InlineControlSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
