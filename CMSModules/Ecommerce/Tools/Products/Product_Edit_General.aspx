<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_General.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Products_Product_Edit_General"
    Theme="Default" Title="Product - edit - general" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/ProductEdit.ascx" TagName="NewProduct"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:NewProduct ID="ctrlProduct" runat="server" />
</asp:Content>
