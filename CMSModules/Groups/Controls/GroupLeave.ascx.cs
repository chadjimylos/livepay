using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.Community;
using CMS.CMSHelper;

public partial class CMSModules_Groups_Controls_GroupLeave : CMSAdminControl
{
    #region "Variables"

    private string mLeaveText;
    private string mSuccessfulLeaveText;
    private string mUnSuccessfulLeaveText;
    private GroupInfo mGroup = null;
    private bool mIsOnModalPage = true;

    #endregion


    #region "Private properties"
    
    /// <summary>
    /// Returns group name of current group
    /// </summary>
    private string GroupName
    {
        get
        {
            if (this.Group != null)
            {
                return " " + this.Group.GroupDisplayName;
            }

            return "";
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the text which should be displayed on join dialog
    /// </summary>
    public string LeaveText
    {
        get
        {
        	 return DataHelper.GetNotEmpty(mLeaveText, ResHelper.GetString("Community.Group.Leave") + HTMLHelper.HTMLEncode(this.GroupName) + "?"); 
        }
        set
        {
        	 mLeaveText = value; 
        }
    }


    /// <summary>
    /// Gets or sets the text which should be displayed on join dialog after successful join action
    /// </summary>
    public string SuccessfulLeaveText
    {
        get
        {
        	 return DataHelper.GetNotEmpty(mSuccessfulLeaveText, ResHelper.GetString("Community.Group.SuccessfulLeave").Replace("##GroupName##", HTMLHelper.HTMLEncode(this.GroupName))); 
        }
        set
        {
        	 mSuccessfulLeaveText = value; 
        }
    }


    /// <summary>
    /// Gets or sets the text which should be displayed on join dialog if join actin was unsuccessful
    /// </summary>
    public string UnSuccessfulLeaveText
    {
        get
        {
        	 return DataHelper.GetNotEmpty(mUnSuccessfulLeaveText, ResHelper.GetString("Community.Group.UnSuccessfulLeave")); 
        }
        set
        {
        	 mUnSuccessfulLeaveText = value; 
        }
    }


    /// <summary>
    /// Gets or sets the group info object for destination group
    /// </summary>
    public GroupInfo Group
    {
        get
        {
            return mGroup;
        }
        set
        {
            mGroup = value;
        }
    }

    
    /// <summary>
    /// Specifies if control is placed on modal page or not.
    /// </summary>
    public bool IsOnModalPage
    {
        get
        {
        	 return mIsOnModalPage; 
        }
        set
        {
        	 mIsOnModalPage = value; 
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        btnLeave.Text = ResHelper.GetString("General.Yes");
        btnCancel.Text = ResHelper.GetString("General.No");

        // Set up js action if webpart is placed on modal page
        if (this.IsOnModalPage)
        {
            btnCancel.OnClientClick = "window.close()";
        }
        else
        {
            // Get return url
            string returnUrl = QueryHelper.GetString("returnurl", "") ;
            if (!string.IsNullOrEmpty(returnUrl))
            {
                // Redirect
                UrlHelper.Redirect(returnUrl);
            }
        }
        lblInfo.Text = this.LeaveText;
    }


    /// <summary>
    /// Join handler
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">EventArgs</param>
    protected void btnLeave_Click(object sender, EventArgs e)
    {        
        btnLeave.Visible = false;
        // Set up js action if webpart is placed on modal page
        if (this.IsOnModalPage)
        {
            btnCancel.Text = ResHelper.GetString("General.Close");
            btnCancel.OnClientClick = "if (wopener != null) {wopener.ReloadPage();} window.close();";
        }
        else
        {
            btnCancel.Text = ResHelper.GetString("General.Ok");
            btnCancel.Click += new EventHandler(btnCancel_Click);            
        }

        if (this.Group == null)
        {
            return;
        }

        // Get group member info        
        GroupMemberInfo gmi = GroupMemberInfoProvider.GetGroupMemberInfo(CMSContext.CurrentUser.UserID, this.Group.GroupID);
        if (gmi != null)
        {
            GroupMemberInfoProvider.DeleteGroupMemberInfo(gmi);
            if (this.Group.GroupSendJoinLeaveNotification)
            {
                GroupMemberInfoProvider.SendNotificationMail("Groups.MemberLeave", CMSContext.CurrentSiteName, gmi, true);
            }
            this.lblInfo.Text = this.SuccessfulLeaveText;
            return;
        }
        

        this.lblInfo.Text = this.SuccessfulLeaveText;
    }


    /// <summary>
    /// Cancel click.
    /// </summary>    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect(UrlHelper.CurrentURL);
    }
}