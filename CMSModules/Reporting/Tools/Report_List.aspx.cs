using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Generic;

using CMS.GlobalHelper;
using CMS.Reporting;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_Report_List : CMSReportingPage
{
    // Report macros
    const string REP_TABLE_MACRO = "%%control:ReportTable?";
    const string REP_GRAPH_MACRO = "%%control:ReportGraph?";
    const string REP_VALUE_MACRO = "%%control:ReportValue?";

    int categoryId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterExportScript();

        categoryId = ValidationHelper.GetInteger(Request.QueryString["categoryId"], 0);

        UniGrid.WhereCondition = "[ReportCategoryId] = " + categoryId;
        UniGrid.OnAction += uniGrid_OnAction;
        UniGrid.OnExternalDataBound += UniGrid_OnExternalDataBound;
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes Master Page.
    /// </summary>
    protected void InitializeMasterPage()
    {
        Title = "Report list";

        // Set actions
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Report_List.NewItemCaption");
        actions[0, 3] = ResolveUrl("Report_New.aspx?categoryid=" + categoryId);
        actions[0, 5] = GetImageUrl("Objects/Reporting_Report/add.png");

        CurrentMaster.HeaderActions.Actions = actions;
    }


    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "export":
                ImageButton img = (ImageButton)sender;
                if (CMSContext.CurrentUser.IsGlobalAdministrator)
                {
                    int reportId = ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["ReportID"], 0);
                    img.OnClientClick = "OpenExportObject('reporting.report', " + reportId + ");return false;";
                }
                else
                {
                    img.Visible = false;
                }
                break;
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        switch (actionName)
        {
            case "edit":
                UrlHelper.Redirect("Report_Edit.aspx?reportId=" + Convert.ToString(actionArgument) + "&categoryId=" + categoryId);
                break;

            case "delete":
                // Check 'Modify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
                {
                    RedirectToAccessDenied("cms.reporting", "Modify");
                }

                // delete ReportInfo object from database
                ReportInfoProvider.DeleteReportInfo(Convert.ToInt32(actionArgument));
                break;

            case "clone":
                Clone(Convert.ToInt32(actionArgument));
                break;
        }
    }


    /// <summary>
    /// Clones the given report (including attachment files).
    /// </summary>
    /// <param name="reportId">Report id</param>
    protected void Clone(int reportId)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }

        // Try to get report info
        ReportInfo oldri = ReportInfoProvider.GetReportInfo(reportId);
        if (oldri == null)
        {
            return;
        }

        DataSet graph_ds = ReportGraphInfoProvider.GetGraphs(reportId);
        DataSet table_ds = ReportTableInfoProvider.GetTables(reportId);
        DataSet value_ds = ReportValueInfoProvider.GetValues(reportId);

        // Duplicate report info object
        ReportInfo ri = new ReportInfo(oldri, false);
        ri.ReportID = 0;
        ri.ReportGUID = Guid.NewGuid();

        // Duplicate report info
        string reportName = ri.ReportName;
        string oldReportName = ri.ReportName;

        string reportDispName = ri.ReportDisplayName;

        while (ReportInfoProvider.GetReportInfo(reportName) != null)
        {
            reportName = Increment(reportName, "_", "", 100);
            reportDispName = Increment(reportDispName, "(", ")", 450);
        }

        ri.ReportName = reportName;
        ri.ReportDisplayName = reportDispName;

        ReportInfoProvider.SetReportInfo(ri);

        string name;

        // Duplicate graph data
        if (!DataHelper.DataSourceIsEmpty(graph_ds))
        {
            foreach (DataRow dr in graph_ds.Tables[0].Rows)
            {
                // Duplicate the graph
                ReportGraphInfo rgi = new ReportGraphInfo(dr);
                rgi.GraphID = 0;
                rgi.GraphGUID = Guid.NewGuid();
                rgi.GraphReportID = ri.ReportID;
                name = rgi.GraphName;

                while (ReportGraphInfoProvider.GetReportGraphInfo(name) != null)
                {
                    name = Increment(name, "_", "", 100);
                }
                ri.ReportLayout = ReplaceMacro(ri.ReportLayout, REP_GRAPH_MACRO, rgi.GraphName, name, oldReportName, reportName);
                rgi.GraphName = name;

                ReportGraphInfoProvider.SetReportGraphInfo(rgi);
            }
        }

        // Duplicate table data
        if (!DataHelper.DataSourceIsEmpty(table_ds))
        {
            foreach (DataRow dr in table_ds.Tables[0].Rows)
            {
                // Duplicate the table
                ReportTableInfo rti = new ReportTableInfo(dr);
                rti.TableID = 0;
                rti.TableGUID = Guid.NewGuid();
                rti.TableReportID = ri.ReportID;
                name = rti.TableName;

                while (ReportTableInfoProvider.GetReportTableInfo(name) != null)
                {
                    name = Increment(name, "_", "", 100);
                }
                ri.ReportLayout = ReplaceMacro(ri.ReportLayout, REP_TABLE_MACRO, rti.TableName, name, oldReportName, reportName);
                rti.TableName = name;

                ReportTableInfoProvider.SetReportTableInfo(rti);
            }
        }

        // Duplicate value data
        if (!DataHelper.DataSourceIsEmpty(value_ds))
        {
            foreach (DataRow dr in value_ds.Tables[0].Rows)
            {
                // Duplicate the value
                ReportValueInfo rvi = new ReportValueInfo(dr);
                rvi.ValueID = 0;
                rvi.ValueGUID = Guid.NewGuid();
                rvi.ValueReportID = ri.ReportID;
                name = rvi.ValueName;

                while (ReportValueInfoProvider.GetReportValueInfo(name) != null)
                {
                    name = Increment(name, "_", "", 100);
                }
                ri.ReportLayout = ReplaceMacro(ri.ReportLayout, REP_VALUE_MACRO, rvi.ValueName, name, oldReportName, reportName);
                rvi.ValueName = name;

                ReportValueInfoProvider.SetReportValueInfo(rvi);
            }
        }

        List<Guid> convTable = new List<Guid>();
        try
        {
            MetaFileInfoProvider.CopyMetaFiles(reportId, ri.ReportID, ReportingObjectType.REPORT, MetaFileInfoProvider.OBJECT_CATEGORY_LAYOUT, convTable);
        }
        catch (Exception e)
        {
            lblError.Visible = true;
            lblError.Text = e.Message;
            ReportInfoProvider.DeleteReportInfo(ri);
            return;
        }

        for (int i = 0; i < convTable.Count; i += 2)
        {
            Guid oldGuid = convTable[i];
            Guid newGuid = convTable[i + 1];
            ri.ReportLayout = ri.ReportLayout.Replace(oldGuid.ToString(), newGuid.ToString());
        }

        ReportInfoProvider.SetReportInfo(ri);
    }


    /// <summary>
    /// Increment counter at the end of string.
    /// </summary>
    /// <param name="s">String</param>
    /// <param name="lpar">Left parathenses</param>
    /// <param name="rpar">Right parathenses</param>
    /// <param name="lenghtLimit">Maximum length of output string</param>
    protected string Increment(string s, string lpar, string rpar, int lenghtLimit)
    {
        int i = 1;
        s = s.Trim();
        if ((rpar == String.Empty) || s.EndsWith(rpar))
        {
            int leftpar = s.LastIndexOf(lpar);
            if (lpar == rpar)
            {
                leftpar = s.LastIndexOf(lpar, leftpar - 1);
            }

            if (leftpar >= 0)
            {
                i = ValidationHelper.GetSafeInteger(s.Substring(leftpar + lpar.Length, s.Length - leftpar - lpar.Length - rpar.Length), 0);
                if (i > 0) // Remove parathenses only if parentheses found
                {
                    s = s.Remove(leftpar);
                }
                i++;
            }
        }

        string tail = lpar + i + rpar;
        if (s.Length + tail.Length > lenghtLimit)
        {
            s = s.Substring(0, s.Length - (s.Length + tail.Length - lenghtLimit));
        }
        s += tail;
        return s;
    }


    /// <summary>
    /// Replaces old macro with new macro in template string.
    /// </summary>
    /// <param name="template">Template</param>
    /// <param name="macro">Macro</param>
    /// <param name="oldValue">Old macro value</param>
    /// <param name="newValue">New macro value</param>
    /// <param name="oldReportName">Old report name</param>    
    /// <param name="newReportName">New report name</param>
    protected string ReplaceMacro(string template, string macro, string oldValue, string newValue, string oldReportName, string newReportName)
    {
        // old name, old macro style, backward compatible
        string oldOrigValue = macro + oldValue + "%%";
        // old macro name, but with full specification
        string oldFullValue = macro + oldReportName + "." + oldValue + "%%";

        newValue = macro + newReportName + "." + newValue + "%%";

        return template.Replace(oldOrigValue, newValue).Replace(oldFullValue, newValue);
    }
}
