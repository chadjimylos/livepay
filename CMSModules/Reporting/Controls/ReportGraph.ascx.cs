using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Reporting;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.CMSHelper;

public partial class CMSModules_Reporting_Controls_ReportGraph : AbstractReportControl
{
    private string mReportGraphName = "";

    /// <summary>
    /// Value name
    /// </summary>
    private string ReportGraphName
    {
        get
        {
            return mReportGraphName;
        }
        set
        {
            mReportGraphName = value;
        }
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData(bool forceLoad)
    {
        // ReportGraphName is set from AbstractReportControl parameter
        this.ReportGraphName = this.Parameter;


        // If saved report guid is empty ==> create "live" graph, else load saved graph
        if (this.SavedReportGuid == Guid.Empty)
        {
            // Get graph info object
            ReportGraphInfo rgi = ReportGraphInfoProvider.GetReportGraphInfo(this.ReportGraphName);
            if (rgi != null)
            {
                // Create graph image
                string parameters = HttpUtility.HtmlEncode(GetQueryStringParameters());
                if (String.IsNullOrEmpty(parameters))
                {
                    parameters = ";";
                }
                imgGraph.ImageUrl = ResolveUrl("~/CMSModules/Reporting/CMSPages/GetReportGraph.aspx") + "?graphId=" + rgi.GraphID.ToString() + "&parameters=" + parameters + "&UILang=" + System.Globalization.CultureInfo.CurrentUICulture.IetfLanguageTag;
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "[ReportGraph.ascx] Report graph '" + this.ReportGraphName + "' not found.";

                imgGraph.Visible = false;

                EventLogProvider ev = new EventLogProvider();
                ev.LogEvent("Report graph", "E", new Exception("Report graph '" + this.ReportGraphName + "' not found."));
            }
        }
        else
        {
            // Get report graph info object
            ReportGraphInfo rgi = ReportGraphInfoProvider.GetReportGraphInfo(this.ReportGraphName);
            if (rgi != null)
            {
                rgi.GraphTitle = ResHelper.LocalizeString(rgi.GraphTitle);
                rgi.GraphXAxisTitle = ResHelper.LocalizeString(rgi.GraphXAxisTitle);
                rgi.GraphYAxisTitle = ResHelper.LocalizeString(rgi.GraphYAxisTitle);

                this.QueryIsStoredProcedure = rgi.GraphQueryIsStoredProcedure;
                this.QueryText = ResolveMacros(rgi.GraphQuery);

                // Load data, generate image
                ReportGraph rg = new ReportGraph();
                // Save image
                SavedGraphInfo sgi = new SavedGraphInfo();

                try
                {
                    sgi.SavedGraphBinary = rg.CreateGraph(rgi, this.LoadData());
                }
                catch (Exception ex)
                {
                    rg.DataSource = null;
                    EventLogProvider ev = new EventLogProvider();
                    ev.LogEvent("Report graph", "E", ex);


                    byte[] invalidGraph = rg.CreateInvalidGraph(rgi);
                    sgi.SavedGraphBinary = invalidGraph;
                }

                sgi.SavedGraphGUID = this.SavedReportGuid;
                sgi.SavedGraphMimeType = "image/png";
                sgi.SavedGraphSavedReportID = this.SavedReportID;

                SavedGraphInfoProvider.SetSavedGraphInfo(sgi);

                // Create graph image
                imgGraph.ImageUrl = ResolveUrl("~/CMSModules/Reporting/CMSPages/GetReportGraph.aspx") + "?graphguid=" + SavedReportGuid.ToString();
            }
        }
    }


    /// <summary>
    /// Rerurn parameters to the querystring
    /// </summary>
    protected string GetQueryStringParameters()
    {
        string result = "";
        IFormatProvider culture = DateTimeHelper.DefaultIFormatProvider;

        if (this.ReportParameters != null)
        {
            // Build the results array
            foreach (DataColumn col in this.ReportParameters.Table.Columns)
            {

                if ((col.DataType.Name.ToLower() == "datetime") && ((ValidationHelper.GetDateTime(this.ReportParameters[col.ColumnName], DataHelper.DATETIME_NOT_SELECTED)) != DataHelper.DATETIME_NOT_SELECTED))
                {
                    result += col.ColumnName + ";" + ((DateTime)this.ReportParameters[col.ColumnName]).ToString(culture) + ";";
                }
                else
                {
                    result += col.ColumnName + ";" + Convert.ToString(this.ReportParameters[col.ColumnName]) + ";";
                }

            }
        }

        if ((CMSContext.CurrentPageInfo != null) && (CMSContext.CurrentPageInfo.NodeAliasPath != ""))
        {
            result += "AliasPathGraph;" + CMSContext.CurrentPageInfo.NodeAliasPath;
        }

        // Removes last semicolon (we can't use trimend because of situation when last parametr doesn't have value)
        if (result.EndsWith(";"))
        {
            result = result.Substring(0, result.Length - 1);
        }


        return result;
    }
}
