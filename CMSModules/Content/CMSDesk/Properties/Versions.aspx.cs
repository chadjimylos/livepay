using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_CMSDesk_Properties_Versions : CMSPropertiesPage
{
    #region "Private variables"

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;

    #endregion


    #region "Protected variables"

    protected TreeNode node = null;
    protected TreeProvider tree = null;
    protected WorkflowInfo wi = null;
    protected int nodeId = 0;
    protected bool mCanDestroy = false;
    protected bool mCanModify = false;
    protected bool mCanApprove = false;
    protected bool mCheckedOutByAnotherUser = false;
    protected bool mCanCheckIn = false;

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Versions"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Versions");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);

        UIContext.PropertyTab = PropertyTabEnum.Versions;

        // Current Node ID
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        ltlScript.Text += ScriptHelper.GetScript(
            "var varConfirmDestroy='" + ResHelper.GetString("VersionProperties.ConfirmDestroy") + "'; \n"
        );

        gridHistory.OnAction += gridHistory_OnAction;

        // Get the node
        tree = new TreeProvider(CMSContext.CurrentUser);
        node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

        if (node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }

            // Prepare the query parameters
            object[,] parameters = new object[1, 3];
            parameters[0, 0] = "@DocumentID";
            parameters[0, 1] = node.DocumentID;

            gridHistory.QueryParameters = parameters;
            gridHistory.OnExternalDataBound += gridHistory_OnExternalDataBound;

            ReloadData();
        }
        else
        {
            // Hide all if no node is specified
            gridHistory.GridName = string.Empty;
            pnlContent.Visible = false;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        plcVersions.Visible = !gridHistory.IsEmpty;
        if (gridHistory.IsEmpty)
        {
            lblInfo.Text = ResHelper.GetString("worfklowproperties.documenthasnohistory");
        }
        lblInfo.Visible = (lblInfo.Text != "");
    }


    /// <summary>
    /// Converts given server time to user time zone.
    /// </summary>
    /// <param name="time">Server time</param>
    private string ConvertToUserTimeZone(object time, bool tooltip)
    {
        string result = null;

        if (!String.IsNullOrEmpty(time.ToString()))
        {
            if (currentUserInfo == null)
            {
                currentUserInfo = CMSContext.CurrentUser;
            }
            if (currentSiteInfo == null)
            {
                currentSiteInfo = CMSContext.CurrentSite;
            }
            result = TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(time, DateTimeHelper.ZERO_TIME), currentUserInfo, currentSiteInfo, out usedTimeZone);

            DateTime dt = ValidationHelper.GetDateTime(time, DateTimeHelper.ZERO_TIME);
            if (!tooltip)
            {
                return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(dt, currentUserInfo, currentSiteInfo, out usedTimeZone);
            }
            else
            {
                if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                {
                    TimeZoneHelper.GetCurrentTimeZoneDateTimeString(dt, currentUserInfo, currentSiteInfo, out usedTimeZone);
                }
                return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
            }
        }
        return result;
    }
    protected object gridHistory_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "rollback":
                ImageButton imgRollback = ((ImageButton)sender);
                if (!mCanApprove || !mCanModify || (mCheckedOutByAnotherUser && !mCanCheckIn))
                {
                    imgRollback.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/undodisabled.png");
                    imgRollback.Enabled = false;
                }
                break;
            case "allowdestroy":
                ImageButton imgDestroy = ((ImageButton)sender);
                if (!mCanDestroy || (mCheckedOutByAnotherUser && !mCanCheckIn))
                {
                    imgDestroy.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/deletedisabled.png");
                    imgDestroy.Enabled = false;
                }
                break;

            case "tobepublished":
                if (ValidationHelper.GetBoolean(parameter, false))
                {
                    return "<span class=\"StatusEnabled\">" + ResHelper.GetString("general.yes") + "</span>";
                }
                else
                {
                    return "";
                }
            case "publishfrom":
            case "publishto":
            case "waspublishedfrom":
            case "waspublishedto":

            case "publishfromtooltip":
            case "publishtotooltip":
            case "waspublishedfromtooltip":
            case "waspublishedtotooltip":
                bool tooltip = sourceName.EndsWith("tooltip");
                return ConvertToUserTimeZone(parameter, tooltip);
            case "modifiedwhenby":
            case "modifiedwhenbytooltip":
                DataRowView data = (DataRowView)parameter;
                object modifiedwhen = data["ModifiedWhen"];
                if (sourceName == "modifiedwhenbytooltip")
                {
                    return ConvertToUserTimeZone(modifiedwhen, true);
                }
                else
                {
                    string userName = ValidationHelper.GetString(data["UserName"], String.Empty);
                    string fullName = ValidationHelper.GetString(data["FullName"], String.Empty);

                    return ConvertToUserTimeZone(modifiedwhen, false) + "<br />" + HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(userName, fullName));
                }
        }

        return parameter;
    }


    void gridHistory_OnAction(string actionName, object actionArgument)
    {
        int versionHistoryId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName.ToLower())
        {
            case "rollback":
                if (versionHistoryId > 0)
                {
                    int checkedOutByUserId = ValidationHelper.GetInteger(node.GetValue("DocumentCheckedOutByUserId"), 0);
                    if (checkedOutByUserId > 0)
                    {
                        // Document is checked out
                        lblError.Text += ResHelper.GetString("VersionProperties.CannotRollbackCheckedOut");
                    }
                    else
                    {
                        // Check permissions
                        if (!mCanApprove || !mCanModify)
                        {
                            lblError.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                            return;
                        }

                        // Rollback version
                        VersionManager verMan = new VersionManager(tree);

                        try
                        {
                            verMan.RollbackVersion(versionHistoryId);

                            // Refresh content tree (for the case that document name has been changed)
                            this.ltlScript.Text += ScriptHelper.GetScript("RefreshTree(" + node.NodeParentID + ", " + node.NodeID + ");");

                            lblInfo.Text = ResHelper.GetString("VersionProperties.RollbackOK");
                        }
                        catch (Exception ex)
                        {
                            lblError.Text += ex.Message;
                        }
                    }
                }
                break;

            case "destroy":
                if (versionHistoryId > 0)
                {
                    if (node != null)
                    {
                        // Check permissions
                        if (!mCanDestroy || (mCheckedOutByAnotherUser && !mCanCheckIn))
                        {
                            lblError.Text = ResHelper.GetString("History.ErrorNotAllowedToDestroy");
                            return;
                        }

                        // Destroy version
                        VersionManager verMan = new VersionManager(tree);

                        verMan.DestroyDocumentVersion(versionHistoryId);
                        lblInfo.Text = ResHelper.GetString("VersionProperties.DestroyOK");
                    }
                }
                break;
        }
    }


    /// <summary>
    /// Reloads the page data
    /// </summary>
    private void ReloadData()
    {
        mCanDestroy = (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Destroy) ==
            AuthorizationResultEnum.Allowed);
        mCanModify = (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) ==
            AuthorizationResultEnum.Allowed);

        WorkflowManager wm = new WorkflowManager(tree);
        wi = wm.GetNodeWorkflow(node);

        // If no workflow set for node, hide the data  
        if (wi == null)
        {
            lblInfo.Text = ResHelper.GetString("VersionsProperties.ScopeNotSet");
            DisableForm();
            pnlVersions.Visible = false;
        }
        else
        {
            // Get current step info, do not update document
            WorkflowStepInfo si = wm.GetStepInfo(node, false) ??
                                  wm.GetFirstWorkflowStep(node);

            string stepName = si.StepName.ToLower();
            mCanApprove = wm.CanUserApprove(node, CMSContext.CurrentUser);

            switch (stepName)
            {
                case "edit":
                case "published":
                case "archived":
                    break;

                default:
                    if (!mCanApprove)
                    {
                        this.lblApprove.Visible = true;
                        this.lblApprove.Text = ResHelper.GetString("EditContent.NotAuthorizedToApprove") + "<br />";
                    }
                    break;
            }

            mCanApprove = (mCanApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived"));
        }

        // Check modify permissions
        if (!mCanModify)
        {
            DisableForm();
            plcForm.Visible = false;
            lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"),
                                         node.NodeAliasPath);
        }
        else if (VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName))
        {
            btnCheckout.Visible = false;
            btnCheckout.Enabled = true;
            btnCheckin.Visible = false;
            btnCheckin.Enabled = true;
            btnUndoCheckout.Visible = false;
            btnUndoCheckout.Enabled = true;
            txtComment.Enabled = true;
            txtVersion.Enabled = true;
            lblComment.Enabled = true;
            lblVersion.Enabled = true;

            // Check whether to check out or in
            if (wi == null)
            {
                btnCheckout.Visible = true;
                lblCheckInfo.Text = ResHelper.GetString("VersionsProperties.CheckOut");
                lblInfo.Text = ResHelper.GetString("VersionsProperties.NotSupportWorkflow");
                DisableForm();
            }
            else if (node.GetValue("DocumentCheckedOutByUserID") == null)
            {
                lblCheckInfo.Text = ResHelper.GetString("VersionsProperties.CheckOut");
                lblInfo.Text = ResHelper.GetString("VersionsProperties.InfoCheckedIn");
                DisableForm();
                btnCheckout.Visible = true;
                btnCheckout.Enabled = true;
            }
            else
            {
                int checkedOutBy = ValidationHelper.GetInteger(node.GetValue("DocumentCheckedOutByUserID"), 0);
                // If checked out by current user, allow to check-in
                if (checkedOutBy == CMSContext.CurrentUser.UserID)
                {
                    btnCheckin.Visible = true;
                    btnUndoCheckout.Visible = true;
                    lblCheckInfo.Text = ResHelper.GetString("VersionsProperties.CheckIn");
                    lblInfo.Text = ResHelper.GetString("VersionsProperties.InfoCheckedOut");
                }
                else
                {
                    mCheckedOutByAnotherUser = true;
                    // Else checked out by somebody else
                    btnCheckout.Visible = true;
                    btnCheckin.Visible = true;
                    btnCheckout.Visible = false;
                    lblCheckInfo.Text = ResHelper.GetString("VersionsProperties.CheckIn");
                    lblInfo.Text = ResHelper.GetString("VersionsProperties.IsCheckedOut");

                    mCanCheckIn = CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Content", "CheckInAll");

                    btnUndoCheckout.Visible = mCanCheckIn;
                    btnUndoCheckout.Enabled = mCanCheckIn;
                    btnCheckin.Enabled = mCanCheckIn;
                    txtComment.Enabled = mCanCheckIn;
                    txtVersion.Enabled = mCanCheckIn;
                }
            }

            if (!mCanApprove)
            {
                DisableForm();
            }
        }
        else
        {
            plcForm.Visible = false;
        }
        btnDestroy.Enabled = (mCanDestroy && (!mCheckedOutByAnotherUser || mCanCheckIn));
    }


    /// <summary>
    /// Disables the editing form
    /// </summary>
    private void DisableForm()
    {
        txtComment.Enabled = false;
        txtVersion.Enabled = false;

        btnCheckin.Enabled = false;
        btnCheckout.Enabled = false;
        btnUndoCheckout.Enabled = false;
    }


    protected void btnCheckout_Click(object sender, EventArgs e)
    {
        VersionManager verMan = new VersionManager(tree);

        verMan.EnsureVersion(node, node.IsPublished);

        bool wasArchived = (node.WorkflowStepName.ToLower() == "archived");

        // Check out the document
        verMan.CheckOut(node);

        if (wasArchived)
        {
            this.ltlScript.Text += ScriptHelper.GetScript("RefreshTree(" + node.NodeParentID + ", " + node.NodeID + ");");
        }

        ReloadData();
        gridHistory.ReloadData();
    }


    protected void btnCheckin_Click(object sender, EventArgs e)
    {
        VersionManager verMan = new VersionManager(tree);

        // Check in the document        
        string version = null;
        if (txtVersion.Text.Trim() != "")
        {
            version = txtVersion.Text.Trim();
        }
        string comment = null;
        if (txtComment.Text.Trim() != "")
        {
            comment = txtComment.Text.Trim();
        }

        verMan.CheckIn(node, version, comment);

        txtComment.Text = "";
        txtVersion.Text = "";

        ReloadData();
        gridHistory.ReloadData();
    }


    protected void btnUndoCheckout_Click(object sender, EventArgs e)
    {
        VersionManager verMan = new VersionManager(tree);

        // Undo check out
        verMan.UndoCheckOut(node);

        txtComment.Text = "";
        txtVersion.Text = "";

        ReloadData();
        gridHistory.ReloadData();
    }


    protected void btnDestroy_Click(object sender, EventArgs e)
    {
        if (node != null)
        {
            // Check permissions
            if (!mCanDestroy || (mCheckedOutByAnotherUser && !mCanCheckIn))
            {
                lblError.Text = ResHelper.GetString("History.ErrorNotAllowedToDestroy");
                return;
            }

            VersionManager verMan = new VersionManager(tree);

            verMan.DestroyDocumentHistory(node.DocumentID);

            ReloadData();
            gridHistory.ReloadData();
        }
    }
}

