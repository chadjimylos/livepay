<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ManufacturerSelector.ascx.cs" Inherits="CMSModules_Ecommerce_FormControls_ManufacturerSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%ManufacturerDisplayName%}"
            ReturnColumnName="ManufacturerID" ObjectType="ecommerce.manufacturer" ResourcePrefix="manufacturerselector"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>