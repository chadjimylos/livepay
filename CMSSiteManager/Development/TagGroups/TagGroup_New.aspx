<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="TagGroup_New.aspx.cs" Inherits="CMSSiteManager_Development_TagGroups_TagGroup_New"
    Title="Tag Group - New" Theme="Default" %>

<%@ Register Src="~/CMSSiteManager/Development/TagGroups/TagEdit.ascx" TagName="GroupEdit"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" runat="Server">
    <cms:GroupEdit ID="groupEdit" runat="server" IsEdit="false" />
</asp:Content>
