// Create and define new FCKEditor button command openning the popup window

var QuicklyInsertImageCommand = function() {
}

QuicklyInsertImageCommand.GetState = function() {
    if (FCK.EditMode != FCK_EDITMODE_WYSIWYG)
        return FCK_TRISTATE_DISABLED;
    else {
        return FCK_TRISTATE_OFF;
    }
}

QuicklyInsertImageCommand.Execute = function() {

}

function CustomChangeState(newState, force) {
    if (!force && this.State == newState)
        return;

    var e = this.MainElement;
    if (!e)
        return;

    switch (parseInt(newState, 10)) {
        case FCK_TRISTATE_ON:
            e.className = 'TB_Button_On';
            QuicklyInsertImage.Show(e);
            break;

        case FCK_TRISTATE_OFF:
            e.className = 'TB_Button_Off';
            QuicklyInsertImage.Show(e);
            break;

        case FCK_TRISTATE_DISABLED:
            e.className = 'TB_Button_Disabled';
            QuicklyInsertImage.Hide(e);
            break;
    }

    this.State = newState;
}

FCKCommands.RegisterCommand('QuicklyInsertImage', QuicklyInsertImageCommand);

var QuicklyInsertImage = function() {
}

QuicklyInsertImage.prototype = new FCKToolbarButton('QuicklyInsertImage', FCKLang.QuicklyInsertImageButton, FCKLang.QuicklyInsertImageButton, null, false, false, '');

QuicklyInsertImage.prototype.Create = function(parentElement) {

    this._UIButton = new FCKToolbarButtonUI(this.CommandName, this.Label, this.Tooltip, this.IconPath, this.Style);
    this._UIButton.ChangeState = CustomChangeState;

    // Check if in url is defined DocumentID or FormGUID
    if (QuicklyInsertImage.IsDocument(FCKConfig["QuickInsertURL"])) {
        this._UIButton._ToolbarButton = this;
        this._UIButton.OnClick = this.Click;
        this._UIButton.Create(parentElement);

        var imgs = parentElement.getElementsByTagName('img');
        var div = imgs[0].parentNode;

        // Override some parent div css properties
        div.style.width = "20px";
        div.style.height = "20px";
        div.style.margin = "0px";
        div.parentNode.style.filter = "";

        // Create IFRAME for the uploader
        div.innerHTML = '<iframe name="QuickInsertImage" src="' + FCKPlugins.Items['InsertLink'].Path + FCKConfig["QuickInsertURL"] + '" frameborder="0" marginWidth="0" marginHeight="0" scrolling="no" width="20" height="20" border="0" /><img src="' + FCKPlugins.Items['QuicklyInsertImage'].Path + 'fckIcon.gif' + '" class="QII_disable" style="width:20px; height:20px; display:none;"/>';
        div.innerHTML += '<img src="' + FCKPlugins.Items['QuicklyInsertImage'].Path + 'fckIcon.gif" style="width:20px; height:20px; display:none;" />';
        FCK.ToolbarSet.quickFrame = div.firstChild;

        setTimeout("QuicklyInsertImage.SetTitle('QuickInsertImage','" + this.Tooltip + "',600);", 100);

        if (!FCKBrowserInfo.IsGecko) {
            if ((typeof (FCKeditorAPI) != 'undefined') && (FCKeditorAPI.__Instances != null)) {
                for (var name in FCKeditorAPI.__Instances) {
                    var oEditor = FCKeditorAPI.__Instances[name];
                    oEditor.Focus = FCK_Custom_Focus;
                }
            }
        }
    }
}

QuicklyInsertImage.Show = function(e) {
    var frames = e.getElementsByTagName('iframe');
    if ((frames != null) && (frames.length > 0)) {
        frames[0].style.display = "block";
        var fakeImgs = e.getElementsByTagName('img');
        if ((fakeImgs != null) && (fakeImgs.length > 0)) {
            fakeImgs[0].style.display = "none";
        }
    }
}

QuicklyInsertImage.Hide = function(e) {
    var frames = e.getElementsByTagName('iframe');
    if ((frames != null) && (frames.length > 0)) {
        frames[0].style.display = "none";
        var fakeImgs = e.getElementsByTagName('img');
        if ((fakeImgs != null) && (fakeImgs.length > 0)) {
            fakeImgs[0].style.display = "block";
            if (document.all) {
                fakeImgs[0].style.filter = "gray() alpha(opacity=30)";
            }
        }
    }
}

QuicklyInsertImage.SetTitle = function(name, title, count) {
    count--;
    var timeOutFunction = "QuicklyInsertImage.SetTitle('" + name + "','" + title + "'," + count + ");";
    if (count > 0) {
        try {
            var toolbarDoc = FCK.ToolbarSet._IFrame.contentDocument;
            if (toolbarDoc != null) {
                var frames = toolbarDoc.getElementsByTagName('iframe');
                for (var i = 0; i < frames.length; i++) {
                    if (frames[i].name == name) {
                        if (frames[i].contentDocument != null) {
                            var inputs = frames[i].contentDocument.getElementsByTagName('input');
                            var found = false;
                            for (var j = 0; j < inputs.length; j++) {
                                if (inputs[j].type == 'file') {
                                    found = true;
                                    inputs[j].title = title;
                                }
                            }
                            if (!found) {
                                setTimeout(timeOutFunction, 100);
                            }
                        }
                        else {
                            setTimeout(timeOutFunction, 100);
                        }
                    }
                }
            }
            else {
                setTimeout(timeOutFunction, 100);
            }
        }
        catch (e) {
            setTimeout(timeOutFunction, 100);
        }
    }
}

QuicklyInsertImage.IsDocument = function(url) {
    if (url != null) {
        // Get form guid from url
        var formGuid = url.match(/(?:formGuid=)([a-z\d-]+)/i);
        // Get document id from url
        var documentId = url.match(/(?:documentid=)([\d]+)/i);
        // Get parent id form url
        var parentId = url.match(/(?:parentid=)([\d]+)/i);

        if ((formGuid != null) && (parentId != null)) {
            if ((/^[a-f\d]{8}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{4}-[a-f\d]{12}$/i.test(formGuid[1])) && (parseInt(parentId[1], 10) >= 0)) {
                return true;
            }
        }
        if (documentId != null) {
            if (parseInt(documentId[1], 10) >= 0) {
                return true;
            }
        }
    }
    return false;
}

var oQuicklyInsertImage = new QuicklyInsertImage();
FCKToolbarItems.RegisterItem('QuicklyInsertImage', oQuicklyInsertImage);

function FCK_OnBlur(editorInstance) {
}

function FCK_Custom_Focus() {
    window.parent.currentFCKInstance = this;
    //FCK.EditingArea.Focus();
}