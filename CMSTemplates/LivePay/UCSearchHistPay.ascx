<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchHistPay.ascx.vb" Inherits="CMSTemplates_LivePay_UCSearchHistPay" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCReceipt.ascx" TagName="Receipt" TagPrefix="uc"  %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCTransDetails.ascx" TagName="TransDetails" TagPrefix="uc"  %>
<script src="../../CMSScripts/LivePay/LivePay.js" type="text/javascript"></script>
<script src="../../CMSScripts/LivePay/RegisteredUsers.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript" >


    $(document).ready(function () {
        if (document.all) {
            $('.DllBorder').css("border", "1px solid #3b6db4")
        } else {
            $('.DllBorder select').css("border", "1px solid #3b6db4")
        }
    });
    var event_c;
    var obj_c;

    function handle_keydownHome(e) {


    }
    function Hist_RebindRpt(CachValue) {

        if (CheckAction(event_c, obj_c)) {
            CachValue = CachValue.replace(/[']/gi, "@@@@")
            setTimeout("Hist_ExecSearch('" + CachValue + "')", 400)
        } else {
            ControlRows(event_c, obj_c)
        }
       
    }

    function Hist_ExecSearch(CachValue) {
        var RealValue = document.getElementById('<%=txtMerchant.ClientID %>').value
        RealValue = RealValue.replace(/[']/gi, "@@@@") 

        if (RealValue == CachValue) {
            var text = document.getElementById('<%=txtMerchant.clientID %>').value;
            text = text.replace(/[']/gi, "@@@@") 
            if (text != null && text.length > 0) {
                GetMerchantHistData(RealValue)
            } else {
                document.getElementById('<%=HiddentxtMerchantID.clientID %>').value = ''
                Hist_ShowHideQuickRes(false, 0)
            }
        }
    }

    function GetMerchantHistData(val) {
        var x;
        if (window.ActiveXObject) {
            x = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            x = new XMLHttpRequest();
        }
        val = escape(val)
        x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val + "&lang=" + '<%=request("lang") %>', true);
        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                var Results = x.responseText;
                var RowCount = Results.split("#]")[0]
                Results = Results.split("#]")[1]
                if (RowCount.length > 0) {
                  
                    document.getElementById('Hist_QuickSrcResAll').innerHTML = Results;
                    Hist_ShowHideQuickRes(true, RowCount);
                } else {
                    Hist_ShowHideQuickRes(false, 0);
                }

            }
        }
        x.send(null);
    }

    function Hist_ShowHideQuickRes(IsVisible, RowCount) {
        if (IsVisible) {
            if (RowCount == 1)
                document.getElementById('Hist_QuickSrcResMain').className = 'QuickSrcResMain_1'
            if (RowCount == 2)
                document.getElementById('Hist_QuickSrcResMain').className = 'QuickSrcResMain_2'
            if (RowCount > 2)
                document.getElementById('Hist_QuickSrcResMain').className = 'QuickSrcResMain'
            document.getElementById('Hist_QuickResDiv').style.display = '';
            ControlRows(event_c, obj_c)
        }
        else {
            document.getElementById('Hist_QuickResDiv').style.display = 'none';
        }
    }

    function SetSuggestValue(txt, Val) {
        document.getElementById('<%=txtMerchant.clientID %>').value = txt;
        document.getElementById('<%=HiddentxtMerchantID.clientID %>').value = Val;
        document.getElementById('Hist_QuickResDiv').style.display = 'none';
    }

    function ShowTransDetails() {
        $.blockUI({ css: { top: '20%', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#ReceiptPopUp') });
    }


    function ExportFile(url) {
        document.getElementById('<%=exportFrame.ClientID %>').src =  url;
        return false
    }

    function ControlRows(e, obj) {
        var keynum = 0;
        var QuickResDiv = document.getElementById('Hist_QuickResDiv')
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

        if (keynum == 38) { // up
            //Move selection up
            if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {

                ChangeSelection(-1)
            }
        }
        if (keynum == 40) { // down
            //Move selection down
            if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {
                ChangeSelection(1)
            }
        }
        if (keynum == 13) { // enter
            if (obj.value.length > 0) {
                var IsSelected = false
                var SelectedObj = null;
                var AllHtml = $('.QuickSrcResTitle').get()

                $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
                    if ($(obj).attr("IsSelected") == 'true') {
                        IsSelected = true
                        SelectedObj = obj
                    }
                });

                if (IsSelected == false) { // if nothing is selected go to search page
                    if (window.event) {
                        return handle_keydownHome(event);
                    }
                    else if (e.which) {
                        return handle_keydownHome(e);
                    }    // Netscape/Firefox/Opera
                } else {

                    $(SelectedObj).find("a").click()

                }
            }
        }
    }

    function ChangeSelection(GoTo) {

        var LastSelected = -1
        //QuickSrcResTitle
        var IsSelected = false
        var AllHtml = $('.QuickSrcResTitle').get()

        $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
            if ($(obj).attr("IsSelected") == 'true') {
                IsSelected = true
                LastSelected = index
            }
        });

        if (IsSelected == false) { //-- If Not Selected Select Firstone
            $.each(AllHtml, function (index, obj) {
                if (index == 0) {
                    $(obj).attr("IsSelected", "true")
                    $(obj).css("background-color", "#d2d6db")
                }
            });
        } else {
            $.each(AllHtml, function (index, obj) {
                var GetNextSelection = LastSelected + GoTo

                if (GetNextSelection > -1 && GetNextSelection < 3) {
                    $(obj).attr("IsSelected", "false")
                    $(obj).css("background-color", "transparent")

                    if (index == GetNextSelection) {
                        $(obj).attr("IsSelected", "true")
                        $(obj).css("background-color", "#d2d6db")
                    }
                }
            });
        }

    }

    function CheckAction(e, obj) {
        var keynum = 0;
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

        var IsSearching = true

        if (keynum == 38) { // up
            IsSearching = false
        }

        if (keynum == 40) { // down
            IsSearching = false
        }

        if (keynum == 13) { // enter
            IsSearching = false
        }

        return IsSearching
    }

    function Setent(e) {
        var keynum = 0;
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

        if (keynum == 13) {
            return false
        } else {
            return true
        }


    }
</script>



<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}

</style>
<asp:UpdatePanel ID="uptPanel" runat="server" UpdateMode="Always" >
<ContentTemplate >
<div id="ReceiptPopUp" style="display:none">
        <uc:Receipt ID="Receipt" runat="server" />
</div>
<div id="TransDetailsPopUp" style="display:none">
        <uc:TransDetails ID="TransDetails" runat="server" />
</div>
<asp:Label ID="tt" runat="server"></asp:Label>
<asp:Label ID="lblError2" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

 <div class="SrHistPayBG">
	<div class="SrHistPayTitle"><%= ResHelper.LocalizeString("{$=�������� ��������|en-us=Payment History$}")%></div>
	<div class="SrHistPaySrcTitle"><%= ResHelper.LocalizeString("{$=��������� ��������|en-us=Payment Search$}")%></div>
	<div class="SrHistPaySrcDescr"><%=ResHelper.LocalizeString("{$=�� ���� ��� ������� �������� �� ����� ��� ��������� �������� ��� ����� ���������������. ��������������� �� �������� �������� ���������� ��� �� ����������� �� ������������.<BR /> <b>��������: ��� �������� ��� ���������������� �� ��������������, ������ � �� ��������� ������ ���� ��� 9�� �������� �� ��� ����������� �� ��� ���������� ��� �������� ��������� ������.</b>|en-us=You can view your recent payments here. Please use the following search criteria  to minimize the results.$}")%></div>
    <div style="padding-top:8px;padding-top:expression(5)"><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
	<div>
    <div id="lblError" runat="server" visible="false" style="color:Red;font-weight:bold;padding-left:20px"></div>
	   <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=��������|en-us=Period$}") %>:</div>
	   <div class="SrHistPaySrcDtmRBL">
	    <asp:RadioButtonList ID="rbtDate" runat="server" RepeatDirection="Horizontal" cssClass="SrHistPayrbl" >
	       
	    </asp:RadioButtonList>
	   </div>
	   <div class="Clear"></div>
	   <div class="PT5">
		<div class="SrHistPaySrcDtmFrom"><%= ResHelper.LocalizeString("{$=���|en-us=From$}")%>:</div>
		<div class="SrHistPaySrcFrom"><asp:textbox CssClass="SrcHisPayTxt" id="txtFrom" runat="server" autocomplete="off" style="width:142px;background-image:url('/app_themes/LivePay/Calendar.png');background-position:right center;background-repeat:no-repeat" /></div>
      	<div class="SrHistPaySrcDtmTo"><%=ResHelper.LocalizeString("{$=���|en-us=To$}") %>:</div>
		<div class="SrHistPaySrcTo"><asp:textbox CssClass="SrcHisPayTxt" id="txtTo" runat="server" autocomplete="off" style="border:1px solid #3b6db4;width:142px;background-image:url('/app_themes/LivePay/Calendar.png');background-position:right center;background-repeat:no-repeat" /></div>
		<div class="Clear"></div>
	   </div>
       <div><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
       <div id="DivFullSrc" class="SrHistPayBigForm" style="display:none">
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%=ResHelper.LocalizeString("{$=���� ��������|en-us=Payment amount$}") %> (&euro;):</div>
                <div class="SrHistPayTableFrom"><%=ResHelper.LocalizeString("{$=���|en-us=From$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox onkeyup="FixMoney(this,false,false)" onblur="FixMoney(this,true,false)" CssClass="SrcHisPayTxt" id="txtFromPrice" runat="server" style="border:1px solid #3b6db4;width:142px" /></div>

                <div class="SrHistPayTableTo"><%=ResHelper.LocalizeString("{$=���|en-us=To$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox onkeyup="FixMoney(this,false,false)" onblur="FixMoney(this,true,false)" CssClass="SrcHisPayTxt" id="txtToPrice" runat="server" style="border:1px solid #3b6db4;width:142px" /></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%=ResHelper.LocalizeString("{$=������� ����|en-us=Payment to$}") %>:</div>
                <div style="float:left"><asp:textbox  CssClass="SrcHisPayTxt" id="txtMerchant" autocomplete="off" onkeydown="return Setent(event)" onkeyup="event_c = event; obj_c = this;Hist_RebindRpt(this.value);" runat="server" style="border:1px solid #3b6db4;width:178px" /></div>
                <div class="Clear"></div><asp:Button style="display:none" Text="hidbtn" ID="Hist_hidBtn" runat="server" /><asp:TextBox ID="HiddentxtMerchantID" runat="server" style="display:none"></asp:TextBox>
                <div class="HistQuickSrcResTopMainNewPay" id="Hist_QuickResDiv" style="display:none">
                    <div class="QuickSrcResMain" id="Hist_QuickSrcResMain">
                        <div class="QuickSrcResAll" id="Hist_QuickSrcResAll">
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%= ResHelper.LocalizeString("{$=�����|en-us=Card$}")%>:</div>
                <div style="float:left" class="DllBorder"><asp:DropDownList CssClass="SrcHisPayDDL"  ID="drpSavedCards" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%= ResHelper.LocalizeString("{$=����������|en-us=Result$}")%>:</div>
                <div style="float:left" class="DllBorder"><asp:DropDownList  CssClass="SrcHisPayDDL" ID="drpTxnResult" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%= ResHelper.LocalizeString("{$=������� ��������|en-us=Number of Transactions$}")%>:</div>
                <div style="float:left" class="DllBorder">
                    
                    <asp:DropDownList  CssClass="SrcHisPayDDL" ID="drpTransCount" runat="server" >
                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                        <asp:ListItem Value="20" Text="20"></asp:ListItem>
                        <asp:ListItem Value="40" Text="40"></asp:ListItem>
                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
       </div>
       <div class="SrHistPayResTitle" style="padding-left:15px"><a onclick="ShowHideSrcForm()" class="Chand"><img border="0" id="imgPointSrcType" src="~/app_themes/LivePay/PointerDown.gif"/> <span ID="spnSrcType"><%= ResHelper.LocalizeString("{$=��������� ���������|en-us=Detailed Search$}")%></span></a></div>
	   <div class="SrHistPayResBtnSearch"><asp:ImageButton runat="server" ID="btnSearchPayments" /></div>
	   <div class="Clear"></div>

       <div class="SrcHisPayMainGrid">
            <div class="SrcHisPayGridBGTop">
                <div class="SrcHisPayGridTopTitle" id="divHeader" runat="server" visible="false"><%=ResHelper.LocalizeString("{$=��������� ��������|en-us=Recent Transactions$}") %></Div>
                <div class="SrcHisPayGridDiv">
                    <asp:GridView ID="GridViewPayments" EmptyDataTemplate-CssClass="SrcHisPayPagerEmpty" PagerStyle-CssClass="SrcHisPayPager" GridLines="None" runat="server"  AutoGenerateColumns="false" AllowPaging="true" Width="642px">
                    <HeaderStyle BackColor="#ffffff" ForeColor="#58595b" Height="35px" HorizontalAlign="Center"/>
                    <AlternatingRowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="12px" Font-Names="tahoma" HorizontalAlign="Center"/>
                    <RowStyle Height="25px" BackColor="#f7f7f7" ForeColor="#58595b" Font-Size="12px" Font-Names="tahoma" HorizontalAlign="Center" />
                        <Columns>                        
                            <asp:TemplateField HeaderStyle-CssClass="SrcHisPayGridHeader" ItemStyle-CssClass="SrcHisPayGridItem" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <HeaderStyle Width="120" />
                                <ItemTemplate>
                                     <asp:Label ID="lbltransactionCode" runat="server" ></asp:Label>
                                </ItemTemplate> 
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-CssClass="SrcHisPayGridHeader" ItemStyle-CssClass="SrcHisPayGridItem" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <HeaderStyle Width="120" />
                                <ItemTemplate>
                                      <asp:Label ID="lbltransactiondDte" runat="server" Text='<%#Convert.ToDateTime(Eval("transactiondDte")).ToString("dd/MM/yyyy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="������� ����"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <HeaderStyle Width="100" />
                                <ItemTemplate>
                                    <asp:Label ID="lblMerchantID" runat="server" Text='<%#Eval("merchantId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="�����"  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <%# "xxxxxxxx-xxxx-" & Right(Replace(Eval("CardNumber"), " ", ""), 4)%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="���� (&euro;)" HeaderStyle-CssClass="SrcHisPayGridHeaderPrice" ItemStyle-CssClass="SrcHisPayGridItemPrice" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Left"  >
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidAmount" runat="server" value='<%#Eval("transactioAamount") %>' />
                                    <asp:Label ID="lblAmount" runat="server" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="����������" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <HeaderStyle Width="70" />
                                <ItemTemplate>
                                 <asp:Label ID="lbltransactionStatus" Text='<%#GetTransactionStatusDescr(Eval("TransactionStatus")) %>' runat="server" />
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="�����" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                 <asp:Label ID="lbltransactionType" Text='<%#Eval("TransactionType") %>' runat="server" />
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="������" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                 <asp:Label ID="lblInstallments" runat="server" />
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="&nbsp;" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="SrcHisPayGridHeaderLast" ItemStyle-CssClass="SrcHisPayGridItemLast" >
                                <ItemTemplate>
                                   <asp:LinkButton ID="openPopup" runat="server" OnClick="openPopup_Click"  CommandArgument='<%# Eval("transactionId") & "_" & Eval("system") %>'><img border="0" src="/app_themes/LivePay/InfoBtn.png" /></asp:LinkButton>  
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                        </Columns>
                        <EmptyDataTemplate >
                            <div class="SrcHisPayPagerEmpty">
                                <div style="padding-top:5px;padding-left:10px"><%=ResHelper.LocalizeString("{$=��� �������� ����������|en-us=NoRecords$}") %></div>
                            </div>
                        </EmptyDataTemplate>
                        <PagerTemplate >
                        <div class="SrcHisPayPagerSpace" >
                            <div>
                                <div class="GridPagerNoLinkDivLeft"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%= ResHelper.LocalizeString("{$=&laquo; �����������|en-us=&laquo; Previous$}")%></asp:linkbutton><font color="#adadad">&nbsp;|&nbsp;</font></div>
                                <div style="float:left;width:220px">
                                    <div class="GridPagerNoLinkDivCentesr" style="float:left;padding-top:13px;padding-top:expression(0);width:140px" id="lblPageIndex" runat="server">&nbsp;</div>
                                    <div class="GridPagerNoLinkDivRight"><font color="#adadad">&nbsp;|&nbsp;</font><asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=������� &raquo;|en-us=Next &raquo;$}") %></asp:linkbutton></div>
                                    <div class="Clear"></div>
                                </div>
                                <div class="PagerCompleteSec"><asp:ImageButton class="Chand" ID="BtnExportToPDF" runat="server"  OnClick="btnSaveToPDFClick"   /></div>
                                <div class="GridPagerLast"><asp:ImageButton class="Chand" ID="BtnExportTEXCEL" runat="server"  OnClick="btnSaveToExcelClick"  /></div>
                                <div class="Clear"></div>
                            </div>
                       </div>
                         </PagerTemplate>
                    </asp:GridView>
                </div>
                <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
                <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
                <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
            </div>
            
       </div>
       <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
	</div>
 </div>
 </div>
 <div style="display:none" id="FramDiv" runat="server" >
   <iframe id="exportFrame"  runat="server"></iframe>
 </div>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnSearchPayments" />
</Triggers>
</asp:UpdatePanel>

 <script language="javascript" type="text/javascript" >


     CheckAddtributes()
     function CheckAddtributes() {
         var GetSrcType = document.getElementById('<%=HiddenSearchType.ClientID %>').value
         var GetForm = document.getElementById('DivFullSrc');
         if (GetSrcType == '1') {
             GetForm.style.display = ''
         }
     }

     function GoToPage(no) {
         document.getElementById('<%=HiddenPage.ClientID %>').value = no;
         document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
         var GetID = '<%=HiddenPageBtn.ClientID %>'
         GetID = GetID.replace(/[_]/gi, "$")
         __doPostBack(GetID, '')
     }

   

     function ShowHideSrcForm(checkfirst) {
         var GetForm = document.getElementById('DivFullSrc');
         var GetimgPoint = document.getElementById('imgPointSrcType');
         var GetSrcType = document.getElementById('spnSrcType');

         if (checkfirst) {
             var SectedValue = document.getElementById('<%=HiddenSearchType.ClientID %>').value
             if (SectedValue == '0') { //- Closed Form
                 GetForm.style.display = 'none'
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '0';
                 GetimgPoint.src = '/app_themes/LivePay/PointerDown.gif'
                 GetSrcType.innerHTML = '<%= ResHelper.LocalizeString("{$=��������� ���������|en-us=Detailed Search$}")%>'
             } else {
                 GetForm.style.display = ''
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '1';
                 GetimgPoint.src = '/app_themes/LivePay/PointerUp.png'
                 GetSrcType.innerHTML ='<%= ResHelper.LocalizeString("{$=���� ���������|en-us=Simple Search$}")%>'
             }
         } else {
             if (GetForm.style.display == '') {
                 GetForm.style.display = 'none'
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '0';
                 GetimgPoint.src = '/app_themes/LivePay/PointerDown.gif'
                 GetSrcType.innerHTML = '<%= ResHelper.LocalizeString("{$=��������� ���������|en-us=Detailed Search$}")%>'
             } else {
                 GetForm.style.display = ''
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '1';
                 GetimgPoint.src = '/app_themes/LivePay/PointerUp.png'
                 GetSrcType.innerHTML = '<%= ResHelper.LocalizeString("{$=���� ���������|en-us=Simple Search$}")%>'
             }
         }
         PosBottom();
     }

     function SetRblDates(val, Today, Oneweek, Onemonth, Towmonth, Sixmonths) {
         var GetVal = $("[id$=" + val.id + "] input:checked").val();
         var StartDtm = Today;
         var EndDtm;
         if (GetVal == '1') { // Simera
             EndDtm = Today;
         }
         if (GetVal == '2') { // 1 week
             EndDtm = Oneweek;
         }
         if (GetVal == '3') { // 1 month
             EndDtm = Onemonth;
         }
         if (GetVal == '4') { // 2 month
             EndDtm = Towmonth;
         }
         if (GetVal == '5') { // 6 months
             EndDtm = Sixmonths;
         }

         document.getElementById('<%=txtFrom.clientid %>').value = EndDtm
         document.getElementById('<%=txtTo.clientid %>').value = StartDtm
     }

 
   


    
   
</script>