using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Countries_Country_List : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Country_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Country/object.png");
        this.CurrentMaster.Title.HelpTopicName = "countries_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Country_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Country_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_Country/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Country_Edit_Frameset.aspx?countryID=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            int countryId = ValidationHelper.GetInteger(actionArgument, 0);

            bool dependent = false;

            // Get country states
            DataSet ds = StateInfoProvider.GetCountryStates(countryId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Check dependency of all state at first
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int stateId = ValidationHelper.GetInteger(dr["StateId"], 0);
                    if (StateInfoProvider.CheckDependencies(stateId))
                    {
                        dependent = true;
                        break;
                    }
                }
            }

            // Get country dependency without states
            DataSet dsCountryDependency = CountryInfoProvider.GetCountries(String.Format("(CountryID IN (SELECT AddressCountryID FROM COM_Address WHERE AddressCountryID={0}) OR CountryID IN (SELECT CustomerCountryID FROM COM_Customer WHERE CustomerCountryID={0}) OR CountryID IN (SELECT CountryID FROM COM_TaxClassCountry WHERE CountryID={0}))", countryId), null, 1, "CountryID");

            // Check dependency of country itself
            dependent |= (!DataHelper.DataSourceIsEmpty(dsCountryDependency));

            if (dependent)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("ecommerce.deletedisabledwithoutenable");
                return;
            }
            else
            {

                // Delete all states
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int stateId = ValidationHelper.GetInteger(dr["StateId"], 0);
                    StateInfoProvider.DeleteStateInfo(stateId);
                }

                // Delete CountryInfo object from database
                CountryInfoProvider.DeleteCountryInfo(countryId);
            }
        }
    }
}
