﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls

Imports CMS.SiteProvider
Imports CMS.CMSHelper
Imports System.Threading

Partial Class CMSTemplates_LivePay_UcAccountActivation
    Inherits CMSUserControl

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init


        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            'GET USER AND ACTIVATE HIM

            Try

                Dim g As Guid = New Guid(Request.QueryString("userguid").ToString)

                ' grab a user from the database.
                Dim usr As UserInfo = UserInfoProvider.GetUserInfoByGUID(g)
                If Not IsNothing(usr) Then
                    If usr.Enabled Then
                        lblResultTitle.Text = ResHelper.LocalizeString("{$=ο χρήστης είναι ήδη ενεργοποιημένος.|en-us=The user is already registered$}")
                        lblResultTitle.ForeColor = Drawing.Color.Red
                        lblInfo.Visible = False
                    Else
                        usr.Enabled = True
                        UserInfoProvider.SetUserInfo(usr)
                        lblResultTitle.Text = ResHelper.LocalizeString("{$=Ο λογαριασμός σας ενεργοποιήθηκε με επιτυχία.|en-us=Your account was successfully activated.$}")
                    End If
                Else
                    lblResultTitle.Text = ResHelper.LocalizeString("{$=Ο λογαριασμός δεν ενεργοποιήθηκε.<br />Βεβαιωθείτε ότι ακολουθήσατε το σωστό σύνδεσμο (link) και ξαναπροσπαθήστε.|en-us=Your account could not be activated.<br />Please make sure you followed the correct link and try again.$}")
                    lblResultTitle.ForeColor = Drawing.Color.Red
                    lblInfo.Visible = False
                End If


                'if success

            Catch ex As Exception
                'if not success
                lblResultTitle.Text = ResHelper.LocalizeString("{$=Ο λογαριασμός δεν ενεργοποιήθηκε.<br />Βεβαιωθείτε ότι ακολουθήσατε το σωστό σύνδεσμο (link) και ξαναπροσπαθήστε.|en-us=Your account could not be activated.<br />Please make sure you followed the correct link and try again.$}")
                lblResultTitle.ForeColor = Drawing.Color.Red
                lblInfo.Visible = False
            End Try




        End If
    End Sub

    Public Overloads Sub ReloadData()

    End Sub

#End Region

End Class
