CREATE TABLE [Forums_ForumModerators] (
		[UserID]      int NOT NULL,
		[ForumID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Forums_ForumModerators]
	ADD
	CONSTRAINT [PK_Forums_ForumModerators]
	PRIMARY KEY
	([UserID], [ForumID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Forums_ForumModerators]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_Forum]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumModerators_ForumID_Forums_Forum]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumModerators]
			ADD CONSTRAINT [FK_Forums_ForumModerators_ForumID_Forums_Forum]
			FOREIGN KEY ([ForumID]) REFERENCES [Forums_Forum] ([ForumID])
END
IF OBJECT_ID(N'[Forums_ForumModerators]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumModerators_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumModerators]
			ADD CONSTRAINT [FK_Forums_ForumModerators_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
