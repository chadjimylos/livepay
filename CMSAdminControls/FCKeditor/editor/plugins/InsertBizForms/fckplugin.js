// Register the related command.
FCKCommands.RegisterCommand( 'InsertBizForms', new FCKDialogCommand( 'InsertBizForms', FCKLang.InsertBizFormsTitle, FCKPlugins.Items['InsertBizForms'].Path + '../../../../../CMSModules/BizForms/Controls/FCKEditor/fck_InsertBizForms.aspx?' + FCKConfig.DialogParameters, 430, 420 ) ) ;

// Create the "Placeholder" toolbar button.
var oInsertBizForms = new FCKToolbarButton( 'InsertBizForms', FCKLang.InsertBizFormsButton ) ;
oInsertBizForms.IconPath = FCKPlugins.Items['InsertBizForms'].Path + 'fckBizForms.gif' ;
FCKToolbarItems.RegisterItem( 'InsertBizForms', oInsertBizForms ) ;
