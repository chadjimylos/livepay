<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_VolumeDiscount_List.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Products_Product_Edit_VolumeDiscount_List"
    Theme="Default" Title="Product edit - volume discount" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Product_Edit_VolumeDiscount_List.xml"
        IsLiveSite="false" Columns="VolumeDiscountID,VolumeDiscountValue,VolumeDiscountMinCount,VolumeDiscountIsFlatValue" />
</asp:Content>
