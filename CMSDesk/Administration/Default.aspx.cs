using System;

using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSDesk_Administration_Default : CMSAdministrationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureHelper.IsUICultureRTL())
        {
            ControlsHelper.ReverseFrames(colsFrameset);
        }

        this.ltlScript.Text = ScriptHelper.GetTitleScript(ResHelper.GetString("cmsdesk.ui.administration") + " ");
    }
}
