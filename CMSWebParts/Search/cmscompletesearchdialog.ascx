<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Search/cmscompletesearchdialog.ascx.cs" Inherits="CMSWebParts_Search_cmscompletesearchdialog" %>

<div class="SearchDialog">
    <cms:CMSSearchDialog ID="srchDialog" runat="server" />
</div>
<div class="SearchResults">
    <cms:CMSSearchResults ID="srchResults" runat="server" />
</div>