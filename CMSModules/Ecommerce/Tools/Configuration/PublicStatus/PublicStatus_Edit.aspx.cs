using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_PublicStatus_PublicStatus_Edit : CMSEcommerceConfigurationPage
{
    protected int publicStatusId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("publicstatus_edit.errorEmptyDisplayName");
        rfvCodeName.ErrorMessage = ResHelper.GetString("publicstatus_edit.errorEmptyCodeName");

        // control initializations				
        lblPublicStatusName.Text = ResHelper.GetString("PublicStatus_Edit.PublicStatusNameLabel");
        lblPublicStatusDisplayName.Text = ResHelper.GetString("PublicStatus_Edit.PublicStatusDisplayNameLabel");

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentPublicStatus = ResHelper.GetString("PublicStatus_Edit.NewItemCaption");

        // get publicStatus id from querystring		
        publicStatusId = ValidationHelper.GetInteger(Request.QueryString["publicStatusId"], 0);
        if (publicStatusId > 0)
        {
            PublicStatusInfo publicStatusObj = PublicStatusInfoProvider.GetPublicStatusInfo(publicStatusId);
            if (publicStatusObj != null)
            {
                currentPublicStatus = ResHelper.LocalizeString(publicStatusObj.PublicStatusDisplayName);

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(publicStatusObj);

                    // show that the publicStatus was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }

            this.CurrentMaster.Title.TitleText = ResHelper.GetString("PublicStatus_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_PublicStatus/object.png");
        }
        else // Add new
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("PublicStatus_New.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_PublicStatus/new.png");
        }

        this.CurrentMaster.Title.HelpTopicName = "newedit_public_status";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title breadcrumbs control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("PublicStatus_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/PublicStatus/PublicStatus_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentPublicStatus;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Load data of editing publicStatus.
    /// </summary>
    /// <param name="publicStatusObj">PublicStatus object.</param>
    protected void LoadData(PublicStatusInfo publicStatusObj)
    {

        txtPublicStatusName.Text = publicStatusObj.PublicStatusName;
        chkPublicStatusEnabled.Checked = publicStatusObj.PublicStatusEnabled;
        txtPublicStatusDisplayName.Text = publicStatusObj.PublicStatusDisplayName;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        // check input values from textboxes and other contrlos
        string errorMessage = new Validator()
            .NotEmpty(txtPublicStatusDisplayName.Text.Trim(), ResHelper.GetString("publicstatus_edit.errorEmptyDisplayName"))
            .NotEmpty(txtPublicStatusName.Text.Trim(), ResHelper.GetString("publicstatus_edit.errorEmptyCodeName")).Result;

        if (!ValidationHelper.IsCodeName(txtPublicStatusName.Text.Trim()))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            // Check unique name
            PublicStatusInfo publicStatusObj = PublicStatusInfoProvider.GetPublicStatusInfo(txtPublicStatusName.Text.Trim());

            if ((publicStatusObj == null) || (publicStatusObj.PublicStatusID == publicStatusId))
            {
                // Get object
                if (publicStatusObj == null)
                {
                    publicStatusObj = PublicStatusInfoProvider.GetPublicStatusInfo(publicStatusId);
                    if (publicStatusObj == null)
                    {
                        publicStatusObj = new PublicStatusInfo();
                    }
                }


                publicStatusObj.PublicStatusID = publicStatusId;
                publicStatusObj.PublicStatusName = txtPublicStatusName.Text.Trim();
                publicStatusObj.PublicStatusEnabled = chkPublicStatusEnabled.Checked;
                publicStatusObj.PublicStatusDisplayName = txtPublicStatusDisplayName.Text.Trim();

                PublicStatusInfoProvider.SetPublicStatusInfo(publicStatusObj);

                UrlHelper.Redirect("PublicStatus_Edit.aspx?publicStatusId=" + Convert.ToString(publicStatusObj.PublicStatusID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("PublicStatus_Edit.PublicStatusNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
