<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BannedIP_List.aspx.cs" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Banned IPs" Inherits="CMSModules_BannedIP_Tools_BannedIP_List" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<asp:Content ID="cntSiteSelect" runat="server" ContentPlaceHolderID="plcControls">
    <asp:PlaceHolder ID="plcSites" runat="server">
        <div style="padding-bottom: 0px;">
            <asp:Label ID="lblSite" runat="server" EnableViewState="false" />
            <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:CMSUpdatePanel runat="server" ID="pnlUpdateDisabled" UpdateMode="Conditional">
        <ContentTemplate> 
        <asp:Panel runat="server" ID="pnlDisabled">           
            <asp:Label runat="server" ID="lblDisabled" EnableViewState="false" CssClass="InfoLabel" /> 
        </asp:Panel>                      
    </ContentTemplate>
    </cms:CMSUpdatePanel>
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:UniGrid runat="server" ID="UniGrid" GridName="BannedIP_List.xml" IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
