set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Event attendee', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="AttendeeID" fieldcaption="AttendeeID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="4078b055-4042-41d8-9286-fe954ca4783d" /><field column="AttendeeEmail" fieldcaption="AttendeeEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ca9064cb-7f30-43fd-9086-4bcf7452ef97" /><field column="AttendeeFirstName" fieldcaption="AttendeeFirstName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8994e1ec-dc0c-43a2-b745-1e4b1a337681" /><field column="AttendeeLastName" fieldcaption="AttendeeLastName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="62fd46e4-e6c0-4302-98c3-d8bc3f1b0ded" /><field column="AttendeePhone" fieldcaption="AttendeePhone" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="49abfee2-d711-4f27-84e9-73265eeeb1e0" /><field column="AttendeeEventNodeID" fieldcaption="AttendeeEventNodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ff35fcf0-80d0-4347-9343-f9f957a9a971" /><field column="AttendeeGUID" fieldcaption="AttendeeGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="839cd8b5-afa2-48d6-ac9a-cd8c2eb166d1" /><field column="AttendeeLastModified" fieldcaption="AttendeeLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="42c38270-1b2f-4f8d-ac55-b31b776727a3" /></form>', '20100224 16:04:21', 1511, N'cms.EventAttendee', 0, 0, 0, '3eb7dbd1-e72a-4381-817a-789413c477c6', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Events_Attendee', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Events_Attendee">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AttendeeID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AttendeeEmail">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttendeeFirstName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttendeeLastName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttendeePhone" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttendeeEventNodeID" type="xs:int" />
              <xs:element name="AttendeeGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="AttendeeLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Events_Attendee" />
      <xs:field xpath="AttendeeID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Web part layout', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="WebPartLayoutID" fieldcaption="WebPartLayoutID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="fb354b46-35b3-4cd9-bbf0-85117bc33359" /><field column="WebPartLayoutCodeName" fieldcaption="WebPartLayoutCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="94d0fbe1-b684-4f7c-a501-0008a0a29e45" /><field column="WebPartLayoutDisplayName" fieldcaption="WebPartLayoutDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a2407cc-9146-4950-90c4-78549ca7efaf" /><field column="WebPartLayoutDescription" fieldcaption="WebPartLayoutDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3678f082-f1da-4f2b-bd29-3f3dac4e1273" /><field column="WebPartLayoutCode" fieldcaption="WebPartLayoutCode" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e0430f63-4ce2-4527-81e0-1cdc94ee2e20" /><field column="WebPartLayoutCheckedOutFilename" fieldcaption="WebPartLayoutCheckedOutFilename" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="74793c5c-c957-4acf-bd50-ca6d0eb66c79" /><field column="WebPartLayoutCheckedOutByUserID" fieldcaption="WebPartLayoutCheckedOutByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f75c8c60-43f9-4dd1-a09d-3ede582c983c" /><field column="WebPartLayoutCheckedOutMachineName" fieldcaption="WebPartLayoutCheckedOutMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="93580f0b-9557-4520-97c3-0879d8a287fa" /><field column="WebPartLayoutVersionGUID" fieldcaption="WebPartLayoutVersionGUID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="eaf51ec6-ab5a-468c-a3c2-b81427650c82" /><field column="WebPartLayoutWebPartID" fieldcaption="WebPartLayoutWebPartID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e38dfda5-7c17-42b4-bffb-8ddf13bb8680" /><field column="WebPartLayoutGUID" fieldcaption="WebPartLayoutGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43c97c99-eb8d-4ef3-b5d2-7805ba29df79" /><field column="WebPartLayoutLastModified" fieldcaption="WebPartLayoutLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f40f5517-7586-425e-aaac-f014ed51ba53" /></form>', '20090102 11:09:48', 1514, N'cms.WebPartLayout', 0, 0, 0, '05f3851c-9830-4bb4-a717-601e585211d3', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_WebPartLayout', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebPartLayout">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WebPartLayoutID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WebPartLayoutCodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutCode" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutCheckedOutFilename" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutCheckedOutByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="WebPartLayoutCheckedOutMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutVersionGUID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartLayoutWebPartID" type="xs:int" />
              <xs:element name="WebPartLayoutGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="WebPartLayoutLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebPartLayout" />
      <xs:field xpath="WebPartLayoutID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Credit Event', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="EventID" fieldcaption="EventID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="632bf090-0a62-4ab4-ad7a-bb5ade15d292" /><field column="EventName" fieldcaption="EventName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a24805b1-9e76-48e9-9dd7-f08a536ce86d" /><field column="EventCreditChange" fieldcaption="EventCreditChange" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="38a6bf91-170b-46fe-b8ec-62cb3f682199" /><field column="EventDate" fieldcaption="EventDate" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e839a80a-98e9-4639-badd-24cc7da72eab" /><field column="EventDescription" fieldcaption="EventDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2f7f0148-cf71-4cf0-baae-7d43a0a26f54" /><field column="EventCustomerID" fieldcaption="EventCustomerID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fec1ab02-ae4e-40d3-8a6f-a6862a2e7e1a" /><field column="EventCreditGUID" fieldcaption="EventCreditGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a62b7d78-29e6-4ae5-affc-a4b81512272e" /><field column="EventCreditLastModified" fieldcaption="EventCreditLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="198520a6-2159-459b-88d8-6c02fc8e538e" /></form>', '20091103 09:01:44', 1518, N'ecommerce.creditevent', 0, 0, 0, 'b27d0a43-6e82-4dce-819e-940363aba30e', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_CustomerCreditHistory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_CustomerCreditHistory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EventID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="EventName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventCreditChange" type="xs:double" />
              <xs:element name="EventDate" type="xs:dateTime" />
              <xs:element name="EventDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventCustomerID" type="xs:int" />
              <xs:element name="EventCreditGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="EventCreditLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Payment Gateway', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="GatewayID" fieldcaption="GatewayID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a1fe5e85-0b61-4e76-b7ee-457aa52bd38c" /><field column="GatewayCodeName" fieldcaption="GatewayCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f5c0ffe5-5f30-4394-a6e1-8e3538f64528" /><field column="GatewayDisplayName" fieldcaption="GatewayDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ce56315f-2bc3-44be-bed9-6870d341f921" /><field column="GatewayAssemblyName" fieldcaption="GatewayAssemblyName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="83ef6ab4-1104-4363-ae52-1c0113b83201" /><field column="GatewayClassName" fieldcaption="GatewayClassName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0412c2e2-1063-4d66-b26d-78bab3aa699d" /></form>', '20081229 09:07:02', 1519, N'ecommerce.paymentgateway', 0, 0, 0, 'c6d8e91e-8ffb-4a08-b134-f768f0ba5d10', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_PaymentGateway', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_PaymentGateway">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="GatewayID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="GatewayCodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayAssemblyName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayClassName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_PaymentGateway" />
      <xs:field xpath="GatewayID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'MetaFile', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="MetaFileID" fieldcaption="MetaFileID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="35d65b34-c36f-4378-baca-c732dc160c11" /><field column="MetaFileObjectID" fieldcaption="MetaFileObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="15e2572b-61cd-4cca-a8fa-f82c11c60272" /><field column="MetaFileObjectType" fieldcaption="MetaFileObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c6880452-cf2a-4dc7-a35d-17842b554428" /><field column="MetaFileGroupName" fieldcaption="MetaFileGroupName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4ea15bca-6d35-40b5-b81d-b6fe1f7cb2ec" /><field column="MetaFileName" fieldcaption="MetaFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d80f5285-6c32-482e-b5c7-e83c60bdd66e" /><field column="MetaFileExtension" fieldcaption="MetaFileExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7cc08f1f-f483-4fb3-84ef-4c0192c6dbe2" /><field column="MetaFileSize" fieldcaption="MetaFileSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="825039bf-d2cc-41dc-b805-52a3935baee7" /><field column="MetaFileMimeType" fieldcaption="MetaFileMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a2a2b992-e52f-4429-a0ab-948b69c2dc24" /><field column="MetaFileBinary" fieldcaption="MetaFileBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1f0a6168-494b-4a70-b0c0-1903995e5a76" /><field column="MetaFileImageWidth" fieldcaption="MetaFileImageWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ce204c6c-ffad-495b-a3f7-ac7252863b63" /><field column="MetaFileImageHeight" fieldcaption="MetaFileImageHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="883493f1-5fbd-42d9-aed7-92438680c0b8" /><field column="MetaFileGUID" fieldcaption="MetaFileGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="101bf156-b48c-4fa2-ad40-30daf0303393" /><field column="MetaFileLastModified" fieldcaption="MetaFileLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c1745a03-c620-4ebc-80b2-21f5ce2a85c4" /><field column="MetaFileSiteID" fieldcaption="MetaFileSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="63caffdb-de78-4f1a-aef2-fa565194f36d" /></form>', '20081229 09:07:12', 1527, N'cms.metafile', 0, 0, 0, '4b42d5a7-a5c9-4804-a25a-0aaee71ba138', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_MetaFile', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_MetaFile">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="MetaFileID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="MetaFileObjectID" type="xs:int" />
              <xs:element name="MetaFileObjectType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MetaFileGroupName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MetaFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MetaFileExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MetaFileSize" type="xs:int" />
              <xs:element name="MetaFileMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MetaFileBinary" type="xs:base64Binary" minOccurs="0" />
              <xs:element name="MetaFileImageWidth" type="xs:int" minOccurs="0" />
              <xs:element name="MetaFileImageHeight" type="xs:int" minOccurs="0" />
              <xs:element name="MetaFileGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="MetaFileLastModified" type="xs:dateTime" minOccurs="0" />
              <xs:element name="MetaFileSiteID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_MetaFile" />
      <xs:field xpath="MetaFileID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Messaging - Message', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="MessageID" fieldcaption="MessageID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d10cab69-17aa-460a-9e46-55b1ecc29c63" /><field column="MessageSenderUserID" fieldcaption="MessageSenderUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="09e1e4d8-9f62-4178-8bd1-78499f7a6a1e" /><field column="MessageSenderNickName" fieldcaption="MessageSenderNickName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="868f3d19-4631-46b6-85c9-cc2ab3a13e5d" /><field column="MessageRecipientUserID" fieldcaption="MessageRecipientUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="713b6789-5e57-4603-9d09-faf5c138437a" /><field column="MessageRecipientNickName" fieldcaption="MessageRecipientNickName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8a55702c-e2e2-4682-aa48-6bc39c3a55c5" /><field column="MessageSent" fieldcaption="MessageSent" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6921597c-e3f2-4120-880d-5e0f4d5035bf" /><field column="MessageSubject" fieldcaption="MessageSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="35ee1d32-ddc1-4e34-9d00-f5f564b01eec" /><field column="MessageBody" fieldcaption="MessageBody" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d74f1f55-8c02-40a9-9661-744de1dc64b0" /><field column="MessageRead" fieldcaption="MessageRead" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="30db0e2f-1ca1-4d33-a7fd-6bd61e8b42d4" /><field column="MessageSenderDeleted" fieldcaption="MessageSenderDeleted" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3fee4bc2-4467-4951-a6c8-ec886766735d" /><field column="MessageRecipientDeleted" fieldcaption="MessageRecipientDeleted" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01b7b11d-d0d3-4fbe-8683-e710d22a4682" /><field column="MessageGUID" fieldcaption="MessageGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5279f37b-0f30-435d-a1c7-2359dd4966eb" /><field column="MessageLastModified" fieldcaption="MessageLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e9808b50-db94-45ce-a5dd-c0feeae50c38" /></form>', '20091012 08:47:31', 1593, N'Messaging.Message', 0, 0, 0, '79e25187-258a-4c42-8849-da3dc3a03ccc', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Messaging_Message', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Messaging_Message">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="MessageID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="MessageSenderUserID" type="xs:int" minOccurs="0" />
              <xs:element name="MessageSenderNickName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageRecipientUserID" type="xs:int" minOccurs="0" />
              <xs:element name="MessageRecipientNickName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageSent" type="xs:dateTime" />
              <xs:element name="MessageSubject" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageBody">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MessageRead" type="xs:dateTime" minOccurs="0" />
              <xs:element name="MessageSenderDeleted" type="xs:boolean" minOccurs="0" />
              <xs:element name="MessageRecipientDeleted" type="xs:boolean" minOccurs="0" />
              <xs:element name="MessageGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="MessageLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Messaging_Message" />
      <xs:field xpath="MessageID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Object relationship', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="RelationshipLeftObjectID" fieldcaption="RelationshipLeftObjectID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="cd870be4-6274-4a42-9fa6-19a0a4cfef57" /><field column="RelationshipLeftObjectType" fieldcaption="RelationshipLeftObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="71716bc9-57a9-4553-9ffd-51debb299ea2" /><field column="RelationshipNameID" fieldcaption="RelationshipNameID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="696e81f8-3ed5-4ee5-ae5b-11e9d0d2d46c" /><field column="RelationshipRightObjectID" fieldcaption="RelationshipRightObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0e5b653d-88a9-41d1-8d3a-406a9b6ce23d" /><field column="RelationshipRightObjectType" fieldcaption="RelationshipRightObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c11228f-45a6-4cde-8afe-7221a6da741f" /><field column="RelationshipCustomData" fieldcaption="RelationshipCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="51d01890-8e3c-48c5-b9b0-aec0fe3c550e" /></form>', '20081229 09:07:13', 1594, N'CMS.ObjectRelationship', 0, 0, 0, '6e6c9ed8-b600-48d7-8e71-88886c6a0470', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_ObjectRelationship', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_ObjectRelationship">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="RelationshipLeftObjectID" type="xs:int" />
              <xs:element name="RelationshipLeftObjectType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RelationshipNameID" type="xs:int" />
              <xs:element name="RelationshipRightObjectID" type="xs:int" />
              <xs:element name="RelationshipRightObjectType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RelationshipCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_ObjectRelationship" />
      <xs:field xpath="RelationshipLeftObjectID" />
      <xs:field xpath="RelationshipLeftObjectType" />
      <xs:field xpath="RelationshipNameID" />
      <xs:field xpath="RelationshipRightObjectID" />
      <xs:field xpath="RelationshipRightObjectType" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Option category', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="CategoryID" fieldcaption="CategoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="2a21007d-55e7-447e-ac5b-891f3d50e815" /><field column="CategoryDisplayName" fieldcaption="CategoryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="23e8b92a-f166-486a-b0df-67da57a305b2" /><field column="CategoryName" fieldcaption="CategoryName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="d02ea635-6450-4362-89a2-dac7d42da241" /><field column="CategorySelectionType" fieldcaption="CategorySelectionType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="10aa4111-ca00-4787-8a56-e1723a49cb59" /><field column="CategoryDefaultOptions" fieldcaption="CategoryDefaultOptions" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="357d2463-d90c-41d8-86fd-3c9528bb3117" /><field column="CategoryDescription" fieldcaption="CategoryDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b20c1bd6-4f86-458a-a3b6-8995aec34f53" /><field column="CategoryDefaultRecord" fieldcaption="CategoryDefaultRecord" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="b97ed292-d585-4d99-976c-67684a61fab7" /><field column="CategoryEnabled" fieldcaption="CategoryEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a2d20f0e-7c4b-4ce1-bfbd-d8d0f36ebd86" /><field column="CategoryGUID" fieldcaption="CategoryGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c942aa89-ccdf-4b24-bfc3-4c38ac651109" /><field column="CategoryLastModified" fieldcaption="CategoryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e68a0390-00d2-41e3-819a-eeb7b65d5f0f" /><field column="CategoryDisplayPrice" fieldcaption="CategoryDisplayPrice" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f968f678-e508-4770-ba35-9a57d7997a8d" defaultvalue="true" /></form>', '20081229 09:07:01', 1596, N'ecommerce.optioncategory', 0, 0, 0, 'dda1df37-bc59-4541-a568-c69aec9d93fc', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_OptionCategory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_OptionCategory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CategoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CategoryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategorySelectionType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryDefaultOptions" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryDefaultRecord">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryEnabled" type="xs:boolean" />
              <xs:element name="CategoryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="CategoryLastModified" type="xs:dateTime" />
              <xs:element name="CategoryDisplayPrice" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_OptionCategory" />
      <xs:field xpath="CategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Shopping cart item', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="CartItemID" fieldcaption="CartItemID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ddd0b393-ea5e-46aa-89eb-16771b8249ed" /><field column="ShoppingCartID" fieldcaption="ShoppingCartID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="109dea76-7132-4052-8d95-8247d6dc94bc" /><field column="SKUID" fieldcaption="SKUID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8ccf5472-5f5c-4bab-bf88-9256e8dcd515" /><field column="SKUUnits" fieldcaption="SKUUnits" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="233f8ac2-2329-4188-8812-23aa0c4977f6" /><field column="CartItemCustomData" fieldcaption="CartItemCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c6af4828-879f-4203-8298-373e12011dca" /><field column="CartItemGuid" fieldcaption="CartItemGuid" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ff1bdf4f-ec66-4d98-9e55-26a5608bcff0" /><field column="CartItemParentGuid" fieldcaption="CartItemParentGuid" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ed4485db-5dcc-4deb-a1c6-f3749f10a89a" /></form>', '20090422 16:13:58', 1606, N'ecommerce.shoppingcartitem', 0, 0, 0, '936fda11-e521-4885-be89-a085f440ba4e', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_ShoppingCartSKU', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_ShoppingCartSKU">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CartItemID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ShoppingCartID" type="xs:int" />
              <xs:element name="SKUID" type="xs:int" />
              <xs:element name="SKUUnits" type="xs:int" />
              <xs:element name="CartItemCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CartItemGuid" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="CartItemParentGuid" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_ShoppingCartSKU" />
      <xs:field xpath="CartItemID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Settings key', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="KeyID" fieldcaption="KeyID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ac7779ce-7c76-40ad-9c17-b489b895b4a9" /><field column="KeyName" fieldcaption="KeyName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="efbde045-fadd-4730-8fa6-162653f115ae" /><field column="KeyDisplayName" fieldcaption="KeyDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="93a202e2-0a05-4cbc-bb24-4bb746e0161e" /><field column="KeyDescription" fieldcaption="KeyDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="831a5e01-d23e-497f-a3b4-abb2716d084f" /><field column="KeyValue" fieldcaption="KeyValue" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cd572f36-fab6-4ad3-8877-a34305ab3048" /><field column="KeyType" fieldcaption="KeyType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e32d876e-e786-482c-b263-d2a7dcd4215d" /><field column="KeyCategoryID" fieldcaption="KeyCategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5bf162ea-b507-4da2-b6a3-bb47f825c2a5" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="240d094e-b136-4330-a989-ed12135f49a2" /><field column="KeyGUID" fieldcaption="KeyGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6a1e6aa0-7b6a-4c8e-9292-22d58c69f9c1" /><field column="KeyLastModified" fieldcaption="KeyLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="713fe666-07c5-446b-b544-48a8205d8b31" /><field column="KeyOrder" fieldcaption="KeyOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="85d56e5e-9514-448c-8a52-618368567044" /><field column="KeyDefaultValue" fieldcaption="KeyDefaultValue" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8dbf84ce-f792-4223-a7b7-ca7ff1937384" /><field column="KeyValidation" fieldcaption="KeyValidation" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8e89cb6f-c568-4622-9d7b-2fd551d121d7" /><field column="KeyLoadGeneration" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6b4d0589-810b-49d1-8cb1-912b27c85df1" /><field column="KeyIsGlobal" visible="false" defaultvalue="false" columntype="boolean" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8e638167-3952-467a-b074-3f97e18a6a77" visibility="none" ismacro="false" /></form>', '20091026 14:34:13', 1611, N'CMS.SettingsKey', 0, 0, 0, 'ec796166-5adf-43fa-9508-c9791db1b6dd', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_SettingsKey', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_SettingsKey">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="KeyID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="KeyName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyValue" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyCategoryID" type="xs:int" minOccurs="0" />
              <xs:element name="SiteID" type="xs:int" minOccurs="0" />
              <xs:element name="KeyGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="KeyLastModified" type="xs:dateTime" />
              <xs:element name="KeyOrder" type="xs:int" minOccurs="0" />
              <xs:element name="KeyDefaultValue" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyValidation" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="255" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyEditingControlPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="KeyLoadGeneration" type="xs:int" />
              <xs:element name="KeyIsGlobal" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_SettingsKey" />
      <xs:field xpath="KeyID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Export - history', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ExportID" fieldcaption="ExportID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="149cdd85-2558-4c2a-9ffe-6801a4effe1c" /><field column="ExportDateTime" fieldcaption="ExportDateTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f558feb5-f0f3-4cae-8bca-e0f2af7099f6" /><field column="ExportFileName" fieldcaption="ExportFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0377b17d-6321-4169-a4b0-a34a4e683a6c" /><field column="ExportSiteID" fieldcaption="ExportSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="da436881-5b56-45a2-bea4-3e4a8bd2d36c" /><field column="ExportUserID" fieldcaption="ExportUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b78c3e62-84bd-4068-b18c-6b79fdaba365" /><field column="ExportSettings" fieldcaption="ExportSettings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="78ffe32e-8ea4-4cea-a3ef-e475a55dfc96" /></form>', '20090218 19:17:53', 1616, N'export.history', 0, 0, 0, 'ab5857eb-879b-422c-82ba-0acc49df79a2', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Export_History', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Export_History">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ExportID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ExportDateTime" type="xs:dateTime" />
              <xs:element name="ExportFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ExportSiteID" type="xs:int" />
              <xs:element name="ExportUserID" type="xs:int" />
              <xs:element name="ExportSettings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Export_History" />
      <xs:field xpath="ExportID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Export - task', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="TaskID" fieldcaption="TaskID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="63d312c3-c5a7-43ff-8574-84125264f844" /><field column="TaskSiteID" fieldcaption="TaskSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8c5930e5-9b1c-45f1-9ad9-c35581eed7c7" /><field column="TaskTitle" fieldcaption="TaskTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="15588e8b-2e40-4481-a023-91d383fc7761" /><field column="TaskData" fieldcaption="TaskData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fdc52e2c-fadd-4090-b576-5d22596feb75" /><field column="TaskTime" fieldcaption="TaskTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="928995c4-1a63-4615-95b8-18d7ef928ee6" /><field column="TaskType" fieldcaption="TaskType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d533781a-cdde-43b8-aadb-058cf9d3d476" /><field column="TaskObjectType" fieldcaption="TaskObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9d58620e-cbc1-4db1-a17f-7c4920989286" /><field column="TaskObjectID" fieldcaption="TaskObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e5f956e9-cfbe-4668-8143-afe634d44d76" /></form>', '20091012 07:43:05', 1617, N'export.task', 0, 0, 0, '5038ed34-d6c2-4598-b10b-d51cb6f8945b', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Export_Task', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Export_Task">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaskID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TaskSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="TaskTitle">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskData">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskTime" type="xs:dateTime" />
              <xs:element name="TaskType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskObjectType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskObjectID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Export_Task" />
      <xs:field xpath="TaskID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'CSS stylesheet site', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="StylesheetID" fieldcaption="StylesheetID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="fcd83ef9-440c-4e12-a296-e397203420e5" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="67a1ad4c-a0be-4254-9bb9-ced08b9f7bfc" /></form>', '20081231 14:54:23', 1629, N'CMS.CSSStylesheetSite', 0, 0, 0, 'f5c63ca9-1ac4-4d41-8977-d129c00d9019', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_CssStylesheetSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_CssStylesheetSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StylesheetID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_CssStylesheetSite" />
      <xs:field xpath="StylesheetID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'InlineControlSite', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ControlID" fieldcaption="ControlID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="6bd20653-e9ac-448a-8dd8-16b0dd778ae0" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f95ecd72-a16b-4293-adac-3972773154a9" /></form>', '20081229 09:07:09', 1630, N'CMS.InlineControlSite', 0, 0, 0, 'abff72a6-3558-4bba-bb86-7117e9ca8525', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_InlineControlSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_InlineControlSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ControlID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_InlineControlSite" />
      <xs:field xpath="ControlID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'PageTemplateSite', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="PageTemplateID" fieldcaption="PageTemplateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="b03c6f39-3d55-477b-8b7e-dffd2f64fba3" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9c557e72-06ed-423b-aa10-09a543d97976" /></form>', '20081229 09:07:15', 1631, N'CMS.PageTemplateSite', 0, 0, 0, '64b959d5-ca01-4aab-a467-ad473346e040', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_PageTemplateSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_PageTemplateSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PageTemplateID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_PageTemplateSite" />
      <xs:field xpath="PageTemplateID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'ResourceSite', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ResourceID" fieldcaption="ResourceID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e1cca274-6aa7-4034-86b9-5ee02af2952b" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="436efd78-8b1d-49cd-b073-4024a9c69b0f" /></form>', '20081229 09:07:22', 1632, N'CMS.ResourceSite', 0, 0, 0, 'b97b7997-ea51-4c6e-898a-3091106ba5ad', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_ResourceSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_ResourceSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ResourceID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_ResourceSite" />
      <xs:field xpath="ResourceID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Culture site', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="759423f2-9e97-4a8d-b32b-657a669f1014" /><field column="CultureID" fieldcaption="CultureID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3e2f0e62-9076-4d68-be90-3898671e094c" /></form>', '20081231 14:52:18', 1633, N'CMS.CultureSite', 0, 0, 0, '2b81bbf8-bf34-465b-ae17-32089398d076', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_SiteCulture', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_SiteCulture">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SiteID" type="xs:int" />
              <xs:element name="CultureID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_SiteCulture" />
      <xs:field xpath="SiteID" />
      <xs:field xpath="CultureID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'UserSite', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e470c61e-0451-4341-b811-6ef885da1b5a" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5e78e5d-f413-4b81-8d7b-d3e1c17125f2" /></form>', '20081229 09:07:40', 1634, N'CMS.UserSite', 0, 0, 0, 'f41cb287-9535-4707-a557-0e2806a9f682', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_UserSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_UserSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UserID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_UserSite" />
      <xs:field xpath="UserID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'WorkflowStepRole', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="StepID" fieldcaption="StepID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="b3b646b4-a9da-47de-9a80-2afa11060e74" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="76e118eb-ac03-40a2-8551-36079bbecde7" /></form>', '20081229 09:07:41', 1635, N'CMS.WorkflowStepRole', 0, 0, 0, '6b32cc52-df22-44f5-a0e8-1a227f31689d', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_WorkflowStepRoles', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WorkflowStepRoles">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StepID" type="xs:int" />
              <xs:element name="RoleID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WorkflowStepRoles" />
      <xs:field xpath="StepID" />
      <xs:field xpath="RoleID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Discount level department', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="DiscountLevelID" fieldcaption="DiscountLevelID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="c3d48dbf-6cfc-4108-8c2a-9b858772c219" /><field column="DepartmentID" fieldcaption="DepartmentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3366ade1-3d2b-45fb-abb8-f18ff869213a" /></form>', '20081231 14:54:38', 1636, N'ECommerce.DiscountLevelDepartment', 0, 0, 0, '61481a6d-d6ee-4076-acee-a7dea19d36ce', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_DiscountLevelDepartment', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_DiscountLevelDepartment">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DiscountLevelID" type="xs:int" />
              <xs:element name="DepartmentID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_DiscountLevelDepartment" />
      <xs:field xpath="DiscountLevelID" />
      <xs:field xpath="DepartmentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
set identity_insert [cms_class] off
