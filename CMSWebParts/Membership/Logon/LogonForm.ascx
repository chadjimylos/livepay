<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Logon/LogonForm.ascx.cs"
    Inherits="CMSWebParts_Membership_Logon_LogonForm" %>
<asp:Panel ID="pnlBody" runat="server" CssClass="LogonPageBackground">
    <table class="DialogPosition">
        <tr style="vertical-align: middle;">
            <td>
                <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Default.aspx">
                    <LayoutTemplate>
                        <asp:Panel runat="server" ID="pnlLogin" DefaultButton="LoginButton">
                            <table border="0">
                                <tr>
                                    <td class="TopLeftCorner">
                                    </td>
                                    <td class="TopMiddleBorder">
                                    </td>
                                    <td class="TopRightCorner">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="LogonDialog">
                                        <table border="0" align="center" width="100%" style="border-collapse: separate;">
                                            <tr>
                                                <td nowrap="nowrap">
                                                    <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName"
                                                        EnableViewState="false" />
                                                </td>
                                                <td nowrap="nowrap">
                                                    <asp:TextBox ID="UserName" runat="server" MaxLength="100" CssClass="LogonTextBox" />
                                                    <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                                                        ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1"
                                                        EnableViewState="false">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap="nowrap">
                                                    <cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password"
                                                        EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" MaxLength="110" CssClass="LogonTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="left" nowrap="nowrap">
                                                    <cms:LocalizedCheckBox ID="chkRememberMe" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="color: red">
                                                    <cms:LocalizedLiteral ID="FailureText" runat="server" EnableViewState="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td align="left">
                                                    <cms:LocalizedButton ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="Login1"
                                                        EnableViewState="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </LayoutTemplate>
                </asp:Login>
            </td>
        </tr>
        <tr>
            <td>
                <asp:LinkButton ID="lnkPasswdRetrieval" runat="server" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlPasswdRetrieval" runat="server" CssClass="LoginPanelPasswordRetrieval"
                    DefaultButton="btnPasswdRetrieval">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblPasswdRetrieval" runat="server" EnableViewState="false" AssociatedControlID="txtPasswordRetrieval" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPasswordRetrieval" runat="server" />
                                <cms:CMSButton ID="btnPasswdRetrieval" runat="server" ValidationGroup="PsswdRetrieval"
                                    EnableViewState="false" /><br />
                                <asp:RequiredFieldValidator ID="rqValue" runat="server" ControlToValidate="txtPasswordRetrieval"
                                    ValidationGroup="PsswdRetrieval" EnableViewState="false" />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblResult" runat="server" Visible="false" EnableViewState="false" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />
