using System;
using System.Web.UI;
using System.Web;
using System.Web.Security;

using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.MembershipProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;
using System.Data;

/// <summary>
/// This page handles the login, logout and clearcookie Web Auth
/// actions.  When you create a Windows Live application, you must
/// specify the URL of this handler page.
/// </summary>
public partial class CMSPages_LiveIDLogin : CMSPage
{
    #region "Private fields"

    private static string defaultPage = UrlHelper.ResolveUrl("~/Default.aspx");
    private static string loginPage = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSecuredAreasLogonPage");
    private static string logoutPage = defaultPage;

    private const string liveCookieName = "webauthtoken";

    #endregion


    #region "Methods"

    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.WindowsLiveID);

        // Sitename must be set
        if (string.IsNullOrEmpty(CMSContext.CurrentSiteName))
        {
            return;
        }

        string siteName = CMSContext.CurrentSiteName;

        // Get LiveID settings
        string appId = SettingsKeyProvider.GetStringValue(siteName + ".CMSApplicationID");
        string secret = SettingsKeyProvider.GetStringValue(siteName + ".CMSApplicationSecret");
        string algorithm = SettingsKeyProvider.GetStringValue(siteName + ".CMSsecurityAlgorithm");

        // Check valid Windows LiveID parameters
        if ((appId == string.Empty) || (secret == string.Empty) || (algorithm == string.Empty))
        {
            return;
        }

        WindowsLiveLogin wll = new WindowsLiveLogin(appId, secret, algorithm);

        // Extract the 'action' parameter from the request, if any.
        string action = QueryHelper.GetString("action", "");

        /*
          If action is 'logout', clear the login cookie and redirect
          to the logout page.

          If action is 'clearcookie', clear the login cookie and
          return a GIF as response to signify success.

          By default, try to process a login. If login was
          successful, cache the user token in a cookie and redirect
          to the site's main page.  If login failed, clear the cookie
          and redirect to the main page.
        */

        if (action == "logout")
        {
            // Validate logout reguest, must not be older than one minute
            if ((Session != null) && (ValidationHelper.GetDateTime(Session["liveidlogout"], DateTime.MinValue).AddMinutes(1) > DateTime.Now))
            {
                ClearLiveCookie();

                // Clear session parameter
                Session.Remove("liveidlogout");

                // Redirect to logout page
                UrlHelper.Redirect(logoutPage);
            }
        }
        else if (action == "clearcookie")
        {
            // Validate logout reguest, must not be older than one minute
            if ((Session != null) && (ValidationHelper.GetDateTime(Session["liveidlogout"], DateTime.MinValue).AddMinutes(1) > DateTime.Now))
            {
                ClearLiveCookie();

                string type;
                byte[] content;
                wll.GetClearCookieResponse(out type, out content);
                Response.ContentType = type;
                Response.OutputStream.Write(content, 0, content.Length);
            }
        }
        else
        {
            // Process Windows Live login
            WindowsLiveLogin.User user = wll.ProcessLogin(Request.Form);

            if (user != null)
            {
                // Check whether additional user info page is set
                string additionalInfoPage = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSLiveIDRequiredUserDataPage");

                // No page set, user can be created/sign
                if (additionalInfoPage == String.Empty)
                {
                    // Check if IP address is not banned for possible registration
                    bool create = BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.Registration);

                    // Authenticate user
                    UserInfo ui = UserInfoProvider.AuthenticateWindowsLiveUser(user.Id, siteName, create);

                    // If user was found or successfuly created
                    if (ui != null)
                    {
                        if (ui.Enabled)
                        {
                            // Ban IP addresses which are blocked for login
                            BannedIPInfoProvider.CheckIPandRedirect(CMSContext.CurrentSiteName, BanControlEnum.Login);

                            SetAuthCookieAndRedirect(ui, user);
                        }
                        else
                        {
                            ClearCookieAndRedirect();
                        }
                    }                     
                    // Must be banned for registration, otherwise would be created
                    else
                    {
                        BannedIPInfoProvider.CheckIPandRedirect(CMSContext.CurrentSiteName, BanControlEnum.Registration);
                        ClearCookieAndRedirect();
                    }
                }
                else
                {
                    // Authenticate user, but only existing
                    UserInfo ui = UserInfoProvider.AuthenticateWindowsLiveUser(user.Id, siteName, false);

                    // If user was found 
                    if ((ui != null) && (ui.Enabled))
                    {
                        // Ban IP addresses which are blocked for login
                        BannedIPInfoProvider.CheckIPandRedirect(CMSContext.CurrentSiteName, BanControlEnum.Login);

                        SetAuthCookieAndRedirect(ui, user);
                    }
                    // Insufficient rights or new live ID user - redirect to special page
                    else
                    {
                        // Try find user with this liveid
                        DataSet ds = UserSettingsInfoProvider.GetUserSettings("[WindowsLiveId] = '" + user.Id + "'", null);

                        // If not empty means insufficient rights
                        if (!DataHelper.DataSourceIsEmpty(ds))
                        {
                            ClearCookieAndRedirect();
                        }
                        // New liveid user
                        else
                        {
                            // Store user object in session for additional info page
                            Session["windowsliveloginuser"] = user;

                            // Redirect to additional info page
                            UrlHelper.Redirect(UrlHelper.ResolveUrl(additionalInfoPage));
                        }
                    }
                }
            }
            // Invalid login
            else
            {
                ClearCookieAndRedirect();
            }
        }

        RequestHelper.EndResponse();
    }


    /// <summary>
    /// Clear live id cookies and redirect to the logogn page
    /// </summary>
    private void ClearCookieAndRedirect()
    {
        ClearLiveCookie();

        // Redirect to login page
        UrlHelper.Redirect(loginPage);
    }


    /// <summary>
    /// Clears Live authentication cookie
    /// </summary>
    private void ClearLiveCookie()
    {
        HttpCookie loginCookie = new HttpCookie(liveCookieName);
        loginCookie.Secure = true;
        loginCookie.Expires = DateTime.Now.AddYears(-10);

        Response.Cookies.Add(loginCookie);
    }


    /// <summary>
    /// Helper method, set authentication cookie and redirect to return URL or default page
    /// </summary>
    /// <param name="ui">User info</param>
    /// <param name="user">Windows live user</param>
    private void SetAuthCookieAndRedirect(UserInfo ui, WindowsLiveLogin.User user)
    {
        // Create autentification cookie
        UserInfoProvider.SetAuthCookieWithUserData(ui.UserName, user.UsePersistentCookie, Session.Timeout, new string[] { "liveidlogin" });
        UserInfoProvider.SetPreferredCultures(ui);

        // If there is some return url redirect there
        if (!string.IsNullOrEmpty(user.Context))
        {
            UrlHelper.Redirect(user.Context);
        }
        else // Redirect to default page
        {           
            UrlHelper.Redirect(defaultPage);
        }
    }

    #endregion
}
