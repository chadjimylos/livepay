using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.Controls;
using CMS.FileManager;
using CMS.WorkflowEngine;
using CMS.TreeEngine;
using CMS.ExtendedControls;

using TreeNode = CMS.TreeEngine.TreeNode;


public partial class CMSModules_Content_Controls_Dialogs_LinkMediaSelector_MediaView : MediaView
{
    #region "Private variables"

    private int mNodeParentNodeId = 0;
    private TreeProvider mTree = null;
    private SiteInfo mSiteObj = null;
    private TreeNode mTreeNodeObj = null;

    protected string mSaveText = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the value which determineds whtether to show the Parent button or not.
    /// </summary>
    public bool ShowParentButton
    {
        get
        {
            return this.plcParentButton.Visible;
        }
        set
        {
            this.plcParentButton.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets a view mode used to display files.
    /// </summary>
    public override DialogViewModeEnum ViewMode
    {
        get
        {
            return base.ViewMode;
        }
        set
        {
            base.ViewMode = value;
            this.innermedia.ViewMode = value;
        }
    }
    
    /// <summary>
    /// Gets or sets the OutputFormat (needed for correct dialog type reckognition)
    /// </summary>
    public OutputFormatEnum OutputFormat
    {
        get
        {
            return this.innermedia.OutputFormat;
        }
        set
        {
            this.innermedia.OutputFormat = value;
        }
    }


    /// <summary>
    /// Gets or sets text of the information label.
    /// </summary>
    public string InfoText
    {
        get
        {
            return this.innermedia.InfoText;
        }
        set
        {
            this.innermedia.InfoText = value;
        }
    }


    /// <summary>
    /// Gets currently selected page size.
    /// </summary>
    public int CurrentTopN 
    {
        get 
        {
            return this.innermedia.CurrentTopN;
        }
    }


    /// <summary>
    /// Gets or sets ID of the parent node.
    /// </summary>
    public int AtachmentNodeParentID
    {
        get
        {
            return this.mNodeParentNodeId;
        }
        set
        {
            this.mNodeParentNodeId = value;
        }
    }


    /// <summary>
    /// Gets or sets ID of the parent of the curently selected node.
    /// </summary>
    public int NodeParentID
    {
        get
        {
            return ValidationHelper.GetInteger(this.hdnLastNodeParentID.Value, 0);
        }
        set
        {
            this.hdnLastNodeParentID.Value = value.ToString();
        }
    }


    /// <summary>
    /// Gets tree provider for the current user.
    /// </summary>
    private TreeProvider Tree
    {
        get
        {
            if (this.mTree == null)
            {
                this.mTree = new TreeProvider(CMSContext.CurrentUser);
            }
            return this.mTree;
        }
    }


    /// <summary>
    /// Gets the node attachments are related to.
    /// </summary>
    public TreeNode TreeNodeObj
    {
        get
        {
            return mTreeNodeObj;
        }
        set
        {
            mTreeNodeObj = value;
            this.innermedia.TreeNodeObj = value;
        }
    }


    /// <summary>
    /// Gets the site attachments are related to.
    /// </summary>
    public SiteInfo SiteObj
    {
        get
        {
            if (mSiteObj == null)
            {
                if (this.TreeNodeObj != null)
                {
                    mSiteObj = SiteInfoProvider.GetSiteInfo(this.TreeNodeObj.NodeSiteID);
                }
                else
                {
                    mSiteObj = CMSContext.CurrentSite;
                }
            }
            return mSiteObj;
        }
        set 
        {
            this.mSiteObj = value;
        }
    }


    /// <summary>
    /// Indicates whether the content tree is displaying more than max tree nodes.
    /// </summary>
    public bool IsFullListingMode
    {
        get
        {
            return this.innermedia.IsFullListingMode;
        }
        set
        {
            this.innermedia.IsFullListingMode = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // If processing the request should not continue
        if (this.StopProcessing)
        {
            this.Visible = false;
        }
        else
        {
            this.Visible = true;

            // Initialize controls
            SetupControls();
        }
    }


    /// <summary>
    /// Loads control's content.
    /// </summary>
    public void Reload()
    {
        // Initialize controls
        SetupControls();

        ReloadData();
    }


    /// <summary>
    /// Displays listing info message.
    /// </summary>
    /// <param name="infoMsg">Info message to display.</param>
    public void DisplayListingInfo(string infoMsg) 
    {
        if (!string.IsNullOrEmpty(infoMsg))
        {
            this.plcListingInfo.Visible = true;
            this.lblListingInfo.Text = infoMsg;
        }
    }


    #region "Private methods"

    /// <summary>
    /// Initializes all nested controls.
    /// </summary>
    private void SetupControls()
    {
        InitializeControlScripts();

        // Initialize inner view control
        this.innermedia.ViewMode = this.ViewMode;
        this.innermedia.DataSource = this.DataSource;
        this.innermedia.SelectableContent = this.SelectableContent;
        this.innermedia.SourceType = this.SourceType;
        this.innermedia.IsLiveSite = this.IsLiveSite;
        this.innermedia.NodeParentID = this.AtachmentNodeParentID;

        this.innermedia.ResizeToHeight = this.ResizeToHeight;
        this.innermedia.ResizeToMaxSideSize = this.ResizeToMaxSideSize;
        this.innermedia.ResizeToWidth = this.ResizeToWidth;

        // Set grid definition according source type
        string gridName = "";
        if (this.SourceType == MediaSourceEnum.DocumentAttachments)
        {
            gridName = "~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/AttachmentsListView.xml";
        }
        else
        {
            if ((this.OutputFormat == OutputFormatEnum.HTMLLink) || (this.OutputFormat == OutputFormatEnum.BBLink))
            {
                gridName = "~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/ContentListView_Link.xml";
            }
            else
            {
                gridName = "~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/ContentListView.xml";
            }
        }
        this.innermedia.ListViewControl.GridName = gridName;
        ((CMSAdminControls_UI_UniGrid_UniGrid)(innermedia.ListViewControl)).OnPageChanged += new EventHandler<EventArgs>(ListViewControl_OnPageChanged);

        // Set inner control binding columns
        this.innermedia.FileIdColumn = "AttachmentGUID";
        this.innermedia.FileNameColumn = (this.SourceType == MediaSourceEnum.DocumentAttachments) ? "AttachmentName" : "DocumentName";
        this.innermedia.FileExtensionColumn = "AttachmentExtension";
        this.innermedia.FileSizeColumn = "AttachmentSize";
        this.innermedia.FileWidthColumn = "AttachmentImageWidth";
        this.innermedia.FileHeightColumn = "AttachmentImageHeight";

        // Register for inner media events
        this.innermedia.GetArgumentSet += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetArgumentSet(innermedia_GetArgumentSet);
        this.innermedia.GetListItemUrl += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetListItemUrl(innermedia_GetListItemUrl);
        this.innermedia.GetTilesThumbsItemUrl += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetTilesThumbsItemUrl(innermedia_GetTilesThumbsItemUrl);

        // Parent directory button
        if ((this.SourceType == MediaSourceEnum.Content) && this.ShowParentButton)
        {
            this.plcParentButton.Visible = true;
            this.imgParent.ImageUrl = GetImageUrl("Design/Controls/Dialogs/parent.png");
            this.mSaveText = ResHelper.GetString("dialogs.mediaview.parentdocument");
            this.btnParent.OnClientClick = "SetParentAction('" + this.NodeParentID + "'); return false;";
        }
    }


    /// <summary>
    /// Initializes scrips used by the control.
    /// </summary>
    private void InitializeControlScripts()
    {
        string script = @"
                            function SetSelectAction(argument) {
                                // Raise select action
                                SetAction('select', argument);
                                RaiseHiddenPostBack();
                            }
                            function SetParentAction(argument) {
                                // Raise select action
                                SetAction('parentselect', argument);
                                RaiseHiddenPostBack();
                            }";

        ScriptHelper.RegisterStartupScript(this, this.GetType(), "DialogsSelectAction", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Loads data from data source property.
    /// </summary>
    private void ReloadData()
    {
        this.innermedia.Reload(true);
    }

    #endregion


    #region "Inner media view event handlers"

    /// <summary>
    /// Returns argument set according passed DataRow and flag indicating whether the set is obtained for selected item.
    /// </summary>
    /// <param name="dr">DataRow with all the item data.</param>
    /// <param name="isSelected">Indicates whether the set is required for an selected item.</param>
    string innermedia_GetArgumentSet(DataRow dr)
    {
        // Return required argument set
        return GetArgumentSet(dr);
    }


    string innermedia_GetListItemUrl(DataRow dr, bool isPreview, bool notAttachment)
    {
        // Get set of important information
        string arg = GetArgumentSet(dr);

        // Get URL of the list item image
        return GetItemUrl(arg, 0, 0, 0, notAttachment);
    }


    string innermedia_GetTilesThumbsItemUrl(DataRow dr, bool isPreview, int height, int width, int maxSideSize, bool notAttachment)
    {
        string url = "";

        string ext = dr["AttachmentExtension"].ToString();
        string arg = GetArgumentSet(dr);

        // If image is requested for preview
        if (isPreview)
        {
            if (!ImageHelper.IsImage(ext) || notAttachment)
            {
                string className = (this.SourceType == MediaSourceEnum.Content) ? dr["ClassName"].ToString().ToLower() : "";
                if (className == "cms.file")
                {
                    // File isn't image and no preview exists - get default file icon
                    url = GetFileIconUrl(ext, "");
                }
                else
                {
                    url = GetDocumentTypeIconUrl(className, "48x48");
                }
            }
            else
            {
                // Try to get preview or image itself
                url = GetItemUrl(arg, height, width, maxSideSize, notAttachment);
            }
        }
        else
        {
            url = GetItemUrl(arg, 0, 0, 0, notAttachment);
        }

        return url;
    }


    void ListViewControl_OnPageChanged(object sender, EventArgs e)
    {
        RaiseListReloadRequired();
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns argument set for the passed file data row.
    /// </summary>
    /// <param name="dr">Data row object holding all the data on current file.</param>
    public string GetArgumentSet(DataRow dr)
    {
        string name = (this.SourceType == MediaSourceEnum.DocumentAttachments) ?
            AttachmentHelper.GetFullFileName(Path.GetFileNameWithoutExtension(dr["AttachmentName"].ToString()), dr["AttachmentExtension"].ToString()) : dr["DocumentName"].ToString();

        // Common information for both content & attachments
        string result = name + "|" + dr["AttachmentExtension"].ToString() + "|" + dr["AttachmentImageWidth"].ToString() + "|" +
               dr["AttachmentImageHeight"].ToString() + "|";

        // Get source type specific information
        if (this.SourceType == MediaSourceEnum.Content)
        {
            result += dr["NodeSiteID"].ToString() + "|" + dr["SiteName"].ToString() + "|" + dr["NodeGUID"].ToString() + "|" + dr["NodeID"].ToString() + "|" +
                dr["DocumentUrlPath"].ToString() + "|" + dr["NodeAlias"].ToString() + "|" + dr["NodeAliasPath"].ToString() + "|" +
                dr["AttachmentGUID"].ToString();
        }
        else
        {
            string formGuid = dr.Table.Columns.Contains("AttachmentFormGUID") ? dr["AttachmentFormGUID"].ToString() : Guid.Empty.ToString();
            string siteId = dr.Table.Columns.Contains("AttachmentSiteID") ? dr["AttachmentSiteID"].ToString() : "0";

            result += dr["AttachmentGUID"].ToString() + "|" + siteId + "|" + formGuid + "|" + dr["AttachmentDocumentID"].ToString();
        }

        result += "|" + dr["AttachmentSize"].ToString();

        return result;
    }


    /// <summary>
    /// Returns URL of the media item according site settings.
    /// </summary>
    /// <param name="argument">Argument containing information on current media item.</param>
    /// <param name="maxSideSize">Maximum dimension for images displayed for tile and thumbnails view.</param>
    public string GetItemUrl(string argument, int height, int width, int maxSideSize, bool notAttachment)
    {
        string[] argArr = argument.Split('|');
        if (argArr.Length >= 2)
        {
            string url = "";

            // Get image URL
            if (this.SourceType == MediaSourceEnum.Content)
            {
                // Get information from argument
                Guid nodeGuid = ValidationHelper.GetGuid(argArr[6], Guid.Empty);
                string documentUrlPath = argArr[8];
                string nodeAlias = argArr[9];
                string nodeAliasPath = argArr[10];

                // Get content item URL
                url = GetContentItemUrl(nodeGuid, documentUrlPath, nodeAlias, nodeAliasPath, height, width, maxSideSize, notAttachment);
            }
            else
            {
                // Get information from argument
                Guid attachmentGuid = ValidationHelper.GetGuid(argArr[4], Guid.Empty);
                string attachmentName = argArr[0];
                string nodeAliasPath = "";
                if (this.TreeNodeObj != null)
                {
                    nodeAliasPath = this.TreeNodeObj.NodeAliasPath;
                }

                // Get item URL                
                url = GetAttachmentItemUrl(attachmentGuid, attachmentName, nodeAliasPath, height, width, maxSideSize);
            }

            return url;
        }

        return "";
    }


    /// <summary>
    /// Ensures no item is selected.
    /// </summary>
    public void ResetSearch()
    {
        this.dialogSearch.ResetSearch();
    }


    /// <summary>
    /// Ensures first page is displayed in the control displaying the content.
    /// </summary>
    public void ResetPageIndex()
    {
        this.innermedia.ResetPageIndex();
    }


    /// <summary>
    /// Ensure no item is selected in list view
    /// </summary>
    public void ResetListSelection()
    {
        this.innermedia.ResetListSelection();
    }

    #endregion


    #region "Content methods"

    /// <summary>
    /// Returns URL of the media item according site settings.
    /// </summary>
    /// <param name="nodeGuid">Node GUID of the current attachment node.</param>    
    /// <param name="documentUrlPath">URL path of the current attachment document.</param>
    /// <param name="maxSideSize">Maximum dimension for images displayed for tile and thumbnails view.</param>
    /// <param name="nodeAlias">Node alias of the current attachment node.</param>
    /// <param name="nodeAliasPath">Node alias path of the current attachment node.</param>    
    public string GetContentItemUrl(Guid nodeGuid, string documentUrlPath, string nodeAlias, string nodeAliasPath, int height, int width, int maxSideSize, bool notAttachment)
    {
        string result = "";

        // Generate URL
        if (this.UsePermanentUrls)
        {
            bool isLink = (this.OutputFormat == OutputFormatEnum.BBLink || this.OutputFormat == OutputFormatEnum.HTMLLink) ||
                (this.OutputFormat == OutputFormatEnum.URL && this.SelectableContent == SelectableContentEnum.AllContent);
            if (String.IsNullOrEmpty(nodeAlias))
            {
                nodeAlias = "default";
            }
            result = ((notAttachment || isLink) ? TreePathUtils.GetPermanentDocUrl(nodeGuid, nodeAlias, this.SiteObj.SiteName) : AttachmentManager.GetPermanentAttachmentUrl(nodeGuid, nodeAlias));
        }
        else
        {
            result = CMSContext.GetUrl(nodeAliasPath, documentUrlPath);
        }

        // Make URL absolute if required
        if (this.Config.UseFullURL || (CMSContext.CurrentSiteID != this.SiteObj.SiteID) || (CMSContext.CurrentSiteID != base.GetCurrentSiteId()))
        {
            result = UrlHelper.GetAbsoluteUrl(result, this.SiteObj.DomainName, UrlHelper.GetApplicationUrl(this.SiteObj.DomainName), null);
        }

        // Image dimensions to URL
        if (maxSideSize > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "maxsidesize", maxSideSize.ToString());
        }
        if (height > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "height", height.ToString());
        }
        if (width > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "width", width.ToString());
        }

        // Media selctor should returns non-resolved URL in all cases
        bool isMediaSelector = (this.OutputFormat == OutputFormatEnum.URL) && (this.SelectableContent == SelectableContentEnum.OnlyMedia);

        return (isMediaSelector ? result : UrlHelper.ResolveUrl(result));
    }

    #endregion


    #region "Attachment methods"

    /// <summary>
    /// Returns URL for the attachment specified by arguments.
    /// </summary>
    /// <param name="attachmentGuid">GUID of the attachment.</param>
    /// <param name="attachmentName">Name of the attachment.</param>
    /// <param name="attachmentNodeAlias"></param>
    /// <param name="maxSideSize">Maximum size of the item if attachment is image.</param>
    public string GetAttachmentItemUrl(Guid attachmentGuid, string attachmentName, string attachmentNodeAlias, int height, int width, int maxSideSize)
    {
        string result = "";

        if (this.UsePermanentUrls || string.IsNullOrEmpty(attachmentNodeAlias))
        {
            result = AttachmentManager.GetAttachmentUrl(attachmentGuid, attachmentName);
        }
        else
        {
            //string safeFileName = UrlHelper.GetSafeFileName(attachmentName, attachmentSite.SiteName);
            string safeFileName = UrlHelper.GetSafeFileName(attachmentName, this.SiteObj.SiteName);

            result = AttachmentManager.GetAttachmentUrl(safeFileName, attachmentNodeAlias);
        }

        // If current site is different from attachment site make URL absolute (domain included)
        if (this.Config.UseFullURL || (CMSContext.CurrentSiteID != this.SiteObj.SiteID) || (CMSContext.CurrentSiteID != base.GetCurrentSiteId()))
        {
            result = UrlHelper.GetAbsoluteUrl(result, this.SiteObj.DomainName, UrlHelper.GetApplicationUrl(this.SiteObj.DomainName), null);
        }


        // If image dimensions are specified
        if (maxSideSize > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "maxsidesize", maxSideSize.ToString());
        }
        if (height > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "height", height.ToString());
        }
        if (width > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "width", width.ToString());
        }

        // Media selctor should returns non-resolved URL in all cases
        bool isMediaSelector = (this.OutputFormat == OutputFormatEnum.URL) && (this.SelectableContent == SelectableContentEnum.OnlyMedia);

        return (isMediaSelector ? result : UrlHelper.ResolveUrl(result));
    }

    #endregion
}
