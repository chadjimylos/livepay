<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumGroupSelector.ascx.cs" Inherits="CMSModules_Forums_FormControls_ForumGroupSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>



<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" RenderMode="InLine">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ObjectType="Forums.ForumGroup" SelectionMode="SingleDropDownList"  AllowEditTextBox="true" />        
    </ContentTemplate>
</cms:CMSUpdatePanel>
