using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSDesk_MyDesk_RecycleBin_RecycleBin : CMSMyDeskPage
{
    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check UIProfile
        if ((CMSContext.CurrentUser == null) || (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.MyDesk", "MyRecycleBin")))
        {
            RedirectToUINotAvailable();
        }
        recycleBin.SiteID = CMSContext.CurrentSiteID;

        if (!IsCallback)
        {
            // Setup page title text and image
            CurrentMaster.Title.TitleText = ResHelper.GetString("MyDesk.RecycleBinTitle");
            CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_MyDesk/RecycleBin/module.png");

            CurrentMaster.Title.HelpTopicName = "my_recycle_bin";
            CurrentMaster.Title.HelpName = "helpTopic";
        }
    }

    #endregion
}
