set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Message board moderator', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="BoardID" fieldcaption="BoardID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="18895a79-d9a6-4118-b059-55271cff6483" /><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9451413d-6868-4575-9a2a-697220bf8385" /></form>', '20081229 09:07:11', 1804, N'board.moderator', 0, 0, 0, '4df3a8d5-672c-4ada-b4e4-e5731c2ec5de', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Board_Moderator', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Board_Moderator">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="BoardID" type="xs:int" />
              <xs:element name="UserID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Board_Moderator" />
      <xs:field xpath="BoardID" />
      <xs:field xpath="UserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Notification gateway', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="GatewayID" fieldcaption="GatewayID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="998f29e3-f8ff-4cad-848f-7fe56c334eb1" /><field column="GatewayName" fieldcaption="GatewayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="265a729c-4602-4156-a570-dc67c728b9db" /><field column="GatewayDisplayName" fieldcaption="GatewayDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bf9a20e7-1c94-4ce6-aef5-288be9896f8c" /><field column="GatewayAssemblyName" fieldcaption="GatewayAssemblyName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="97622571-8bf3-42b0-be8e-2d04ee6d3305" /><field column="GatewayClassName" fieldcaption="GatewayClassName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="78ed2261-6c6e-4f36-815f-f39c1068dc7d" /><field column="GatewayDescription" fieldcaption="GatewayDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2cc80b89-ef7d-4b6d-908f-4a84ff9c8cd5" /><field column="GatewayEnabled" fieldcaption="Gatewau enabled" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6bb28929-60ca-48c9-bdab-20155e52ddc4" /><field column="GatewaySupportsEmail" fieldcaption="GatewaySupportsEmail" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8961441a-d70a-476b-8686-ac71a619dc6d" /><field column="GatewaySupportsPlainText" fieldcaption="GatewaySupportsPlainText" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01b61ce6-f5ad-41de-b5a7-9343c6c88ed1" /><field column="GatewaySupportsHTMLText" fieldcaption="GatewaySupportsHTMLText" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="394f1ae9-7967-4a9c-a804-0d156a9e7ad6" /><field column="GatewayLastModified" fieldcaption="GatewayLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c320ffe6-2936-4899-ba3c-52e8151313d7" /><field column="GatewayGUID" fieldcaption="GatewayGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0a91dd34-e8fd-46f9-aae5-795d6d7e8d45" /></form>', '20081229 09:07:13', 1808, N'notification.gateway', 0, 0, 0, '3d77378a-a021-4b11-bdce-c8043d45ce4a', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Notification_Gateway', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Notification_Gateway">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="GatewayID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="GatewayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayAssemblyName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayClassName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewayDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GatewaySupportsEmail" type="xs:boolean" />
              <xs:element name="GatewaySupportsPlainText" type="xs:boolean" />
              <xs:element name="GatewaySupportsHTMLText" type="xs:boolean" />
              <xs:element name="GatewayLastModified" type="xs:dateTime" />
              <xs:element name="GatewayGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="GatewayEnabled" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Notification_Gateway" />
      <xs:field xpath="GatewayID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Email', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="EmailID" fieldcaption="EmailID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="521d5e92-1ffd-4b57-8efe-de86ea04e86b" /><field column="EmailFrom" fieldcaption="EmailFrom" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="c2397191-6419-4c90-8602-438740d5de70" /><field column="EmailTo" fieldcaption="EmailTo" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f47a699d-f3d6-4da7-84e8-a7a54e7de8a9" /><field column="EmailReplyTo" fieldcaption="EmailReplyTo" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="7faf43c9-3882-43fb-9004-520b188a8b5a" visibility="none" /><field column="EmailCc" fieldcaption="EmailCc" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="10e8d6ce-d8b9-4bf8-a84d-8f9688e29981" /><field column="EmailBcc" fieldcaption="EmailBcc" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cedfd4fc-8e1b-4156-960e-552fc1246aeb" /><field column="EmailSubject" fieldcaption="EmailSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="601fdfbe-281e-458d-8cc0-a29306087c87" visibility="none" /><field column="EmailBody" fieldcaption="EmailBody" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad000851-1cdf-41c8-95ee-eff669981220" /><field column="EmailPlainTextBody" fieldcaption="EmailPlainTextBody" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c96fa853-4a36-4f3d-b661-fcdb58434d1c" /><field column="EmailFormat" fieldcaption="EmailFormat" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dcfbdec6-2369-4292-b406-9995bb2889bf" /><field column="EmailPriority" fieldcaption="EmailPriority" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b7feb9c1-7f2e-42e1-9be6-8e7100f9c669" /><field column="EmailSiteID" fieldcaption="EmailSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2aa51d80-c928-42d7-90a7-735dc9ddbc80" /><field column="EmailLastSendResult" fieldcaption="EmailLastSendResult" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="40755371-af41-4836-9e1c-6e512a280e66" /><field column="EmailLastSendAttempt" fieldcaption="EmailLastSendAttempt" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b5be875d-5220-454c-a099-6b936c3485c0" /><field column="EmailGUID" fieldcaption="EmailGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c78068ea-bd20-4004-b3d4-25028ffd07e8" /><field column="EmailLastModified" fieldcaption="EmailLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e94a0a0b-86c9-4207-8710-e86533e755bc" /><field column="EmailStatus" fieldcaption="EmailStatus" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="227556a7-9cc7-4f9b-96ad-2ccd667605b1" /><field column="EmailIsMass" fieldcaption="EmailIsMass" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0a21ac5d-68a2-4306-af4e-7ab7b2aa1693" /><field column="EmailSetName" fieldcaption="EmailSetName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="c3b5fdf7-ed07-4ebf-97fd-9e4a21967507" /><field column="EmailSetRelatedID" fieldcaption="EmailSetRelatedID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="814ac56f-1b7c-4593-b5d4-26e39c3624da" /></form>', '20100322 07:53:10', 1809, N'cms.email', 0, 0, 0, '488f275e-7311-4136-a127-4c970cd4060f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Email', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Email">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EmailID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="EmailFrom">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTo" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailCc" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailBcc" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailSubject">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailBody" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailPlainTextBody" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailFormat" type="xs:int" />
              <xs:element name="EmailPriority" type="xs:int" />
              <xs:element name="EmailSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="EmailLastSendResult" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailLastSendAttempt" type="xs:dateTime" minOccurs="0" />
              <xs:element name="EmailGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="EmailLastModified" type="xs:dateTime" />
              <xs:element name="EmailStatus" type="xs:int" minOccurs="0" />
              <xs:element name="EmailIsMass" type="xs:boolean" minOccurs="0" />
              <xs:element name="EmailSetName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailSetRelatedID" type="xs:int" minOccurs="0" />
              <xs:element name="EmailReplyTo" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Email" />
      <xs:field xpath="EmailID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Attachment for email', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="EmailID" fieldcaption="EmailID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="3a1b56eb-fa26-4225-9342-d8b34d9dbe0f" /><field column="AttachmentID" fieldcaption="AttachmentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="53e20c89-9e25-4528-a839-1f2cf10db117" /></form>', '20081231 14:52:49', 1811, N'cms.attachmentforemail', 0, 0, 0, '9a4da0dc-ef38-4485-9d15-65f0c4e629d8', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_AttachmentForEmail', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_AttachmentForEmail">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EmailID" type="xs:int" />
              <xs:element name="AttachmentID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_AttachmentForEmail" />
      <xs:field xpath="EmailID" />
      <xs:field xpath="AttachmentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Notification template', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TemplateID" fieldcaption="TemplateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="649871d8-bbcf-4c0d-9746-388e24871918" /><field column="TemplateName" fieldcaption="TemplateName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6f6f10e9-87e0-4dcc-92ec-bfb547d950d8" /><field column="TemplateDisplayName" fieldcaption="TemplateDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3d31111f-005d-48cb-88f7-e77d0a5fd573" /><field column="TemplateSiteID" fieldcaption="TemplateSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a5f24501-47b5-4328-9210-7c646eb7fb71" /><field column="TemplateCategoryID" fieldcaption="TemplateCategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fcbbc7ea-61bb-44ae-946b-ff56f0a8e1d1" /><field column="TemplateLastModified" fieldcaption="TemplateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c00d4d42-ec45-41ff-baa0-dd81fea1dcef" /><field column="TemplateGUID" fieldcaption="TemplateGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="44d412a0-7ee3-435f-9b96-6b9736325f7f" /></form>', '20081229 09:07:13', 1813, N'notification.template', 0, 0, 0, '91058913-11dd-42fa-a9c6-bd00c1b16382', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Notification_Template', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Notification_Template">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TemplateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TemplateName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="TemplateLastModified" type="xs:dateTime" />
              <xs:element name="TemplateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Notification_Template" />
      <xs:field xpath="TemplateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Notification template text', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TemplateTextID" fieldcaption="TemplateTextID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0fe06997-dfa0-4b7f-96df-cf2b5dba92db" /><field column="TemplateID" fieldcaption="TemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9664235f-533b-40df-9e11-440b7bb620f9" /><field column="GatewayID" fieldcaption="GatewayID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="29dd7102-0ca4-4859-a0b6-3585065ac029" /><field column="TemplateSubject" fieldcaption="TemplateSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="8e0bdef7-be8f-4332-aa84-c344a5f57bf8" /><field column="TemplateHTMLText" fieldcaption="TemplateHTMLText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c11474b9-3241-4b64-ac57-0a5f0791bde5" /><field column="TemplatePlainText" fieldcaption="TemplatePlainText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e90f8207-0398-4c60-8287-25f28f1766e1" /><field column="TemplateTextGUID" fieldcaption="TemplateTextGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3a1a1c8c-e009-487a-b610-db0d7deaf57b" /><field column="TemplateTextLastModified" fieldcaption="TemplateTextLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3e883605-907d-46dc-bc7f-8789db61755c"><settings><editTime>false</editTime></settings></field></form>', '20081229 09:07:13', 1814, N'notification.templatetext', 0, 0, 0, 'e0b0a19f-68db-4784-8134-104a1f574ef8', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Notification_TemplateText', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Notification_TemplateText">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TemplateTextID" type="xs:int" />
              <xs:element name="TemplateID" type="xs:int" />
              <xs:element name="GatewayID" type="xs:int" />
              <xs:element name="TemplateSubject">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateHTMLText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplatePlainText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateTextGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="TemplateTextLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Notification_TemplateText" />
      <xs:field xpath="TemplateTextID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Notification subscription', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="SubscriptionID" fieldcaption="SubscriptionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ed6ab9e8-4d20-4f5c-9844-b20618195aa1" /><field column="SubscriptionGatewayID" fieldcaption="SubscriptionGatewayID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="adc281f6-26da-4323-b23c-d7857a6c959e" /><field column="SubscriptionTemplateID" fieldcaption="SubscriptionTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c062c3e5-9ffa-4068-a022-ea1fb70ab26d" /><field column="SubscriptionEventSource" fieldcaption="SubscriptionEventSource" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b27f6ab9-280e-4c6c-b336-d573e7b37916" /><field column="SubscriptionEventCode" fieldcaption="SubscriptionEventCode" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="de9775c1-737b-47bc-bc95-412b64e7c1a4" /><field column="SubscriptionEventDisplayName" fieldcaption="SubscriptionEventDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c6b3dd2f-b815-48d7-a16a-1f4be2735694" /><field column="SubscriptionEventObjectID" fieldcaption="SubscriptionEventObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="58a66744-4360-4ac8-bc3a-34f835403b12" /><field column="SubscriptionEventData1" fieldcaption="Subscription event data 1" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="17b88874-a1f4-4a9c-94c0-742ac3884f11" /><field column="SubscriptionEventData2" fieldcaption="Subscription event data 2" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c26ff987-a95e-4cba-9f6f-65b51c0b9cb0" /><field column="SubscriptionUseHTML" fieldcaption="SubscriptionUseHTML" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4b506366-c76a-4e38-ae0e-3610c0c6b387" /><field column="SubscriptionTime" fieldcaption="SubscriptionTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="24332b9b-9cb0-499b-8cf2-e147f681f1a1" /><field column="SubscriptionUserID" fieldcaption="SubscriptionUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f9b4aa4-19f2-4879-93ab-4675f16704da" /><field column="SubscriptionTarget" fieldcaption="SubscriptionTarget" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="98654069-4985-40fc-bedd-0a0630b81399" /><field column="SubscriptionSiteID" fieldcaption="SubscriptionSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2227c603-af2e-4417-be3d-bf9c07dc856f" /><field column="SubscriptionLastModified" fieldcaption="SubscriptionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d2f9415b-6a51-4a69-af0e-c24152d77d13" /><field column="SubscriptionGUID" fieldcaption="SubscriptionGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0e034e8a-7d78-4618-b64f-33e978dc94ea" /></form>', '20081229 09:07:13', 1815, N'notification.subscription', 0, 0, 0, 'e9e38a33-6bd0-4265-aba3-1babb390b000', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Notification_Subscription', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Notification_Subscription">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubscriptionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SubscriptionGatewayID" type="xs:int" />
              <xs:element name="SubscriptionTemplateID" type="xs:int" />
              <xs:element name="SubscriptionEventSource" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionEventCode" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionEventDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionEventObjectID" type="xs:int" minOccurs="0" />
              <xs:element name="SubscriptionTime" type="xs:dateTime" />
              <xs:element name="SubscriptionUserID" type="xs:int" />
              <xs:element name="SubscriptionTarget">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionLastModified" type="xs:dateTime" />
              <xs:element name="SubscriptionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="SubscriptionEventData1" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionEventData2" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionUseHTML" type="xs:boolean" minOccurs="0" />
              <xs:element name="SubscriptionSiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Notification_Subscription" />
      <xs:field xpath="SubscriptionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Personalization', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="PersonalizationID" fieldcaption="PersonalizationID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="false" guid="3988441a-55dc-49e7-a66c-e7358beb375b" visibility="none" ismacro="false" /><field column="PersonalizationGUID" fieldcaption="PersonalizationGUID" visible="true" columntype="guid" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="false" guid="e306eeb6-d3c8-4070-820d-140205886fcf" /><field column="PersonalizationLastModified" fieldcaption="PersonalizationLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="false" guid="f91bbd1f-9447-4cff-aa85-9f9410c33b01"><settings><editTime>true</editTime></settings></field><field column="PersonalizationUserID" fieldcaption="PersonalizationUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="false" guid="5ba5b18b-42c2-4971-a249-8bbc000befd8" /><field column="PersonalizationDocumentID" fieldcaption="PersonalizationDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="false" guid="cb5b6418-3eaf-4647-9711-967ad0583179" /><field column="PersonalizationWebParts" fieldcaption="PersonalizationWebParts" visible="true" columntype="longtext" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="false" guid="51172fcd-2c07-420d-a30f-42e9e91f4884" /></form>', '20091016 11:33:52', 1841, N'cms.personalization', 0, 0, 0, '385d6f75-3d20-42d7-ba0d-6ac36201a4b1', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Personalization', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Personalization">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PersonalizationID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PersonalizationGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="PersonalizationLastModified" type="xs:dateTime" />
              <xs:element name="PersonalizationUserID" type="xs:int" minOccurs="0" />
              <xs:element name="PersonalizationDocumentID" type="xs:int" />
              <xs:element name="PersonalizationWebParts" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Personalization" />
      <xs:field xpath="PersonalizationID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Messaging - Contact List', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ContactListUserID" fieldcaption="ContactListUserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="c23c4f2b-7c56-447b-a5db-de8062e7596c" /><field column="ContactListContactUserID" fieldcaption="ContactListContactUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fd3b6f9d-4f9e-4725-835f-47f7cdb0f412" /></form>', '20090819 18:51:00', 1887, N'messaging.contactlist', 0, 0, 0, 'c7118d45-532c-4c43-aa2c-7b1c1d94bf24', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Messaging_ContactList', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Messaging_ContactList">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ContactListUserID" type="xs:int" />
              <xs:element name="ContactListContactUserID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Messaging_ContactList" />
      <xs:field xpath="ContactListUserID" />
      <xs:field xpath="ContactListContactUserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Messaging - Ignore List', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'', '20090819 18:51:19', 1888, N'messaging.ignorelist', 0, 0, 0, '8da9ee4e-4f7b-48f0-8384-1d9636707338', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Messaging_IgnoreList', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Messaging_IgnoreList">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="IgnoreListUserID" type="xs:int" />
              <xs:element name="IgnoreListIgnoredUserID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Messaging_IgnoreList" />
      <xs:field xpath="IgnoreListUserID" />
      <xs:field xpath="IgnoreListIgnoredUserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Message board subscription', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SubscriptionID" fieldcaption="SubscriptionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="434e2549-3146-4267-b6df-6649b01881b3" /><field column="SubscriptionBoardID" fieldcaption="SubscriptionBoardID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="baef33f3-c2b1-43b7-8b8f-30f3fc74924d" /><field column="SubscriptionUserID" fieldcaption="SubscriptionUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5dbcd520-8f23-40de-8cdb-af13dd26fc5d" /><field column="SubscriptionEmail" fieldcaption="SubscriptionEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5abf6e8-65a7-4caa-9c7d-bf10c756604a" /><field column="SubscriptionLastModified" fieldcaption="SubscriptionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="28cb6883-250f-47a2-bb4c-29dd2cfe98e0" /><field column="SubscriptionGUID" fieldcaption="SubscriptionGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aff7604e-2934-4ea4-97a8-982cda84f5fd" /></form>', '20091103 08:42:21', 1895, N'board.subscription', 0, 0, 0, 'ffbbfc2a-fe68-4f1f-8ad7-ee8115499f77', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Board_Subscription', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Board_Subscription">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubscriptionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SubscriptionBoardID" type="xs:int" />
              <xs:element name="SubscriptionUserID" type="xs:int" minOccurs="0" />
              <xs:element name="SubscriptionEmail">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionLastModified" type="xs:dateTime" />
              <xs:element name="SubscriptionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Board_Subscription" />
      <xs:field xpath="SubscriptionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Abuse report', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ReportID" fieldcaption="ReportID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="892db787-1eca-47ae-b962-5d43f284dfe2" /><field column="ReportGUID" fieldcaption="ReportGUID" visible="true" columntype="guid" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5025a50d-7931-4b7f-a575-75b48a32851a" /><field column="ReportTitle" fieldcaption="ReportTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="32e9eb2a-1c55-44e5-a0fd-28001a43211b" /><field column="ReportURL" fieldcaption="ReportURL" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="1000" publicfield="false" spellcheck="true" guid="b37f7666-cd87-4b00-ba9d-e4df36997db0" /><field column="ReportCulture" fieldcaption="ReportCulture" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="bc3f0126-2000-4e7e-9415-d714619a49aa" /><field column="ReportObjectID" fieldcaption="ReportObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6d3e7b93-a903-4a97-9479-460c3d511b5e" /><field column="ReportObjectType" fieldcaption="ReportObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="70fb19e9-e74e-46b9-a06a-6728c540430b" /><field column="ReportComment" fieldcaption="ReportComment" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7ea88b2b-b2d9-4391-8943-588604c80f2b" /><field column="ReportUserID" fieldcaption="ReportUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9641ceb0-b300-414e-9c2e-3f0a9be6ab54" /><field column="ReportWhen" fieldcaption="ReportWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="26defb36-5475-4da5-ba6f-7018757cbf3f"><settings><editTime>true</editTime></settings></field><field column="ReportStatus" fieldcaption="ReportStatus" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="75b84fd4-16c4-45b8-952b-a22c55b50dae" /><field column="ReportSiteID" fieldcaption="ReportSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1ac21a9b-7391-4a2f-9c84-20992ac78861" /></form>', '20090403 10:52:04', 1896, N'CMS.AbuseReport', 0, 0, 0, 'd4b445f8-6f93-412a-8ef2-429ee991473c', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_AbuseReport', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_AbuseReport">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ReportID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ReportGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ReportTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportURL">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportCulture">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportObjectID" type="xs:int" minOccurs="0" />
              <xs:element name="ReportObjectType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportComment">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportUserID" type="xs:int" minOccurs="0" />
              <xs:element name="ReportWhen" type="xs:dateTime" />
              <xs:element name="ReportStatus" type="xs:int" />
              <xs:element name="ReportSiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_AbuseReport" />
      <xs:field xpath="ReportID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="False" name="ReportObjectType" content="False" searchable="False" id="869aa678-04bb-4a7a-9ced-2c004caa81af"></item><item tokenized="False" name="ReportWhen" content="False" searchable="False" id="9ffae5a2-466d-4021-935f-560571650566"></item><item tokenized="False" name="ReportObjectID" content="False" searchable="False" id="a23d12e2-3378-4817-ac71-640b65a28b63"></item><item tokenized="False" name="ReportTitle" content="False" searchable="False" id="2094bc55-1f4e-4d77-bbc2-516caf21d6fb"></item><item tokenized="False" name="ReportStatus" content="False" searchable="False" id="97e70e14-7a72-4e81-879d-906c3e0e5c31"></item><item tokenized="False" name="ReportUserID" content="False" searchable="False" id="7e610b46-acb7-4b72-b0ee-0dc4883bd58a"></item><item tokenized="False" name="ReportURL" content="False" searchable="False" id="fef32681-e394-4b13-9d28-f6d2cbb63698"></item><item tokenized="False" name="ReportGUID" content="False" searchable="False" id="ae380051-e2f1-4d4b-91ee-f670478d8402"></item><item tokenized="False" name="ReportSiteID" content="False" searchable="False" id="afba728d-84de-43b0-9f76-9b021b5f0a51"></item><item tokenized="False" name="ReportCulture" content="False" searchable="False" id="ced1aa4b-00f3-44d2-89ba-7b875b96682e"></item><item tokenized="False" name="ReportID" content="False" searchable="False" id="82533add-2927-493a-9895-f9cd3a222262"></item><item tokenized="False" name="ReportComment" content="False" searchable="False" id="c721fe68-a06e-4254-a9e8-8f220f60bd0b"></item></search>', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Friends', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="FriendID" fieldcaption="FriendID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="c594ffa1-6187-4a76-ad68-4d9f1d4f70c2" /><field column="FriendRequestedUserID" fieldcaption="FriendRequestedUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="048307ca-6750-4859-8f54-92f9199da826" /><field column="FriendUserID" fieldcaption="FriendUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bfb8e040-35f7-4260-99cd-6a1967e7fbdd" /><field column="FriendRequestedWhen" fieldcaption="FriendRequestedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f04975c1-fd57-4bd2-8d62-f04a3171781c" /><field column="FriendComment" fieldcaption="FriendComment" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="526e3ab2-8d68-42b0-aaeb-5a2ed922e23f" /><field column="FriendApprovedBy" fieldcaption="FriendApprovedBy" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="617b041c-4a9b-4f59-bd19-1b52638969a2" /><field column="FriendApprovedWhen" fieldcaption="FriendApprovedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f2b2dfaa-687d-48b3-b4dc-0d0079290f87" /><field column="FriendRejectedBy" fieldcaption="FriendRejectedBy" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="77d78f42-9506-4eb3-8297-25966319cba2" /><field column="FriendRejectedWhen" fieldcaption="FriendRejectedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9b1e26fe-1fb2-47cc-85c0-d7432734ab82" /><field column="FriendGUID" fieldcaption="FriendGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0999b481-bbfc-43de-9ba8-4b3fcaf32a1f" /><field column="FriendStatus" fieldcaption="FriendStatus" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="18565d9f-12e6-40b1-9097-8e1ef9866c29" /></form>', '20100406 13:06:00', 1898, N'community.friend', 0, 0, 0, '82c1f83e-19ca-4567-8d02-3d8c271e91bd', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Community_Friend', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Community_Friend">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="FriendID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="FriendRequestedUserID" type="xs:int" />
              <xs:element name="FriendUserID" type="xs:int" />
              <xs:element name="FriendRequestedWhen" type="xs:dateTime" />
              <xs:element name="FriendComment" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FriendApprovedBy" type="xs:int" minOccurs="0" />
              <xs:element name="FriendApprovedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="FriendRejectedBy" type="xs:int" minOccurs="0" />
              <xs:element name="FriendRejectedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="FriendGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="FriendStatus" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Community_Friend" />
      <xs:field xpath="FriendID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ForumAttachment', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="AttachmentID" fieldcaption="AttachmentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="8f5d85ba-6df6-4eb4-8974-17543631936a" /><field column="AttachmentFileName" fieldcaption="AttachmentFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="52e2442a-8e21-448a-ba9f-870a254b50af" /><field column="AttachmentFileExtension" fieldcaption="AttachmentFileExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="10" publicfield="false" spellcheck="true" guid="7fadf09b-8dee-489b-8d9b-7d25c6187aae" /><field column="AttachmentBinary" fieldcaption="AttachmentBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3c04aa2f-f86a-458b-8aea-a75676cf565c" /><field column="AttachmentGUID" fieldcaption="AttachmentGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ee2d7dc9-b1b8-495c-b5d1-72e41529c19b" /><field column="AttachmentLastModified" fieldcaption="AttachmentLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e44931f3-dfaa-46d2-9fbc-b2a1384a1d8a" /><field column="AttachmentMimeType" fieldcaption="AttachmentMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="934b7a20-9e7b-472c-80f6-55f06355c1db" /><field column="AttachmentFileSize" fieldcaption="AttachmentFileSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b1cef1bb-75f4-4d55-b915-9b56d3c44547" /><field column="AttachmentImageHeight" fieldcaption="AttachmentImageHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ed9f7f94-7442-4008-a13d-454059b5792c" /><field column="AttachmentImageWidth" fieldcaption="AttachmentImageWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0dfa0f44-f8d3-4bf7-aa5e-75c02e22fd9f" /><field column="AttachmentPostID" fieldcaption="AttachmentPostID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bcda16be-c192-41d3-8c5f-5711e9a812e6" /><field column="AttachmentSiteID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="50fec5e1-a402-475a-a5e9-bdcd6cd24180" /></form>', '20081229 09:07:07', 1912, N'Forums.ForumAttachment', 0, 0, 0, '604d3bca-4ff8-47d7-9c18-c7637675ee04', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Forums_Attachment', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Forums_Attachment">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AttachmentID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AttachmentFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentFileExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentBinary" type="xs:base64Binary" minOccurs="0" />
              <xs:element name="AttachmentGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="AttachmentLastModified" type="xs:dateTime" />
              <xs:element name="AttachmentMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentFileSize" type="xs:int" />
              <xs:element name="AttachmentImageHeight" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentImageWidth" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentPostID" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentSiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Forums_Attachment" />
      <xs:field xpath="AttachmentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ForumUserFavorites', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="FavoriteID" fieldcaption="FavoriteID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="b024900c-68e3-4fd8-a6e2-294e6b5aa3a5" /><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="13a08f04-c86f-4cc2-99b5-71ce271ceb0d" /><field column="PostID" fieldcaption="PostID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c88f5e6d-9851-479d-affd-f025eb91b53f" /><field column="ForumID" fieldcaption="ForumID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fb3ded78-3c7f-40fa-9633-4b091dab8f1a" /><field column="FavoriteName" fieldcaption="FavoriteName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="5a832edd-f529-4f92-bcb7-445372cc0cf7" /><field column="SiteID" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e2fc938c-660a-4c63-9200-dfe67bc5103e" /><field column="FavoriteGUID" visible="false" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="63923c0b-402a-4652-b2f6-094f6f03c1b5" allowusertochangevisibility="false" /><field column="FavoriteLastModified" visible="false" columntype="datetime" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="36de5980-e5cd-4961-9b68-c4088f97f84f" allowusertochangevisibility="false"><settings><timezonetype>inherit</timezonetype></settings></field></form>', '20090104 20:37:40', 1913, N'Forums.ForumUserFavorites', 0, 0, 0, '14c4b710-4504-4c22-83bc-d460e13a7fd0', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Forums_UserFavorites', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Forums_UserFavorites">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="FavoriteID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="UserID" type="xs:int" />
              <xs:element name="PostID" type="xs:int" minOccurs="0" />
              <xs:element name="ForumID" type="xs:int" minOccurs="0" />
              <xs:element name="FavoriteName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteID" type="xs:int" />
              <xs:element name="FavoriteGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="FavoriteLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Forums_UserFavorites" />
      <xs:field xpath="FavoriteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Email user', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="EmailID" fieldcaption="EmailID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="46ad051a-403a-4ae1-a5cc-97ce20bc3682" /><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bd8d5cc3-cbf2-46b1-b187-e2429f53a12c" /><field column="LastSendResult" fieldcaption="LastSendResult" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1b5c35bd-1482-4dbc-b03d-1e603acbd35e" /><field column="LastSendAttempt" fieldcaption="LastSendAttempt" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f484db51-d9f5-41da-b811-42dc6e2d7a6d" /><field column="Status" fieldcaption="Status" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="883bbd0b-e27e-4d94-8e3f-92d2878a9af4" /></form>', '20081231 15:36:28', 1926, N'CMS.EmailUser', 0, 0, 0, '047de70d-4d44-4530-9bcf-5baafad2d62d', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_EmailUser', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_EmailRole">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EmailID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="UserID" type="xs:int" />
              <xs:element name="LastSendResult" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LastSendAttempt" type="xs:dateTime" minOccurs="0" />
              <xs:element name="Status" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_EmailRole" />
      <xs:field xpath="EmailID" />
      <xs:field xpath="UserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Media library', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="LibraryID" fieldcaption="LibraryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="00fdb6b0-5f2e-4ef9-8648-1d3c7af8b721" /><field column="LibraryName" fieldcaption="LibraryName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="e4081abf-652e-47bd-82d0-313752f01873" /><field column="LibraryDisplayName" fieldcaption="LibraryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="9b4aa82c-02df-4712-a5b9-3b9dee377b45" /><field column="LibraryDescription" fieldcaption="LibraryDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ac965989-ec7a-446f-81bb-ccb5043abf0b" /><field column="LibraryFolder" fieldcaption="LibraryFolder" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="06f6b6a9-08ca-4735-8732-20cc75d11802" /><field column="LibraryAccess" fieldcaption="LibraryAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="720d2865-0be4-43d3-8ed8-412b269b1d00" /><field column="LibraryGroupID" fieldcaption="LibraryGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="27889b22-33b2-48e6-b273-0d18c250676c" /><field column="LibrarySiteID" fieldcaption="LibrarySiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="99746a1e-3514-4c13-b878-7bb7b39ddb3d" /><field column="LibraryGUID" fieldcaption="LibraryGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="512c11dd-a325-4bf2-b1be-4f7c54397132" /><field column="LibraryLastModified" fieldcaption="LibraryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="54f9c352-5ac2-4e4b-ab98-65d41240e9e4" /><field column="LibraryTeaserPath" fieldcaption="LibraryTeaserPath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="5de9f8d9-3e2d-4d83-8a63-6c2bdfb76629" /></form>', '20100128 18:01:23', 1934, N'media.library', 0, 0, 0, 'dead7673-d566-4f83-87e3-e9a235b70e4f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Media_Library', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Media_Library">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="LibraryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="LibraryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LibraryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LibraryDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LibraryFolder">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LibraryAccess" type="xs:int" />
              <xs:element name="LibraryGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="LibrarySiteID" type="xs:int" />
              <xs:element name="LibraryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="LibraryLastModified" type="xs:dateTime" />
              <xs:element name="LibraryTeaserPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Media_Library" />
      <xs:field xpath="LibraryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Media file', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="FileID" fieldcaption="FileID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="be274203-89f9-46c6-8430-686c3f4ed2ee" /><field column="FileName" fieldcaption="FileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="e5922646-9755-419f-b97c-4da13c11e537" /><field column="FileTitle" fieldcaption="File title" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="69be119f-9a88-4ee5-8d15-7f3fca93abc1" /><field column="FileDescription" fieldcaption="FileDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="61714ed7-0b1a-40ee-9dcf-a03c3ecff77e" /><field column="FileExtension" fieldcaption="FileExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="2fa8bf43-d045-4ad6-805d-22adcd05cddf" /><field column="FileMimeType" fieldcaption="FileMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="75fbd642-1882-4463-b813-00feddb929b7" /><field column="FilePath" fieldcaption="FilePath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="6d1230c9-478c-4b29-b2e1-d055d0173bc7" /><field column="FileSize" fieldcaption="FileSize" visible="true" columntype="longinteger" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e230f41a-6ff1-4975-af9e-546c35cf50a2" visibility="none" ismacro="false" /><field column="FileImageWidth" fieldcaption="FileImageWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e106b686-4bcc-467c-8c9e-08554ed47551" /><field column="FileImageHeight" fieldcaption="FileImageHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ddc0242f-cd85-42e1-aaf9-baa98c0fcba2" /><field column="FileGUID" fieldcaption="FileGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e146f69c-1f65-433c-a091-616d9ac28aa7" /><field column="FileLibraryID" fieldcaption="FileLibraryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5b7f4a0a-386a-4575-beea-126f94381abb" /><field column="FileSiteID" fieldcaption="FileSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5657aab6-21f0-42aa-a901-c4396fb82a98" /><field column="FileCreatedByUserID" fieldcaption="FileCreatedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2c37082f-cc5f-4ef1-b272-d28166037df1" /><field column="FileCreatedWhen" fieldcaption="FileCreatedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f4c0a650-7f2d-43d3-b56a-a5ae117c1f01" /><field column="FileModifiedByUserID" fieldcaption="FileModifiedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8a3a9510-9f16-4760-937e-ff733dfe39ed" /><field column="FileModifiedWhen" fieldcaption="FileModifiedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="63d7a235-fae6-4383-b251-7269b71f648d" /><field column="FileCustomData" fieldcaption="FileCustomData" visible="true" columntype="longtext" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ea859358-cb3e-4fd6-a16c-bfca37c7484c" visibility="none" ismacro="false" /></form>', '20091216 12:42:52', 1935, N'media.file', 0, 0, 0, 'd511179e-7fd5-42ee-8b51-048f58e45f6f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Media_File', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Media_File">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="FileID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="FileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FileTitle">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FileDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FileExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FileMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FilePath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FileSize" type="xs:long" />
              <xs:element name="FileImageWidth" type="xs:int" minOccurs="0" />
              <xs:element name="FileImageHeight" type="xs:int" minOccurs="0" />
              <xs:element name="FileGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="FileLibraryID" type="xs:int" />
              <xs:element name="FileSiteID" type="xs:int" />
              <xs:element name="FileCreatedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="FileCreatedWhen" type="xs:dateTime" />
              <xs:element name="FileModifiedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="FileModifiedWhen" type="xs:dateTime" />
              <xs:element name="FileCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Media_File" />
      <xs:field xpath="FileID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Media library role permission', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="LibraryID" fieldcaption="LibraryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="38f76421-d982-45b1-8cc6-7c1b8f6c6655" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e48a6d51-7ef0-4059-bd5e-013c8e401606" /><field column="PermissionID" fieldcaption="PermissionID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fb93207d-e013-4f0d-bdac-0d4182519e0e" /></form>', '20081229 09:07:11', 1937, N'media.libraryrolepermission', 0, 0, 0, '7707e68b-a76e-42ad-b07b-93f26d529778', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Media_LibraryRolePermission', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Media_LibraryRolePermission">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="LibraryID" type="xs:int" />
              <xs:element name="RoleID" type="xs:int" />
              <xs:element name="PermissionID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Media_LibraryRolePermission" />
      <xs:field xpath="LibraryID" />
      <xs:field xpath="RoleID" />
      <xs:field xpath="PermissionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', NULL, NULL, N'Invitation', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="InvitationID" fieldcaption="InvitationID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a07dad62-5624-4db6-8cbe-c8e10672fdc9" /><field column="InvitedUserID" fieldcaption="InvitedUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4cc438c8-8e41-406e-a500-4f9e1145cb06" /><field column="InvitedByUserID" fieldcaption="InvitedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="94945103-4e87-489a-9230-f30cff5fa957" /><field column="InvitationGroupID" fieldcaption="InvitationGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d4e30801-fd08-4f41-b7a3-32da2ee7a32e" /><field column="InvitationCreated" fieldcaption="InvitationCreated" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="19e7bd69-0588-4d86-a766-6b98adde8a05" /><field column="InvitationValidTo" fieldcaption="InvitationValidTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0b47e1d8-37cc-4fe7-b8b0-ccda4122597b" /><field column="InvitationComment" fieldcaption="InvitationComment" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8a54a0bc-67e0-454a-abd7-2f33e74ad2bb" /><field column="InvitationGUID" fieldcaption="InvitationGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="66fbf8be-0aed-43e8-bd59-4b7eddbb62c2" /><field column="InvitationLastModified" fieldcaption="InvitationLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0d726f6e-93c0-42dd-8767-4720376a5528" /><field column="InvitationUserEmail" fieldcaption="InvitationUserEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="5caa5bab-b7ef-4bb8-b582-41c8948ed0c5" /></form>', '20081229 09:07:09', 1962, N'Community.Invitation', 0, 0, 0, 'a6d0fba0-8420-4a6a-a4e5-a94babe73387', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Community_Invitation', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Community_Invitation">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="InvitationID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="InvitedUserID" type="xs:int" minOccurs="0" />
              <xs:element name="InvitedByUserID" type="xs:int" />
              <xs:element name="InvitationGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="InvitationCreated" type="xs:dateTime" minOccurs="0" />
              <xs:element name="InvitationValidTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="InvitationComment" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="InvitationGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="InvitationLastModified" type="xs:dateTime" />
              <xs:element name="InvitationUserEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Community_Invitation" />
      <xs:field xpath="InvitationID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 0)
set identity_insert [cms_class] off
