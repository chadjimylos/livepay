using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSFormControls_TaxID : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;

            this.TaxID.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "";
            }

            return TaxID.Text;
        }
        set
        {
            string number = (string)value;
            Clear();

            // Parse numbers from incoming string.
            if ((number != null) && (number != ""))
            {
                try
                {
                    TaxID.Text = number;
                }
                catch
                {
                }
            }
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    public void Clear()
    {
        TaxID.Text = "";
    }


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(TaxID.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }

        Validator val = new Validator();
        string result = val.IsRegularExp(TaxID.Text, @"\d{9}", "error").Result;

        if (result != "")
        {
            return false;
        }

		int telestis = 2;
        int sum = 0;
        string afm = TaxID.Text;
        for (int i = 7; i >=0; i--)
		{
            sum += int.Parse(afm[i].ToString()) * telestis;
            telestis = telestis * 2;
		}
        int checkDigit = int.Parse(afm[8].ToString());
        if (sum % 11 % 10 != checkDigit){
            return false;
        }


        return true;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        cv_TaxID.Attributes.Add("ControlValidated", TaxID.ClientID);
        cv_TaxID.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_TaxID.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);


        string script = string.Empty;
        script = "function " + this.ClientID + "_checkTaxID(sender, args) { \n"+ 
                    "var afm = args.Value;\n"+
                    "var bIsValid = false;\n"+
                    "if (!afm.match(/^\\d{9}$/)) {\n"+
                        "bIsValid = false;\n" +
                    "}\n"+
                    " \n" +
                    "afm = afm.split('').reverse().join(''); \n"+
                    " \n" +
                    "var Num1 = 0; \n"+
                    "for (var iDigit = 1; iDigit <= 8; iDigit++) { \n"+
                        "Num1 += afm.charAt(iDigit) << iDigit; \n"+
                    "}\n"+
                    " \n" +
                    "if (Num1 == 0) { \n"+
                        "bIsValid = false; \n"+
                    " \n" +
                    "}else if ((Num1 % 11) % 10 == afm.charAt(0)) { \n"+
                        "bIsValid = true; \n"+
                    "} \n"+
                    "else { \n"+
                        "bIsValid = false; \n"+
                    "} \n"+
                    " \n" +
                    "var IsCutomValid = sender.getAttribute('IsCustomValidator') \n"+
                    "var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage') \n"+
                    "if (IsCutomValid != null) { \n"+
                        "if (IsCutomValid.indexOf('CustomCloud_') != -1) { \n"+
                            "if (!bIsValid) { \n"+
                                "Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '') \n"+
                            "} else { \n"+
                                "Tab1_ValidationCloud('#' + IsCutomValid, '', false, '') \n"+
                            "} \n"+
                        "} \n"+

                    "} \n"+
                    " \n"+
                    "args.IsValid = bIsValid; \n" +
                "} \n";

        ScriptManager.RegisterStartupScript(this.Page, GetType(), "checkTaxID_" + Guid.NewGuid(), script, true);
        
        cv_TaxID.ClientValidationFunction = this.ClientID + "_checkTaxID";
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }
}

