CREATE TABLE [CMS_WebPartContainerSite] (
		[ContainerID]     int NOT NULL,
		[SiteID]          int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebPartContainerSite]
	ADD
	CONSTRAINT [PK_CMS_WebPartContainerSite]
	PRIMARY KEY
	([ContainerID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WebPartContainerSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebPartContainer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPartContainerSite_ContainerID_CMS_WebPartContainer]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPartContainerSite]
			ADD CONSTRAINT [FK_CMS_WebPartContainerSite_ContainerID_CMS_WebPartContainer]
			FOREIGN KEY ([ContainerID]) REFERENCES [CMS_WebPartContainer] ([ContainerID])
END
IF OBJECT_ID(N'[CMS_WebPartContainerSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPartContainerSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPartContainerSite]
			ADD CONSTRAINT [FK_CMS_WebPartContainerSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
