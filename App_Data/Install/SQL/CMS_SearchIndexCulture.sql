CREATE TABLE [CMS_SearchIndexCulture] (
		[IndexID]            int NOT NULL,
		[IndexCultureID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_SearchIndexCulture]
	ADD
	CONSTRAINT [PK_CMS_SearchIndexCulture]
	PRIMARY KEY
	([IndexID], [IndexCultureID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_SearchIndexCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Culture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SearchIndexCulture_IndexCultureID_CMS_Culture]') IS NULL
BEGIN
		ALTER TABLE [CMS_SearchIndexCulture]
			ADD CONSTRAINT [FK_CMS_SearchIndexCulture_IndexCultureID_CMS_Culture]
			FOREIGN KEY ([IndexCultureID]) REFERENCES [CMS_Culture] ([CultureID])
END
IF OBJECT_ID(N'[CMS_SearchIndexCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_SearchIndex]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SearchIndexCulture_IndexID_CMS_SearchIndex]') IS NULL
BEGIN
		ALTER TABLE [CMS_SearchIndexCulture]
			ADD CONSTRAINT [FK_CMS_SearchIndexCulture_IndexID_CMS_SearchIndex]
			FOREIGN KEY ([IndexID]) REFERENCES [CMS_SearchIndex] ([IndexID])
END
