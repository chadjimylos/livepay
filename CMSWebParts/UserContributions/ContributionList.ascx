<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/UserContributions/ContributionList.ascx.cs" Inherits="CMSWebParts_UserContributions_ContributionList" %>
<%@ Register Src="~/CMSModules/Content/Controls/UserContributions/ContributionList.ascx" TagName="ContributionList" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/UserContributions/EditForm.ascx" TagName="ContributionEdit" TagPrefix="cms" %>
<cms:ContributionList ID="list" runat="server" />


