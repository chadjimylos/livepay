<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Tools - File import" Inherits="CMSModules_FileImport_Tools_Default" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSModules/Content/FormControls/Documents/SelectSinglePath.ascx" TagName="SelectSinglePath" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Cultures/SiteCultureSelector.ascx" TagName="SiteCultureSelector" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <input type="hidden" id="targetNodeId" name="targetNodeId" />
    <cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:PlaceHolder ID="plcImportContent" runat="server">
        <asp:Panel ID="pnlTitle" runat="server">
            <asp:Label ID="lblTitle" runat="server" EnableViewState="false" /><br />
            <br />
        </asp:Panel>
        <asp:PlaceHolder ID="plcImportList" runat="server">
            <asp:HiddenField ID="hdnSelected" runat="server" />
            <asp:HiddenField ID="hdnValue" runat="server" />
            <asp:Panel ID="pnlGrid" runat="server">
                <cms:UniGrid ID="gridImport" runat="server" GridName="FileImport.xml" DelayedReload="true" />
            </asp:Panel>
            <br />
            <asp:Panel ID="pnlCount" runat="server">
                <b>
                    <asp:Label ID="lblTotal" runat="server" EnableViewState="false" />
                    &nbsp;
                    <asp:Label ID="lblSelected" runat="server" EnableViewState="false" />&nbsp;<asp:Label
                        ID="lblSelectedValue" runat="server" EnableViewState="false" />
                </b>
            </asp:Panel>
            <cms:FileSystemDataSource ID="fileSystemDataSource" runat="server" IncludeSubDirs="true" CacheMinutes="0" />
        </asp:PlaceHolder>
        <br />
        <br />
        <asp:Panel ID="pnlImportControls" runat="server">
            <table border="0">
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblTargetAliasPath" runat="server" ResourceString="Tools.FileImport.TargetAliasPath"
                            DisplayColon="true" EnableViewState="false" />
                    </td>
                    <td colspan="2">
                        <cms:SelectSinglePath runat="server" ID="pathElem" IsLiveSite="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblSelectCulture" runat="server" ResourceString="general.culture"
                            DisplayColon="true" EnableViewState="false" />
                    </td>
                    <td colspan="2">
                        <cms:SiteCultureSelector runat="server" ID="cultureSelector" AddDefaultRecord="false"
                            IsLiveSite="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblDeleteImported" runat="server" ResourceString="Tools.FileImport.DeleteImported"
                            DisplayColon="true" EnableViewState="false" />
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkDeleteImported" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblIncludeExtension" runat="server" ResourceString="Tools.FileImport.RemoveExtension"
                            DisplayColon="true" EnableViewState="false" />
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkIncludeExtension" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td colspan="2">
                        <cms:LocalizedButton ID="btnStartImport" runat="server" ResourceString="Tools.FileImport.StartImport"
                            CssClass="LongSubmitButton" OnClick="btnStartImport_Click" EnableViewState="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:PlaceHolder>
</asp:Content>
