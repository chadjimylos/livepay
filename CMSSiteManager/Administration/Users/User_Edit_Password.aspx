<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Password.aspx.cs"
    Inherits="CMSSiteManager_Administration_Users_User_Edit_Password" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="User Edit - Password" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:PlaceHolder ID="plcTable" runat="server">
        <table>
            <tr>
                <td class="FieldLabel">
                    <asp:Label ID="LabelPassword" runat="server" Text="Label" /></td>
                <td>
                    <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" CssClass="TextBoxField"
                        MaxLength="100"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label ID="LabelConfirmPassword" runat="server" Text="Label" /></td>
                <td>
                    <asp:TextBox ID="TextBoxConfirmPassword" runat="server" TextMode="Password" CssClass="TextBoxField"
                        MaxLength="100"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <asp:CheckBox ID="chkSendEmail" runat="server" Visible="true" CssClass="CheckBoxMovedLeft"
                        Checked="true" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <cms:CMSButton ID="ButtonSetPassword" runat="server" Text="" OnClick="ButtonSetPassword_Click"
                                    CssClass="LongSubmitButton" EnableViewState="false" />
                            </td>
                            <td>
                                <cms:CMSButton ID="btnGenerateNew" runat="server" Visible="true" OnClick="btnGenerateNew_Click"
                                    CssClass="XLongSubmitButton" />
                            </td>
                            <td>
                                <cms:CMSButton ID="btnSendPswd" runat="server" Visible="false" CssClass="LongButton" OnClick="btnSendPswd_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
</asp:Content>
