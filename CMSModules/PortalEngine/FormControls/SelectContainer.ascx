<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectContainer.ascx.cs"
    Inherits="CMSModules_PortalEngine_FormControls_SelectContainer" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="cms" %>
<cms:UniSelector runat="server" ID="selectContainer" ObjectType="cms.WebPartContainer"
    ResourcePrefix="containerselect" AllowEmpty="false" SelectionMode="SingleDropDownList" ReturnColumnName="ContainerName" />
