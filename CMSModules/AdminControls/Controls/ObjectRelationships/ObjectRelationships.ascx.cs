using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;

using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.Scheduler;
using CMS.EmailEngine;
using CMS.WorkflowEngine;
using CMS.WebFarmSync;
using CMS.PortalEngine;
using CMS.ResourceManager;
using CMS.LicenseProvider;
using CMS.FormEngine;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSModules_AdminControls_Controls_ObjectRelationships_ObjectRelationships : CMSUserControl
{
    #region "Private fields"

    private TranslationHelper th = null;

    private IInfoObject mObject = null;
    private IInfoObject mRelatedObject = null;

    private string mObjectType = null;
    private int mObjectID = -1;
    private string[] mObjectTypes = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Collection of available object types
    /// </summary>
    private string[] ObjectTypes
    {
        get
        {
            if (mObjectTypes == null)
            {
                return mObjectTypes =
                    new string[] { 
                        FormObjectType.BIZFORM, 
                        PredefinedObjectType.NEWSLETTER, 
                        PredefinedObjectType.NEWSLETTERISSUE, 
                        PredefinedObjectType.NEWSLETTERSUBSCRIBER, 
                        PredefinedObjectType.NEWSLETTERTEMPLATE,
                        PredefinedObjectType.FORUMGROUP, 
                        PredefinedObjectType.FORUM, 
                        SettingsObjectType.SETTINGSKEY,
                        SiteObjectType.ROLE, 
                        SchedulerObjectType.SCHEDULEDTASK, 
                        EmailObjectType.EMAILTEMPLATE,
                        WorkflowObjectType.WORKFLOWSCOPE, 
                        PredefinedObjectType.ORDER, 
                        PredefinedObjectType.SHIPPINGOPTION,
                        PredefinedObjectType.PAYMENTOPTION, 
                        PredefinedObjectType.POLL, 
                        PredefinedObjectType.REPORTCATEGORY,
                        PredefinedObjectType.REPORT, 
                        PredefinedObjectType.REPORTSAVEDREPORT, 
                        SiteObjectType.USER,
                        SchedulerObjectType.SCHEDULEDTASK, 
                        WebFarmObjectType.WEBFARMSERVER, 
                        SettingsObjectType.SETTINGSKEY,
                        SiteObjectType.CSSSTYLESHEET, 
                        SiteObjectType.COUNTRY, 
                        SiteObjectType.CULTURE, 
                        SiteObjectType.DOCUMENTTYPE,
                        EmailObjectType.EMAILTEMPLATE, 
                        SiteObjectType.FORMUSERCONTROL, 
                        SiteObjectType.INLINECONTROL,
                        SiteObjectType.RESOURCE, 
                        SiteObjectType.PAGELAYOUT, 
                        PortalObjectType.PAGETEMPLATECATEGORY,
                        PortalObjectType.PAGETEMPLATE, 
                        SiteObjectType.RELATIONSHIPNAME, 
                        SiteObjectType.SYSTEMTABLE,
                        SiteObjectType.UICULTURE, 
                        ResourceObjectType.RESOURCESTRING, 
                        PortalObjectType.WEBPARTCONTAINER,
                        PortalObjectType.WEBPARTCATEGORY, 
                        PortalObjectType.WEBPART, 
                        WorkflowObjectType.WORKFLOW,
                        LicenseObjectType.LICENSEKEY, 
                        PredefinedObjectType.CUSTOMER, 
                        PredefinedObjectType.SKU, 
                        PredefinedObjectType.OPTIONCATEGORY, 
                        PredefinedObjectType.MANUFACTURER, 
                        PredefinedObjectType.SUPPLIER,
                        PredefinedObjectType.DISCOUNTCOUPON, 
                        PredefinedObjectType.DISCOUNTLEVEL, 
                        PredefinedObjectType.DEPARTMENT,
                        PredefinedObjectType.TAXCLASS, 
                        PredefinedObjectType.CURRENCY, 
                        PredefinedObjectType.EXCHANGETABLE,
                        PredefinedObjectType.ORDERSTATUS, 
                        PredefinedObjectType.PUBLICSTATUS, 
                        PredefinedObjectType.INTERNALSTATUS
                    };
            }

            return mObjectTypes;
        }
    }


    /// <summary>
    /// Text of the JavaScript function used to pass the selected value from on side of relationship to another
    /// </summary>
    private string SwitchValueScript
    {
        get
        {
            return
                "function SwitchSelectedValue(left)" +
                "{ " +
                "  var selectedItemIndex = -1;" +
                "  if (left)" +
                "  {" +
                "    selectedItemIndex = document.getElementById('" + selRightObj.DropDownSingleSelect.ClientID + "').selectedIndex;" +
                "    document.getElementById('" + selLeftObj.DropDownSingleSelect.ClientID + "').selectedIndex = selectedItemIndex;" +
                "  }" +
                "  else" +
                "  {" +
                "    selectedItemIndex = document.getElementById('" + selLeftObj.DropDownSingleSelect.ClientID + "').selectedIndex;" +
                "    document.getElementById('" + selRightObj.DropDownSingleSelect.ClientID + "').selectedIndex = selectedItemIndex;" +
                "  }" +
                "  return false;" +
                "}";
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Current object
    /// </summary>
    public IInfoObject Object
    {
        get
        {
            if (mObject == null)
            {
                IInfoObject infoObj = CMSObjectHelper.GetObject(ObjectType);
                if (infoObj != null)
                {
                    mObject = infoObj.GetObject(ObjectID);
                }
            }

            return mObject;
        }
    }


    /// <summary>
    /// Related object type
    /// </summary>
    public IInfoObject RelatedObject
    {
        get
        {
            if (mRelatedObject == null)
            {
                mRelatedObject = CMSObjectHelper.GetObject(this.drpRelatedObjType.SelectedValue);
            }

            return mRelatedObject;
        }
    }


    /// <summary>
    /// Type of the current object
    /// </summary>
    public string ObjectType
    {
        get
        {
            return mObjectType;
        }
        set
        {
            mObjectType = value;
        }
    }


    /// <summary>
    /// ID of the current object
    /// </summary>
    public int ObjectID
    {
        get
        {
            return mObjectID;
        }
        set
        {
            mObjectID = value;
        }
    }

    #endregion


    #region "Page events"

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        LoadData();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        int selectedSiteId = ValidationHelper.GetInteger(this.siteSelector.Value, 0);

        this.pnlNew.Visible = !this.pnlSite.Visible || (selectedSiteId >= 0);
    }


    public void LoadData()
    {
        // Setup client IDs
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SwitchSelectedValue", ScriptHelper.GetScript(SwitchValueScript));

        if (!RequestHelper.IsPostBack())
        {
            // Initialize controls
            SetupControl();
        }

        // Initialize the grid view
        InitGridView();

        siteSelector.OnSelectionChanged += UniSelector_OnSelectionChanged;
        siteSelector.DropDownSingleSelect.AutoPostBack = true;

        siteSelector.SpecialFields = new string[,] {
            { ResHelper.GetString("general.all"), "-1" },
            { ResHelper.GetString("general.globalobjects"), "0" }
            };

        // Load the grid view data
        ReloadData();

        if (this.Page.IsCallback)
        {
            DisplayAvailableItems();
        }

        ControlsHelper.RegisterPostbackControl(this.btnOk);
        ControlsHelper.RegisterPostbackControl(this.btnCancel);
    }

    #endregion


    #region "Grid view handling"

    /// <summary>
    /// Initializes the UniGridView properties
    /// </summary>
    private void InitGridView()
    {
        // Setup files path
        gridExistingRelationships.GridName = "~/CMSModules/AdminControls/Controls/ObjectRelationships/ObjectRelationships.xml";
        gridExistingRelationships.ImageDirectoryPath = GetImageUrl("Design/Controls/UniGrid/Actions/");

        // Register for events
        gridExistingRelationships.OnExternalDataBound += gridExistingRelationships_OnExternalDataBound;
        gridExistingRelationships.OnAction += gridExistingRelationships_OnAction;

        gridExistingRelationships.ZeroRowsText = ResHelper.GetString("ObjectRelationships.NoFound");
    }


    protected void gridExistingRelationships_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
                // Look for info on relationship being removed
                int leftObjId = ValidationHelper.GetInteger(Request.Params["leftObjId"], -1);
                string leftObjType = ValidationHelper.GetString(Request.Params["leftObjType"], "");
                int relationshipId = ValidationHelper.GetInteger(Request.Params["relationshipId"], -1);
                int rightObjId = ValidationHelper.GetInteger(Request.Params["rightObjId"], -1);
                string rightObjType = ValidationHelper.GetString(Request.Params["rightObjType"], "");

                // Remove the relationship if all the necessary information available
                if ((leftObjId > -1) && (leftObjType.Trim() != "") && (relationshipId > -1) && (rightObjId > -1) && (rightObjType.Trim() != ""))
                {
                    ObjectRelationshipInfoProvider.RemoveRelationship(leftObjId, leftObjType, rightObjId, rightObjType, relationshipId);
                }

                // Reload the data
                ReloadData();
                break;

            default:
                break;
        }
    }


    protected object gridExistingRelationships_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView dr = null;

        switch (sourceName.ToLower())
        {
            case "leftobject":
                {
                    dr = (DataRowView)parameter;
                    string objectType = ValidationHelper.GetString(dr["RelationshipLeftObjectType"], "");
                    int objectId = ValidationHelper.GetInteger(dr["RelationshipLeftObjectID"], 0);

                    return GetObjectString(objectId, objectType);
                }

            case "rightobject":
                {
                    dr = (DataRowView)parameter;
                    string objectType = ValidationHelper.GetString(dr["RelationshipRightObjectType"], "");
                    int objectId = ValidationHelper.GetInteger(dr["RelationshipRightObjectID"], 0);

                    return GetObjectString(objectId, objectType);
                }

            case "relationshipname":
                // Relationship name
                int relationshipId = ValidationHelper.GetInteger(parameter, 0);
                RelationshipNameInfo ri = RelationshipNameInfoProvider.GetRelationshipNameInfo(relationshipId);
                if (ri != null)
                {
                    return ri.RelationshipDisplayName;
                }
                break;
        }

        return parameter;
    }


    /// <summary>
    /// Gets the string for the given object
    /// </summary>
    /// <param name="objectId">Object ID</param>
    /// <param name="objectType">Object type</param>
    protected string GetObjectString(int objectId, string objectType)
    {
        string result = null;

        if ((objectType == this.ObjectType) && (objectId == this.ObjectID))
        {
            // Identity
            result = this.Object.ObjectDisplayName;
        }
        else
        {
            string safeObjectType = objectType.Replace(".", "_");

            // Prepare the name
            string objectName = null;
            string siteName = null;
            if (th != null)
            {
                DataRow rdr = th.GetRecord(safeObjectType, objectId);
                if (rdr != null)
                {
                    objectName = ValidationHelper.GetString(rdr["CodeName"], "");
                    siteName = ValidationHelper.GetString(rdr["SiteName"], "");
                }
            }

            if (String.IsNullOrEmpty(objectName))
            {
                result = ResHelper.GetString("ObjectType." + safeObjectType) + " '" + objectId + "'";
            }
            else
            {
                result = objectName;
            }

            if (!String.IsNullOrEmpty(siteName))
            {
                result += " (" + siteName + ")";
            }
        }

        return result;
    }
    
    #endregion


    #region "Event handlers"

    /// <summary>
    /// Refreshes the selection of site
    /// </summary>
    protected void RefreshNewSiteSelection()
    {
        if ((RelatedObject.SiteIDColumn != TypeInfo.COLUMN_NAME_UNKNOWN) && (CMSObjectHelper.GetSiteBindingObject(RelatedObject) == null))
        {
            pnlSite.Visible = true;
        }
        else
        {
            pnlSite.Visible = false;
        }
    }


    protected void lnkNewRelationship_Click(object sender, EventArgs e)
    {
        // Hide and disable unused controls
        pnlEditList.Visible = false;
        pnlAddNew.Visible = true;

        RefreshNewSiteSelection();

        // Initialize form labels
        this.leftCell.Text = ResHelper.GetString("ObjRelationship.LeftSide");
        this.middleCell.Text = ResHelper.GetString("Relationship.RelationshipName");
        this.rightCell.Text = ResHelper.GetString("ObjRelationship.RightSide");
        btnSwitchSides.Text = ResHelper.GetString("Relationship.SwitchSides");

        btnOk.Text = ResHelper.GetString("General.OK");
        btnCancel.Text = ResHelper.GetString("General.Cancel");

        // Initialize drop-down list with available relationship types
        DisplayAvailableRelationships();

        // Initialize drop=down list with available relationship items
        DisplayAvailableItems();

        // Supply the current object name
        lblLeftObj.Text = Object.ObjectDisplayName;
        lblRightObj.Text = Object.ObjectDisplayName;
    }


    protected void drpRelatedObjType_SelectedIndexChanged(object sender, EventArgs e)
    {
        RefreshNewSiteSelection();

        // If the new relationship is being added
        if (pnlAddNew.Visible)
        {
            DisplayAvailableItems();
        }
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        // Add the relationship between objects
        if (AddRelationship())
        {
            // Load the list dialog
            pnlEditList.Visible = true;
            pnlAddNew.Visible = false;
            drpRelatedObjType.Enabled = true;

            // Reload the data
            ReloadData();
        }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        // Load the list dialog
        pnlEditList.Visible = true;
        pnlAddNew.Visible = false;
        drpRelatedObjType.Enabled = true;

        // Reload the data
        ReloadData();
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        // If the new relationship is being added
        if (pnlAddNew.Visible)
        {
            DisplayAvailableItems();
        }
        else
        {
            // Reload the data
            ReloadData();
        }

        pnlUpdate.Update();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Reloads the data for the UniGrid control displaying the objects related to the current object
    /// </summary>
    private void ReloadData()
    {
        // Fill the related documents according filter selection
        if (drpRelatedObjType.SelectedItem != null)
        {
            GetRelatedObjects(drpRelatedObjType.SelectedItem.Value);
        }
    }


    /// <summary>
    /// Inserts the new relationship according the selected values
    /// </summary>
    private bool AddRelationship()
    {
        if (ObjectID > 0)
        {
            // Information on current object position supplied
            if (Request.Params["currentOnLeft"] != null)
            {
                if (drpRelationship.SelectedItem == null)
                {
                    this.lblError.Text = ResHelper.GetString("ObjRelationship.MustSelect");
                    this.lblError.Visible = true;
                    return false;
                }

                // Get information on type of the selected relationship
                int selectedRelationshipId = ValidationHelper.GetInteger(drpRelationship.SelectedItem.Value, -1);
                string selectedObjType = null;

                // If the main objectis on the left side selected object is taken from rifht drop-down list
                bool currentOnLeft = ValidationHelper.GetBoolean(Request.Params["currentOnLeft"], false);
                int selectedObjId = currentOnLeft ? ValidationHelper.GetInteger(selRightObj.Value, -1) : ValidationHelper.GetInteger(selLeftObj.Value, -1);

                // Get information on type of the selected object
                selectedObjType = drpRelatedObjType.SelectedItem.Value;

                // If all the necessary information are present
                if ((selectedObjId <= 0) || (selectedRelationshipId <= 0) || (selectedObjType == null))
                {
                    this.lblError.Text = ResHelper.GetString("ObjRelationship.MustSelect");
                    this.lblError.Visible = true;
                    return false;
                }

                if (currentOnLeft)
                {
                    ObjectRelationshipInfoProvider.AddRelationship(ObjectID, ObjectType, selectedObjId, selectedObjType, selectedRelationshipId);
                    return true;
                }
                else
                {
                    ObjectRelationshipInfoProvider.AddRelationship(selectedObjId, selectedObjType, ObjectID, ObjectType, selectedRelationshipId);
                    return true;
                }
            }
        }

        return false;
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    private void SetupControl()
    {
        // Information on current object supplied
        if ((ObjectID != 0) && (ObjectType != null))
        {
            // Setup link texts
            imgNewRelationship.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/addrelationship.png");
            lnkNewRelationship.Text = ResHelper.GetString("ObjRelationship.New");

            // Fill in the available objects into the filter
            DisplayAvailableObjects();
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = "[ObjectRelationships.ascx] SetupControl()- ObjectID or ObjectType wasn't initialized.";
        }
    }


    /// <summary>
    /// Retrieves data on related objects and supply the UniGrid control with it
    /// </summary>
    private void GetRelatedObjects(string selectedObjectType)
    {
        if ((Object != null) && (RelatedObject != null))
        {
            string where = null;

            // Get the site name
            string siteName = TranslationHelper.NO_SITE;
            int selectedSiteId = 0;

            if ((RelatedObject.SiteIDColumn != TypeInfo.COLUMN_NAME_UNKNOWN) && (CMSObjectHelper.GetSiteBindingObject(RelatedObject) == null))
            {
                if (siteSelector.DropDownSingleSelect.Items.Count == 0)
                {
                    siteSelector.Value = CMSContext.CurrentSiteID;
                }

                if (this.siteSelector.HasData)
                {
                    // Set the site name for registration
                    selectedSiteId = ValidationHelper.GetInteger(this.siteSelector.Value, 0);
                    if (selectedSiteId < 0)
                    {
                        // Automatic site name in case of 
                        siteName = TranslationHelper.AUTO_SITENAME;
                    }
                    else
                    {
                        string siteWhere = QueryProvider.GetQuery(RelatedObject.ObjectType + ".selectall", RelatedObject.IDColumn, SqlHelperClass.GetSiteIDWhereCondition(RelatedObject.SiteIDColumn, selectedSiteId), null, 0);

                        // Where condition for the left object
                        string rightWhere = ObjectRelationshipInfoProvider.GetWhereCondition(ObjectID, ObjectType, 0, false, true, selectedObjectType);
                        rightWhere += " AND RelationshipLeftObjectID IN (" + siteWhere + ")";
                        
                        // Where condition for the left object
                        string leftWhere = ObjectRelationshipInfoProvider.GetWhereCondition(ObjectID, ObjectType, 0, true, false, selectedObjectType);
                        leftWhere += " AND RelationshipRightObjectID IN (" + siteWhere + ")";

                        // --- Add site conditions here

                        where = SqlHelperClass.AddWhereCondition(leftWhere, rightWhere, "OR");
                    }
                }
            }
            
            if (String.IsNullOrEmpty(where))
            {
                // Get using regular where
                where = ObjectRelationshipInfoProvider.GetWhereCondition(ObjectID, ObjectType, 0, true, true, selectedObjectType);
            }

            // Get the relationships
            DataSet ds = ObjectRelationshipInfoProvider.GetAllRelationships(null, where, null, 0, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Prepare the translations table
                th = new TranslationHelper();
                th.UseDisplayNameAsCodeName = true;
                th.RegisterRecords(ds.Tables[0], RelatedObject.ObjectType, "RelationshipLeftObjectID", siteName);
                th.RegisterRecords(ds.Tables[0], RelatedObject.ObjectType, "RelationshipRightObjectID", siteName);
            }

            // Refresh the grid
            gridExistingRelationships.DataSource = ds;
            gridExistingRelationships.ReloadData();
        }
    }


    /// <summary>
    /// Fills the given drop-down list with the available relationship types
    /// </summary>
    private void DisplayAvailableRelationships()
    {
        if (drpRelationship.Items.Count == 0)
        {
            // Get the relationships from DB
            DataSet ds = RelationshipNameInfoProvider.GetAllRelationshipNames(CMSContext.CurrentSite.SiteID, "RelationshipAllowedObjects LIKE '%;##OBJECTS##;%'");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    drpRelationship.Items.Add(new ListItem(dr["RelationshipDisplayName"].ToString(), dr["RelationshipNameID"].ToString()));
                }

                drpRelationship.Enabled = true;
            }
            else
            {
                drpRelationship.Items.Add(new ListItem(ResHelper.GetString("General.NoneAvailable"), ""));
                drpRelationship.Enabled = false;
            }
        }
    }


    /// <summary>
    /// Fills given drop-down list with the items of particular type
    /// </summary>
    private void DisplayAvailableItems()
    {
        if (RelatedObject != null)
        {
            // Prepare the site where
            string where = null;
            if (RelatedObject.SiteIDColumn != TypeInfo.COLUMN_NAME_UNKNOWN)
            {
                int selectedSiteId = -1;

                // Add where 
                if (CMSObjectHelper.GetSiteBindingObject(RelatedObject) == null)
                {
                    selectedSiteId = ValidationHelper.GetInteger(this.siteSelector.Value, -1);
                }

                if (selectedSiteId == 0)
                {
                    where = RelatedObject.SiteIDColumn + " IS NULL";
                }
                else
                {
                    where = RelatedObject.SiteIDColumn + " = " + selectedSiteId;
                }
            }

            // Load the object selectors
            selLeftObj.Enabled = true;
            selLeftObj.ObjectType = RelatedObject.ObjectType;
            selLeftObj.WhereCondition = where;
            selLeftObj.Reload(true);
            if (!selLeftObj.HasData)
            {
                selLeftObj.DropDownSingleSelect.Items.Add(new ListItem(ResHelper.GetString("General.NoneAvailable"), ""));
                selLeftObj.Enabled = false;
            }

            selRightObj.Enabled = true;
            selRightObj.ObjectType = RelatedObject.ObjectType;
            selRightObj.WhereCondition = where;
            selRightObj.Reload(true);
            if (!selRightObj.HasData)
            {
                selRightObj.DropDownSingleSelect.Items.Add(new ListItem(ResHelper.GetString("General.NoneAvailable"), ""));
                selRightObj.Enabled = false;
            }
        }
    }


    /// <summary>
    /// Fills given drop-down list with the available object types
    /// </summary>
    private void DisplayAvailableObjects()
    {
        // For each type of available objects create a new item and insert it to the list
        if (ObjectTypes != null)
        {
            SortedDictionary<string, string> types = new SortedDictionary<string, string>();

            // Prepare the list of items
            foreach (string objType in ObjectTypes)
            {
                types[ResHelper.GetString("ObjectTasks." + objType.Replace(".", "_").Replace("#", "_"))] = objType;
            }

            // Fill in the list
            foreach (KeyValuePair<string, string> item in types)
            {
                string objType = item.Value;

                ListItem li = new ListItem(item.Key, objType);
                drpRelatedObjType.Items.Add(li);

                // Preselect the same object type as the main object if available
                if (!IsPostBack && objType.Equals(this.ObjectType, StringComparison.InvariantCultureIgnoreCase))
                {
                    li.Selected = true;
                }
            }

            drpRelatedObjType.DataBind();
        }
    }

    #endregion
}
