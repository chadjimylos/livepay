using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_Emails : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // control initializations				
        lblEmailFrom.Text = ResHelper.GetString("Configuration_StoreSettings.lblEmailFrom");
        lblEmailTo.Text = ResHelper.GetString("Configuration_StoreSettings.lblEmailTo");
        lblSendOrderNotification.Text = ResHelper.GetString("Configuration_StoreSettings.lblSendOrderNotification");
        lblSendPaymentNotification.Text = ResHelper.GetString("Configuration_StoreSettings.lblSendPaymentNotification");
        btnOk.Text = ResHelper.GetString("General.OK");

        // Fill editing form
        if (!RequestHelper.IsPostBack())
        {
            LoadData();           
        }
    }


    /// <summary>
    /// Load data of editing currency.
    /// </summary>
    /// <param name="currencyObj">Currency object.</param>
    protected void LoadData()
    {               
        if (CMSContext.CurrentSite != null)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(CMSContext.CurrentSiteName);
            if (si != null)
            {                
                txtEmailFrom.Text = si.SiteSendStoreEmailsFrom;
                txtEmailTo.Text = si.SiteSendOrderNotificationTo;

                chkSendOrderNotification.Checked = si.SiteSendOrderNotification;
                chkSendPaymentNotification.Checked = si.SiteSendPaymentNotification;
            }
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = "";

        // Check email addresses
        string emailFrom = txtEmailFrom.Text.Trim();
        if ((errorMessage == "") && (emailFrom != "") && (!ValidationHelper.IsEmail(emailFrom)))
        {
            errorMessage = ResHelper.GetString("Ecommerce.Settings.ErrorFormatStoreEmail");
        }            

        string emailTo = txtEmailTo.Text.Trim();
        if ((errorMessage == "") && (emailTo != "") && (!ValidationHelper.AreEmails(emailTo)))
        {
            errorMessage = ResHelper.GetString("Ecommerce.Settings.ErrorFormatOrderNotificationEmail");
        }

        // Process form data
        if (errorMessage == "")
        {
            if (CMSContext.CurrentSite != null)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(CMSContext.CurrentSite.SiteID);
                if (si != null)
                {
                    si.SiteSendOrderNotificationTo = emailTo;
                    si.SiteSendStoreEmailsFrom = emailFrom;

                    si.SiteSendPaymentNotification = chkSendPaymentNotification.Checked;
                    si.SiteSendOrderNotification = chkSendOrderNotification.Checked;

                    SiteInfoProvider.SetSiteInfo(si);
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                    // Display warnings
                    if (chkSendOrderNotification.Checked || chkSendPaymentNotification.Checked)
                    {
                        if (emailTo == "")
                        {
                            lblError.Visible = true;
                            lblError.Text += ResHelper.GetString("Configuration_StoreSettings.EmailToMissing");
                        }
                        if (emailFrom == "")
                        {
                            lblError.Visible = true;
                            if (lblError.Text != "")
                            {
                                lblError.Text += "<br />";
                            }
                            lblError.Text += ResHelper.GetString("Configuration_StoreSettings.EmailFromMissing");
                        }
                    }
                }
            }
        }
        

        if (errorMessage != "")
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
