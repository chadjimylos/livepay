<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscriber_Import.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_ImportExportSubscribers_Subscriber_Import"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - Newsletter subscribers" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>


<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblActions" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <cms:LocalizedRadioButton ID="radSubscribe" runat="server" GroupName="ImportSubscribers"
        ResourceString="Subscriber_Import.SubscribeImported" CssClass="RadioImport" Checked="true" />
        <cms:LocalizedCheckBox ID="chkDoNotSubscribe" runat="server" ResourceString="Subscriber_Import.DoNotSubscribe"
        CssClass="UnderRadioContent" />
        <br />
        <br />
    <cms:LocalizedRadioButton ID="radUnsubscribe" runat="server" GroupName="ImportSubscribers"
        ResourceString="Subscriber_Import.UnsubscribeImported" CssClass="RadioImport" />
    <cms:LocalizedRadioButton ID="radDelete" runat="server" GroupName="ImportSubscribers"
        ResourceString="Subscriber_Import.DeleteImported" CssClass="RadioImport" />
    <br />
    <asp:Label ID="lblImportedSub" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <asp:TextBox ID="txtImportSub" runat="server" TextMode="MultiLine" CssClass="TextAreaLarge"
        Height="170px" />
    <br />
    <br />
    <asp:Label ID="lblNote" runat="server" CssClass="ContentLabel" EnableViewState="false" />
    <br />
    <br />
    <asp:Label ID="lblSelectSub" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <cms:UniSelector ID="usNewsletters" runat="server" IsLiveSite="false" ObjectType="Newsletter.Newsletter"
        SelectionMode="Multiple" ResourcePrefix="newsletterselect" />
    <br />
    <div>
        <asp:CheckBox ID="chkSendConfirmation" runat="server" />
    </div>
    <br />
    <cms:CMSButton ID="btnImport" runat="server" CssClass="LongSubmitButton" EnableViewState="false" />
</asp:Content>
