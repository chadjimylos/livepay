﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.DataEngine;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.FileManager;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.Controls;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_Controls_ViewVersion : CMSUserControl
{
    #region "Variables"

    FormInfo fi = null;
    TreeProvider mTree = null;
    VersionManager vm = null;
    int versionHistoryId = 0;
    int versionCompare = 0;
    TreeNode node = null;
    TreeNode compareNode = null;

    AttachmentsDataSource dsAttachments = null;
    AttachmentsDataSource dsAttachmentsCompare = null;

    Hashtable attachments = null;
    Hashtable attachmentsCompare = null;

    UserInfo mCurrentUser = null;
    bool noCompare = false;
    private bool even = true;
    private const string UNSORTED = "##UNSORTED##";

    TimeZoneInfo usedTimeZone = null;
    TimeZoneInfo serverTimeZone = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Current user info
    /// </summary>
    protected UserInfo CurrentUser
    {
        get
        {
            if (mCurrentUser == null)
            {
                mCurrentUser = CMSContext.CurrentUser;
            }

            return mCurrentUser;
        }
    }


    /// <summary>
    /// Tree provider
    /// </summary>
    protected TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                mTree = new TreeProvider(CurrentUser);
            }

            return mTree;
        }
    }


    /// <summary>
    /// Version manager
    /// </summary>
    protected VersionManager VM
    {
        get
        {
            if (vm == null)
            {
                vm = new VersionManager(Tree);
            }

            return vm;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        noCompare = ValidationHelper.GetBoolean(Request.QueryString["noCompare"], false);
        versionHistoryId = ValidationHelper.GetInteger(Request.QueryString["versionhistoryid"], 0);
        versionCompare = QueryHelper.GetInteger("compareHistoryId", 0);

        // Converting modified time to correct time zone
        TimeZoneHelper.GetCurrentTimeZoneDateTimeString(DateTime.Now, CurrentUser, CMSContext.CurrentSite, out usedTimeZone);
        serverTimeZone = TimeZoneHelper.ServerTimeZone;

        // No comparing available in Recycle bin
        pnlControl.Visible = !noCompare;

        if (versionHistoryId > 0)
        {
            try
            {
                if (VM != null)
                {
                    // Get original version of document
                    node = VM.GetVersion(versionHistoryId, null);
                    compareNode = VM.GetVersion(versionCompare, null);
                }
                if (node != null)
                {
                    // Check read permissions
                    if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) != AuthorizationResultEnum.Allowed)
                    {
                        RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                    }

                    if (!RequestHelper.IsPostBack())
                    {
                        LoadDropDown(node.DocumentID, versionHistoryId);
                        ReloadData();
                    }
                    drpCompareTo.SelectedIndexChanged += drpCompareTo_SelectedIndexChanged;
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ResHelper.GetString("document.viewversionerror") + " " + ex.Message;

                EventLogProvider.LogException("Content", "VIEWVERSION", ex);
            }
        }
    }


    /// <summary>
    /// Reloads control with new data.
    /// </summary>
    private void ReloadData()
    {
        tblDocument.Rows.Clear();

        DataClassInfo ci = DataClassInfoProvider.GetDataClass(node.NodeClassName);
        fi = new FormInfo();
        fi.LoadXmlDefinition(ci.ClassFormDefinition);

        TableHeaderCell valueCell = new TableHeaderCell();
        TableHeaderCell valueCompare = new TableHeaderCell();
        TableHeaderCell labelCell = new TableHeaderCell();

        // Add header column with version number
        if (compareNode == null)
        {
            labelCell.Text = ResHelper.GetString("General.FieldName");
            valueCell.Text = ResHelper.GetString("General.Value");

            AddRow(labelCell, valueCell, "UniGridHead", false);
        }
        else
        {
            labelCell.Text = ResHelper.GetString("lock.versionnumber");
            valueCell.Text = GetVersionNumber(node.DocumentID, versionHistoryId);
            valueCompare.Text = GetVersionNumber(compareNode.DocumentID, versionCompare);

            AddRow(labelCell, valueCell, valueCompare, true, "UniGridHead", false);
        }

        if ((ci != null) && (ci.ClassIsCoupledClass))
        {
            // Add coupled class fields
            IDataClass coupleClass = DataClassFactory.NewDataClass(node.NodeClassName);
            foreach (string col in coupleClass.StructureInfo.ColumnNames)
            {
                // If comparing with other version and current coupled column is not versioned do not display it
                if (!((compareNode != null) && !(VersionManager.IsVersionedCoupledColumn(node.NodeClassName, col))))
                {
                    AddField(node, compareNode, col);
                }
            }
        }

        // Add versioned document class fields
        IDataClass docClass = DataClassFactory.NewDataClass("cms.document");
        foreach (string col in docClass.StructureInfo.ColumnNames)
        {
            // If comparing with other version and current document column is not versioned do not display it
            // One exception is DocumentNamePath column which will be displayed even if it is not marked as a versioned column
            if (!((compareNode != null) && (!(VersionManager.IsVersionedDocumentColumn(col) || (col.ToLower() == "documentnamepath")))))
            {
                AddField(node, compareNode, col);
            }
        }

        // Add versioned document class fields
        IDataClass treeClass = DataClassFactory.NewDataClass("cms.tree");
        foreach (string col in treeClass.StructureInfo.ColumnNames)
        {
            // Do not display cms_tree columns when comparing with other version
            // cms_tree columns are not versioned
            if (compareNode == null)
            {
                AddField(node, compareNode, col);
            }
        }

        // Add unsorted attachments to the table
        AddField(node, compareNode, UNSORTED);
    }


    /// <summary>
    /// Loads dropdown list with versions.
    /// </summary>
    /// <param name="documentId">ID of current document.</param>
    /// <param name="versionHistoryId">ID of current version history.</param>
    private void LoadDropDown(int documentId, int versionHistoryId)
    {
        drpCompareTo.Items.Clear();

        GeneralConnection genConn = ConnectionHelper.GetConnection();
        // Prepare the query parameters
        object[,] parameters = new object[1, 3];
        parameters[0, 0] = "@DocumentID";
        parameters[0, 1] = documentId;

        DataSet ds = genConn.ExecuteQuery("cms.versionhistory.selectdocumenthistory", parameters, "VersionHistoryID !=" + versionHistoryId, "ModifiedWhen DESC, VersionNumber DESC", -1, "VersionHistoryID");

        // Converting modified time to corect time zone
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                // Get modified time in form "version (datetime)"
                string s = ValidationHelper.GetString(dr["VersionModified"], "");
                dr["VersionModified"] = GetVersionDate(s);
            }
        }

        drpCompareTo.DataSource = ds;
        drpCompareTo.DataValueField = "VersionHistoryID";
        drpCompareTo.DataTextField = "VersionModified";
        drpCompareTo.DataBind();

        // If history to compare is available
        if (drpCompareTo.Items.Count > 0)
        {
            drpCompareTo.Items.Insert(0, "(select version)");
        }
        // Otherwise hide dropdown list
        else
        {
            pnlControl.Visible = false;
        }

        // Pre-select dropdown list
        try
        {
            drpCompareTo.SelectedValue = versionCompare.ToString();
        }
        catch
        {
        }
    }


    /// <summary>
    /// Returns versionID with version DateTime modified accordingly to current user time zone.
    /// </summary>
    /// <param name="versionModified">VersionModified column</param>
    /// <returns>Returns time modified according to current user time zone</returns>
    private string GetVersionDate(string versionModified)
    {
        // Get modified time in form "version (datetime)"
        string str = versionModified;

        if (usedTimeZone != null)
        {
            // Parse modified datetime
            int l = versionModified.IndexOf("(");
            int r = versionModified.IndexOf(")");
            if ((l > -1) && (r > -1))
            {
                // Get datetime
                string dateTimeVal = versionModified.Substring(l + 1, r - l - 1);
                // Convert datetime to new timezone and replace old value
                str = versionModified.Replace(dateTimeVal, TimeZoneHelper.ConvertTimeZoneDateTime(
                                                               ValidationHelper.GetDateTime(dateTimeVal, DataHelper.DATETIME_NOT_SELECTED),
                                                               serverTimeZone, usedTimeZone) + TimeZoneHelper.GetGMTStringOffset(" GMT{0: + 00.00; - 00.00}", usedTimeZone));
            }
        }
        return str;
    }


    /// <summary>
    /// Returns version number for given document.
    /// </summary>
    private string GetVersionNumber(int documentId, int version)
    {
        GeneralConnection genConn = ConnectionHelper.GetConnection();
        // Prepare the query parameters
        object[,] parameters = new object[1, 3];
        parameters[0, 0] = "@DocumentID";
        parameters[0, 1] = documentId;

        DataSet result = genConn.ExecuteQuery("cms.versionhistory.selectdocumenthistory", parameters, "VersionHistoryID =" + version, "VersionNumber", 1, "VersionNumber");

        if (DataHelper.DataSourceIsEmpty(result))
        {
            return null;
        }
        else
        {
            return GetVersionDate(ValidationHelper.GetString(result.Tables[0].Rows[0]["VersionModified"], null));
        }
    }


    /// <summary>
    /// Adds the field to the form
    /// </summary>
    /// <param name="node">Document node</param>
    /// <param name="compareNode">Document compare node</param>
    /// <param name="columnName">Column name</param>
    private void AddField(TreeNode node, TreeNode compareNode, string columnName)
    {
        FormFieldInfo ffi = null;
        if (fi != null)
        {
            ffi = fi.GetFormField(columnName);
        }

        TableCell valueCell = new TableCell();
        TableCell valueCompare = new TableCell();
        TableCell labelCell = new TableCell();
        TextComparison comparefirst = null;
        TextComparison comparesecond = null;
        bool switchSides = true;

        bool loadValue = true;
        bool empty = true;
        bool allowLabel = true;

        // Get the caption
        if ((columnName == UNSORTED) || (ffi != null))
        {
            AttachmentInfo aiCompare = null;

            // Compare attachments
            if ((columnName == UNSORTED) || (ffi.DataType == FormFieldDataTypeEnum.DocumentAttachments))
            {
                allowLabel = false;

                string title = null;
                if (columnName == UNSORTED)
                {
                    title = ResHelper.GetString("attach.unsorted") + ":";
                }
                else
                {
                    title = (String.IsNullOrEmpty(ffi.Caption) ? ffi.Name : ffi.Caption) + ":";
                }

                // Prepare DataSource for original node
                loadValue = false;

                dsAttachments = new AttachmentsDataSource();
                dsAttachments.DocumentVersionHistoryID = versionHistoryId;
                if (columnName != UNSORTED)
                {
                    dsAttachments.AttachmentGroupGUID = ffi.Guid;
                }
                dsAttachments.Path = node.NodeAliasPath;
                dsAttachments.CultureCode = CMSContext.PreferredCultureCode;
                SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
                dsAttachments.SiteName = si.SiteName;
                dsAttachments.OrderBy = "AttachmentOrder, AttachmentName, AttachmentHistoryID";
                dsAttachments.SelectedColumns = "AttachmentHistoryID, AttachmentGUID, AttachmentImageWidth, AttachmentExtension, AttachmentName, AttachmentSize, AttachmentOrder";
                dsAttachments.DataSource = null;
                dsAttachments.DataBind();

                // Get the attachments table
                attachments = GetAttachmentsTable((DataSet)dsAttachments.DataSource, versionHistoryId, node.DocumentID);

                // Prepare datasource for compared node
                if (compareNode != null)
                {
                    dsAttachmentsCompare = new AttachmentsDataSource();
                    dsAttachmentsCompare.DocumentVersionHistoryID = versionCompare;
                    if (columnName != UNSORTED)
                    {
                        dsAttachmentsCompare.AttachmentGroupGUID = ffi.Guid;
                    }
                    dsAttachmentsCompare.Path = compareNode.NodeAliasPath;
                    dsAttachmentsCompare.CultureCode = CMSContext.PreferredCultureCode;
                    dsAttachmentsCompare.SiteName = si.SiteName;
                    dsAttachmentsCompare.OrderBy = "AttachmentOrder, AttachmentName, AttachmentHistoryID";
                    dsAttachmentsCompare.SelectedColumns = "AttachmentHistoryID, AttachmentGUID, AttachmentImageWidth, AttachmentExtension, AttachmentName, AttachmentSize, AttachmentOrder";
                    dsAttachmentsCompare.DataSource = null;
                    dsAttachmentsCompare.DataBind();

                    // Get the table to compare
                    attachmentsCompare = GetAttachmentsTable((DataSet)dsAttachmentsCompare.DataSource, versionCompare, node.DocumentID);

                    // Switch the sides if older version is on the right
                    if (versionHistoryId > versionCompare)
                    {
                        Hashtable dummy = attachmentsCompare;
                        attachmentsCompare = attachments;
                        attachments = dummy;
                    }

                    // Add comparison
                    AddTableComparison(attachments, attachmentsCompare, "<strong>" + title + "</strong>", true, true);
                }
                else
                {
                    // Normal display
                    bool first = true;
                    foreach (DictionaryEntry item in attachments)
                    {
                        if (!String.IsNullOrEmpty((string)item.Value))
                        {
                            valueCell = new TableCell();
                            labelCell = new TableCell();

                            if (first)
                            {
                                labelCell.Text = "<strong>" + String.Format(title, item.Key) + "</strong>";
                                first = false;
                            }
                            valueCell.Text = (string)item.Value;

                            AddRow(labelCell, valueCell, null, even);
                            even = !even;
                        }
                    }
                }
            }
            // Compare single file attachment
            else if (((ffi.FieldType == FormFieldControlTypeEnum.DirectUploadControl) || ((ffi.FieldType != FormFieldControlTypeEnum.DirectUploadControl))) && (ffi.DataType == FormFieldDataTypeEnum.File))
            {
                // Get the attachment
                AttachmentInfo ai = DocumentHelper.GetAttachment(ValidationHelper.GetGuid(node.GetValue(columnName), Guid.Empty), versionHistoryId, Tree, false);

                if (compareNode != null)
                {
                    aiCompare = DocumentHelper.GetAttachment(ValidationHelper.GetGuid(compareNode.GetValue(columnName), Guid.Empty), versionCompare, Tree, false);
                }

                loadValue = false;
                empty = true;

                // Prepare text comparison controls
                if ((ai != null) || (aiCompare != null))
                {
                    string textorig = CreateAttachment(ai, versionHistoryId);
                    string textcompare = CreateAttachment(aiCompare, versionCompare);

                    comparefirst = new TextComparison();
                    comparefirst.SynchronizedScrolling = false;
                    comparefirst.IgnoreHTMLTags = true;
                    comparefirst.ConsiderHTMLTagsEqual = true;
                    comparefirst.BalanceContent = false;

                    comparesecond = new TextComparison();
                    comparesecond.SynchronizedScrolling = false;
                    comparesecond.IgnoreHTMLTags = true;

                    // Source text must be older version
                    if (versionHistoryId < versionCompare)
                    {
                        comparefirst.SourceText = textorig;
                        comparefirst.DestinationText = textcompare;
                    }
                    else
                    {
                        comparefirst.SourceText = textcompare;
                        comparefirst.DestinationText = textorig;
                    }

                    comparefirst.PairedControl = comparesecond;
                    comparesecond.RenderingMode = TextComparisonTypeEnum.DestinationText;

                    valueCell.Controls.Add(comparefirst);
                    valueCompare.Controls.Add(comparesecond);
                    switchSides = false;

                    // Add both cells
                    if (compareNode != null)
                    {
                        AddRow(labelCell, valueCell, valueCompare, switchSides, null, even);
                    }
                    // Add one cell only
                    else
                    {
                        valueCell.Controls.Clear();
                        Literal ltl = new Literal();
                        ltl.Text = textorig;
                        valueCell.Controls.Add(ltl);
                        AddRow(labelCell, valueCell, null, even);
                    }
                    even = !even;
                }
            }
        }

        if (allowLabel && (labelCell.Text == ""))
        {
            labelCell.Text = "<strong>" + columnName + ":</strong>";
        }

        if (loadValue)
        {
            string textcompare = null;

            switch (columnName.ToLower())
            {
                // Document content - display content of editable regions and editable web parts
                case "documentcontent":
                    EditableItems ei = new EditableItems();
                    ei.LoadContentXml(ValidationHelper.GetString(node.GetValue(columnName), ""));

                    // Add text comparison control
                    if (compareNode != null)
                    {
                        EditableItems eiCompare = new EditableItems();
                        eiCompare.LoadContentXml(ValidationHelper.GetString(compareNode.GetValue(columnName), ""));

                        // Create editable regions comparison
                        Hashtable hashtable;
                        Hashtable hashtableCompare;

                        // Source text must be older version
                        if (versionHistoryId < versionCompare)
                        {
                            hashtable = ei.EditableRegions;
                            hashtableCompare = eiCompare.EditableRegions;
                        }
                        else
                        {
                            hashtable = eiCompare.EditableRegions;
                            hashtableCompare = ei.EditableRegions;
                        }

                        // Add comparison
                        AddTableComparison(hashtable, hashtableCompare, "<strong>" + columnName + " ({0}):</strong>", false, false);

                        // Create editable webparts comparison
                        // Source text must be older version
                        if (versionHistoryId < versionCompare)
                        {
                            hashtable = ei.EditableWebParts;
                            hashtableCompare = eiCompare.EditableWebParts;
                        }
                        else
                        {
                            hashtable = eiCompare.EditableWebParts;
                            hashtableCompare = ei.EditableWebParts;
                        }

                        // Add comparison
                        AddTableComparison(hashtable, hashtableCompare, "<strong>" + columnName + " ({0}):</strong>", false, false);
                    }
                    // No compare node
                    else
                    {
                        // Editable regions
                        Hashtable hashtable = ei.EditableRegions;

                        foreach (DictionaryEntry region in hashtable)
                        {
                            if (!String.IsNullOrEmpty((string)region.Value))
                            {
                                valueCell = new TableCell();
                                labelCell = new TableCell();

                                labelCell.Text = "<strong>" + columnName + " (" + MultiKeyHashtable.GetFirstKey((string)region.Key) + "):</strong>";
                                valueCell.Text = HttpUtility.HtmlDecode(HTMLHelper.StripTags((string)region.Value, false));

                                AddRow(labelCell, valueCell, null, even);
                                even = !even;
                            }
                        }

                        // Editable web parts
                        hashtable = ei.EditableWebParts;

                        foreach (DictionaryEntry part in hashtable)
                        {
                            if (!String.IsNullOrEmpty((string)part.Value))
                            {
                                valueCell = new TableCell();
                                labelCell = new TableCell();

                                labelCell.Text = "<strong>" + columnName + " (" + MultiKeyHashtable.GetFirstKey((string)part.Key) + "):</strong>";
                                valueCell.Text = HttpUtility.HtmlDecode(HTMLHelper.StripTags((string)part.Value, false));

                                AddRow(labelCell, valueCell, null, even);
                                even = !even;
                            }
                        }
                    }

                    break;

                // Others, display the string value
                default:
                    // Shift date time values to user time zone
                    object origobject = node.GetValue(columnName);
                    string textorig = null;
                    if (origobject is DateTime)
                    {
                        TimeZoneInfo usedTimeZone = null;
                        textorig = TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(origobject, DateTimeHelper.ZERO_TIME), CurrentUser, CMSContext.CurrentSite, out usedTimeZone);
                    }
                    else
                    {
                        textorig = ValidationHelper.GetString(origobject, "");
                    }

                    // Add text comparison control
                    if (compareNode != null)
                    {
                        // Shift date time values to user time zone
                        object compareobject = compareNode.GetValue(columnName);
                        if (compareobject is DateTime)
                        {
                            TimeZoneInfo usedTimeZone = null;
                            textcompare = TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(compareobject, DateTimeHelper.ZERO_TIME), CurrentUser, CMSContext.CurrentSite, out usedTimeZone);
                        }
                        else
                        {
                            textcompare = ValidationHelper.GetString(compareobject, "");
                        }

                        comparefirst = new TextComparison();
                        comparefirst.SynchronizedScrolling = false;

                        comparesecond = new TextComparison();
                        comparesecond.SynchronizedScrolling = false;
                        comparesecond.RenderingMode = TextComparisonTypeEnum.DestinationText;

                        // Source text must be older version
                        if (versionHistoryId < versionCompare)
                        {
                            comparefirst.SourceText = HttpUtility.HtmlDecode(HTMLHelper.StripTags(textorig, false));
                            comparefirst.DestinationText = HttpUtility.HtmlDecode(HTMLHelper.StripTags(textcompare, false));
                        }
                        else
                        {
                            comparefirst.SourceText = HttpUtility.HtmlDecode(HTMLHelper.StripTags(textcompare, false));
                            comparefirst.DestinationText = HttpUtility.HtmlDecode(HTMLHelper.StripTags(textorig, false));
                        }

                        comparefirst.PairedControl = comparesecond;

                        if (Math.Max(comparefirst.SourceText.Length, comparefirst.DestinationText.Length) < 100)
                        {
                            comparefirst.BalanceContent = false;
                        }

                        valueCell.Controls.Add(comparefirst);
                        valueCompare.Controls.Add(comparesecond);
                        switchSides = false;
                    }
                    else
                    {
                        valueCell.Text = HttpUtility.HtmlDecode(HTMLHelper.StripTags(textorig, false));
                    }

                    empty = (String.IsNullOrEmpty(textorig)) && (String.IsNullOrEmpty(textcompare));
                    break;
            }
        }

        if (!empty)
        {
            if (compareNode != null)
            {
                AddRow(labelCell, valueCell, valueCompare, switchSides, null, even);
                even = !even;
            }
            else
            {
                AddRow(labelCell, valueCell, null, even);
                even = !even;
            }
        }
    }


    /// <summary>
    /// Gets the hashtable of attachments from the DataSet
    /// </summary>
    /// <param name="ds">Source DataSet</param>
    /// <param name="versionId">Version history ID</param>
    /// <param name="documentId">Document ID</param>
    protected Hashtable GetAttachmentsTable(DataSet ds, int versionId, int documentId)
    {
        Hashtable result = new Hashtable();
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                // Get attachment GUID
                Guid attachmentGuid = (Guid)(dr["AttachmentGUID"]);

                // Get attachment extension
                string attachmentExt = (string)dr["AttachmentExtension"];
                bool isImage = ImageHelper.IsImage(attachmentExt);
                string iconUrl = GetFileIconUrl(attachmentExt, "List");

                // Get link for attachment
                string attachmentUrl = null;
                string attName = (string)dr["AttachmentName"];
                attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(attachmentGuid, UrlHelper.GetSafeFileName(attName, CMSContext.CurrentSiteName), versionId, "getattachment"));
                //attachmentUrl = UrlHelper.UpdateParameterInUrl(attachmentUrl, "chset", Guid.NewGuid().ToString());

                // Ensure correct URL
                attachmentUrl = UrlHelper.AddParameterToUrl(attachmentUrl, "sitename", CMSContext.CurrentSiteName);

                // Optionally trim attachment name
                string attachmentName = TextHelper.LimitLength(attName, 90, "...");

                // Tooltip
                string tooltip = null;
                if (isImage)
                {
                    int attachmentWidth = ValidationHelper.GetInteger(dr["AttachmentImageWidth"], 0);
                    if (attachmentWidth > 300)
                    {
                        attachmentWidth = 300;
                    }
                    tooltip = "onmouseout=\"UnTip()\" onmouseover=\"TipImage(" + attachmentWidth + ", '" + UrlHelper.AddParameterToUrl(attachmentUrl, "width", "300") + "', '" + attachmentName + "')\"";
                }

                string attachmentSize = SqlHelperClass.GetSizeString(ValidationHelper.GetLong(dr["AttachmentSize"], 0));

                string attachment = null;
                if (isImage)
                {
                    // Icon
                    string imageTag = "<img class=\"Icon\" style=\"cursor: pointer;\"  src=\"" + iconUrl + "\" alt=\"" + attachmentName + "\" " + tooltip + " onclick=\"javascript: window.open('" + attachmentUrl + "'); return false;\" />";
                    attachment = imageTag + " " + attachmentName + " (" + attachmentSize + ")<br />";
                }
                else
                {
                    // Icon
                    string imageTag = "<img class=\"Icon\" style=\"cursor: pointer;\"  src=\"" + iconUrl + "\" alt=\"" + attachmentName + "\" onclick=\"javascript: window.open('" + attachmentUrl + "'); return false;\"  />";
                    attachment = imageTag + " " + attachmentName + " (" + attachmentSize + ")<br />";
                }

                result[attachmentGuid.ToString()] = attachment;
            }
        }

        return result;
    }


    /// <summary>
    /// Adds the table comparisson for matching objects
    /// </summary>
    /// <param name="hashtable">Left side table</param>
    /// <param name="hashtableCompare">Right side table</param>
    /// <param name="titleFormat">Title format string</param>
    /// <param name="attachments">If true, the HTML code is kept (attachment comparison)</param>
    /// <param name="renderOnlyFirstTitle">If true, only first title is rendered</param>
    protected void AddTableComparison(Hashtable hashtable, Hashtable hashtableCompare, string titleFormat, bool attachments, bool renderOnlyFirstTitle)
    {
        TableCell valueCell = new TableCell();
        TableCell valueCompare = new TableCell();
        TableCell labelCell = new TableCell();
        TextComparison comparefirst = null;
        TextComparison comparesecond = null;

        bool firstTitle = true;

        // Go through left column regions
        if (hashtable != null)
        {
            foreach (DictionaryEntry entry in hashtable)
            {
                object value = entry.Value;
                if (value != null)
                {
                    // Initialize comparators
                    comparefirst = new TextComparison();
                    comparefirst.SynchronizedScrolling = false;

                    comparesecond = new TextComparison();
                    comparesecond.SynchronizedScrolling = false;
                    comparesecond.RenderingMode = TextComparisonTypeEnum.DestinationText;

                    if (attachments)
                    {
                        comparefirst.IgnoreHTMLTags = true;
                        comparefirst.ConsiderHTMLTagsEqual = true;

                        comparesecond.IgnoreHTMLTags = true;
                    }

                    comparefirst.PairedControl = comparesecond;

                    // Initialize cells
                    valueCell = new TableCell();
                    valueCompare = new TableCell();
                    labelCell = new TableCell();

                    string key = (string)entry.Key;
                    if (firstTitle || !renderOnlyFirstTitle)
                    {
                        labelCell.Text = String.Format(titleFormat, MultiKeyHashtable.GetFirstKey(key));
                        firstTitle = false;
                    }

                    if (attachments)
                    {
                        comparefirst.SourceText = (string)value;
                    }
                    else
                    {
                        comparefirst.SourceText = HttpUtility.HtmlDecode(HTMLHelper.StripTags((string)value, false));
                    }

                    if ((hashtableCompare != null) && hashtableCompare.Contains(key))
                    {
                        // Compare to the existing other version
                        if (attachments)
                        {
                            comparefirst.DestinationText = (string)hashtableCompare[key];
                        }
                        else
                        {
                            comparefirst.DestinationText = HttpUtility.HtmlDecode(HTMLHelper.StripTags((string)hashtableCompare[key], false));
                        }
                        hashtableCompare.Remove(key);
                    }
                    else
                    {
                        // Compare to an empty string
                        comparefirst.DestinationText = "";
                    }

                    // Do not balance content if too short
                    if (attachments)
                    {
                        comparefirst.BalanceContent = false;
                    }
                    else if (Math.Max(comparefirst.SourceText.Length, comparefirst.DestinationText.Length) < 100)
                    {
                        comparefirst.BalanceContent = false;
                    }

                    // Create cell comparison
                    valueCell.Controls.Add(comparefirst);
                    valueCompare.Controls.Add(comparesecond);

                    AddRow(labelCell, valueCell, valueCompare, false, null, even);
                    even = !even;
                }
            }
        }

        // Go through right column regions which left
        if (hashtableCompare != null)
        {
            foreach (DictionaryEntry entry in hashtableCompare)
            {
                object value = entry.Value;
                if (value != null)
                {
                    // Initialize comparators
                    comparefirst = new TextComparison();
                    comparefirst.SynchronizedScrolling = false;

                    comparesecond = new TextComparison();
                    comparesecond.SynchronizedScrolling = false;
                    comparesecond.RenderingMode = TextComparisonTypeEnum.DestinationText;

                    comparefirst.PairedControl = comparesecond;

                    if (attachments)
                    {
                        comparefirst.IgnoreHTMLTags = true;
                        comparefirst.ConsiderHTMLTagsEqual = true;

                        comparesecond.IgnoreHTMLTags = true;
                    }

                    // Initialize cells
                    valueCell = new TableCell();
                    valueCompare = new TableCell();
                    labelCell = new TableCell();

                    if (firstTitle || !renderOnlyFirstTitle)
                    {
                        labelCell.Text = String.Format(titleFormat, MultiKeyHashtable.GetFirstKey((string)entry.Key));
                        firstTitle = false;
                    }

                    comparefirst.SourceText = "";
                    if (attachments)
                    {
                        comparefirst.DestinationText = (string)value;
                    }
                    else
                    {
                        comparefirst.DestinationText = HttpUtility.HtmlDecode(HTMLHelper.StripTags((string)value, false));
                    }

                    if (attachments)
                    {
                        comparefirst.BalanceContent = false;
                    }
                    else if (Math.Max(comparefirst.SourceText.Length, comparefirst.DestinationText.Length) < 100)
                    {
                        comparefirst.BalanceContent = false;
                    }

                    // Create cell comparison
                    valueCell.Controls.Add(comparefirst);
                    valueCompare.Controls.Add(comparesecond);

                    AddRow(labelCell, valueCell, valueCompare, false, null, even);
                    even = !even;
                }
            }
        }
    }


    /// <summary>
    /// Creates 2 column table.
    /// </summary>
    /// <param name="labelCell">Cell with label</param>
    /// <param name="valueCell">Cell with content</param>
    /// <returns>Returns TableRow object</returns>
    private TableRow AddRow(TableCell labelCell, TableCell valueCell, string cssClass, bool even)
    {

        TableRow newRow = new TableRow();

        // Set CSS
        if (!String.IsNullOrEmpty(cssClass))
        {
            newRow.CssClass = cssClass;
        }
        else if (even)
        {
            newRow.CssClass = "EvenRow";
        }
        else
        {
            newRow.CssClass = "OddRow";
        }

        // Set proper style
        labelCell.Wrap = false;
        newRow.Cells.Add(labelCell);

        valueCell.Width = new Unit(100, UnitType.Percentage);
        newRow.Cells.Add(valueCell);

        tblDocument.Rows.Add(newRow);
        return newRow;

    }


    /// <summary>
    /// Creates 3 column table. Older version must be always in the left column.
    /// </summary>
    /// <param name="labelCell">Cell with label</param>
    /// <param name="valueCell">Cell with content</param>
    /// <param name="compareCell">Cell with compare content</param>
    /// <param name="switchSides">Indicates if cells should be switched to match corresponding version</param>
    /// <param name="cssClass">CSS class</param>
    /// <param name="even">Determines whether row is odd or even</param>
    /// <returns>Returns TableRow object</returns>
    private TableRow AddRow(TableCell labelCell, TableCell valueCell, TableCell compareCell, bool switchSides, string cssClass, bool even)
    {
        TableRow newRow = new TableRow();

        if (!String.IsNullOrEmpty(cssClass))
        {
            newRow.CssClass = cssClass;
        }
        else if (even)
        {
            newRow.CssClass = "EvenRow";
        }
        else
        {
            newRow.CssClass = "OddRow";
        }

        labelCell.Wrap = false;
        newRow.Cells.Add(labelCell);

        const int cellWidth = 40;

        // Switch sides to match version
        if (switchSides)
        {
            // Older version must be in the left column
            if (versionHistoryId < versionCompare)
            {
                valueCell.Width = new Unit(cellWidth, UnitType.Percentage);
                newRow.Cells.Add(valueCell);

                compareCell.Width = new Unit(cellWidth, UnitType.Percentage);
                newRow.Cells.Add(compareCell);
            }
            else
            {
                compareCell.Width = new Unit(cellWidth, UnitType.Percentage);
                newRow.Cells.Add(compareCell);

                valueCell.Width = new Unit(cellWidth, UnitType.Percentage);
                newRow.Cells.Add(valueCell);
            }
        }
        // Do not switch sides
        else
        {
            valueCell.Width = new Unit(cellWidth, UnitType.Percentage);
            newRow.Cells.Add(valueCell);

            compareCell.Width = new Unit(cellWidth, UnitType.Percentage);
            newRow.Cells.Add(compareCell);
        }

        tblDocument.Rows.Add(newRow);
        return newRow;
    }


    /// <summary>
    ///  Returns link to attachment.
    /// </summary>
    private HyperLink GetLink(AttachmentInfo ai)
    {
        if (ai != null)
        {
            HyperLink lnkFile = new HyperLink();
            lnkFile.Text = ai.AttachmentName;
            string url = ResolveUrl(DocumentHelper.GetAttachmentUrl(ai, versionHistoryId));

            // Show tooltip for images
            if (ImageHelper.IsImage(ai.AttachmentExtension))
            {
                int attachmentWidth = ai.AttachmentImageWidth;
                if (attachmentWidth > 300)
                {
                    attachmentWidth = 300;
                }

                lnkFile.Attributes.Add("onmouseover", "Tip('<div style=\\'width:" + attachmentWidth + "px;text-align:center;\\'><img src=\\'" + UrlHelper.AddParameterToUrl(url, "maxsidesize", "300") + "\\' alt=\\'" + ai.AttachmentName + "\\' /></div>')");
                lnkFile.Attributes.Add("onmouseout", "UnTip()");
            }

            lnkFile.Attributes.Add("onclick", "javascript:window.open('" + url + "&maxsidesize=600');return false;");
            lnkFile.NavigateUrl = "#";
            return lnkFile;
        }
        else
        {
            return null;
        }
    }


    /// <summary>
    /// Creates attachment string.
    /// </summary>
    private string CreateAttachment(AttachmentInfo ai, int versionID)
    {
        string result = null;
        if (ai != null)
        {
            bool isImage = ImageHelper.IsImage(ai.AttachmentExtension);
            string iconUrl = GetFileIconUrl(ai.AttachmentExtension, "List");

            // Get link for attachment
            string attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(ai.AttachmentGUID, UrlHelper.GetSafeFileName(ai.AttachmentName, CMSContext.CurrentSiteName), versionID, "getattachment"));

            // Ensure correct URL
            attachmentUrl = UrlHelper.AddParameterToUrl(attachmentUrl, "sitename", CMSContext.CurrentSiteName);

            // Optionally trim attachment name
            string attachmentName = TextHelper.LimitLength(ai.AttachmentName, 90, "...");

            // Tooltip
            string tooltip = null;
            if (isImage)
            {
                int attachmentWidth = ai.AttachmentImageWidth;
                if (attachmentWidth > 300)
                {
                    attachmentWidth = 300;
                }
                tooltip = "onmouseout=\"UnTip()\" onmouseover=\"TipImage(" + attachmentWidth + ", '" + UrlHelper.AddParameterToUrl(attachmentUrl, "width", "300") + "', '" + attachmentName + "')\"";
            }
            string attachmentSize = SqlHelperClass.GetSizeString(ai.AttachmentSize);
            if (isImage)
            {
                // Icon
                string imageTag = "<img class=\"Icon\" style=\"cursor: pointer;\"  src=\"" + iconUrl + "\" alt=\"" + attachmentName + "\" " + tooltip + " onclick=\"javascript: window.open('" + attachmentUrl + "'); return false;\" />";
                result += imageTag + " " + attachmentName + " (" + attachmentSize + ")<br />";
            }
            else
            {
                // Icon
                string imageTag = "<img class=\"Icon\" style=\"cursor: pointer;\"  src=\"" + iconUrl + "\" alt=\"" + attachmentName + "\" onclick=\"javascript: window.open('" + attachmentUrl + "'); return false;\"  />";
                result += imageTag + " " + attachmentName + " (" + attachmentSize + ")<br />";
            }
        }
        return result;
    }
    #endregion


    #region "Events"

    /// <summary>
    /// Dropdown list selection changed.
    /// </summary>
    void drpCompareTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string url = UrlHelper.CurrentURL;
        if (drpCompareTo.SelectedIndex == 0)
        {
            url = UrlHelper.RemoveParameterFromUrl(url, "compareHistoryId");
        }
        else
        {
            url = UrlHelper.AddParameterToUrl(url, "compareHistoryId", drpCompareTo.SelectedValue);
        }
        UrlHelper.Redirect(url);
    }

    #endregion
}
