<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FriendsList.ascx.cs" Inherits="CMSModules_Friends_Controls_FriendsList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<%@ Register Src="~/CMSModules/Friends/Controls/FriendsSearchBox.ascx" TagName="FriendsSearchBox"
    TagPrefix="cms" %>
<asp:Panel ID="pnlBody" runat="server" CssClass="Panel" EnableViewState="true">
    <div>
        <cms:LocalizedLinkButton ID="btnRejectSelected" runat="server" ResourceString="friends.friendrejectall"
            EnableViewState="false" CssClass="ContentLinkButton" />&nbsp;
        <cms:LocalizedLinkButton ID="btnRemoveSelected" OnClick="btnRemoveSelected_Click"
            runat="server" ResourceString="friends.friendremoveall" EnableViewState="false"
            CssClass="ContentLinkButton" />
    </div>
    <br />
    <cms:FriendsSearchBox ID="searchElem" runat="server" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="~/CMSModules/Friends/Controls/FriendsList.xml" />
    <asp:HiddenField runat="server" ID="hdnRefresh" />
</asp:Panel>
