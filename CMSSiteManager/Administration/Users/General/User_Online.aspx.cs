using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSSiteManager_Administration_Users_General_User_Online : CMSUsersPage
{
    #region "Variables"

    protected int siteId = 0;
    protected string siteName = string.Empty;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check permissions
        LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.OnlineUsers);

        // try to get siteid
        siteId = QueryHelper.GetInteger("siteid", 0);

        // Set the master page header
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("administration.users.onlineusers");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_User/online.png");
        this.CurrentMaster.Title.HelpTopicName = "User_OnlineUsers";
        // If session management is disabled informm about it
        if (!SessionManager.OnlineUsersEnabled)
        {
            this.lblDisabled.Visible = true;
            this.userFilterElem.Visible = false;
            this.gridElem.Visible = false;
            return;
        }

        // Get sitename        
        if (siteId > 0)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
            if (si != null)
            {
                siteName = si.SiteName;
            }
        }

        // Get count of users
        int publicUsers = 0;
        int authenticatedUsers = 0;
        SessionManager.GetUsersNumber(siteName, null, true, false, out publicUsers, out authenticatedUsers);

        this.lblGeneralInfo.Text = String.Format(ResHelper.GetString("OnlineUsers.GeneralInfo"), publicUsers + authenticatedUsers, publicUsers, authenticatedUsers) + "<br /><br />";

        // Get online users condition
        string usersWhere = ValidationHelper.GetString(SessionManager.GetUsersWhereCondition(null, siteName, true, true), String.Empty);

        if (!String.IsNullOrEmpty(usersWhere))
        {
            // Non-DB users
            if (!SessionManager.StoreOnlineUsersInDatabase)
            {
                this.gridElem.Query = "cms.user.selectallview";
            }
            // DB users
            else
            {
                this.gridElem.Query = "cms.user.selectusersession";
            }
            this.gridElem.Columns = "UserID, UserName, FullName, Email, UserNickName, UserCreated, UserEnabled, UserIsGlobalAdministrator";
            this.gridElem.WhereClause = usersWhere;
        }
        else
        {
            // Clear query
            this.gridElem.Query = "";
        }

        // Set up unigrid events
        this.gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        this.gridElem.OnBeforeDataReload += new OnBeforeDataReload(gridElem_OnBeforeDataReload);
        this.gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        this.gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");

    }

    #endregion


    #region "Unigrid"

    /// <summary>
    /// Set where condition before data binding
    /// </summary>
    protected void gridElem_OnBeforeDataReload()
    {
        if (!RequestHelper.IsAsyncPostback())
        {
            gridElem.WhereCondition = userFilterElem.WhereCondition;
        }
    }


    /// <summary>
    ///  On action event.
    /// </summary>
    void gridElem_OnAction(string actionName, object actionArgument)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        int userId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName.ToLower())
        {
            // Kick action
            case "kick":
                SessionManager.KickUser(userId);
                break;
            // Undo kick action
            case "undokick":
                SessionManager.RemoveUserFromKicked(userId);
                break;
        }
    }


    /// <summary>
    ///  On external databound event.
    /// </summary>
    object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        int userID = 0;
        switch (sourceName)
        {
            // Check if user was kicked and if so inform about it
            case "formattedusername":
                DataRowView drv = (DataRowView)parameter;
                if (drv != null)
                {
                    UserInfo ui = new UserInfo(drv.Row);
                    if (ui != null)
                    {
                        string userName = Functions.GetFormattedUserName(ui.UserName);
                        if (UserInfoProvider.UserKicked(ui.UserID))
                        {
                            return HTMLHelper.HTMLEncode(userName) + " <span style=\"color:#ee0000;\">" + ResHelper.GetString("administration.users.onlineusers.kicked") + "</span>";
                        }

                        return HTMLHelper.HTMLEncode(userName);
                    }
                }
                return "";

            // Is user enabled
            case "userenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);

            case "kick":
                userID = ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["UserID"], 0);
                bool userIsAdmin = ValidationHelper.GetBoolean(((DataRowView)((GridViewRow)parameter).DataItem).Row["UserIsGlobalAdministrator"], false);

                if (UserInfoProvider.UserKicked(userID) || userIsAdmin)
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Kickdisabled.png");
                    button.Enabled = false;
                }
                else
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Kick.png");
                    button.Enabled = true;
                }
                return "";

            case "undokick":
                userID = ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["UserID"], 0);
                if (UserInfoProvider.UserKicked(userID))
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Undo.png");
                    button.Enabled = true;
                }
                else
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Undodisabled.png");
                    button.Enabled = false;
                }
                return "";

            default:
                return "";
        }
    }

    #endregion
}
