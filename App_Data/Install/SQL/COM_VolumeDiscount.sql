CREATE TABLE [COM_VolumeDiscount] (
		[VolumeDiscountID]               int NOT NULL IDENTITY(1, 1),
		[VolumeDiscountSKUID]            int NOT NULL,
		[VolumeDiscountMinCount]         int NOT NULL,
		[VolumeDiscountValue]            float NOT NULL,
		[VolumeDiscountIsFlatValue]      bit NOT NULL,
		[VolumeDiscountGUID]             uniqueidentifier NOT NULL,
		[VolumeDiscountLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_VolumeDiscount]
	ADD
	CONSTRAINT [PK_COM_VolumeDiscount]
	PRIMARY KEY
	([VolumeDiscountID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_VolumeDiscount]') IS NOT NULL
	AND OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_VolumeDiscount_VolumeDiscountSKUID_COM_SKU]') IS NULL
BEGIN
		ALTER TABLE [COM_VolumeDiscount]
			ADD CONSTRAINT [FK_COM_VolumeDiscount_VolumeDiscountSKUID_COM_SKU]
			FOREIGN KEY ([VolumeDiscountSKUID]) REFERENCES [COM_SKU] ([SKUID])
END
CREATE INDEX [IX_COM_VolumeDiscount_VolumeDiscountSKUID]
	ON [COM_VolumeDiscount] ([VolumeDiscountSKUID])
	ON [PRIMARY]
