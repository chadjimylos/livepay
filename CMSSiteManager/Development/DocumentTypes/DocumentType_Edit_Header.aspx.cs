using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Header : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int documentTypeId = QueryHelper.GetInteger("documenttypeid", 0);
        if (documentTypeId > 0)
        {            
            DataClassInfo classInfo = DataClassInfoProvider.GetDataClass(documentTypeId);

            // Document exists
            if (classInfo != null)
            {
                //initializes PageTitle
                string documentTypes = ResHelper.GetString("DocumentType_Edit_Header.DocumentTypes");
                string actualDocumentType = classInfo.ClassDisplayName;
                string[,] pageTitleTabs = new string[2, 3];
                pageTitleTabs[0, 0] = documentTypes;
                pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/DocumentTypes/DocumentType_List.aspx";
                pageTitleTabs[0, 2] = "_parent";
                pageTitleTabs[1, 0] = actualDocumentType;
                pageTitleTabs[1, 1] = "";
                pageTitleTabs[1, 2] = "";

                this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("DocumentType_Edit_Header.Title");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_DocumentType/objectproperties.png");
                this.CurrentMaster.Title.HelpTopicName = "general_tab18";
                this.CurrentMaster.Title.HelpName = "helpTopic";

                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ShowContent", ScriptHelper.GetScript("function ShowContent(contentLocation) { parent.content.location.href = contentLocation; }"));

                if (!RequestHelper.IsPostBack())
                {
                    // initialize menu
                    InitializeMenu(classInfo);
                }
            }
        }
    }


    /// <summary>
    /// Initializes DocumentType edit menu
    /// </summary>
    /// <param name="docTypeId">DocumentTypeID</param>
    protected void InitializeMenu(DataClassInfo classObj)
    {
        string[,] tabs = new string[11, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab18');";
        tabs[0, 2] = "DocumentType_Edit_General.aspx?documenttypeid=" + classObj.ClassID;
        tabs[1, 0] = ResHelper.GetString("general.fields");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'fields_tab2');";
        tabs[1, 2] = "DocumentType_Edit_Fields.aspx?documenttypeid=" + classObj.ClassID;
        tabs[2, 0] = ResHelper.GetString("general.form");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'form_tab3');";
        tabs[2, 2] = "DocumentType_Edit_Form.aspx?documenttypeid=" + classObj.ClassID;
        tabs[3, 0] = ResHelper.GetString("DocumentType_Edit_Header.Transformations");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'transformations_tab');";
        tabs[3, 2] = "DocumentType_Edit_Transformation_List.aspx?documenttypeid=" + classObj.ClassID;
        tabs[4, 0] = ResHelper.GetString("DocumentType_Edit_Header.Queries");
        tabs[4, 1] = ""; // "SetHelpTopic('helpTopic', 'queries_tab');";
        tabs[4, 2] = "DocumentType_Edit_Query_List.aspx?documenttypeid=" + classObj.ClassID;
        tabs[5, 0] = ResHelper.GetString("DocumentType_Edit_Header.ChildTypes");
        tabs[5, 1] = ""; // "SetHelpTopic('helpTopic', 'child_types_tab');";
        tabs[5, 2] = "DocumentType_Edit_ChildTypes.aspx?documenttypeid=" + classObj.ClassID;
        tabs[6, 0] = ResHelper.GetString("general.sites");
        tabs[6, 1] = ""; // "SetHelpTopic('helpTopic', 'sites_tab7');";
        tabs[6, 2] = "DocumentType_Edit_Sites.aspx?documenttypeid=" + classObj.ClassID;

        int indx = 7;
        if (classObj.ClassIsProduct && ModuleEntry.IsModuleLoaded(ModuleEntry.ECOMMERCE))
        {
            tabs[indx, 0] = ResHelper.GetString("DocumentType_Edit_Header.Ecommerce");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'e_commerce_tab');";
            tabs[indx, 2] = ResolveUrl("~/CMSModules/Ecommerce/Development/DocumentTypes/DocumentType_Edit_Ecommerce.aspx") + "?documenttypeid=" + classObj.ClassID;
            indx++;
        }        

        tabs[indx, 0] = ResHelper.GetString("doc.header.altforms");
        tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'doctype_alt_form');";
        tabs[indx, 2] = "AlternativeForms/AlternativeForms_List.aspx?classid=" + classObj.ClassID;
        indx++;

        tabs[indx, 0] = ResHelper.GetString("srch.fields");
        tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'search_fields');";
        tabs[indx, 2] = "DocumentType_Edit_SearchFields.aspx?documenttypeid=" + classObj.ClassID;
        indx++;

        tabs[indx, 0] = ResHelper.GetString("general.documents");
        tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'doctype_documents');";
        tabs[indx, 2] = "DocumentType_Edit_Documents.aspx?documenttypeid=" + classObj.ClassID;
        
        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
