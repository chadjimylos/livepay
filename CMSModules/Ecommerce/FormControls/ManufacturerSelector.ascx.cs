using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.Ecommerce;
using CMS.ExtendedControls;

public partial class CMSModules_Ecommerce_FormControls_ManufacturerSelector : FormEngineUserControl
{
    #region "Variables"

    private bool mAddAllItemsRecord = true;
    private bool mAddNoneRecord = false;
    private StatusEnum mDisplayItems = StatusEnum.Both;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Indicates which objects should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.ManufacturerID;
        }
        set
        {
            this.ManufacturerID = ValidationHelper.GetInteger(value, 0);
        }
    }


    /// <summary>
    /// Gets or sets the Manufacturer ID.
    /// </summary>
    public int ManufacturerID
    {
        get
        {
            return ValidationHelper.GetInteger(uniSelector.Value, 0);
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            this.uniSelector.Value = value;
        }
    }


    /// <summary>
    /// Gets or sets the Manufacturer code name.
    /// </summary>
    public string ManufacturerName
    {
        get
        {
            int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
            ManufacturerInfo mi = ManufacturerInfoProvider.GetManufacturerInfo(id);
            if (mi != null)
            {
                return mi.ManufacturerDisplayName;
            }
            return "";
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            ManufacturerInfo mi = ManufacturerInfoProvider.GetManufacturerInfo(value);
            if (mi != null)
            {
                this.uniSelector.Value = mi.ManufacturerID;
            }
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add all item record to the dropdownlist.
    /// </summary>
    public bool AddAllItemsRecord
    {
        get
        {
            return mAddAllItemsRecord;
        }
        set
        {
            mAddAllItemsRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }


    /// <summary>
    /// Inner control
    /// </summary>
    public DropDownList InnerControl
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.EnabledColumnName = "ManufacturerEnabled";
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowAll = this.AddAllItemsRecord;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;
        if (this.DisplayItems == StatusEnum.Disabled)
        {
            this.uniSelector.WhereCondition = "ManufacturerEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            this.uniSelector.WhereCondition = "ManufacturerEnabled = 1";
        }
    }
}
