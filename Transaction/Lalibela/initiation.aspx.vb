Imports System.Data
Imports System.Data.SqlClient
Partial Class TRANSACTION_INITIATION
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSubmit.Attributes("onClick") = "return payClicked();"
        merchantID.Value = System.Configuration.ConfigurationManager.AppSettings("LalibelaApacsUserID")
        APACSURL.Text = System.Configuration.ConfigurationManager.AppSettings("LalibelaAPACSURL")

        Dim remoteAddr As String = Request.UserHostAddress

        'Try
        Dim AmountEncrypt As String
        If Request.QueryString("Am") <> "" Then
            AmountEncrypt = CryptoFunctions.DecryptString(Request("Am"))
        Else
            AmountEncrypt = Request("Amount")
        End If
	
	Session("BizFormID") = Request("BizFormID")

        amount.Value = AmountEncrypt 'GET THE CORRECT AMOUNT HERE
        'Var1.Value = email 'GET THE TRANSACTIONCODE
        lang.Value = Request("Lang") '/EN GET THE CORRECT LANGUAGE
	lang.Value = lang.Value.toUpper()
        merchantRef.Value = GetMerchantRef(amount.Value)
        'Catch ex As Exception
        'Response.Redirect("OnlineCheckOut.aspx")
        'End Try
    End Sub

    Private Function GetMerchantRef(ByVal Amount As String) As String
        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("Lalibela_GetMerchantRef", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@Amount", Amount)

        Dim merchantRef As String = objCommand.ExecuteScalar()

        objConnection.Close()
        Return merchantRef
    End Function
End Class
