using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Controls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;

/// <summary>
/// Summary description for CMSUserControlEvents
/// </summary>
public static class CMSUserControlEvents
{
    /// <summary>
    /// Fires before UserControl PreInit
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforeUserControlPreInit(object sender, EventArgs e)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after UserControl PreInit
    /// </summary>
    public static void AfterUserControlPreInit(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before UserControl Init
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforeUserControlInit(object sender, EventArgs e)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after UserControl Init
    /// </summary>
    public static void AfterUserControlInit(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before UserControl Load
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforeUserControlLoad(object sender, EventArgs e)
    {
        // Add your custom actions
        return true;
    }


    /// <summary>
    /// Fires after UserControl Load
    /// </summary>
    public static void AfterUserControlLoad(object sender, EventArgs e)
    {
        // Add your custom actions
        /*
        CMSUserControl userControl = (CMSUserControl)sender;
        switch (userControl.RelativePath.ToLower())
        {
            case "/cmsadmincontrols/pagetitle.ascx":
                // Modify the control
                break;
        }
        */
    }


    /// <summary>
    /// Fires before UserControl PreRender
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforeUserControlPreRender(object sender, EventArgs e)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after UserControl PreRender
    /// </summary>
    public static void AfterUserControlPreRender(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before UserControl Render
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforeUserControlRender(object sender, HtmlTextWriter writer)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after UserControl Render
    /// </summary>
    public static void AfterUserControlRender(object sender, HtmlTextWriter writer)
    {
        // Add your custom actions
    }
}
