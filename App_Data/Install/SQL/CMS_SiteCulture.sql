CREATE TABLE [CMS_SiteCulture] (
		[SiteID]        int NOT NULL,
		[CultureID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_SiteCulture]
	ADD
	CONSTRAINT [PK_CMS_SiteCulture]
	PRIMARY KEY
	([SiteID], [CultureID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_SiteCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Culture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SiteCulture_CultureID_CMS_Culture]') IS NULL
BEGIN
		ALTER TABLE [CMS_SiteCulture]
			ADD CONSTRAINT [FK_CMS_SiteCulture_CultureID_CMS_Culture]
			FOREIGN KEY ([CultureID]) REFERENCES [CMS_Culture] ([CultureID])
END
IF OBJECT_ID(N'[CMS_SiteCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SiteCulture_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_SiteCulture]
			ADD CONSTRAINT [FK_CMS_SiteCulture_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
