<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxClass_List.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="TaxClass_List.xml" OrderBy="TaxClassDisplayName"
        IsLiveSite="false" Columns="TaxClassID,TaxClassDisplayName" />
</asp:Content>
