using System;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSModules_Ecommerce_Tools_Products_Product_Edit_Documents : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get product ID form url
        productDocuments.ProductID = QueryHelper.GetInteger("productid", 0);
    }
}
