<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Logon/PasswdRetrieve.ascx.cs"
    Inherits="CMSWebParts_LivePay_Logon_PasswdRetrieve" %>

<style>
.passwdret-rieval
{
    width:500px;
}
.passwdret-rieval .label
{
    float:left;
    padding-right:10px;
    line-height:22px;
}
.passwdret-rieval .text
{
    float:left;
}

.button
{
    background:transparent url(/App_Themes/LivePay/Contact/btnSend.png) no-repeat;
    width:80px;
    height:23px;
    border:0px;
    cursor:hand;
    border-top-width:0px;
    border-bottom-width:0px;
}
</style>
<asp:Panel ID="Panel1" runat="server">
    <div class="logon-form" style="width:670px;background-color:#f1f1f1;padding-top:0px;font-size:12px;min-height:130px">
        <div style="padding:15px 0px 10px 15px">
                <asp:Panel ID="pnlPasswdRetrieval" runat="server" DefaultButton="btnPasswdRetrieval">
                    <div class="passwdret-rieval">
                        <div class="label">
                                <asp:Label ID="lblPasswdRetrieval" runat="server" EnableViewState="false" AssociatedControlID="txtPasswordRetrieval" />
                        </div>

                        <div class="text" id="EmailDiv" runat="server">
                                <asp:TextBox ID="txtPasswordRetrieval" runat="server" />
                                <cms:CMSButton ID="btnPasswdRetrieval" runat="server" CssClass="button" ValidationGroup="PsswdRetrieval" EnableViewState="false" Text="23333" /><br />
                                <asp:RequiredFieldValidator ID="rqValue" runat="server"  ControlToValidate="txtPasswordRetrieval" ValidationGroup="PsswdRetrieval" EnableViewState="false" />
                        </div>
                        <div class="Clear"></div>

                        <div class="text" style="display:none;color:black" id="QuestDiv" runat="server">
                            <div id="QuestAnsDescr" runat="server" visible="false" style="padding-bottom:10px;font-weight:bold"><%=ResHelper.LocalizeString("{$=��������� ����� ���� ������� ���������|en-us=��������� ����� ���� ������� ���������$}")%></div> 
                            <div class="Clear"></div>
                            <div class="FLEFT" style="color:Black"><%=ResHelper.LocalizeString("{$=������� ���������|en-us=������� ���������$}")%>:</div>
                            <div class="FLEFT" style="color:Black;padding-left:10px;font-weight:bold" id="quest" runat="server" ></div>
                            <div class="Clear"></div>
                            <div style="padding-top:5px">
                                <div class="FLEFT" style="color:Black"><%=ResHelper.LocalizeString("{$=��������|en-us=��������$}")%>:</div>
                                <div class="FLEFT" style="padding-left:10px;">
                                    <asp:TextBox  Width="300px" ID="txtAnswer" runat="server"></asp:TextBox>
                                </div>
                                <div class="Clear"></div>
                                <div style="padding-left:70px;padding-top:5px"><cms:CMSButton ID="CMSBtnSendAns" runat="server" CssClass="button" EnableViewState="false"  />&nbsp;<asp:Label ID="lblEroorAns" runat="server" Visible="false" EnableViewState="false" /></div>
                            </div>
                        </div> 
                    </div>
                    <asp:Label ID="lblResult" runat="server" Visible="false" EnableViewState="false" />
                </asp:Panel>
        </div>
    </div>
    
</asp:Panel>
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />
