﻿// Select node in CMSDesk-Content tree
function TreeSelectNode(nodeId) {
    if (parent.parent != null) {
        if (parent.parent.frames['contenttree'] != null) {
            if (parent.parent.frames['contenttree'].SelectNode != null) {
                parent.parent.frames['contenttree'].SelectNode(nodeId);

            }
        }
    }
}

// Refresh node in CMSDesk-Content tree
function TreeRefreshNode(nodeId, selectNodeId) {
    if (parent.parent != null) {
        if (parent.parent.frames['contenttree'] != null) {
            if (parent.parent.frames['contenttree'].RefreshNode != null) {
                parent.parent.frames['contenttree'].RefreshNode(nodeId, selectNodeId);
            }
        }
    }
}