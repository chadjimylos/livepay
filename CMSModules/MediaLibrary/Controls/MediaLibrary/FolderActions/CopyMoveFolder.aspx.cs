using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Security.Principal;
using System.Data;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.EventLog;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.MediaLibrary;

public partial class CMSModules_MediaLibrary_Controls_MediaLibrary_FolderActions_CopyMoveFolder : CMSLiveModalPage
{
    #region "Variables"

    private static Hashtable mErrors = new Hashtable();
    private static Hashtable mInfos = new Hashtable();

    private static string refreshScript = null;

    private int mMediaLibraryID = 0;
    private MediaLibraryInfo mLibraryInfo = null;
    private SiteInfo mLibrarySiteInfo = null;
    private string mLibraryRootFolder = null;
    private string mLibraryPath = "";
    private bool mAllFiles = false;
    private string mAction = null;
    private string mRootFolder = null;
    private string mFolderPath = null;
    private string mNewPath = null;
    private string mFiles = null;
    private CurrentUserInfo mCurrentUser = null;

    private string currUrl = "";

    #endregion


    #region "Private properties"

    /// <summary>
    /// ID of the media library.
    /// </summary>
    private int MediaLibraryID
    {
        get
        {
            return this.mMediaLibraryID;
        }
        set
        {
            this.mMediaLibraryID = value;
        }
    }


    /// <summary>
    /// Gets current library info.
    /// </summary>
    private MediaLibraryInfo LibraryInfo
    {
        get
        {
            if ((this.mLibraryInfo == null) && (this.MediaLibraryID > 0))
            {
                this.mLibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(this.MediaLibraryID);
            }
            return this.mLibraryInfo;
        }
    }


    /// <summary>
    /// Gets info on site library belongs to.
    /// </summary>
    private SiteInfo LibrarySiteInfo
    {
        get
        {
            if ((this.mLibrarySiteInfo == null) && (this.LibraryInfo != null))
            {
                this.mLibrarySiteInfo = SiteInfoProvider.GetSiteInfo(this.LibraryInfo.LibrarySiteID);
            }
            return this.mLibrarySiteInfo;
        }
    }


    /// <summary>
    /// Type of the action.
    /// </summary>
    private string Action
    {
        get
        {
            return this.mAction;
        }
        set
        {
            this.mAction = value;
        }
    }


    /// <summary>
    /// Media library Folder path.
    /// </summary>
    private string FolderPath
    {
        get
        {
            return this.mFolderPath;
        }
        set
        {
            this.mFolderPath = value;
        }
    }


    /// <summary>
    /// Media library root folder path.
    /// </summary>
    private string RootFolder
    {
        get
        {
            return this.mRootFolder;
        }
        set
        {
            this.mRootFolder = value;
        }
    }


    /// <summary>
    /// Path where the item(s) should be copied/moved.
    /// </summary>
    private string NewPath
    {
        get
        {
            return this.mNewPath;
        }
        set
        {
            this.mNewPath = value;
        }
    }


    /// <summary>
    /// List of files to copy/move.
    /// </summary>
    private string Files
    {
        get
        {
            return this.mFiles;
        }
        set
        {
            this.mFiles = value;
        }
    }


    /// <summary>
    /// Determines whether all files should be copied
    /// </summary>
    private bool AllFiles
    {
        get
        {
            return this.mAllFiles;
        }
        set
        {
            this.mAllFiles = value;
        }
    }


    /// <summary>
    /// Current Error
    /// </summary>
    private string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mErrors["ProcessingError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["ProcessingError_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current Info
    /// </summary>
    private string CurrentInfo
    {
        get
        {
            return ValidationHelper.GetString(mInfos["ProcessingInfo_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mInfos["ProcessingInfo_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Indicates whether the properties are just loaded - no folder was previously selected.
    /// </summary>
    private bool IsLoad
    {
        get
        {
            return QueryHelper.GetBoolean("load", false);
        }
    }

    /// <summary>
    /// Current log context
    /// </summary>
    private LogContext CurrentLog
    {
        get
        {
            return EnsureLog();
        }
    }


    /// <summary>
    /// User performing the action.
    /// </summary>
    private CurrentUserInfo CurrentUser 
    {
        get 
        {
            if (this.mCurrentUser == null) 
            {
                this.mCurrentUser = CMSContext.CurrentUser;
            }
            return this.mCurrentUser;
        }
    }

    #endregion
    

    #region "Public properties"

    /// <summary>
    /// Returns media library root folder path.
    /// </summary>
    public string LibraryRootFolder
    {
        get
        {
            if ((this.LibrarySiteInfo != null) && (mLibraryRootFolder == null))
            {
                mLibraryRootFolder = MediaLibraryHelper.GetMediaRootFolderPath(this.LibrarySiteInfo.SiteName);
            }
            return mLibraryRootFolder;
        }
    }


    /// <summary>
    /// Gets library relative url path.
    /// </summary>
    public string LibraryPath
    {
        get
        {
            if (String.IsNullOrEmpty(this.mLibraryPath))
            {
                if (this.LibraryInfo != null)
                {
                    this.mLibraryPath = this.LibraryRootFolder + this.LibraryInfo.LibraryFolder;
                }
            }
            return this.mLibraryPath;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        this.mCurrentUser = CMSContext.CurrentUser;

        // Initialize events
        this.ctlAsync.OnFinished += ctlAsync_OnFinished;
        this.ctlAsync.OnError += ctlAsync_OnError;
        this.ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        this.ctlAsync.OnCancel += ctlAsync_OnCancel;

        // Get the sorce node
        this.MediaLibraryID = QueryHelper.GetInteger("libraryid", 0);
        this.Action = QueryHelper.GetString("action", "");
        this.FolderPath = QueryHelper.GetString("folderpath", "").Replace("/", "\\");
        this.Files = HttpUtility.UrlDecode(QueryHelper.GetString("files", "").Trim('|'));
        this.RootFolder = MediaLibraryHelper.GetMediaRootFolderPath(CMSContext.CurrentSiteName);
        this.AllFiles = QueryHelper.GetBoolean("allFiles", false);
        this.NewPath = QueryHelper.GetString("newpath", "").Replace("/", "\\");

        this.btnCancel.Text = ResHelper.GetString("general.cancel");

        // Target folder
        string tarFolder = this.NewPath;
        if (string.IsNullOrEmpty(tarFolder) && (this.LibraryInfo != null))
        {
            tarFolder = this.LibraryInfo.LibraryFolder + " (root)";
        }
        this.lblFolder.Text = tarFolder;

        if (!this.IsLoad)
        {
            if (this.AllFiles || String.IsNullOrEmpty(this.Files))
            {
                if (this.AllFiles)
                {
                    this.lblFilesToCopy.ResourceString = "media.folder.filestoall" + this.Action.ToLower();
                }
                else
                {
                    this.lblFilesToCopy.ResourceString = "media.folder.folderto" + this.Action.ToLower();
                }

                // Source folder
                string srcFolder = this.FolderPath;
                if (string.IsNullOrEmpty(srcFolder) && (this.LibraryInfo != null))
                {
                    srcFolder = this.LibraryInfo.LibraryFolder + "&nbsp;(root)";
                }
                this.lblFileList.Text = HTMLHelper.HTMLEncode(srcFolder);
            }
            else
            {
                this.lblFilesToCopy.ResourceString = "media.folder.filesto" + this.Action.ToLower();
                string[] fileList = this.Files.Split('|');
                foreach (string file in fileList)
                {
                    this.lblFileList.Text += HTMLHelper.HTMLEncode(this.FolderPath.TrimEnd('\\') + "\\" + file) + "<br />";
                }
            }

            if (!this.Page.IsCallback && !UrlHelper.IsPostback())
            {
                bool performAction = QueryHelper.GetBoolean("performaction", false);
                if (performAction)
                {
                    // Perform Move or Copy
                    PerformAction();

                    currUrl = UrlHelper.RemoveParameterFromUrl(UrlHelper.CurrentURL, "performaction");
                }
            }

            this.pnlInfo.Visible = true;
            this.pnlEmpty.Visible = false;
        }
        else
        {
            this.pnlInfo.Visible = false;
            this.pnlEmpty.Visible = true;
            this.lblEmpty.Text = ResHelper.GetString("media.copymove.select");
          
            // Disable New folder button
            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "DisableNewFolderOnLoad", ScriptHelper.GetScript("if((window.parent != null) && (typeof(window.parent.DisableNewFolderBtn) == 'function' )) { window.parent.DisableNewFolderBtn(); }"));
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!String.IsNullOrEmpty(lblError.Text))
        {
            lblError.Visible = true;
        }
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (this.Action.ToLower() == "copy")
        {
            CurrentError = ResHelper.GetString("media.copy.canceled");
        }
        else
        {
            CurrentError = ResHelper.GetString("media.move.canceled");
        }
        ctlAsync.RaiseCancel(sender, e);
    }

    #endregion


    /// <summary>
    /// Moves document
    /// </summary>
    private void PerformAction(object parameter)
    {
        if (this.Action.ToLower() == "copy")
        {
            AddLog(ResHelper.GetString("media.copy.startcopy"));
        }
        else
        {
            AddLog(ResHelper.GetString("media.move.startmove"));
        }

        if (this.LibraryInfo != null)
        {
            // Library path (used in recursive copy process)
            string libPath = MediaLibraryInfoProvider.GetMediaLibraryFolderPath(CMSContext.CurrentSiteName, this.LibraryInfo.LibraryFolder);

            // Ensure libPath is in original path type
            libPath = Path.GetFullPath(libPath);

            // Original path on disk from query
            string origPath = Path.GetFullPath(libPath + "\\" + this.FolderPath);

            // New path on disk
            string newPath = null;

            // Original path in DB
            string origDBPath = MediaLibraryHelper.EnsurePath(this.FolderPath);

            // New path in DB
            string newDBPath = null;

            AddLog(this.NewPath);

            // Check if requested folder is in library root folder
            if (!origPath.StartsWith(libPath, StringComparison.CurrentCultureIgnoreCase))
            {
                CurrentError = ResHelper.GetString("media.folder.nolibrary");
                AddLog(CurrentError);
                ctlAsync.RaiseError(null, null);
                return;
            }

            string origFolderName = Path.GetFileName(origPath);

            if ((String.IsNullOrEmpty(this.Files) && !mAllFiles) && string.IsNullOrEmpty(origFolderName))
            {
                this.NewPath = this.NewPath + "\\" + this.LibraryInfo.LibraryFolder;
                this.NewPath = this.NewPath.Trim('\\');
            }
            newPath = this.NewPath;

            // Process current folder copy/move action
            if (String.IsNullOrEmpty(this.Files) && !this.AllFiles)
            {
                newPath = newPath.TrimEnd('\\') + '\\' + origFolderName;
                newPath = newPath.Trim('\\');

                // Check if moving into same folder
                if ((this.Action.ToLower() == "move") && (newPath == this.FolderPath))
                {
                    CurrentError = ResHelper.GetString("media.move.foldermove");
                    AddLog(CurrentError);
                    ctlAsync.RaiseError(null, null);
                    return;
                }

                // Error if moving folder into itself
                string newRootPath = Path.GetDirectoryName(newPath).Trim();
                string newSubRootFolder = Path.GetFileName(newPath).ToLower().Trim();
                string originalSubRootFolder = Path.GetFileName(this.FolderPath).ToLower().Trim();
                if (String.IsNullOrEmpty(this.Files) && (this.Action.ToLower() == "move") && newPath.StartsWith(this.FolderPath + "\\")
                    && (originalSubRootFolder == newSubRootFolder) && (newRootPath == this.FolderPath))
                {
                    CurrentError = ResHelper.GetString("media.move.movetoitself");
                    AddLog(CurrentError);
                    ctlAsync.RaiseError(null, null);
                    return;
                }

                // Get unique path for copy or move
                string path = Path.GetFullPath(libPath + "\\" + newPath);
                path = MediaLibraryHelper.EnsureUniqueDirectory(path);
                newPath = path.Remove(0, (libPath.Length + 1));

                // Get new DB path
                newDBPath = MediaLibraryHelper.EnsurePath(newPath.Replace(libPath + "\\", ""));
            }
            else
            {
                origDBPath = MediaLibraryHelper.EnsurePath(this.FolderPath);
                newDBPath = MediaLibraryHelper.EnsurePath(newPath.Replace(libPath, "")).Trim('/');
            }

            // Error if moving folder into its subfolder
            if ((String.IsNullOrEmpty(this.Files) && !this.AllFiles) && (this.Action.ToLower() == "move") && newPath.StartsWith(this.FolderPath + "\\"))
            {
                CurrentError = ResHelper.GetString("media.move.parenttochild");
                AddLog(CurrentError);
                ctlAsync.RaiseError(null, null);
                return;
            }

            // Error if moving files into same directory
            if ((!String.IsNullOrEmpty(this.Files) || this.AllFiles) && (this.Action.ToLower() == "move") && (newPath.TrimEnd('\\') == this.FolderPath.TrimEnd('\\')))
            {
                CurrentError = ResHelper.GetString("media.move.fileserror");
                AddLog(CurrentError);
                ctlAsync.RaiseError(null, null);
                return;
            }

            this.NewPath = newPath;
            refreshScript = "if ((typeof(window.top.opener) != 'undefined') && (typeof(window.top.opener.RefreshLibrary) != 'undefined')) {window.top.opener.RefreshLibrary(" + ScriptHelper.GetString(this.NewPath.Replace('\\', '|')) + ");} else if ((typeof(window.top.wopener) != 'undefined') && (typeof(window.top.wopener.RefreshLibrary) != 'undefined')) { window.top.wopener.RefreshLibrary(" + ScriptHelper.GetString(this.NewPath.Replace('\\', '|')) + "); } window.top.close();";

            // If mFiles is empty handle directory copy/move
            if (String.IsNullOrEmpty(this.Files) && !mAllFiles)
            {
                try
                {
                    switch (this.Action.ToLower())
                    {
                        case "move":
                            MediaLibraryInfoProvider.MoveMediaLibraryFolder(CMSContext.CurrentSiteName, this.MediaLibraryID, origDBPath, newDBPath, false);
                            break;

                        case "copy":
                            MediaLibraryInfoProvider.CopyMediaLibraryFolder(CMSContext.CurrentSiteName, this.MediaLibraryID, origDBPath, newDBPath, false, this.CurrentUser.UserID);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    CurrentError = ResHelper.GetString("general.erroroccurred") + " " + ex.Message;
                    AddLog(CurrentError);
                    ctlAsync.RaiseError(null, null);
                    return;
                }
            }
            else
            {
                string origDBFilePath = null;
                string newDBFilePath = null;

                if (!mAllFiles)
                {
                    try
                    {
                        string[] files = this.Files.Split('|');
                        foreach (string filename in files)
                        {
                            origDBFilePath = (string.IsNullOrEmpty(origDBPath)) ? filename : origDBPath + "/" + filename;
                            newDBFilePath = (string.IsNullOrEmpty(newDBPath)) ? filename : newDBPath + "/" + filename;
                            AddLog(filename);
                            CopyMove(origDBFilePath, newDBFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        CurrentError = ResHelper.GetString("general.erroroccurred") + " " + ex.Message;
                        AddLog(CurrentError);
                        ctlAsync.RaiseError(null, null);
                        return;
                    }
                }
                else
                {
                    HttpContext context = (parameter as HttpContext);
                    if (context != null)
                    {
                        HttpContext.Current = context;

                        DataSet files = GetFileSystemDataSource();
                        if (!DataHelper.IsEmpty(files))
                        {
                            foreach (DataRow file in files.Tables[0].Rows)
                            {
                                string fileName = ValidationHelper.GetString(file["FileName"], "");

                                AddLog(fileName);

                                origDBFilePath = (string.IsNullOrEmpty(origDBPath)) ? fileName : origDBPath + "/" + fileName;
                                newDBFilePath = (string.IsNullOrEmpty(newDBPath)) ? fileName : newDBPath + "/" + fileName;

                                // Clear current httpcontext for CopyMove action in threat
                                HttpContext.Current = null;

                                try
                                {
                                    CopyMove(origDBFilePath, newDBFilePath);
                                }
                                catch (Exception ex)
                                {
                                    CurrentError = ResHelper.GetString("general.erroroccurred") + " " + ex.Message;
                                    AddLog(CurrentError);
                                    ctlAsync.RaiseError(null, null);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Performes the Move of Copy action.
    /// </summary>
    public void PerformAction()
    {
        if (!this.IsLoad)
        {
            if (CheckPermissions())
            {
                this.pnlInfo.Visible = true;
                this.pnlEmpty.Visible = false;

                RunAsync(PerformAction);
            }
        }
        else
        {
            this.pnlInfo.Visible = false;
            this.pnlEmpty.Visible = true;
            this.lblEmpty.Text = ResHelper.GetString("media.copymove.noselect");
        }
    }


    /// <summary>
    /// Performs action itself.
    /// </summary>
    /// <param name="origDBFilePath">Path of the file specified in DB.</param>
    /// <param name="newDBFilePath">New path of the file being inserted into DB.</param>
    private void CopyMove(string origDBFilePath, string newDBFilePath)
    {
        switch (this.Action.ToLower())
        {
            case "move":
                MediaFileInfoProvider.MoveMediaFile(CMSContext.CurrentSiteName, this.MediaLibraryID, origDBFilePath, newDBFilePath, false);
                break;

            case "copy":
                MediaFileInfoProvider.CopyMediaFile(CMSContext.CurrentSiteName, this.MediaLibraryID, origDBFilePath, newDBFilePath, false, this.CurrentUser.UserID);
                break;
        }
    }


    /// <summary>
    /// Returns set of files in the file system.
    /// </summary>
    private DataSet GetFileSystemDataSource()
    {
        this.fileSystemDataSource.Path = this.LibraryPath + "/" + MediaLibraryHelper.EnsurePath(this.FolderPath) + "/";
        this.fileSystemDataSource.Path = this.fileSystemDataSource.Path.Replace("/", "\\").Replace("|", "\\");

        return (DataSet)this.fileSystemDataSource.GetDataSource();
    }


    #region "Help methods"

    /// <summary>
    /// Ensures the logging context
    /// </summary>
    protected LogContext EnsureLog()
    {
        LogContext currentLog = LogContext.EnsureLog(ctlAsync.ProcessGUID);

        currentLog.Reversed = true;
        currentLog.LineSeparator = "<br />";

        return currentLog;
    }

    /// <summary>
    /// Adds the alert message to the output request window
    /// </summary>
    /// <param name="message">Message to display</param>
    private void AddAlert(string message)
    {
        this.ltlScript.Text += ScriptHelper.GetAlertScript(message);
    }


    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    public override void AddScript(string script)
    {
        this.ltlScript.Text += ScriptHelper.GetScript(script);
    }


    /// <summary>
    /// Check perrmissions for selected library.
    /// </summary>
    private bool CheckPermissions()
    {
        // If mFiles is empty handle directory copy/move
        if (String.IsNullOrEmpty(this.Files) && !mAllFiles)
        {
            if (this.Action.ToLower().Trim() == "copy")
            {
                // Check 'Folder create' permission
                if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(this.LibraryInfo, "foldercreate"))
                {
                    this.lblError.Text = MediaLibraryHelper.GetAccessDeniedMessage("foldercreate");
                    return false;
                }
            }
            else
            {

                // Check 'Folder modify' permission
                if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(this.LibraryInfo, "foldermodify"))
                {
                    this.lblError.Text = MediaLibraryHelper.GetAccessDeniedMessage("foldermodify");
                    return false;
                }
            }
        }
        else
        {
            if (this.Action.ToLower().Trim() == "copy")
            {
                // Check 'File create' permission
                if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(this.LibraryInfo, "filecreate"))
                {
                    this.lblError.Text = MediaLibraryHelper.GetAccessDeniedMessage("filecreate");
                    return false;
                }
            }
            else
            {
                // Check 'File modify' permission
                if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(this.LibraryInfo, "filemodify"))
                {
                    this.lblError.Text = MediaLibraryHelper.GetAccessDeniedMessage("filemodify");
                    return false;
                }
            }
        }
        return true;
    }

    #endregion


    #region "Handling async thread"

    private void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        pnlLog.Visible = false;
        pnlInfo.Visible = true;

        AddScript("var __pendingCallbacks = new Array();DestroyLog();");
        HandlePossibleErrors();
        CurrentLog.Close();
    }


    private void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog.Log;
    }


    private void ctlAsync_OnError(object sender, EventArgs e)
    {
        if (ctlAsync.Status == AsyncWorkerStatusEnum.Running)
        {
            ctlAsync.Stop();
        }

        pnlLog.Visible = false;
        pnlInfo.Visible = true;

        AddScript("DestroyLog();");
        HandlePossibleErrors();
    }


    private void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        if (!HandlePossibleErrors())
        {
            AddScript(refreshScript);
        }
        else
        {
            AddScript("DestroyLog();");
        }
        CurrentLog.Close();
    }


    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        EnsureLog();
        LogContext.AppendLine(newLog);
    }


    /// <summary>
    /// Runs async thread
    /// </summary>
    /// <param name="action">Method to run</param>
    protected void RunAsync(AsyncAction action)
    {
        pnlLog.Visible = true;
        pnlInfo.Visible = false;

        CurrentLog.Close();
        CurrentError = string.Empty;
        CurrentInfo = string.Empty;

        AddScript("InitializeLog();");
        ctlAsync.Parameter = HttpContext.Current;
        ctlAsync.RunAsync(action, WindowsIdentity.GetCurrent());
    }


    /// <summary>
    /// Ensures any error or info is displayed to user
    /// </summary>
    /// <returns>True if error occurred.</returns>
    protected bool HandlePossibleErrors()
    {
        if (!string.IsNullOrEmpty(CurrentError))
        {
            lblError.Text = CurrentError;
            ctlAsync.Log = CurrentLog.Log;
            AddScript("var __pendingCallbacks = new Array();DestroyLog();");
            return true;
        }
        return false;
    }

    #endregion
}
