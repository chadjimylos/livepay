<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UICulture_Edit.aspx.cs" Inherits="CMSSiteManager_Development_UICultures_UICulture_Edit"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" ResourceString="General.ChangesSaved" />
    <cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblUICultureName" runat="server" EnableViewState="false"
                    ResourceString="Development-UICulture_Edit.UICultureName" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtUICultureName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvUICultureName" runat="server" ControlToValidate="txtUICultureName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblUICultureCode" runat="server" EnableViewState="false"
                    ResourceString="Development-UICulture_Edit.UICultureCode" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtUICultureCode" runat="server" CssClass="TextBoxField" MaxLength="10" />
                <asp:RequiredFieldValidator ID="rfvUICultureCode" runat="server" ControlToValidate="txtUICultureCode"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click"
                    EnableViewState="false" ResourceString="general.ok" />
            </td>
        </tr>
    </table>
</asp:Content>
