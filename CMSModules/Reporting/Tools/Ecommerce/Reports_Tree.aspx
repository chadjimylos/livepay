<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reports_Tree.aspx.cs" Inherits="CMSModules_Reporting_Tools_Ecommerce_Reports_Tree"
    Theme="Default" %>
<%@ Register Src="~/CMSAdminControls/UI/Trees/TreeBorder.ascx" TagName="TreeBorder" TagPrefix="cms" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>E-ccommerce - Reports</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body class="TreeBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="Panel1" CssClass="ContentTree">
        <cms:TreeBorder ID="borderElem" runat="server" MinSize="10,*" FramesetName="colsFramesetEcommReports" />
        <div class="TreeArea">
            <div class="TreeAreaTree" style="display: table;">
                <asp:TreeView ID="treeStats" runat="server" ShowLines="true" CssClass="ContentTree" />
            </div>
        </div>
    </asp:Panel>
    <asp:Literal runat="server" ID="ltlScript" />
    </form>

    <script type="text/javascript">
    //<![CDATA[
		var currentNode = document.getElementById('treeSelectedNode');
    
		function ShowDesktopContent(contentUrl, nodeElem)
		{
    		if ( (currentNode != null) && (nodeElem != null) )
            {
                currentNode.className = 'ContentTreeItem';
            }

		    parent.frames['ecommreports'].location.href= contentUrl;

            if ( nodeElem != null )
            {
                currentNode = nodeElem;
                currentNode.className = 'ContentTreeSelectedItem';
            }
		}
    //]]>    
    </script>

</body>
</html>
