CREATE TABLE [Messaging_ContactList] (
		[ContactListUserID]            int NOT NULL,
		[ContactListContactUserID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Messaging_ContactList]
	ADD
	CONSTRAINT [PK_Messaging_ContactList]
	PRIMARY KEY
	([ContactListUserID], [ContactListContactUserID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Messaging_ContactList]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Messaging_ContactList_ContactListContactUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Messaging_ContactList]
			ADD CONSTRAINT [FK_Messaging_ContactList_ContactListContactUserID_CMS_User]
			FOREIGN KEY ([ContactListContactUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Messaging_ContactList]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Messaging_ContactList_ContactListUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Messaging_ContactList]
			ADD CONSTRAINT [FK_Messaging_ContactList_ContactListUserID_CMS_User]
			FOREIGN KEY ([ContactListUserID]) REFERENCES [CMS_User] ([UserID])
END
