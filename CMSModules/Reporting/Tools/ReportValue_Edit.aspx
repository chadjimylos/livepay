<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportValue_Edit.aspx.cs"
    Inherits="CMSModules_Reporting_Tools_ReportValue_Edit" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="ReportValue Edit" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="vertical-align: top; width: 100%;">
            <asp:Panel runat="server" ID="pnlElements">
                <tr>
                    <td class="FieldLabel">
                        <cms:LocalizedLabel ID="lblDisplayName" runat="server" ResourceString="general.displayname"
                            DisplayColon="true" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisplayName" MaxLength="450" runat="server" CssClass="TextBoxField" />
                        <br />
                        <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <cms:LocalizedLabel ID="lblCodeName" runat="server" ResourceString="general.codename"
                            DisplayColon="true" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCodeName" MaxLength="100" runat="server" CssClass="TextBoxField" />
                        <br />
                        <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblQuery" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtQuery" TextMode="MultiLine" runat="server" CssClass="TextAreaLarge" />
                        <br />
                        <asp:RequiredFieldValidator ID="rfvQuery" runat="server" ControlToValidate="txtQuery"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblStoredProcedure" runat="server" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkStoredProcedure" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFormatString" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtFormatString" MaxLength="200" runat="server" CssClass="TextBoxField" />
                    </td>
                </tr>
            </asp:Panel>
        </table>
        <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" /><cms:CMSButton
            ID="btnCancel" runat="server" OnClientClick="window.close(); return false;" CssClass="SubmitButton" />
    </div>
</asp:Content>
