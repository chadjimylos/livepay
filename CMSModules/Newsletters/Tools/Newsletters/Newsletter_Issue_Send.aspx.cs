using System;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_Send : CMSNewsletterPage
{
    protected int issueId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get issue ID from querystring
        issueId = QueryHelper.GetInteger("issueid", 0);
        Issue issue = IssueProvider.GetIssue(issueId);
        if (issue == null)
        {
            btnSend.Enabled = false;
            return;
        }
        
        // Display information wheather the issue was sent
        bool isSent = (issue.IssueMailoutTime == DateTimeHelper.ZERO_TIME ? false : (issue.IssueMailoutTime < DateTime.Now));
        if (isSent)
        {
            lblSent.Text = ResHelper.GetString("Newsletter_Issue_Header.AlreadySent");
        }
        else
        {
            lblSent.Text = ResHelper.GetString("Newsletter_Issue_Header.NotSentYet");
        }

        if (!RequestHelper.IsPostBack())
        {
            radSendNow.Checked = true;
            calendarControl.Enabled = false;
        }

        calendarControl.SupportFolder = "~/CMSAdminControls/Calendar";
        calendarControl.DateTimeTextBox.CssClass = "EditingFormCalendarTextBox";
    }


    protected void btnSend_Click(object sender, EventArgs e)
    {
        // Check permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "authorissues");
        }

        string result = null;

        if (radSchedule.Checked)
        {
            if (calendarControl.SelectedDateTime != DateTimeHelper.ZERO_TIME)
            {
                // Schedule sending
                IssueProvider.SendIssue(issueId, calendarControl.SelectedDateTime);
            }
            else
            {
                result= ResHelper.GetString("newsletter.incorrectdate");
            }
        }
        else if (radSendToSpecified.Checked)
        {
            if (!String.IsNullOrEmpty(txtSendToSpecified.Text))
            {
                if (ValidationHelper.AreEmails(txtSendToSpecified.Text))
                {
                    // Send to specified receivers
                    IssueProvider.SendIssue(issueId, txtSendToSpecified.Text);
                }
                else
                {
                    result = ResHelper.GetString("newsletter.wrongemailformat");
                }
            }
            else
            {
                result = ResHelper.GetString("newsletter.recipientsmissing");
            }
        }
        else
        {
            // Send now to all subscribers
            IssueProvider.SendIssue(issueId);
        }

        if (!String.IsNullOrEmpty(result))
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
        else
        {
            ltlScript.Text = ScriptHelper.GetScript("RefreshPage(); setTimeout(\"top.window.close()\",200);");
        }
    }

    
    /// <summary>
    /// On radio buttons check changed.
    /// </summary>
    protected void radGroupSend_CheckedChanged(object sender, EventArgs e)
    {
        // Send now checked
        if (radSendNow.Checked)
        {
            calendarControl.Enabled = false;
            txtSendToSpecified.Enabled = false;
        }
        // Schedule mailout
        else if (radSchedule.Checked)
        {
            txtSendToSpecified.Enabled = false;
            calendarControl.Enabled = true;
        }
        // Send to specified e-mails
        else if (radSendToSpecified.Checked)
        {
            txtSendToSpecified.Enabled = true;
            calendarControl.Enabled = false;
        }
    }
}
