CREATE TABLE [Newsletter_EmailTemplate] (
		[TemplateID]                 int NOT NULL IDENTITY(1, 1),
		[TemplateDisplayName]        nvarchar(250) NOT NULL,
		[TemplateName]               nvarchar(250) NOT NULL,
		[TemplateBody]               nvarchar(max) NOT NULL,
		[TemplateSiteID]             int NOT NULL,
		[TemplateHeader]             nvarchar(max) NOT NULL,
		[TemplateFooter]             nvarchar(max) NOT NULL,
		[TemplateType]               nvarchar(50) NOT NULL,
		[TemplateStylesheetText]     nvarchar(max) NULL,
		[TemplateGUID]               uniqueidentifier NOT NULL,
		[TemplateLastModified]       datetime NOT NULL,
		[TemplateSubject]            nvarchar(450) NULL
)
ON [PRIMARY]
ALTER TABLE [Newsletter_EmailTemplate]
	ADD
	CONSTRAINT [PK_Newsletter_EmailTemplate]
	PRIMARY KEY
	NONCLUSTERED
	([TemplateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Newsletter_EmailTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_EmailTemplate_TemplateSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_EmailTemplate]
			ADD CONSTRAINT [FK_Newsletter_EmailTemplate_TemplateSiteID_CMS_Site]
			FOREIGN KEY ([TemplateSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_Newsletter_EmailTemplate_TemplateSiteID_TemplateDisplayName]
	ON [Newsletter_EmailTemplate] ([TemplateSiteID], [TemplateDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_Newsletter_EmailTemplate_TemplateSiteID_TemplateName]
	ON [Newsletter_EmailTemplate] ([TemplateSiteID], [TemplateName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
