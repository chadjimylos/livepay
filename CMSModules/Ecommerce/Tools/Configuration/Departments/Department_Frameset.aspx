<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Department_Frameset.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Configuration_Departments_Department_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Untitled Page</title>
</head>
	<frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="Department_Header.aspx?departmentId=<%=Request.QueryString["departmentId"]%> " scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="content" src="Department_General.aspx?departmentId=<%=Request.QueryString["departmentId"]%>&saved=<%=Request.QueryString["saved"]%> " frameborder="0" noresize="noresize" />
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
