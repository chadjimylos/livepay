﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserControlSelector.ascx.cs"
    Inherits="CMSFormControls_System_UserControlSelector" %>
<asp:Panel ID="pnlForm" runat="server">
    <asp:TextBox ID="txtPath" runat="server" CssClass="SelectorTextBox" /><cms:LocalizedButton
        ID="btnSelect" runat="server" EnableViewState="false" CssClass="ContentButton" /><cms:LocalizedButton ID="btnClear"
            runat="server" EnableViewState="false" CssClass="ContentButton"  />
</asp:Panel>
