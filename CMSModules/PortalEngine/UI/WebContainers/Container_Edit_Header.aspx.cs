using System;

using CMS.PortalEngine;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebContainers_Container_Edit_Header : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {       
        string currentContainer = "";
        int containerId = QueryHelper.GetInteger("containerid", 0);
        
        // Initialize page title
        WebPartContainerInfo webPartContainerObj = WebPartContainerInfoProvider.GetWebPartContainerInfo(containerId);
        if (webPartContainerObj != null)
        {
            currentContainer = webPartContainerObj.ContainerDisplayName;
        }

        // Initialize master page elements
        InitializeMasterPage(currentContainer, containerId);
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage(string currentContainer, int containerId)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Container_Edit.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_WebPartContainer/object.png");

        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "newedit_container";

        // Set breadcrumbs
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Container_Edit.ItemListLink");
        breadcrumbs[0, 1] = ResolveUrl("Container_List.aspx");
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = currentContainer;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;

        // Set tabs
        string[,] tabs = new string[2, 4];
        tabs[0, 0] = ResHelper.GetString("General.General");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'newedit_container');";
        tabs[0, 2] = "Container_Edit_General.aspx?containerid=" + containerId;
        tabs[1, 0] = ResHelper.GetString("General.Sites");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'webpartcontainer_sites_tab');";
        tabs[1, 2] = "Container_Edit_Sites.aspx?containerId=" + containerId;
               
        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
