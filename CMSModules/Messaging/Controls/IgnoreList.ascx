<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IgnoreList.ascx.cs" Inherits="CMSModules_Messaging_Controls_IgnoreList" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Users/selectuser.ascx" TagName="SelectUser" TagPrefix="cms" %>
<cms:CMSUpdatePanel ID="pnlIgnoreList" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlInfo" CssClass="Info" Visible="false" runat="server">
            <asp:Label runat="server" ID="lblInfo" EnableViewState="false" CssClass="InfoLabel" />
            <asp:Label runat="server" ID="lblError" EnableViewState="false" CssClass="ErrorLabel" />
        </asp:Panel>
        <div class="ListPanel">
            <div class="GeneralActions">
                <cms:LocalizedLabel ID="lblAvialable" runat="server" CssClass="BoldInfoLabel" ResourceString="ignorelist.available"
                    DisplayColon="true" EnableViewState="false" />
                <cms:SelectUser ID="usUsers" runat="server" SelectionMode="Multiple" />
            </div>
        </div>
    </ContentTemplate>
</cms:CMSUpdatePanel>
