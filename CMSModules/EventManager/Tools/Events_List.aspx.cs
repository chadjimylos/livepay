using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.EventManager;
using CMS.LicenseProvider;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.SiteProvider;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_EventManager_Tools_Events_List : CMSEventManagerPage
{
    #region "Private variables"

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the page title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Events_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_EventManager/object.png");
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "booking_system_list";

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SelectEvent",
            ScriptHelper.GetScript("function SelectItem(nodeId, culture) { " +
            " if ( nodeId != 0 ) { parent.parent.location.href = '" + ResolveUrl("~/CMSDesk/default.aspx") +
            "?section=content&action=edit&nodeid=' + nodeId + '&culture=' + culture; } } "));

        gridElem.HideControlForZeroRows = false;
        gridElem.ZeroRowsText = ResHelper.GetString("Events_List.NoBookingEvent");

        // Check existence of CMS.BookingEvent dataclass
        if (DataClassInfoProvider.GetDataClass("CMS.BookingEvent") != null)
        {
            gridElem.WhereCondition = "(NodeLinkedNodeID IS NULL AND SiteName LIKE '" + CMSContext.CurrentSiteName + "')";
            gridElem.OrderBy = "EventDate DESC";

            gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        }
        else
        {
            // Document type with code name 'CMS.BookingEvent' does not exist
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Events_List.NoBookingEventClass");
        }
    }




    /// <summary>
    /// Handles data bound event.
    /// </summary>
    object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        string result = "";
        DataRowView data = null;

        switch (sourceName.ToLower())
        {
            case "documentname":
                return HTMLHelper.HTMLEncode(parameter.ToString());

            case "eventtooltip":
                data = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(data);

            case "eventdate":
            case "eventopenfrom":
            case "eventopento":
            case "eventdatetooltip":
            case "eventopenfromtooltip":
            case "eventopentotooltip":
                if (!String.IsNullOrEmpty(parameter.ToString()))
                {
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }
                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (!sourceName.EndsWith("tooltip"))
                    {
                        result = TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME),
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME),
                                currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        result = TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }
                return result;
        }
        return parameter;
    }
}
