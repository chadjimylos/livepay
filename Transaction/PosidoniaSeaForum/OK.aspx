<%@ Page Language="VB" AutoEventWireup="false" Inherits="OK" CodeFile="OK.aspx.vb" %>
<html>
  <head>
    <title>Aegean Power</title>
  </head>
<body>
    <script defer="defer">
        window.opener.document.location.href = '/Registration-Form/Successful-Registration---Payment.aspx';
        window.close();
    </script>
</body>
</html>