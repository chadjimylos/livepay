﻿<%@ Page Title="Module edit - User interface - General" Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    AutoEventWireup="true" CodeFile="Module_UI_General.aspx.cs" Inherits="CMSSiteManager_Development_Modules_Module_UI_General" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/UIElementEdit.ascx" TagName="UIElementEdit"
    TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcBeforeBody" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="plcControls" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="plcContent" runat="Server">
    <cms:UIElementEdit ID="editElem" runat="server" />
</asp:Content>
