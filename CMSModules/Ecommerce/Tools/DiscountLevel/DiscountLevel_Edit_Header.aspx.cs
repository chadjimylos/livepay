using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountLevel_DiscountLevel_Edit_Header : CMSEcommercePage
{
    protected int discountLevelId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        string currentDiscount = "";
        
        // Get discountlLevel id from querystring		
        discountLevelId = ValidationHelper.GetInteger(Request.QueryString["discountLevelId"], 0);
        if (discountLevelId > 0)
        {
            DiscountLevelInfo di = DiscountLevelInfoProvider.GetDiscountLevelInfo(discountLevelId);

            if (di != null)
            {
                currentDiscount = ResHelper.LocalizeString(di.DiscountLevelDisplayName);
            }
        }

        int hideBreadcrumbs = ValidationHelper.GetInteger(Request.QueryString["hidebreadcrumbs"], 0);

        if (hideBreadcrumbs == 0)
        {
            // Initializes page title breadcrumbs control		
            string[,] pageTitleTabs = new string[2, 3];
            pageTitleTabs[0, 0] = ResHelper.GetString("DiscountLevel_Edit.ItemListLink");
            pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/DiscountLevel/DiscountLevel_List.aspx";
            pageTitleTabs[0, 2] = "ecommerceContent";
            pageTitleTabs[1, 0] = currentDiscount;
            pageTitleTabs[1, 1] = "";
            pageTitleTabs[1, 2] = "";
            this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        }

        // Page title
        this.CurrentMaster.Title.HelpTopicName = "new_levelgeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Tabs
        this.CurrentMaster.Tabs.Tabs = new string[3, 4];
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("General.General");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'new_levelgeneral_tab');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "DiscountLevel_Edit.aspx?discountLevelId=" + discountLevelId.ToString();
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("DiscountLevel_Edit.Departments");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'departments_tab2');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "DiscountLevel_Edit_Departments.aspx?discountLevelId=" + discountLevelId.ToString();
        this.CurrentMaster.Tabs.UrlTarget = "DiscountLevelContent";
    }
}
