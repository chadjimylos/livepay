using System;
using System.Web;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.PortalEngine;
using CMS.TreeEngine;

public partial class CMSModules_Content_CMSDesk_TemplateSelection : CMSContentPage, IPostBackEventHandler
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblChoose.Text = ResHelper.GetString("TemplateSelection.chooseTemplate");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SaveDocument", ScriptHelper.GetScript("function SaveDocument(nodeId, createAnother) { " + ControlsHelper.GetPostBackEventReference(this, "#", false).Replace("'#'", "createAnother+''") + "; return false; }"));

        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
                ltlScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSFormControls/LiveSelectors/SpellCheck.aspx") + "\"; var offsetValue = 7; //]]> </script>";
        }
        else
        {
            ltlScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSModules/Content/CMSDesk/Edit/SpellCheck.aspx") + "\"; var offsetValue = 7; //]]> </script>";
        }

        int nodeID = QueryHelper.GetInteger("nodeid", 0);

        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        // For new node is not document culture important, preffered culture is used
        TreeNode node = tree.SelectSingleNode(nodeID);
        if (node != null)
        {
            templateSelector.DocumentID = node.DocumentID;
        }
    }


    #region IPostBackEventHandler Members

    public void RaisePostBackEvent(string eventArgument)
    {
        UrlHelper.Redirect("~/CMSModules/Content/CMSDesk/Edit/edit.aspx" + HttpContext.Current.Request.Url.Query + "&templateid=" + templateSelector.SelectedItem);
    }

    #endregion
}
