﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCTransReceipt.ascx.vb" Inherits="CMSTemplates_LivePay_UCTransReceipt" %>
<%@ Register Src="/CMSWebParts/Text/editabletext.ascx" TagName="editabletext"
TagPrefix="uc1" %>

<div class="PT20">
    <div class="TransRecMainContent" style="padding-bottom:10px">
	<div class="TransRecTblTitle">
        <div id="Div_TransRecTitle" class="transRec_check" runat="server">
            <div><uc1:editabletext ID="txtDescription" runat="server" RegionType="HtmlEditor" HtmlAreaToolbarLocation="Out:FCKToolbar" DialogHeight="80" DialogWidth="560" /></div>
	        <div class="TransRecTblGreenTitle" id="MessageTitle" runat="server" ></div>
        </div>
    </div>
	<div class="Clear"></div>
	<div style="padding-top:20px;padding-left:20px" id="MainDivForm" runat="server">
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Date & Time$}") %></div>
	   <div class="TransRecRightRow" id="tdDateTime" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Payment to$}") %></div>
	   <div class="TransRecRightRow" id="tdMerchant" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow" runat="server" id="txtTransCode"><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής|en-us=Transaction Code$}") %></div>
	   <div class="TransRecRightRow" id="tdTransCode" runat="server" ></div>
	   <div class="Clear"></div>
       <div style="display:none">
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη|en-us=Customer Code$}") %></div>
	   <div class="TransRecRightRow" id="tdCustCode" runat="server" ></div>
	   <div class="Clear"></div>
       </div>
	   <div id="CustomFieldsPanel" runat="server">
       </div>
      
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}") %></div>
	   <div class="TransRecRightRow" id="tdPrice" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κάρτα|en-us=Card$}") %></div>
	   <div class="TransRecRightRow" id="tdCard" runat="server" ></div>
	   <div class="Clear"></div>
       <asp:PlaceHolder ID="plc_Installments" runat="server" Visible="false" EnableViewState="false">
	       <div class="TransRecLeftRow"><%= ResHelper.LocalizeString("{$=Αριθμός δόσεων |en-us=Number of installments$}")%></div>
	       <div class="TransRecRightRow" id="tdInstallments" runat="server" ></div>
	       <div class="Clear"></div>
       </asp:PlaceHolder>
	</div>
    <div style="color:#43474a;min-height:300px; padding-top:20px;display:none;padding-left:10px;padding-bottom:40px;font-size:150%" id="MainDivErrorContainer" runat="server"></div> 
    <div class="PT10" style="padding-top:50px;width:100%;font-size:140%;text-align:center">
        <div>
            <div class="btn" style="width:100%;text-align:center;padding-bottom:10px">
                <asp:LinkButton ID="btnNewPayment" runat="server" CssClass="" EnableViewState="false" >
                    <asp:label runat="server" style="font-family:arial;display:none " ID="labNewPayment" />
                   <img style="zoom:1.2" src="/App_Themes/LivePay/Mobile/btnNewPay.png" />
                </asp:LinkButton>
            </div>
        </div>
    </div>
    </div>
  </div>