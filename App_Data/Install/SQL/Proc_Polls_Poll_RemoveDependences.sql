-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Polls_Poll_RemoveDependences]
	@PollID int
AS
BEGIN
	DELETE FROM Polls_PollRoles WHERE PollID=@PollID;
	DELETE FROM Polls_PollSite WHERE PollID=@PollID;
	DELETE FROM Polls_PollAnswer WHERE AnswerPollID=@PollID;
END
