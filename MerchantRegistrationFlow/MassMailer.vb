﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Services
Imports CMS.SiteProvider
Imports CMS.EventLog


Namespace Kentico_MerchantActivationWorkflow
    Public Class EuronetActivation
        Inherits System.Web.UI.Page



        Public Function SetUserToRole(requesteduserGuid As Guid, rolename As String) As String

            Dim ev As New EventLogProvider()

            Dim res As String = String.Empty
            Dim theUser As UserInfo = Nothing
            '    If usn = "tHaNaSiS" AndAlso pass = "tH@n@S1s123!@#" Then
            Try
                theUser = UserInfoProvider.GetUserInfoByGUID(requesteduserGuid)
            Catch ex As Exception
                ev.LogEvent("E", "HellasPayCmsService - SetUserToRole - GetUserInfoByGUID", ex)
                res = "ERROR: " & ex.ToString()
            End Try

            If theUser IsNot Nothing Then
                Try
                    CMS.SiteProvider.UserInfoProvider.AddUserToRole(theUser.UserName, rolename, CMS.CMSHelper.CMSContext.CurrentSiteName)
                    Dim errMsg As String = String.Format("HellasPayCmsService - User {0} ({2}) was added to role {1} ", theUser.UserName, rolename, requesteduserGuid.ToString())

                    ev.LogEvent("I", errMsg, New Exception())
                Catch ex As Exception
                    ev.LogEvent("E", "HellasPayCmsService - SetUserToRole - AddUserToRole", ex)
                    res = ex.ToString()
                End Try
            Else

                Dim errMsg As String = "No user with such guid: " & requesteduserGuid.ToString()
                ev.LogEvent("E", "HellasPayCmsService - SetUserToRole - SetRole", New Exception(errMsg))
                res = "ERROR: " & errMsg
            End If
            '      Else
            '     res = "ERROR: Login Failed"
            '       End If
            Return res
        End Function



        Public Function RemoveUserFromRole(requesteduserGuid As Guid, rolename As String) As String

            Dim ev As New EventLogProvider()

            Dim res As String = String.Empty
            Dim theUser As UserInfo = Nothing
            '     If usn = "tHaNaSiS" AndAlso pass = "tH@n@S1s123!@#" Then
            Try
                theUser = UserInfoProvider.GetUserInfoByGUID(requesteduserGuid)
            Catch ex As Exception
                ev.LogEvent("E", "HellasPayCmsService - RemoveUserFromRole - GetUserInfoByGUID", ex)
                res = "ERROR: " & ex.ToString()
            End Try

            If theUser IsNot Nothing Then
                Try
                    CMS.SiteProvider.UserInfoProvider.RemoveUserFromRole(theUser.UserName, rolename, CMS.CMSHelper.CMSContext.CurrentSiteName)
                    Dim errMsg As String = String.Format("HellasPayCmsService - User {0} ({2}) was removed from role {1} ", theUser.UserName, rolename, requesteduserGuid.ToString())
                    ev.LogEvent("I", errMsg, New Exception())
                Catch ex As Exception
                    ev.LogEvent("E", "HellasPayCmsService - RemoveUserFromRole - RemoveUserFromRole", ex)
                    res = ex.ToString()
                End Try
            Else

                Dim errMsg As String = "No user with such guid: " & requesteduserGuid.ToString()
                ev.LogEvent("E", "HellasPayCmsService - RemoveUserFromRole ", New Exception(errMsg))
                res = "ERROR: " & errMsg
            End If
            '  Else
            '   res = "ERROR: Login Failed"
            '   End If
            Return res
        End Function




        Protected Sub Send_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            Dim Guids = txtGuids.Text.Split(",").Select(Function(x) New Guid(x)).ToList()

            For Each g In Guids
                SendToMerchant(g)
            Next

        End Sub



        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Me.IsPostBack Then

            End If
        End Sub


        Protected Sub SendToMerchant(ByVal MerchantId As Guid)
            Dim ApiClient = New HellasPayInternalService.InternalAPIClient()

            Dim Merchant = ApiClient.GetMerchantById(MerchantId)

            Dim Success As Boolean = False

            If Merchant IsNot Nothing Then

                Dim StatusCrm = ApiClient.GetStatusCRM(Me.MerchantId)

                Dim Body As String = "<div style=""font-family:Verdana""><IMG src='https://www.hellaspay.gr/Images/logoHellasPay/'/><IMG src='https://www.hellaspay.gr/el/Images/steps/newStep3'><br /><br />Αγαπητέ/ή, " & Merchant.Person.LastName & " " & Merchant.Person.FirstName & "<br /><br /> Με το e-mail αυτό σας ενημερώνουμε πως ο λογαριασμός σας έχει ενεργοποιηθεί, δίνοντάς σας πρόσβαση στο σύνολο των υπηρεσιών της Hellas Pay.<br /><br /> Αν θέλετε να διασυνδέσετε το website σας με την υπηρεσία Web Checkout της Hellas Pay, συμβουλευθείτε τις προδιαγραφές διασύνδεσης που θα κατεβάσετε από εδώ: https://www.hellaspay.gr/el/Documents/HellasPay-API-documentation <br /><br /> Θα χαρούμε να σας λύσουμε οποιαδήποτε απορία αναφορικά με τις υπηρεσίες μας. <br /><br /> Μη διστάσετε να επικοινωνήσετε μαζί μας στο 2117604000 ή μέσω e-mail στη διεύθυνση support@hellaspay.gr <br /><br /> Για την Hellas Pay <br /><br />Customer Relations Dpt.</div>"

                Dim cms As New HellasPayCmsService
                Dim emailOptions As New EmailOptions

                emailOptions.From = "info@hellaspay.gr"
                emailOptions.SendTo = Merchant.Person.Email
                emailOptions.Bcc = "registrations@hellasPay.gr"
                emailOptions.Subject = "Hellas Pay: Oλοκλήρωση Ενεργοποίησης"
                emailOptions.Body = Body

                Dim MailSend As Boolean = cms.sendEmail(emailOptions)

                StatusCrm.Status = "A"

                ApiClient.UpdateStatusCrm(StatusCrm)
                Merchant.Person.IsActive = 1


                Dim s0 = RemoveUserFromRole(Merchant.MerchantId, "HellasPayMerchantWebPos")
                Dim s1 = RemoveUserFromRole(Merchant.MerchantId, "HellasPayMerchantCard")
                Dim s3 = SetUserToRole(Merchant.MerchantId, "HellasPayMerchantWebPos")
                Dim s4 = SetUserToRole(Merchant.MerchantId, "HellasPayMerchantCard")

                Success = ApiClient.UpdateMerchant(Merchant)

                If (Success) Then
                    Me.Response.Write(Merchant.Person.PersonId.ToString() + " SEND OK")
                Else
                    Me.Response.Write(Merchant.Person.PersonId.ToString() + " SEND FAILED")
                End If
            End If


        End Sub
    End Class
End Namespace