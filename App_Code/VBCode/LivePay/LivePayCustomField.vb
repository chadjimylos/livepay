﻿Imports Microsoft.VisualBasic

Public Class LivePayFormEngineUserControl
    Inherits CMS.FormControls.FormEngineUserControl

    Private _RowCustomField As Integer
    Public Property RowCustomField() As Integer
        Get
            Return _RowCustomField
        End Get
        Set(ByVal value As Integer)
            _RowCustomField = value
        End Set
    End Property

    Private _RegularExp As String
    Public Property RegularExp() As String
        Get
            Return _RegularExp
        End Get
        Set(ByVal value As String)
            _RegularExp = value
        End Set
    End Property

    Private _Mandatory As Boolean
    Public Property Mandatory() As Boolean
        Get
            Return _Mandatory
        End Get
        Set(ByVal value As Boolean)
            _Mandatory = value
        End Set
    End Property

    Private _MaxLength As Integer
    Public Overridable Property MaxLength() As Integer
        Get
            Return _MaxLength
        End Get
        Set(ByVal value As Integer)
            _MaxLength = value
        End Set
    End Property

    Private _OrganismCode As Integer
    Public Overridable Property OrganismCode() As Integer
        Get
            Return _OrganismCode
        End Get
        Set(ByVal value As Integer)
            _OrganismCode = value
        End Set
    End Property

    Private _TypePayment As Integer
    Public Overridable Property TypePayment() As Integer
        Get
            Return _TypePayment
        End Get
        Set(ByVal value As Integer)
            _TypePayment = value
        End Set
    End Property

    Private _NDate As String
    Public Overridable Property NDate() As String
        Get
            Return _NDate
        End Get
        Set(ByVal value As String)
            _NDate = value
        End Set
    End Property

    Protected Shared Function Left(ByVal param As String, ByVal length As Integer) As String
        'we start at 0 since we want to get the characters starting from the
        'left and with the specified lenght and assign it to a variable
        Dim result As String = param.Substring(0, length)
        'return the result of the operation
        Return result
    End Function

    Protected Shared Function Right(ByVal param As String, ByVal length As Integer) As String
        'start at the index based on the lenght of the sting minus
        'the specified lenght and assign it a variable
        Dim result As String = param.Substring(param.Length - length, length)
        'return the result of the operation
        Return result
    End Function

    Protected Shared Function Mid(ByVal param As String, ByVal startIndex As Integer, ByVal length As Integer) As String
        'start at the specified index in the string ang get N number of
        'characters depending on the lenght and assign it to a variable
        Dim result As String = param.Substring(startIndex, length)
        'return the result of the operation
        Return result
    End Function

    Protected Shared Function modulus11(ByVal value As String, ByVal length As Integer) As Integer
        Dim g As Integer = 0
        Dim cd As Integer = 0
        Dim i As Integer = length

        For s As Integer = 2 To length
            g += (i * Convert.ToInt32(value.Substring(s - 2, 1)))
            i -= 1
        Next

        Dim y As Integer = 0
        y = g Mod 11

        If y = 0 Then
            cd = 1
        ElseIf y = 1 Then
            cd = 0
        Else
            cd = 11 - y
        End If

        Return cd
    End Function

    Protected Function ValidatorTaxID(ByVal TaxID As String) As Boolean
        Dim telestis As Integer = 2
        Dim sum As Integer = 0
        Dim afm As String = TaxID
        For i As Integer = 7 To 0 Step -1
            sum += Integer.Parse(afm(i).ToString()) * telestis
            telestis = telestis * 2
        Next
        Dim checkDigit As Integer = Integer.Parse(afm(8).ToString())

        If sum Mod 11 Mod 10 <> checkDigit Then
            Return False
        Else
            Return True
        End If
    End Function


End Class


Public Class LivePayCustomField
    Public Shared ArrCustomField As New ArrayList()

    Shared Sub New()
    End Sub

    Public Shared Function AddField(ByVal UserID As Integer, ByVal FieldID As Integer, ByVal Value As String) As CustomFields
        Dim field As New CustomFields
        field.UserID = UserID
        field.FieldID = FieldID
        field.FieldValue = Value
        Return field
    End Function

    Public Shared Sub Add(ByVal field As CustomFields)
        If field.FieldID = "-1" Then
            Return
        End If
        Dim StateIndex As Integer = Index(field)
        If StateIndex < 0 Then
            ArrCustomField.Add(field)
        End If
    End Sub

    Public Shared Sub RemoveFieldsCurrentUser(ByVal UserID As Integer)
        Dim UserIndex As Integer = IndexUser(UserID)
        If UserIndex >= 0 Then
            ArrCustomField.RemoveAt(UserIndex)
        End If

        'ArrCustomField.Clear()
    End Sub

    Private Shared Function Index(ByVal field As CustomFields) As Integer

        For i As Integer = 0 To ArrCustomField.Count - 1
            If DirectCast(ArrCustomField(i), CustomFields).FieldID = field.FieldID AndAlso DirectCast(ArrCustomField(i), CustomFields).UserID = field.UserID Then
                Return i
            End If
        Next
        Return -1
    End Function

    Private Shared Function IndexUser(ByVal UserID As Integer) As Integer

        For i As Integer = 0 To ArrCustomField.Count - 1
            If DirectCast(ArrCustomField(i), CustomFields).UserID = UserID Then
                Return i
            End If
        Next
        Return -1
    End Function

    Public Shared Function GetPathFormControl(ByVal FormControlID As Integer) As String
        Dim customTableClassName As String = "LivePay.FormControls"
        Dim Path As String = String.Empty

        ' Get data class using custom table name
        Dim customTableClassInfo As CMS.SettingsProvider.DataClassInfo = CMS.SettingsProvider.DataClassInfoProvider.GetDataClass(customTableClassName)
        If customTableClassInfo Is Nothing Then
            Throw New Exception("Given custom table does not exist.")
        End If

        ' Initialize custom table item provider with current user info and general connection
        Dim ctiProvider As New CMS.SiteProvider.CustomTableItemProvider(CMSContext.CurrentUser, CMS.DataEngine.ConnectionHelper.GetConnection())

        ' Get custom table items
        Dim dsItems As System.Data.DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, "ItemID = " & FormControlID, Nothing, 1, "ItemFileName")

        ' Check if DataSet is not empty
        If Not DataHelper.DataSourceIsEmpty(dsItems) Then
            ' Handle the retrieved data
            Path = dsItems.Tables(0).Rows(0)("ItemFileName").ToString()
        End If

        Return Path
    End Function

End Class

Public Class CustomFields
    Private _UserID As Integer
    Private _FieldID As Integer
    Private _FieldValue As String

    Public Property UserID() As Integer
        Get
            Return _UserID
        End Get
        Set(ByVal value As Integer)
            _UserID = value
        End Set
    End Property

    Public Property FieldID() As Integer
        Get
            Return _FieldID
        End Get
        Set(ByVal value As Integer)
            _FieldID = value
        End Set
    End Property

    Public Property FieldValue() As String
        Get
            Return _FieldValue
        End Get
        Set(ByVal value As String)
            _FieldValue = value
        End Set
    End Property

End Class
